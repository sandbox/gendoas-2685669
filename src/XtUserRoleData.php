<?php

/**
 * @file
 * Contains \Drupal\slogxt\XtUserRoleData.
 */

namespace Drupal\slogxt;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Static functions and callbacks for extended user data.
 * 
 * Data stored in table {users_data}.
 */
class XtUserRoleData {

  /**
   * Add fields to config_export for entity user_role.
   * 
   * @see data for @ConfigEntityType annotation in Role
   * 
   * @param array $entity_types
   *  Array of Drupal\Core\Entity\EntityTypeInterface
   */
  public static function entityTypeBuild(array &$entity_types) {
    if (isset($entity_types['user_role'])) {
      $user_role_type = $entity_types['user_role'];
      $config_export = $user_role_type->get('config_export');
      $config_export[] = 'slogxt_is_xtrole';
      $config_export[] = 'slogxt_xtrole_description';
      $user_role_type->set('config_export', $config_export);
    }
  }

  /**
   * Add form fields for slogxt module.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAlter(&$form, FormStateInterface $form_state) {
    $role = $form_state->getFormObject()->getEntity();
    $weight = -100;
    $form['label']['#weight'] = $weight++;
    $role_id = $role->id();
    $is_fix_xt_role = ($role_id === 'anonymous' || $role_id === AccountInterface::AUTHENTICATED_ROLE);

    $form['slogxtdata'] = [
      '#type' => 'details',
      '#title' => t('Slogxt settings'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    //todo::current::slogxt_is_xtrole
    $last_xt_status = $role->get('xt_status');
    $last_slogxt_is_xtrole = $role->get('slogxt_is_xtrole');
    $set_slogxt_is_xtrole = $last_xt_status ? TRUE : $last_slogxt_is_xtrole;
    $set_xtrole_description = $last_xt_status //
        ? $role->get('description') : $role->get('slogxt_xtrole_description');

    if (isset($last_xt_status)) {
      $form['slogxtdata']['xt_status'] = [
        '#type' => 'checkbox',
        '#title' => t('Active as XT-Role'),
        '#description' => t('Select for activating this role as xt-role.'),
        '#default_value' => ($is_fix_xt_role ? TRUE : $last_xt_status),
        '#disabled' => TRUE,
      ];
      $form['slogxtdata']['description'] = [
        '#type' => 'textarea',
        '#title' => t('DescriptionXXX'),
        '#description' => t("Give the user an idea of what this role is intended for."),
        '#default_value' => $role->get('description'),
        '#disabled' => TRUE,
      ];
    }

    $form['slogxtdata']['slogxt_is_xtrole'] = [
      '#type' => 'checkbox',
      '#title' => t('Active as SXT-Role'),
      '#description' => t('Select for activating this role as sxt-role.'),
      '#default_value' => ($is_fix_xt_role ? TRUE : $set_slogxt_is_xtrole),
      '#disabled' => $is_fix_xt_role,
    ];
    $form['slogxtdata']['slogxt_xtrole_description'] = [
      '#type' => 'textarea',
      '#title' => t('SXT-Role Description'),
      '#description' => t("Give the user an idea of what this role is intended for."),
      '#default_value' => $set_xtrole_description,
    ];
  }

}
