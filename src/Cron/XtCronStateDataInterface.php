<?php

/**
 * @file
 * Contains \Drupal\slogtx\Cron\XtCronStateDataInterface.
 */

namespace Drupal\slogxt\Cron;

/**
 * //todo::docu
 */
interface XtCronStateDataInterface {

  /**
   */
  public function getCronStateData($module, $key);

  /**
   */
  public function setCronStateData($module, $key, $data);

  /**
   */
  public function deleteCronStateData($module, $key);

}
