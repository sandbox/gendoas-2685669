<?php

/**
 * @file
 * Contains \Drupal\slogtx\Cron\XtCronStateData.
 */

namespace Drupal\slogxt\Cron;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Provides a repository for installed entity definitions.
 */
class XtCronStateData implements XtCronStateDataInterface {

  /**
   * The key-value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * Constructs a new XtCronStateData.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key-value factory.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory) {
    $this->keyValueFactory = $key_value_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getCronStateData($module, $key) {
    return $this->keyValueFactory->get('slogxt.cron_state_data')->get("$module.cron.$key");
  }

  /**
   * {@inheritdoc}
   */
  public function setCronStateData($module, $key, $data) {
    $this->keyValueFactory->get('slogxt.cron_state_data')->set("$module.cron.$key", $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCronStateData($module, $key) {
    $this->keyValueFactory->get('slogxt.cron_state_data')->delete("$module.cron.$key");
    return $this;
  }

}
