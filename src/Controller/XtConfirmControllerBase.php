<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\XtConfirmControllerBase.
 */

namespace Drupal\slogxt\Controller;

use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
abstract class XtConfirmControllerBase extends XtEditControllerBase {

  abstract public static function formSubmit(array &$form, FormStateInterface $form_state);

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $request->attributes->set('confirm_request_data', $this->getConfirmRequestData());
    return 'Drupal\slogxt\Form\XtGenericConfirmForm';
  }

  protected function getConfirmRequestData() {
    return [];
  }

  protected function getSubmitLabel() {
    return t('Execute');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $op = \Drupal::request()->get('op', false);
    $this->opSave = ($op && $op === (string) t('Save'));
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    if ($form_state->isSubmitted() && !$form_state->hasAnyErrors()) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $controllerClass = get_class($this);
    $form['#validate'][] = "$controllerClass::formValidate";
  }

  /**
   * Validate callback.
   * 
   * Use own validator for adding own submitter.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
    $submit_handlers[] = [get_class(self::calledObject()), 'formSubmit'];
    $form_state->setSubmitHandlers($submit_handlers);
  }

}
