<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\AjaxFormControllerBase.
 */

namespace Drupal\slogxt\Controller;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\XtDateTimeTrait;
use Drupal\slogxt\XtDateFormaterTrait;
use Drupal\slogxt\Ajax\SxtInvokeCommand;
use Drupal\slogxt\Event\SlogxtGenericEvent;
use Drupal\slogxt\Event\SlogxtEvents;
use Drupal\node\NodeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\StatusMessages;

/**
 * Defines a controller ....
 */
abstract class AjaxFormControllerBase extends ControllerBase {

  use XtDateTimeTrait;
  use XtDateFormaterTrait;

  static protected $ajax_called_object = false;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The event dispatcher to notify of routes.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;
  protected $form_title = '';
  protected $formResult = '';
  protected $formSubmitResult = [];
  protected $hasUrgentMessage = FALSE;
  protected $final_more_messages = [];
  protected $newInsertedNode = FALSE;
  protected $hasErrors = FALSE;
  protected $submitted = FALSE;
  protected $htmlPreview = FALSE;

  /**
   * Construct a slogitem content controller object.
   *
   */
  public function __construct(ModuleHandlerInterface $module_handler, RendererInterface $renderer, ClassResolverInterface $class_resolver, EventDispatcherInterface $dispatcher) {
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->classResolver = $class_resolver;
    $this->dispatcher = $dispatcher;

//    $xx_formater = $this->getXtDateFormater();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('module_handler'), //
        $container->get('renderer'), //
        $container->get('class_resolver'), //
        $container->get('event_dispatcher')
    );
  }

  abstract protected function getFormObjectArg();

  abstract protected function hasLabels();

  /**
   * Get things done before calling getFormObjectArg().
   */
  protected function preFormObjectArg() {
    // Optional, do nothing by default.
  }

  /**
   * Ajax called object is read only.
   * 
   * @return \Drupal\slogxt\Controller\AjaxFormControllerBase
   */
  public static function getCalledObject() {
    return self::$ajax_called_object;
  }

  protected function getPathFromRoute($route_name, $route_parameters) {
    $url = Url::fromRoute($route_name, $route_parameters);
    return $url->getInternalPath();
  }

  public function setNewInsertedNode(NodeInterface $node) {
    $this->newInsertedNode = $node;
  }

  public function getNewInsertedNode() {
    return $this->newInsertedNode;
  }

  /**
   * Allows forms to provide a submit result.
   * 
   * @param array $result
   *  If set, success item ist required
   */
  public function setSubmitResult($result) {
    $this->formSubmitResult = $result;
  }

  /**
   * Allows forms to leave a final error message for getOnWizardFinished function.
   * 
   * @param string $message
   */
  public function addFinalMoreMessage($message, $success = TRUE) {
    $this->final_more_messages[] = [
      'type' => ($success ? 'status' : 'error'),
      'message' => $message,
    ];
  }

  /**
   * Set final messages which are set before.
   */
  public function setFinalMoreMessages() {
    if (!empty($this->final_more_messages)) {
      foreach ($this->final_more_messages as $msg) {
        drupal_set_message($msg['message'], $msg['type']);
      }
    }
  }

  /**
   * Set/Get globalized instance of this class.
   * 
   * - Required for later use
   * - Depositing in $form_state is not possible,
   *   since the instance of this class is not serializable.
   * 
   * @param type $calledObject
   * @return type
   */
  protected static function calledObject($calledObject = NULL) {
    if ($calledObject) {
      self::$ajax_called_object = $calledObject;
    }
    return self::$ajax_called_object;
  }

  /**
   * Do validate by $form's validators.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function doFormValidate(array &$form, FormStateInterface $form_state) {
    $form_handlers = $form['#validate'];
    foreach ($form_handlers as $callback) {
      call_user_func_array($form_state->prepareCallback($callback), [&$form, &$form_state]);
    }
  }

  protected function getFormTitle() {
    return $this->form_title ?: 'none';
  }

  protected function getTabLabel() {
    return $this->getFormTitle();
  }

  protected function getSubmitLabel() {
    return $this->getFormTitle();
  }

  protected function getLabels() {
    $labels = false;
    if ($this->hasLabels()) {
      $labels = [
        'dialogTitle' => $this->getFormTitle(),
        'tabLabel' => $this->getTabLabel(),
        'submitLabel' => $this->getSubmitLabel(),
      ];
    }
    return $labels;
  }

  protected function setSubmitDanger(&$form) {
    if (!empty($form['actions']['submit'])) {
      $form['actions']['submit']['#button_type'] = 'danger';
    }
  }

  /**
   * Initialize $form_state before building form.
   * 
   * @param FormStateInterface $form_state
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    //todo::disableRedirect::only::??? - see getContentResult()
    $form_state->set('response', false);  // prevent redirection after submission
    $form_state->set('slogxt', ['ajax' => true]); // more slogxt infos, server side
    $form_state->set('slogxtData', ['runCommand' => 'default']); // more slogxt infos, client side
  }

  /**
   * Hook for hacks after building form.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    // Optional, do nothing by default.
  }

  /**
   * Implements hook_form_alter() for ajax called forms.
   * 
   * Do nothing by default, allow to override.
   * 
   * @see hook in slogxt.module
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @param string $form_id
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    // Do nothing by default.
    // override this method for altering form
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    $this->preRenderFormBase($form, $form_state);
  }

  protected function preRenderFormBase(&$form, FormStateInterface $form_state) {
    $this->slogxt_data_final = $form_state->get('slogxt');
    if (!empty($form['actions'])) {
      $form['actions']['#attributes']['class'][] = 'visually-hidden';
    }
  }

  protected function postRenderForm(&$form, FormStateInterface $form_state) {
    // Optional, do nothing by default.
  }

  protected function preSendResponse($response) {
    return $response;
  }

  /**
   * Register client side finished function.
   * 
   * @return array|FALSE
   *  Return finished function name and arguments for the function.
   *  Defaults to FALSE (do nothing)
   */
  protected function getOnWizardFinished() {
    return FALSE;
  }

  public function getContentResult() {
    // clear messages list
    drupal_get_messages();

    // make this object global for static functions
    // NOTE: we cannot put this into $form_state, because $form_state
    //       has to be serialized and this object is not serializable
    self::calledObject($this);

    //todo::debug::AjaxFormControllerBase.getContentResult()
    $request = \Drupal::request();
    $baseEntityId = $request->get('base_entity_id');
    $nextEntityId = $request->get('next_entity_id');


    // build form by FormObjectArg
    $this->preFormObjectArg();
    $form_arg = $this->getFormObjectArg();
    $form_state = new FormState();
    // prevent redirection for ajax call
    $form_state->disableRedirect();

    $this->preBuildForm($form_state);
    $form = \Drupal::formBuilder()->buildForm($form_arg, $form_state);
    $this->postBuildForm($form, $form_state);

    // ensure $wrapperId transport
    $input = $form_state->getUserInput();
    $wrapperId = !empty($input['wrapper_id']) ? $input['wrapper_id'] : \Drupal::request()->get('wrapperId');
    if (isset($wrapperId)) {
      $form['wrapper_id'] = $this->buildWrapperId($form['#form_id'], $wrapperId, $form_state);
    }

    //switch for $this->hasUrgentMessage
    if ($this->getUrgentMessageData($form_state)) {
      $result = $this->buildUrgentMsgContentResult($form, $form_state);
    }
    else {
      $result = $this->buildContentResult($form, $form_state);
    }

    // 
    // We may have to go back to the beginning
    //
    if (!empty($form_state->get('slogxtAjaxRebuild'))) {
      // ******************************** return ****
      return $this->getContentResult();
      // ******************************** return ****
    }

    //test: result is an array
    if (!is_array($result)) {
      $class = get_class($this);
      $message = t("Result is expected to be an array: @class", ['@class' => $class]);
      throw new \LogicException($message);
    }

    // 
    $result += [
      'messages' => $this->renderedMessages(),
      'wrapperId' => $wrapperId,
      'labels' => $this->getLabels(),
    ];

    // trigger event for altering result
    $event_data = [
      'form' => &$form,
      'result' => &$result,
    ];
    $event = new SlogxtGenericEvent($event_data);
    $this->dispatcher->dispatch(SlogxtEvents::AJAXFORM_RESPONSE_ALTER, $event);

//    \Drupal::logger('xxx')->notice('no_redirect: ' . $form_state->isRedirectDisabled());
    $response = new AjaxResponse();
    if ($this->formResult !== 'saved' || $this->appendAttachments()) {
      $form['#attached']['drupalSettings']['slogxt']['permissions']['toolbars'] = SlogXt::getTxPermissions();
      $response->setAttachments($form['#attached']);
    }
    $response->addCommand(new SxtInvokeCommand($result));
    // alter $response if needed
    return $this->preSendResponse($response);
  }

  protected function appendAttachments() {
    return FALSE;
  }

  /**
   * Return data for urgent message
   * 
   * @param FormStateInterface $form_state
   * @return array 
   */
  protected function getUrgentMessageData(FormStateInterface $form_state) {
    $slogxt = $form_state->get('slogxt');
    if (isset($slogxt['urgentMsgData']) && is_array($slogxt['urgentMsgData'])) {
      return $slogxt['urgentMsgData'];
    }
  }

  /**
   * Return for response prepared data (urgent message only)
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array 
   */
  protected function buildUrgentMsgContentResult(&$form, FormStateInterface $form_state) {
    $this->hasUrgentMessage = TRUE;
    $this->formResult = 'edit';
    $this->submitted = FALSE;
    $this->hasErrors = FALSE;
    $this->htmlPreview = FALSE;

    drupal_get_messages();  // clear messages
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardUrgent';

    if (!empty($this->client_resolve)) {
      $slogxtData['clientResolve'] = $this->client_resolve;
    }
    else {
      $uMsgData = $this->getUrgentMessageData($form_state);
      $msg = !empty($uMsgData['message']) ? $uMsgData['message'] : t('Not implemented !!!');
      $type = !empty($uMsgData['type']) ? $uMsgData['type'] : 'warning';
      drupal_set_message($msg, strtolower($type));
    }

    return $this->_doBuildContentResult($form, $form_state);
  }

  /**
   * Return for response prepared data
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array 
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    return $this->_doBuildContentResult($form, $form_state);
  }

  /**
   * Prepare and return response data
   * 
   * Do not override this function
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  protected function _doBuildContentResult(&$form, FormStateInterface $form_state) {
    if ($this->hasUrgentMessage) {
      // prevent some functions from being performed
      $this->htmlContent = $this->renderer->renderRoot($form);
    }
    else {
      // allow to alter form data
      // call this before retriving errors, labels and slogxtData
      $this->preRenderForm($form, $form_state);

      // 
      $this->submitted = $form_state->isSubmitted();
      $this->hasErrors = $form_state->hasAnyErrors();
      $this->htmlPreview = FALSE;
      $this->htmlContent = '';

      //
      $this->renderFormPlus($form, $form_state);

      //
      $this->postRenderForm($form, $form_state);
    }
    // prepare response data
    $slogxtData = [
      'formResult' => $this->formResult,
      'formBroken' => isset($this->formBroken) ? $this->formBroken : false,
      'htmlContent' => $this->htmlContent,
      'htmlPreview' => $this->htmlPreview,
        ] + $form_state->get('slogxtData');

    return [
      'submitted' => $this->submitted,
      'hasError' => $this->hasErrors,
      'slogxtData' => $slogxtData,
    ];
  }

  /**
   * Prepare and return response data for dialog view content
   * 
   * Do not override this function
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  protected function _doBuildDialogViewContentResult(&$form, FormStateInterface $form_state) {
    return [
      'submitted' => FALSE,
      'hasError' => $this->hasErrors,
      'slogxtData' => $form_state->get('slogxtData'),
    ];
  }

  /**
   * Return rendered dialog view content.
   * 
   * @param string $header
   * @param string $content
   * @return string
   */
  public function renderedDialogViewContent($header, $content) {
    $header = '<h2 class="slogxt-preview-title">' . $header . '</h2>';
    $element = [
      '#prefix' => '<div class="xt-dialog-view preview">' . $header,
      '#suffix' => '</div>',
      '#markup' => $content,
    ];

    $html = $this->renderer->renderRoot($element);
    return (string) $html;
  }

  /**
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  protected function renderFormPlus(&$form, FormStateInterface $form_state) {
    if ($this->submitted && !$this->hasErrors) {
      $this->formResult = 'saved';
      $this->htmlContent = '';
    }
    else {
      $this->formResult = 'edit';
      $this->htmlContent = $this->renderer->renderRoot($form);
    }
  }

  protected function renderedMessages($clear_queue = TRUE) {
    if ($this->hasUrgentMessage) {  // prefer urgent message !!
      $messages = StatusMessages::renderMessages(NULL);
    }
    elseif ($this->formResult === 'edit') {
      $messages = StatusMessages::renderMessages('error');
    }
    elseif ($this->formResult === 'saved') {
      $messages = StatusMessages::renderMessages(NULL);
    }

    if (!empty($messages)) {
      $messages = trim($this->renderer->render($messages));
    }

    if ($clear_queue) {
      drupal_get_messages();
    }

    return !empty($messages) ? $messages : false;
  }

  protected function buildWrapperId($form_id, $wrapperId, $form_state) {
    $build = [
      '#type' => 'hidden',
      '#value' => $wrapperId,
      '#id' => Html::getUniqueId("edit-$form_id-wrapper_id"),
      '#parents' => ['wrapper_id'],
    ];
    return \Drupal::formBuilder()->doBuildForm($form_id, $build, $form_state);
  }

  protected function buildStatus($form_id, $enabled, $form_state) {
    $build = [
      '#type' => 'hidden',
      '#value' => $enabled,
      '#id' => Html::getUniqueId("edit-$form_id-status"),
      '#parents' => ['status'],
    ];
    return \Drupal::formBuilder()->doBuildForm($form_id, $build, $form_state);
  }

  protected function getEntityFormObject($form_arg, $entity = NULL, array $values = []) {
    return SlogXt::getEntityFormObject($form_arg, $entity, $values);
  }

  /**
   * Set the default saved message.
   */
  public function setSavedMessage($append = '') {
    drupal_get_messages();  // clear messages
    if (empty($append) && !empty($this->savedEntity)) {
      $append = $this->savedEntity->label();
    }
    $append = empty($append) ? '.' : ": $append";
    drupal_set_message(t('Changes have been saved%append', ['%append' => $append]));
  }

}
