<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\DefaultRoleController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtUserData;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Render\Element;

/**
 * Defines a controller ....
 */
class DefaultRoleController extends ProfileEditControllerBase {

  /**
   * {@inheritdoc}
   */
  function getAllowedAccountFields() {
    return ['xt_default_role'];
  }
  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Change your current default role');
  }

  protected function getLabels() {
    $labels = false;
    if ($this->hasLabels()) {
      $labels = [
        'dialogTitle' => $this->getFormTitle(),
        'submitLabel' => t('Set as default'),
      ];
    }
    return $labels;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    // hide all form fields and add role items for display 
    $xtrole_items = [];
    $default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    $default_role_id = $default_role->id();
    $is_admin_role = $default_role->isAdmin();
    foreach (Element::children($form) as $field_name) {
      $field = &$form[$field_name];
      if ($field_name === 'xtdata') {
        $xt_roles = XtUserData::getUserXtRoles($this->user, TRUE);
        $form[$field_name]['#attributes']['class'][] = 'visually-hidden';

        foreach (Element::children($field) as $inner_field_name) {
          if ($inner_field_name === 'xt_default_role') {
            foreach ($field[$inner_field_name]['#options'] as $key => $value) {
              $role = $xt_roles[$key] ?: FALSE;
              if ($role && ($is_admin_role || $role->id() !== AccountInterface::ANONYMOUS_ROLE)) {
                $xtrole_items[] = [
                  'entityid' => $role->id(),
                  'liTitle' => $role->label(),
                  'liDescription' => $role->get('description'),
                ];
              }
            }
          }
          else {
            unset($field[$inner_field_name]);
          }
        }
      }
      else {
        $form[$field_name]['#attributes']['class'][] = 'visually-hidden';
      }
    }

    // prepare slogxt data
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardDefaultRole';
    $slogxtData['items'] = $xtrole_items;
    $slogxtData['default_role'] = $default_role_id;
    if ($form_state->isSubmitted() && !$form_state->hasAnyErrors()) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }

    $result = XtEditControllerBase::buildContentResult($form, $form_state);
    return $result;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    return [
      'command' => 'slogxt::reload',
      'args' => ['reloadImmediately' => TRUE],
    ];
  }

}
