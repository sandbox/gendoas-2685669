<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\EMailChangeController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class EMailChangeController extends ProfileEditControllerBase {

  /**
   * {@inheritdoc}
   */
  function getAllowedAccountFields() {
    return ['current_pass', 'mail'];
  }
  
  /**
   * {@inheritdoc}
   */
  protected function getCurPasswordTarget() {
    return t('Email address');
  }

  /**
   * {@inheritdoc}
   */
  protected function getLabels() {
    return [
      'dialogTitle' => t('Change email address: %name', ['%name' => $this->user->label()]),
      'submitLabel' => t('Save'),
    ];
  }

  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $called_object = self::calledObject();
    $current_email = $called_object->user->getEmail();
    $input = $form_state->getUserInput();
    if (empty($input['current_pass'])) {
      $msg = t('Your current password is required.');
      $form_state->setErrorByName('current_pass', $msg);
    }
    elseif ($current_email === $input['mail']) {
      $msg = t('Your did not change your email address. Try again.');
      $form_state->setErrorByName('current_pass', $msg);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    drupal_set_message(t('Email address has been changed.'));
    return FALSE; // no finished command
  }

}
