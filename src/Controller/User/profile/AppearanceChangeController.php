<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\AppearanceChangeController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\XtUserData;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class AppearanceChangeController extends ProfileEditControllerBase {

  /**
   * {@inheritdoc}
   */
  function getAllowedAccountFields() {
    return ['xt_color_tb', 'xt_color_dialog', 'xt_color_active', 'xt_color_mwcomment', 'xt_colors_by_device'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getLabels() {
    return [
      'dialogTitle' => t('Change appearance: %name', ['%name' => $this->user->label()]),
      'submitLabel' => $this->user->isAnonymous() ? t('Change') : t('Save'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    parent::postBuildForm($form, $form_state);
    $form['xtdata']['#title'] = t('Appearance');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['alterForm'] = ['command' => 'slogxt::alterFormAppearance'];
    $slogxtData['transparentOverlay'] = TRUE;
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    drupal_set_message(t('Appearance has been changed.'));
    return [
      'command' => 'slogxt::finishedAppearance',
      'args' => XtUserData::getAppearanceData(),
    ];
  }

}
