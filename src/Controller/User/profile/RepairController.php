<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\RepairController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class RepairController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\profile\ResetOptionsForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    drupal_get_messages();  // clear messages
    $msg = t('All cached data will be removed, including the current status.');
    drupal_set_message($msg, 'warning');
    $this->hasUrgentMessage = TRUE;
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'checkboxesAllHidden';
    return ['slogxtData' => $slogxtData];
  }

  protected function getLabels() {
    $labels = false;
    if ($this->hasLabels()) {
      $labels = [
        'dialogTitle' => t('Repair'),
        'submitLabel' => t('Repair'),
      ];
    }
    return $labels;
  }

}
