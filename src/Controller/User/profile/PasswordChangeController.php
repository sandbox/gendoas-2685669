<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\PasswordChangeController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class PasswordChangeController extends ProfileEditControllerBase {

  /**
   * {@inheritdoc}
   */
  function getAllowedAccountFields() {
    return ['current_pass', 'pass'];
  }
  
  /**
   * {@inheritdoc}
   */
  protected function getCurPasswordTarget() {
    return t('Password');
  }

  /**
   * {@inheritdoc}
   */
  protected function getLabels() {
    return [
      'dialogTitle' => t('Change password: %name', ['%name' => $this->user->label()]),
      'submitLabel' => t('Save'),
    ];
  }

  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    $current_pass = $input['current_pass'];
    $pass = $input['pass'];
    if (empty($input['current_pass'])) {
      $msg = t('Your current password is required.');
      $form_state->setErrorByName('current_pass', $msg);
    }
    elseif (empty($pass['pass1']) && empty($pass['pass1'])) {
      $msg = t('You didn\'t enter your new password.');
      $form_state->setErrorByName('current_pass', $msg);
    }
    elseif ($current_pass === $pass['pass1']) {
      $msg = t('Your did not change your password. Try again.');
      $form_state->setErrorByName('current_pass', $msg);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    drupal_set_message(t('Password has been changed.'));
    return FALSE; // no finished command
//    return ['command' => 'slogxt::logout'];
  }

}
