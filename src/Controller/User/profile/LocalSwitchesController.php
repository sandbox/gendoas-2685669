<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\LocalSwitchesController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class LocalSwitchesController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\profile\LocalSwitchesForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Local switches');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = $form_state->get('slogxtData');
    $slogxtData['isEditForm'] = TRUE;
    $slogxtData['storageLocalKey'] = 'slogxt:profile::localSwitches';
    return ['slogxtData' => $slogxtData];
  }

  protected function getLabels() {
    $labels = false;
    if ($this->hasLabels()) {
      $labels = [
        'dialogTitle' => t('Local switches'),
        'submitLabel' => t('Save'),
      ];
    }
    return $labels;
  }

}
