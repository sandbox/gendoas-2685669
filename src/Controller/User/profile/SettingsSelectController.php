<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\SettingsSelectController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class SettingsSelectController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\profile\SettingsSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Settings: Select action');
  }

}
