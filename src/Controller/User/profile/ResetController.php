<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\ResetController.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class ResetController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\profile\ResetOptionsForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Select reset options');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    return ['slogxtData' => $form_state->get('slogxtData')];
  }

  protected function getLabels() {
    $labels = false;
    if ($this->hasLabels()) {
      $labels = [
        'dialogTitle' => t('Select actions to execute'),
        'submitLabel' => t('Execute'),
      ];
    }
    return $labels;
  }

}
