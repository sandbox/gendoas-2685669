<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\profile\ProfileEditControllerBase.
 */

namespace Drupal\slogxt\Controller\User\profile;

use Drupal\slogxt\SlogXt;
use Drupal\user\Entity\User;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
abstract class ProfileEditControllerBase extends XtEditControllerBase {

  /**
   * Retrieve data to send by ajax::SxtInvokeCommand
   */
  abstract protected function getAllowedAccountFields();

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->user = User::load(\Drupal::currentUser()->id());
    return $this->getEntityFormObject('user.default', $this->user);
  }

  protected function getCurPasswordTarget() {
    return '<target>';
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    $allowed = SlogXt::allowedFormFields();
    $wrapper = $this->getInputFieldWrapper();
    foreach (Element::children($form) as $key) {
      $form_field = &$form[$key];
      if (in_array($key, ['account', 'xtdata'])) {
        $acc_allowed = $this->getAllowedAccountFields();
        foreach (Element::children($form[$key]) as $acc_key) {
          $acc_field = &$form[$key][$acc_key];
          if (!in_array($acc_key, $acc_allowed)) {
            if ($key === 'account' && $acc_key === 'roles') {
              $acc_field['#attributes']['class'] = ['visually-hidden'];
            }
            else {
              unset($form[$key][$acc_key]);
            }
          }
          else {
            if ($acc_key === 'current_pass') {
              $args = ['%target' => $this->getCurPasswordTarget()];
              $acc_field['#description'] = $this->t('Required if you want to change your %target.', $args);
              $acc_field += $wrapper + ['#required' => TRUE];
            }
            elseif ($acc_key === 'pass') {
              $acc_field['pass1']['#title'] = t('New Password');
              $acc_field['pass1'] += $wrapper + ['#required' => TRUE];
              $acc_field['pass2']['#title'] = t('Confirm new Password');
              $acc_field['pass2'] += $wrapper + ['#required' => TRUE];
              ;
            }
            elseif (!empty($acc_field['#type']) && $acc_field['#type'] !== 'hidden') {
              $acc_field += $wrapper;
            }
          }
        }

        if (empty(Element::children($form[$key]))) {
          unset($form[$key]);
        }
      }
      elseif ($key === 'actions') {
        unset($form[$key]['delete']);
      }
      elseif (!in_array($key, $allowed)) {
        $form[$key]['#attributes']['class'][] = 'visually-hidden';
      }
      elseif (!empty($form_field['#type']) && $form_field['#type'] !== 'hidden') {
        $acc_field += $wrapper;
      }
    }
  }

  public static function formValidate(array &$form, FormStateInterface $form_state) {
    // do nothing by default
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $slogxt = &$form_state->get('slogxt');
    if ($slogxt && $slogxt['ajax']) {
      // add callbacks
      $controllerClass = get_class($this);
      $form['#validate'][] = "$controllerClass::formValidate";
      // preserve status
      $node = $form_state->getFormObject()->getEntity();
      $slogxt['saved_status'] = (integer) $node->status->value;

      // allow preview and submit as the only actions
      // and add callbacks
      $allowed = ['submit'];
      $actions = &$form['actions'];
      foreach (Element::children($actions) as $key) {
        if (!in_array($key, $allowed)) {
          unset($actions[$key]);
        }
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::renderFormPlus();
   */
  protected function renderFormPlus(&$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$this->hasErrors) {
      $this->formResult = 'saved';
      $this->htmlContent = '';
    }
    else {
      $this->formResult = 'edit';
    }
    parent::renderFormPlus($form, $form_state);
  }

  //
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // Static callbacks, 
  // use self::calledObject() as the current ajax form controller
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /**
   * After form build handler, added by self::hookFormAlter()
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAfterBuild($form, FormStateInterface $form_state) {
    $form_state->setTriggeringElement($form['actions']['submit']);
    return $form;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    if ($form_state->isSubmitted() && !$form_state->hasAnyErrors()) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }
    return parent::buildContentResult($form, $form_state);
  }

}
