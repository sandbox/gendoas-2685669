<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\LoginController.
 */

namespace Drupal\slogxt\Controller\User;

/**
 * Defines a controller ....
 */
class LoginController extends UserControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\XtUserLoginForm';
  }
  
}
