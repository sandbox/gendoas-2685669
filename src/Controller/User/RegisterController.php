<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\RegisterController.
 */

namespace Drupal\slogxt\Controller\User;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class RegisterController extends UserControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\XtUserRegisterForm';
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $slogxt = $form_state->get('slogxt');
    if ($slogxt && $slogxt['ajax']) {
      $form['actions']['submit']['#value'] = t('Submit');
      $form['#after_build'][] = [get_class($this), 'formAfterBuild'];
    }
  }

  public static function formAfterBuild($form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    if (!empty($input) && !empty($input['name'])) {
      $form_state->setTriggeringElement($form['actions']['submit']);
    }
    return $form;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $remove_fields = ['timezone', 'user_picture', 'contact'];
    foreach ($remove_fields as $field) {
      unset($form[$field]);
    }
  }

}
