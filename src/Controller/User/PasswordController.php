<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\PasswordController.
 */

namespace Drupal\slogxt\Controller\User;

/**
 * Defines a controller ....
 */
class PasswordController extends UserControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\user\Form\UserPasswordForm';
  }
    
}
