<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\User\UserControllerBase.
 */

namespace Drupal\slogxt\Controller\User;

use Drupal\slogxt\Controller\AjaxFormControllerBase;

/**
 * Defines a controller ....
 */
abstract class UserControllerBase extends AjaxFormControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::hasLabels();
   */
  protected function hasLabels() {
    return false;
  }
  
}
