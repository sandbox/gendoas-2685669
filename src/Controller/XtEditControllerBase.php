<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\XtEditControllerBase.
 */

namespace Drupal\slogxt\Controller;

use Drupal\slogxt\XtExtrasTrait;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
abstract class XtEditControllerBase extends AjaxFormControllerBase {

  use XtExtrasTrait;

  protected $opSave = false;
  protected $has_submit_error = false;
  protected $savedValues = [];

  protected function hasSubmitError() {
    return $this->has_submit_error;
  }

  protected function setSubmitError($value = TRUE) {
    $this->has_submit_error = $value;
  }

  protected function hasLabels() {
    return true;
  }

  protected function getTabLabel() {
    return false;
  }

  protected function getSubmitLabel() {
    return $this->t('Next');
  }

  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if (isset($form['captcha']) && is_array($form['captcha'])) {
      $wrapper = $this->getInputFieldWrapper();
      $form['captcha'] += $wrapper;
      $widgets = &$form['captcha']['captcha_widgets'];
      if (isset($widgets['captcha_refresh'])) {
        unset($widgets['captcha_refresh']);
      }
    }
  }

  /**
   * Add an every page message for the form.
   * 
   * @param string $msg
   * @param FormStateInterface $form_state
   */
  public function setPreFormMessage($msg, FormStateInterface $form_state, $warn_only_msg = '') {
    if (!empty($msg)) {
      $type = 'status';
      if (!$form_state->isSubmitted() || $form_state->hasAnyErrors()) {
        $type = 'warning';
        if (!empty($warn_only_msg)) {
          $msg .= $warn_only_msg;
        }
      }
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['message'] = ['msg' => $msg, 'type' => $type];
    }
  }

}
