<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\dummy\DummyCheckboxesController.
 */

namespace Drupal\slogxt\Controller\dummy;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class DummyCheckboxesController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\dummy\DummyCheckboxesForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Dummy checkboxes');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    return ['slogxtData' => $form_state->get('slogxtData')];
  }

  protected function getLabels() {
    $labels = false;
    if ($this->hasLabels()) {
      $labels = [
        'dialogTitle' => t('Dummy. No execution.'),
        'submitLabel' => t('Dummy-Go'),
      ];
    }
    return $labels;
  }

}
