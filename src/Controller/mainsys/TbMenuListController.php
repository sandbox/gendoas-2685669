<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\mainsys\TbMenuListController.
 */

namespace Drupal\slogxt\Controller\mainsys;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Controller\AjaxFormControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class TbMenuListController extends AjaxFormControllerBase {

  protected function getBaseRootterm($request_entity_id) {
//    $request_entity_id = 'archive~node~_sys~453';
    $rootterm_id = (integer) $request_entity_id;
    if ($rootterm_id > 0) {
      return SlogTx::getRootTerm($rootterm_id);
    }

    $DL = SlogXt::XTURL_DELIMITER;
    $DL5 = SlogXt::XTURL_DELIMITER5;
    list($t1, $t2, $tb, $entity_id) = explode($DL, $request_entity_id . $DL5);
    $entity_id = (integer) $entity_id;
    if ($entity_id > 0 && $tb === SlogTx::TOOLBAR_ID_SYS) {
      $target = $t1;  // archive, trash, ..
      $toolbar_id = $t2;
      $toolbar = SlogTx::getToolbar($toolbar_id);
      $enforce_teid = $toolbar->getEnforceTargetEntity();
//      $tb_part = $toolbar->isUnderscoreToolbar() ? 'sys' . $toolbar_id : $toolbar_id;
//      $toolbartab = "{$target}_{$tb_part}";
//      $vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, $toolbartab]);
//      $vid = $toolbar->getSysToolbarVid($target);
      $entity = \Drupal::entityTypeManager()
          ->getStorage($enforce_teid)
          ->load($entity_id);
      if ($entity) {
//        return SlogTx::getTbtabRootTerm($vid, $entity);
//todo::current:: TxRootTermGetPlugin - new
        $plugin_id = $toolbar->getSysMenuTbRootTermPluginId($target);
        return SlogTx::getSysSysRootTerm($plugin_id, $entity);
      }
    }
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $request_entity_id = $request->get('slogtx_rt');
    $rootterm = $this->getBaseRootterm($request_entity_id);
    if ($rootterm && $rootterm->isRootTerm()) {
      $this->vocabulary = $rootterm->getVocabulary();
      $request->attributes->set('root_term', $rootterm);
      return 'Drupal\slogxt\Form\TbMenuListForm';
    }
    else {
      $message = t("Root term not found: @tid", ['@tid' => $rootterm_id]);
      throw new \LogicException($message);
    }
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::hasLabels();
   */
  protected function hasLabels() {
    return false;
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    if ($this->vocabulary->isSysToolbar()) {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['attachCommand'] = 'attachSysTbmenuList';
    }
  }

}
