<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\AjaxDefaultControllerBase.
 */

namespace Drupal\slogxt\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\slogxt\Ajax\SxtInvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

abstract class AjaxDefaultControllerBase extends ControllerBase {

  use CommandWithAttachedAssetsTrait;

  /**
   * The content for the matched element(s).
   *
   * Either a render array or an HTML string.
   *
   * @var string|array
   */
  protected $content = 'none';

  /**
   * Retrieve data to send by ajax::SxtInvokeCommand
   */
  abstract protected function responseData();

  /**
   * @return Drupal\Core\Ajax\AjaxResponse
   */
  public function ajaxResponse() {
    $data = $this->responseData();
    $response = new AjaxResponse();
    $response->addCommand(new SxtInvokeCommand($data, $this->getAttachedAssets()));
    $response = $this->responseAlter($response);
    return $response;
  }
  
  /**
   * Hook for altering response object.
   * 
   * @param AjaxResponse $response
   * @return AjaxResponse
   */
  protected function responseAlter($response) {
    return $response;
  }

}
