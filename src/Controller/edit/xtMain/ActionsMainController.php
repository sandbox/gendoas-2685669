<?php

/**
 * @file
 * Contains \Drupal\slogxt\Controller\edit\xtMain\ActionsMainController.
 */

namespace Drupal\slogxt\Controller\edit\xtMain;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class ActionsMainController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    //todo::actions::main
    return 'Drupal\slogxt\Form\edit\xtMain\MainActionSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Select edit action');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    return ['slogxtData' => $form_state->get('slogxtData')];
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::appendAttachments();
   */
  protected function appendAttachments() {
    return TRUE;
  }

}
