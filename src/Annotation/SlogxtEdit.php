<?php

/**
 * @file
 * Contains \Drupal\slogxt\Annotation\SlogxtEdit.
 */

namespace Drupal\slogxt\Annotation;

/**
 * Defines a Plugin annotation object for slog edit plugins.
 *
 * @Annotation
 */
class SlogxtEdit extends XtAnnotationBase {

  /**
   * Required permissions
   *
   * @var array  
   */
  public $permissions = FALSE;
  
}
