<?php

/**
 * @file
 * Contains \Drupal\slogxt\Annotation\XtAnnotationBase.
 */

namespace Drupal\slogxt\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a base class for some slogxt plugin annotations.
 *
 * @Annotation
 */
class XtAnnotationBase extends Plugin {
  
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin bundle, e.g. content.
   *
   * @var string
   */
  public $bundle = '';

  /**
   * The plugin title used in forms.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title = '';

  /**
   * The description edit action in forms.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The route name of the plugin.
   *
   * @var string
   */
  public $route_name;
  
  
  public $resolve_base_command = 'slogxt::dummyCommand';
  public $resolve_next_command = FALSE;
  public $resolve_link_command = FALSE;
  public $resolve_args = FALSE;
  
  
  
  
  

  /**
   * The page is skipped when there is only one item.
   *
   * @var boolean (optional) defaults TRUE
   */
  public $skipable = TRUE;
  
  /**
   * A default weight for the edit action.
   *
   * @var int (optional)
   */
  public $weight = 0;
  
}
