<?php

/**
 * @file
 * Contains \Drupal\slogxt\Annotation\SlogxtAction.
 */

namespace Drupal\slogxt\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for slogxt action plugins.
 *
 * @Annotation
 */
class SlogxtAction extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The menu in which the action is available.
   *
   * @var string optional
   */
  public $menu = 'none';

  /**
   * The group the action belongs to.
   * 
   * @var string
   */
  public $group = null;

  /**
   * The path portion for creating the action's path.
   * 
   * By default there is a path for a dummy action.
   *
   * @var string optional
   */
  public $path = 'dummy';

  /**
   * The value used as actions parameter.
   * 
   * @var string optional
   */
  public $value = null;

  /**
   * The css class for the action, e.g. the icon class.
   *
   * @var string optional
   */
  public $cssClass = 'icon-dummy';

  /**
   * The css id for action's item, e.g. list item.
   *
   * @var string optional
   */
  public $cssItemId = '';

  /**
   * The css class for action's item, e.g. list item.
   *
   * @var string optional
   */
  public $cssItemClass = '';

  /**
   * Whether the action depends on user's default role.
   *
   * @var boolean optional
   */
  public $dependsOnRole = FALSE;

  /**
   * An integer to determine the weight of this action relative to other
   * actions within a listing of actions.
   *
   * @var int optional
   */
  public $weight = 0;

  /**
   * The name of the module providing the action.
   *
   * Not provider, so we can use theme name too, or other names.
   * 
   * @var string
   */
  public $xtProvider;

  /**
   * The name of the module providing the action.
   *
   * @var string
   */
  public $theme = null;

  /**
   * Whether to prevent this action to move to top of the dropbutton.
   *
   * @var bool (optional)
   */
  public $notop = null;
  
  /**
   * Whether this action is for authenticated user only.
   *
   * @var bool (optional)
   */
  public $needsAuth = null;
  
  /**
   * Whether this action is for authenticated user only.
   *
   * @var bool (optional)
   */
  public $useFilter = null;

  /**
   * Whether this action is enabled or disabled by default.
   *
   * @var bool (optional)
   */
  public $status = TRUE;

}
