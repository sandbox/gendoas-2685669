<?php

/**
 * @file
 * Contains \Drupal\slogxt\Annotation\SlogxtContent.
 */

namespace Drupal\slogxt\Annotation;

/**
 * Defines a Plugin annotation object for ... plugins.
 *
 * @Annotation
 */
class SlogxtContent extends XtAnnotationBase {

}
