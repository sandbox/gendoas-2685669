<?php

/**
 * @file
 * Contains \Drupal\slogxt\Annotation\SlogxtProps.
 */

namespace Drupal\slogxt\Annotation;

/**
 * Defines a Plugin annotation object for slog props plugins.
 *
 * @Annotation
 */
class SlogxtProps extends XtAnnotationBase {

  /**
   * Required permissions
   *
   * @var array  
   */
  public $permissions = FALSE;
  
}
