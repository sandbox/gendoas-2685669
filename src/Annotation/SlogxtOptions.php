<?php

/**
 * @file
 * Contains \Drupal\slogxt\Annotation\SlogxtOptions.
 */

namespace Drupal\slogxt\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for slogxt option plugins.
 *
 * @Annotation
 */
class SlogxtOptions extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The menu in which the action is available.
   *
   * @var string optional
   */
  public $bundle = '';

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description edit action in forms.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The path portion for creating the action's path.
   * 
   * By default there is a path for a dummy action.
   *
   * @var string optional
   */
  public $path = 'dummy';

  /**
   * The value used as actions parameter.
   * 
   * @var string optional
   */
  public $value = null;

  /**
   * The name of the module providing the action.
   *
   * Not provider, so we can use theme name too, or other names.
   * 
   * @var string
   */
  public $xtProvider;

  /**
   * An integer to determine the weight of this action relative to other
   * actions within a listing of actions.
   *
   * @var int optional
   */
  public $weight = 0;

  /**
   * Whether this action is enabled or disabled by default.
   *
   * @var bool (optional)
   */
  public $status = TRUE;

}
