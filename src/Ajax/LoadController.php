<?php

/**
 * @file
 * Contains \Drupal\slogxt\Ajax\LoadController.
 */

namespace Drupal\slogxt\Ajax;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\slogxt\Ajax\SxtInvokeCommand;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

/**
 */
class LoadController extends ControllerBase {

  use CommandWithAttachedAssetsTrait;

  /**
   * Loads an library by ajax call.
   * 
   * @param string $module the provider of the library
   * @param string $library library id in $module.libraries.yml
   * @return Drupal\Core\AjaxAjaxResponse
   * 
   * @see Drupal\slogxt\SlogXt::libraryAutoloadAdd()
   * @see Drupal.slogxt.AutoloadModel (JS)
   */
  public function loadLibrary($module, $library) {
    $library = str_replace('__', '.', $library);
    $this->content = [
        '#markup' => 'dummy',
        '#attached' => [
            'library' => ["$module/$library"],
        ],
    ];
    $content = $this->getRenderedContent();
    $data = ['content' => $content];

    $response = new AjaxResponse();
    $response->addCommand(new SxtInvokeCommand($data, $this->getAttachedAssets()));
    return $response;
  }

}
