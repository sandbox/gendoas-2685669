<?php

/**
 * @file
 * Definition of Drupal\slogxt\Ajax\SxtInvokeCommand.
 */

namespace Drupal\slogxt\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;
use Drupal\Core\Asset\AttachedAssets;

/**
 * An AJAX command for transporting various data.
 *
 * The client side calling function is responsible for syncing.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.slogxtInvoke()
 * defined in slogxt/js/sxt.ajax.js.
 *
 * @ingroup ajax
 */
class SxtInvokeCommand implements CommandInterface, CommandWithAttachedAssetsInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * Data to send by ajax.
   *
   * @var array
   */
  protected $data;

  /**
   * The constructor reseives the data to send.
   * 
   * @param array $data
   * @param AttachedAssets $attachedAssets
   */
  public function __construct($data, $attachedAssets = false) {
    $this->data = $data;
    if ($attachedAssets && $attachedAssets instanceof AttachedAssets) {
      $this->attachedAssets = $attachedAssets;
    } else {
      $this->attachedAssets = new AttachedAssets();
    }
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   * 
   * @see Drupal.AjaxCommands.prototype.slogxtInvoke JS function
   */
  public function render() {
    return [
        'command' => 'slogxtInvoke',
        'data' => $this->data,
    ];
  }

}
