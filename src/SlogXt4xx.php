<?php

/**
 * @file
 * Contains \Drupal\slogxt\SlogXt4xx.
 */

namespace Drupal\slogxt;

use Drupal\node\Entity\Node;
use Drupal\node\Controller\NodeViewController;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ...
 */
class SlogXt4xx {

  public static function responseData($err_code) {
    $custom_4xx_path = \Drupal::configFactory()
        ->get('system.site')
        ->get("page.$err_code");

    if (!empty($custom_4xx_path)) {
      $data = self::_customResponseData($err_code, $custom_4xx_path);
    }
    else {
      $data = self::_defaultResponseData($err_code);
    }

    if (empty($data)) {
      $data = [
        'showMessage' => TRUE,
        'type' => 'error',
        'message' => 'SlogXt4xx: Unknown error.'
      ];
    }

    return $data;
  }

  public static function setFormStateErr4xx(FormStateInterface $form_state, $err_code) {
    $slogxt = & $form_state->get('slogxt');
    $slogxt['urgentMsgData'] = self::responseData($err_code);
    unset($slogxt['urgentMsgData']['showMessage']);
  }
  
  private static function _customResponseData($err_code, $custom_4xx_path) {
    //todo
    $custom_4xx_path = trim($custom_4xx_path, '/');
    list($type, $node_id, $fault) = explode('/', $custom_4xx_path);
    if ($type === 'node' && !empty($node_id) && empty($fault)) {
      if ($node = Node::load($node_id)) {
        $node_view_controller = NodeViewController::create(\Drupal::getContainer());
        $build = $node_view_controller->view($node);
        if (isset($build['#attached']['html_head_link'])) {
          unset($build['#attached']['html_head_link']);
        }
        $rendered = \Drupal::service('renderer')->render($build);
        $data = [
          'showMessage' => TRUE,
          'type' => 'error',
          'message' => $rendered,
        ];
      }
    }

    if (empty($data)) {
      $data = self::_defaultResponseData($err_code);
    }

    return $data;
  }

  private static function _defaultResponseData($err_code) {
    $err_msg = t('A client error happened.');
    switch ($err_code) {
      case 401:
        $err_msg = t('Please log in to access this page.');
        break;
      case 403:
        $err_msg = t('You are not authorized to access this page.');
        break;
      case 404:
        $err_msg = t('The requested page could not be found.');
        break;
    }
    $data = [
      'showMessage' => TRUE,
      'type' => 'error',
      'message' => $err_msg,
    ];

    return $data;
  }

}
