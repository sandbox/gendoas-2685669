<?php

/**
 * @file
 * Contains \Drupal\slogtx\XtRenderCache.
 */

namespace Drupal\slogxt;

use Drupal\Core\Render\RenderCache;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Cache\CacheFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * 
 */
class XtRenderCache extends RenderCache {
  
  /**
   * The renderer configuration array.
   *
   * @var array
   */
  protected $rendererConfig;
  
  public function __construct(RequestStack $request_stack, CacheFactoryInterface $cache_factory, CacheContextsManager $cache_contexts_manager, $renderer_config) {
    parent::__construct($request_stack, $cache_factory, $cache_contexts_manager);
    $this->rendererConfig = $renderer_config;
  }

  public function setRequiredCacheContexts(&$elements) {
      $required_cache_contexts = $this->rendererConfig['required_cache_contexts'];
      if (isset($elements['#cache']['contexts'])) {
        $elements['#cache']['contexts'] = Cache::mergeContexts($elements['#cache']['contexts'], $required_cache_contexts);
      }
      else {
        $elements['#cache']['contexts'] = $required_cache_contexts;
      }
  }
  
  public function addCacheableDependency(array &$elements, $dependency) {
    $meta_a = CacheableMetadata::createFromRenderArray($elements);
    $meta_b = CacheableMetadata::createFromObject($dependency);
    $meta_a->merge($meta_b)->applyTo($elements);
  }

  

  public function getCacheID(&$elements) {
    return $this->createCacheID($elements);
  } 
  

  /**
   * {@inheritdoc}
   */
  public function get(array $elements) {
    $request = \Drupal::request();
    if (!($request->isMethodCacheable() || $request->isMethod('POST')) || !$cid = $this->createCacheID($elements)) {
      return FALSE;
    }
    $bin = isset($elements['#cache']['bin']) ? $elements['#cache']['bin'] : 'render';

    if (!empty($cid) && ($cache_bin = $this->cacheFactory->get($bin)) && $cache = $cache_bin->get($cid)) {
      $cached_element = $cache->data;
      // Two-tier caching: redirect to actual (post-bubbling) cache item.
      // @see \Drupal\Core\Render\RendererInterface::render()
      // @see ::set()
      if (isset($cached_element['#cache_redirect'])) {
        return $this->get($cached_element);
      }
      // Return the cached element.
      return $cached_element;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function set(array &$elements, array $pre_bubbling_elements) {
    $request = \Drupal::request();
    if (!($request->isMethodCacheable() || $request->isMethod('POST')) || !$cid = $this->createCacheID($elements)) {
      return FALSE;
    }
    $bin = isset($elements['#cache']['bin']) ? $elements['#cache']['bin'] : 'render';
    if (!empty($cid) && ($cache_bin = $this->cacheFactory->get($bin))) {
      $data = $pre_bubbling_elements;
      $max_age = $this->maxAgeToExpire($elements['#cache']['max-age']);
      $cache_bin->set($cid, $data, $max_age, $elements['#cache']['tags']);
    }
  }
  
}
