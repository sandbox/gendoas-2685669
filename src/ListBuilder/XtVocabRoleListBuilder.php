<?php

/**
 * @file
 * Contains \Drupal\slogxt\ListBuilder\XtVocabRoleListBuilder.
 */

namespace Drupal\slogxt\ListBuilder;

use Drupal\slogtx_ui\ListBuilder\TxVocabularyListBuilder;
use Drupal\slogtx\SlogTx;
use Drupal\taxonomy\VocabularyListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Build a listing of slog vocabulariy entities for a toolbar set by request.
 */
class XtVocabRoleListBuilder extends TxVocabularyListBuilder {

  /**
   * Whether access is allowed.
   * 
   * @return Drupal\Core\Access\AccessResult
   */
  public static function access(RoleInterface $user_role) {
    if ($user_role->access('update') && SlogTx::entityInstance('slogtx_tb', 'role')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $this->user_role = \Drupal::request()->get('user_role');

    $slogtx_tb = 'role';
    $this->toolbar = SlogTx::entityInstance('slogtx_tb', $slogtx_tb);
    if ($this->toolbar) {
      return VocabularyListBuilder::render();
    }

    $args = ['%entity_id' => $slogtx_tb];
    drupal_set_message(t('Toolbar %entity_id not found.', $args), 'error');
    return $this->redirect('entity.user_role.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_rolevocabs_overview';
  }

  public function roleVocabsTitle(RoleInterface $user_role) {
    return 'TbTabs for role ' . $user_role->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = [];

    $vid = $entity->id();
    $role_id = $this->user_role->id();
//    $rootterm = SlogTx::getTbtabRootTerm($vid, $this->user_role);
//todo::current:: TxRootTermGetPlugin - new
    $plugin_id = SlogTx::getTxMenuToolbarPluginId($vid);
    $rootterm = SlogTx::getTbMenuRootTerm($plugin_id, $this->user_role);
    if (!$rootterm) {
      $operations['prepare'] = [
        'title' => t('Prepare role tab'),
        'weight' => 10,
        'url' => Url::fromRoute('slogxt.tbtab.prepare_confirm_role', [
          'user_role' => $role_id,
          'vocabulary' => $vid,
        ]),
      ];
    }
    else {
      $operations['edit_title'] = [
        'title' => t('Edit title'),
        'weight' => 10,
        'url' => Url::fromRoute('slogxt.tbtab.edit_role', [
          'user_role' => $role_id,
          'vocabulary' => $vid,
        ]),
      ];
      $operations['menu_terms'] = [
        'title' => t('Menu terms'),
        'weight' => 20,
        'url' => Url::fromRoute('entity.slogtx_rt.overview_form', ['slogtx_rt' => $rootterm->id()]),
      ];
    }

    return $operations;
  }

}
