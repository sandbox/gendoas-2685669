<?php

/**
 * @file
 * Contains \Drupal\slogxt\ListBuilder\XtVocabUserListBuilder.
 */

namespace Drupal\slogxt\ListBuilder;

use Drupal\slogtx_ui\ListBuilder\TxVocabularyListBuilder;
use Drupal\slogtx\SlogTx;
use Drupal\taxonomy\VocabularyListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Drupal\Core\Access\AccessResult;

/**
 * Build a listing of slog vocabulariy entities for a toolbar set by request.
 */
class XtVocabUserListBuilder extends TxVocabularyListBuilder {
  
  /**
   * Whether access is allowed.
   * 
   * @return Drupal\Core\Access\AccessResult
   */
  public static function access(UserInterface $user) {
    if ($user->access('update') && SlogTx::entityInstance('slogtx_tb', 'user')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $this->user = \Drupal::request()->get('user');

    $slogtx_tb = 'user';
    $this->toolbar = SlogTx::entityInstance('slogtx_tb', $slogtx_tb);
    if ($this->toolbar) {
      return VocabularyListBuilder::render();
    }

    $args = ['%entity_id' => $slogtx_tb];
    drupal_set_message(t('Toolbar %entity_id not found.', $args), 'error');
    return $this->redirect('entity.user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_uservocabs_overview';
  }

  public function userVocabsTitle(UserInterface $user) {
    return 'TbTabs for user ' . $user->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = [];

    $vid = $entity->id();
    $user_id = $this->user->id();
//    $rootterm = SlogTx::getTbtabRootTerm($vid, $this->user);
//todo::current:: TxRootTermGetPlugin - new
    $plugin_id = SlogTx::getTxMenuToolbarPluginId($vid);
    $rootterm = SlogTx::getTbMenuRootTerm($plugin_id, $this->user);
    if (!$rootterm) {
      $operations['prepare'] = [
        'title' => t('Prepare user tab'),
        'weight' => 10,
        'url' => Url::fromRoute('slogxt.tbtab.prepare_confirm_user', [
          'user' => $user_id,
          'vocabulary' => $vid,
        ]),
      ];
    }
    else {
      $operations['edit_title'] = [
        'title' => t('Edit title'),
        'weight' => 10,
        'url' => Url::fromRoute('slogxt.tbtab.edit_user', [
          'user' => $user_id,
          'vocabulary' => $vid,
        ]),
      ];
      $operations['menu_terms'] = [
        'title' => t('Menu terms'),
        'weight' => 20,
        'url' => Url::fromRoute('entity.slogtx_rt.overview_form', ['slogtx_rt' => $rootterm->id()]),
      ];
    }

    return $operations;
  }

}
