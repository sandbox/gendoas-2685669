<?php

/**
 * @file
 * Contains \Drupal\slogxt\XtNotHandledTrait.
 */

namespace Drupal\slogxt;

use Drupal\slogxt\SlogXt;

/**
 * A Trait for slogxt date functionality.
 */
trait XtNotHandledTrait {

  protected function notHandled($method) {
    $class = get_class($this);
    $msg = t('Not handled: @method', ['@method' => "$class::$method"]);
    SlogXt::logger('sxt_workflow')->error($msg);
  }

}
