<?php

/**
 * @file
 * Contains \Drupal\slogxt\SlogXt.
 */

namespace Drupal\slogxt;

use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Entity\TxTermBase;
use Drupal\slogxt\Event\SlogxtEvents;
use Drupal\slogxt\Event\SlogxtGenericEvent;
use Drupal\slogxt\Handler\SlogxtHandlerInterface;
use Drupal\slogxt\Handler\XtGrpPermHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Routing\RouteMatch;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Static functions for slogxt module
 */
class SlogXt {

  const XTOPTS_TARGETS_BEFORE = 0;
  const XTOPTS_TARGETS_AFTER = 1;
  const XTOPTS_TARGETS_PREPEND = 2;
  const XTOPTS_TARGETS_APPEND = 3;
  const XTOPTS_TARGETS_SELF_REPLACE = 4;
  const XTOPTS_TARGETS_SELF_PREPEND = 5;
  const XTURL_DELIMITER = "~";
  const XTURL_DELIMITER3 = "~~~";
  const XTURL_DELIMITER5 = "~~~~~";

  /**
   * The id of the trash vocabulary within the _sys toolbar.
   */
  const SYS_VOCAB_ID_TRASH = 'trash';

  /**
   * The id of the archiv vocabulary within the _sys toolbar.
   */
  const SYS_VOCAB_ID_ARCHIVE = 'archive';

  /**
   * Cache object for table 'cache_slogapp'
   *
   * @var Drupal\Core\Cache\CacheBackendInterface 
   */
  static $cache;

  /**
   * Cache retrieved base path for modules
   *
   * @var array of path strings 
   */
  protected static $cacheBasePath = [];

  /**
   * Cache retrieved logger objects for channels
   *
   * @var array of \Psr\Log\LoggerInterface objects
   */
  protected static $cacheLogger = [];

  /**
   * Cached slogxt handler
   *
   * @var \Drupal\slogxt\Handler\SlogxtHandlerInterface 
   */
  protected static $slogxt_handler;
  protected static $slogxt_group_perm_handler;
  protected static $sys_toolbartabs = FALSE;
  //
  protected static $permissions;
  protected static $all_permissions = NULL;
  protected static $config = NULL;

  /**
   * Whether the current route has the option '_admin_route' set to true.
   * 
   * @return boolean
   */
  public static function txtSeeLogMessages() {
    return (string) t('See log messages for more information.');
  }

  public static function isAdminRoute() {
    return \Drupal::service('router.admin_context')->isAdminRoute();
  }

  public static function getConfig() {
    if (empty(self::$config)) {
      self::$config = \Drupal::config('slogxt.settings');
    }
    return self::$config;
  }

  public static function getMaxToolbarTabs() {
    return (integer) (self::getConfig()->get('max_toolbar_tabs') ?? 4);
  }

  public static function isAllowedAppearanceChange() {
    return (boolean) (self::getConfig()->get('allow_appearance_change'));
  }

  public static function isAllowedAppearanceByDevice() {
    return (boolean) (self::getConfig()->get('allow_appearance_clientside'));
  }

  /**
   * 
   */
  public static function getMwConfigValue($field) {
    if (\Drupal::moduleHandler()->moduleExists('sxt_mediawiki')) {
      return self::getConfig()->get($field);
    }
    return FALSE;
  }

  public static function isAllowedAppearanceMwComments() {
    return ((boolean) self::getMwConfigValue('allow_appearance_mwcomments'));
  }

  public static function getMwCommentMaxDepth() {
    return ((integer) (self::getMwConfigValue('mwcomment_max_depth') ?? 3));
  }

  public static function getMwCommentTocCompact() {
    return ((integer) (self::getMwConfigValue('mwcomment_toc_compact') ?? 15));
  }

  /**
   * Add required fields to allready set
   * 
   * @param array $fields optionals
   * @return array
   */
  public static function allowedFormFields($fields = []) {
    return array_merge($fields, ['form_build_id', 'form_id', 'form_token', 'actions', 'wrapper_id']);
  }

  public static function getAdminRole($as_object = FALSE) {
    $role_storage = \Drupal::entityTypeManager()->getStorage('user_role');
    $admin_roles = $role_storage->getQuery()
        ->condition('is_admin', TRUE)
        ->execute();
    $admin_role = reset($admin_roles);

    if ($as_object) {
      return Role::load($admin_role);
    }

    return $admin_role;
  }

  public static function getUserHasAdminRole(AccountInterface $account = NULL) {
    $account = $account ?: \Drupal::currentUser();
    //
    if ($account->isAnonymous()) {
      return FALSE;
    }
    //
    if (User::load($account->id())->hasRole(self::getAdminRole())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Whether a role is a Slog Xt-Role.
   * 
   * @param RoleInterface $role
   * @return boolean
   */
  public static function isSxtRole(RoleInterface $role) {
    return (boolean) $role->get('slogxt_is_xtrole');
  }

  /**
   * Return all Slog Xt Roles.
   * 
   * @param boolean $as_object
   * @return array of role ids or role objects
   */
  public static function getSxtRoles($as_object = TRUE) {
    $roles = [];
    foreach (Role::loadMultiple() as $name => $role) {
      if (SlogXt::isSxtRole($role)) {
        $roles[$name] = $role;
      }
    }

    if ($as_object) {
      return $roles;
    }
    return array_keys($roles);
  }
  
  public static function getTbDeafultTargetEntity($toolbar_id) {
    $account = \Drupal::currentUser();
    if ($toolbar_id === 'user') {
      $entity = User::load($account->id());
    }
    elseif ($toolbar_id === 'role') {
      $entity =  XtUserData::getDefaultRole($account, TRUE);
    }

    return ($entity ?? NULL);
  }

  
  

  /**
   * Determine the handler for handling slog toolbar events.
   * 
   * Concerns the events slog toolbar does not handle itself,
   * e.g. a menu item has been clicked.
   * 
   * @return \Drupal\slogxt\Handler\SlogxtHandlerInterface
   * @throws \InvalidArgumentException
   */
  public static function getSlogxtHandler() {
    if (!isset(self::$slogxt_handler)) {
      // ensure theme is initialized
      $theme = \Drupal::theme()->getActiveTheme()->getName();

      // We have to get one handler.
      // see \Drupal\slogxt\Event\SlogxtEventSubscriber::onRegisterSlogxtHandler()
      // If no other module handles event, this module handles it by dummy handler.
      $event = \Drupal::service('event_dispatcher')->dispatch(SlogxtEvents::REGISTER_SLOGXT_HANDLER);
      $handler_class = !empty($event->slogxtHandlerClass) //
          ? $event->slogxtHandlerClass : 'Drupal\slogxt\Handler\SlogxtDummyHandler';

      // create an instance of the handler class
      $class_resolver = \Drupal::service('class_resolver');
      $handler = $class_resolver->getInstanceFromDefinition($handler_class);
      if (!is_object($handler) || !($handler instanceof SlogxtHandlerInterface)) {
        throw new \RuntimeException("The Handler '{$handler_class}' does not implement SlogxtHandlerInterface.");
      }

      self::$slogxt_handler = $handler;
    }

    return self::$slogxt_handler;
  }

  public static function hasMainEditOtherPerm() {
    $result = [];
    $data = ['result' => &$result];
    $event = new SlogxtGenericEvent($data);
    \Drupal::service('event_dispatcher')
        ->dispatch(SlogxtEvents::SLOGXT_HAS_MAIN_EDIT_PERM, $event);
    return !empty($result);
  }

  /**
   * {@inheritdoc}
   */
  public static function getJsDataResolvePathNode($key) {
    return SlogXt::getSlogxtHandler()->getJsDataResolvePathNode($key);
  }

  /**
   */
  public static function getSlogxtGroupPermHandler() {
    if (!isset(self::$slogxt_group_perm_handler)) {
      // We have to get one handler.
      $event = \Drupal::service('event_dispatcher')->dispatch(SlogxtEvents::REGISTER_GROUP_PERM_HANDLER);
      $handler_class = !empty($event->slogxtHandlerClass) //
          ? $event->slogxtHandlerClass : 'Drupal\slogxt\Handler\XtGrpPermDummyHandler';

      // create an instance of the handler class
      $class_resolver = \Drupal::service('class_resolver');
      $handler = $class_resolver->getInstanceFromDefinition($handler_class);
      if (!is_object($handler) || !($handler instanceof XtGrpPermHandlerInterface)) {
        throw new \RuntimeException("The Handler '{$handler_class}' does not implement XtGrpPermHandlerInterface.");
      }

      self::$slogxt_group_perm_handler = $handler;
    }

    return self::$slogxt_group_perm_handler;
  }

  /**
   */
  public static function addDummyActions() {
    return FALSE;
//    $add_dummy = \Drupal::config('slogxt.settings')->get('add_dummy_actions');
//    return isset($add_dummy) ? $add_dummy : TRUE;
  }

  /**
   * Return configurable base path for a module.
   * 
   * @param string $module
   * @return string 
   */
  public static function getBasePath($module = 'slogxt') {
    if (!isset(self::$cacheBasePath[$module])) {
      $base_path = \Drupal::config("$module.settings")
              ->get('module_base_path') ?: $module;
      self::$cacheBasePath[$module] = trim($base_path, '/') . '/';
    }
    return self::$cacheBasePath[$module];
  }

  /**
   * Return the ajax base path.
   * 
   * All ajax paths for slog pages have 'xtajx/' appended behind the base path.
   * 
   * @return string
   */
  public static function getBaseAjaxPath($module = 'slogxt') {
    return self::getBasePath($module) . 'xtajx/';
  }

  public static function getAllUserPermissions() {
    if (!isset(self::$all_permissions)) {
      self::$all_permissions = \Drupal::service('user.permissions')->getPermissions();
    }
    return self::$all_permissions;
  }

  public static function isSuperUser(AccountInterface $account = NULL) {
    $account = $account ?: \Drupal::currentUser();
    return ((int) $account->id() === 1);
  }

  public static function getUserPermissions(AccountInterface $account = NULL) {
    $account = $account ?: \Drupal::currentUser();
    $roles = Role::loadMultiple($account->getRoles());
    $permissions = [];
    foreach ($roles as $key => $role) {
      $role_perms = $role->getPermissions();
      $permissions = array_merge($permissions, $role_perms);
    }
    return array_unique($permissions);
  }

  public static function getTxPermissions($exclude_sys = TRUE) {
    if (!isset(self::$permissions)) {
      $exclude = [];
      if ($exclude_sys) {
        $exclude = [
          'administer toolbar user',
          'administer toolbar role',
          'administer toolbar _sys',
          'administer toolbar _node',
        ];
      }

      $permissions_all = self::getAllUserPermissions();
      $permissions_slogtx = [];
      foreach ($permissions_all as $key => $permission) {
        if ($permission['provider'] === 'slogtx' && !in_array($key, $exclude)) {
          $permissions_slogtx[] = $key;
        }
      }

      self::$permissions = [];
      $permissions = array_intersect($permissions_slogtx, self::getUserPermissions());
      foreach ($permissions as $permission) {
        self::$permissions[$permission] = TRUE;
      }
      foreach (SlogTx::getToolbarsVisibleGlobal() as $toolbar_id => $toolbar) {
        $permission = "administer toolbar $toolbar_id";
        if (!isset(self::$permissions[$permission])) {
          self::$permissions[$permission] = FALSE;
        }
      }
    }

    return self::$permissions;
  }

  /**
   * Whether current user has required permission.
   * 
   * For $toolbar_id==role test for user's current default role.
   * 
   * @param string $toolbar_id
   *  Toolbar id
   * @param array $action_perms
   *  required permissions to test
   * @return boolean
   */
  public static function hasPermToolbar($toolbar_id, $action_perms, $role_admin_perm = 'admin sxtrole-menu') {
    $has_perm = FALSE;
    switch ($toolbar_id) {
      case 'user':
        $has_perm = (boolean) $action_perms['user'];
        break;
      case 'role':
        $action_perm = $action_perms['role'];
        if (is_bool($action_perm)) {
          $has_perm = $action_perm;
        }
        elseif (!empty($action_perm)) {
          $handler = self::getSlogxtGroupPermHandler();
          $role_perms = $handler->getPermissionsCurrent();
          $has_perm = in_array($role_admin_perm, $role_perms);
          if (!$has_perm) {
            $has_perm = in_array($action_perm, $role_perms);
          }
        }
        break;
      case SlogTx::TOOLBAR_ID_NODE_SUBSYS:
        $has_perm = TRUE;  // this is not decided here
        break;
      default:
        $xt_perms = self::getTxPermissions();
        $has_perm = in_array('administer slog taxonomy', $xt_perms);
        if (!$has_perm) {
          $has_perm = in_array('administer toolbar ' . $toolbar_id, $xt_perms);
        }
        break;
    }

    return $has_perm;
  }

  public static function hasViewPermToolbar(TxTermBase $menu_term, AccountInterface $account = NULL) {
    if (empty($menu_term) || !$menu_term->isMenuTerm()) {
      return FALSE;
    }

    $vocabulary = $menu_term->getVocabulary();
    $toolbar_id = $vocabulary->getToolbarId();
    if (SlogTx::getToolbarsVisibleGlobal()) {
      return TRUE;  // visible for all
    }

    $account = $account ?: \Drupal::currentUser();
    $target_term = $menu_term->getTargetTerm();

//    $test_name = $vocabulary->getTargetEntityPlugin()->buildTargetTermName($node_id);
//    return ($test_name === $target_term->label());



    switch ($toolbar_id) {
      case 'user':
        $test_name = $vocabulary->getTargetEntityPlugin()->buildTargetTermName($account->id());
        return ($test_name === $target_term->label());  // visible for own menu
      case 'role':
        $user_roles = $account->getRoles();
        $target_term_name = $target_term->label();
        foreach ($user_roles as $role_id) {
          $test_name = $vocabulary->getTargetEntityPlugin()->buildTargetTermName($role_id);
          if ($test_name === $target_term_name) {
            return TRUE;  // visible for member in role
          }
        }
        break;
      case SlogTx::TOOLBAR_ID_NODE_SUBSYS:
        return TRUE;  // this is not decided here
    }

    return FALSE;
  }

  /**
   * //todo::text::??? - function getHRefClientside()
   * 
   * @param string $path
   * @return string
   */
  public static function getHRefClientside($path = '', $module = 'slogxt') {
    return '/' . self::getBaseAjaxPath($module) . 'run/' . $path;
  }

  /**
   * Return cache object for table 'cache_slogxt'
   *
   * @return Drupal\Core\Cache\CacheBackendInterface 
   */
  public static function cache() {
    //todo::cache::??? - do we need own cache table ???
    if (!self::$cache) {
      self::$cache = \Drupal::cache('slogxt');
    }
    return self::$cache;
  }

  public static function getXtSysToolbartabs() {
    if (self::$sys_toolbartabs === FALSE) {
      self::$sys_toolbartabs = [];
      self::pluginManager('action')->getActionsData('main_dropbutton', FALSE, TRUE);
    }
    return self::$sys_toolbartabs;
  }

  public static function getToolbarsForSysTabs() {
    $result = [];
    $plugins = SlogTx::getTargetEntityPlugins();
    $toolbars = SlogTx::getToolbarsSorted();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $enforce_teid = $toolbar->getEnforceTargetEntity();
      if ($enforce_teid && !empty($plugins[$enforce_teid])) {
        $result[$toolbar_id] = $toolbar;
      }
    }

    return $result;
  }

  public static function getSysRootTermsToolbar($base_toolbartab) {
    $root_terms = [];
    $plugins = SlogTx::getTargetEntityPlugins();
    $toolbars = self::getToolbarsForSysTabs();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $enforce_teid = $toolbar->getEnforceTargetEntity();
      $plugin = $plugins[$enforce_teid];
      $vid = $toolbar->getSysToolbarVid($base_toolbartab, $toolbartab);
      self::$sys_toolbartabs[$toolbartab] = SlogTx::TOOLBAR_ID_SYS;
      $entity = !empty($plugin->getCurrentEntity()) ? $plugin->getCurrentEntity() : $enforce_teid;
//      $root_terms[$toolbar_id] = SlogTx::getTbtabRootTerm($vid, $entity);
//todo::current:: TxRootTermGetPlugin - new
      $plugin_id = $toolbar->getSysMenuTbRootTermPluginId($base_toolbartab);
      $root_terms[$toolbar_id] = SlogTx::getSysSysRootTerm($plugin_id, $entity);
      if ($plugin->isEntitySettable()) {
        $root_terms["$toolbar_id::resolve"] = $base_toolbartab      //
            . self::XTURL_DELIMITER . $toolbar_id //
            . self::XTURL_DELIMITER . SlogTx::TOOLBAR_ID_SYS;
      }
    }

    return $root_terms;
  }

  public static function getToolbarsRankSorted($as_object = FALSE) {
    $tb_sorted = SlogTx::getToolbarsSorted();
    $head = [];
    $tail = [];
    foreach ($tb_sorted as $toolbar_id => $toolbar) {
      if ($toolbar_id !== 'user') {
        if ($toolbar->isUnderscoreToolbar()) {
          $tail[] = $toolbar_id;
        }
        else {
          $head[] = $toolbar_id;
        }
      }
    }

    $toolbars = array_merge($head, ['user'], $tail);
    if ($as_object) {
      $tbs = [];
      foreach ($toolbars as $toolbar_id) {
        $tbs[$toolbar_id] = $tb_sorted[$toolbar_id];
      }
      $toolbars = $tbs;
    }

    return $toolbars;
  }

  public static function getTrashRootTerms() {
    return self::getSysRootTermsToolbar(self::SYS_VOCAB_ID_TRASH);
  }

  public static function getTrashRootTerm($toolbar_id, EntityInterface $entity = NULL, $do_create = FALSE, $rt_name = SlogTx::ROOTTERM_DEFAULT) {
    $toolbar = SlogTx::getToolbar($toolbar_id);
    $plugin_id = $toolbar->getSysMenuTbRootTermPluginId(SlogXt::SYS_VOCAB_ID_TRASH);
    return SlogTx::getSysSysRootTerm($plugin_id, $entity, $do_create, $rt_name);
//todo::current:: TxRootTermGetPlugin - new
//                  if ($entity) {
//                    $targetentity_id = SlogTx::getToolbar($toolbar_id)->getEnforceTargetEntity();
//                    $plugin = SlogTx::getTargetEntityPlugin($targetentity_id);
//                    $plugin->setEntity($entity);
//                  }
//
//                  $root_terms = self::getTrashRootTerms();
//                  return $root_terms[$toolbar_id] ?: NULL;
  }

  public static function getArchiveRootTerms() {
    return self::getSysRootTermsToolbar(self::SYS_VOCAB_ID_ARCHIVE);
  }

  public static function getArchiveRootTerm($toolbar_id, EntityInterface $entity = NULL, $do_create = FALSE, $rt_name = SlogTx::ROOTTERM_DEFAULT) {
    $toolbar = SlogTx::getToolbar($toolbar_id);
    $plugin_id = $toolbar->getSysMenuTbRootTermPluginId(SlogXt::SYS_VOCAB_ID_ARCHIVE);
    return SlogTx::getSysSysRootTerm($plugin_id, $entity, $do_create, $rt_name);
//todo::current:: TxRootTermGetPlugin - new
//              if ($entity) {
//                $targetentity_id = SlogTx::getToolbar($toolbar_id)->getEnforceTargetEntity();
//                $plugin = SlogTx::getTargetEntityPlugin($targetentity_id);
//                $plugin->setEntity($entity);
//              }
//
//              $root_terms = self::getArchiveRootTerms();
//              return $root_terms[$toolbar_id] ?: NULL;
  }

  /**
   * Return a logger object.
   * 
   * @return \Psr\Log\LoggerInterface
   */
  public static function logger($channel = 'slogxt') {
    if (!isset(self::$cacheLogger[$channel])) {
      self::$cacheLogger[$channel] = \Drupal::logger($channel);
    }
    return self::$cacheLogger[$channel];
  }

  public static function notImplemented($info = '???', $channel = 'slogxt') {
    self::logger($channel)->error("Not implemented: $info");
  }

  /**
   * Returns the plugin manager for a certain plugin type.
   *
   * @param string $type
   *   The plugin type, e.g. target_entity.
   * @param string $provider
   *   The module that provides the manager, defaults to slogxt.
   *
   * @return object 
   *   A plugin manager instance.
   */
  public static function pluginManager($type, $provider = 'slogxt') {
    return \Drupal::service("plugin.manager.$provider.$type");
  }

  public static function getEntityFormObject($form_arg, $entity = NULL, array $values = []) {
    // If no operation is provided, use 'default'.
    $form_arg .= '.default';
    list ($entity_type, $operation) = explode('.', $form_arg);
    if (!isset($entity)) {
      $entity = \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->create($values);
    }

    return \Drupal::entityTypeManager()
            ->getFormObject($entity_type, $operation)
            ->setEntity($entity);
  }

  public static function sortElements($elements, $no_weight = false) {
    $result = [];
    $sorted = Element::children($elements, true);
    foreach ($sorted as $key) {
      $result[] = $elements[$key];
    }
    if ($no_weight) {
      foreach ($result as &$item) {
        unset($item['#weight']);
      }
    }
    return $result;
  }

  public static function libraryAutoloadAdd(&$element, $libraries) {
    if (is_array($libraries) && !empty($libraries)) {
      $baseAjaxPath = trim(self::getBaseAjaxPath(), '/');
      foreach ($libraries as $key => &$library) {
        $libPath = str_replace('.', '__', trim($key, '/'));
        $library['path'] = "$baseAjaxPath/load/library/$libPath";
      }

      if (!isset($element['#attached']['drupalSettings'])) {
        $element['#attached']['drupalSettings'] = [];
      }
      $drupalSettings = & $element['#attached']['drupalSettings'];
      if (!isset($drupalSettings['slogxt']['autoload'])) {
        $drupalSettings['slogxt']['autoload'] = [];
      }
      $autoload = & $drupalSettings['slogxt']['autoload'];
      $autoload = NestedArray::mergeDeepArray([$autoload, $libraries]);
    }
  }

  public static function getEntityTypeByRouteName($route_name, $return_as_object = FALSE) {
    $parts = explode('.', $route_name);
    if (count($parts) > 1 && $parts[0] === 'entity') {
      $type = $parts[1];
      if ($return_as_object) {
        return \Drupal::entityTypeManager()->getDefinition($type);
      }
      return $type;
    }
  }

  public static function getTbSysInfoData() {
    $route_provider = \Drupal::service('router.route_provider');
    $route_name = 'slogxt.mainsys.tbmenus_list';
    $toolbars = self::getToolbarsForSysTabs();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $parameters = ['slogtx_rt' => 'sysresolve__' . $toolbar_id];
      $url = Url::fromRoute($route_name, $parameters);
      $options[] = [
        'entityid' => $toolbar_id,
        'liTitle' => $toolbar->label(),
        'liDescription' => $toolbar->getDescription(),
        'provider' => $toolbar->getProvider(),
        'path' => $url->getInternalPath(),
      ];
    }
    return $options;
  }

  public static function getRouteMatch($route_name, array $parameters = []) {
    try {
      $route_provider = \Drupal::service('router.route_provider');
      $route = $route_provider->getRouteByName($route_name, $parameters);

      // ParamConverterManager relies on the route name and object being
      // available from the parameters array.
      $parameters[RouteObjectInterface::ROUTE_NAME] = $route_name;
      $parameters[RouteObjectInterface::ROUTE_OBJECT] = $route;
      $paramconverter_manager = \Drupal::service('paramconverter_manager');
      $upcasted_parameters = $paramconverter_manager->convert($parameters + $route->getDefaults());

      $route_match = new RouteMatch($route_name, $route, $upcasted_parameters, $parameters);
      return $route_match;
    }
    catch (Exception $ex) {
      return NULL;
    }
  }

  public static function getRenderer() {
    return \Drupal::service('renderer');
  }

  public static function xtRenderMessage($message, $type, $html = FALSE) {
    $return = [
      '#theme' => 'status_messages',
      '#message_list' => [$type => [$message]],
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'emptyWarning' => t('Warning message'),
      ],
    ];

    if ($html) {
      $return = self::getRenderer()->render($return);
    }

    return $return;
  }

  /**
   * Invalidate tags for main dropbutton.
   */
  public static function invalidateTagsMainDropButton() {
    Cache::invalidateTags(['slogxt_main_dropbutton']);
  }

  public static function getAddTargetOptions($add_inside = TRUE) {
    $options = [
      self::XTOPTS_TARGETS_BEFORE => t('Before selected'),
      self::XTOPTS_TARGETS_AFTER => t('After selected'),
    ];
    if ($add_inside) {
      $options[self::XTOPTS_TARGETS_PREPEND] = t('Prepend inside selected');
      $options[self::XTOPTS_TARGETS_APPEND] = t('Append inside selected');
    }

    return $options;
  }

  /**
   * Deposit urgent message data and retur the class of the urgent message form.
   * 
   * Use it in ajax_contoller::getFormObjectArg() with ajax_contoller derived 
   * from \Drupal\slogxt\Controller\AjaxFormControllerBase
   * 
   * @param string $msg 
   * @param string $type
   * @return class of the targeted form
   */
  public static function arrangeUrgentMessageForm($msg = '', $type = '') {
    $request = \Drupal::request();
    $request->attributes->set('urgent_msg_message', $msg);
    $request->attributes->set('urgent_msg_type', $type);
    return 'Drupal\slogxt\Form\XtUrgentMessageForm';
  }

  public static function arrangeUrgentUnexpectedErrorForm() {
    return self::arrangeUrgentMessageForm(t('Unexpected error.'), 'error');
  }

  public static function htmlMessage($message = '???-message', $type = 'status') {
    $class = '"messages messages--' . $type . '"';
    return "<div class=$class>$message</div>";
  }

  public static function htmlLiButton($content = '?') {
//todo::current:: ----sxt_sxt_workflow --- current - htmlLiButton
    return $content;
//    $class = '"slogxt-li-button"';
//    return "<div class=$class>$content</div>";
  }

  public static function htmlHighlightText($text, $warn = FALSE) {
    $cls = 'class="xt-highlight' . ($warn ? ' xt-warn' : '') . '"';
    return "<span $cls>$text</span>";
  }

  public static function htmlErrText($err_code) {
    $text = t('Unknown');
    switch ($err_code) {
      case 403:
        $text = t('Forbidden');
        break;
      case 404:
        $text = t('Not Found');
        break;
    }
    return $text;
  }

  public static function replaceFirstTag($text, $tag_name) {
    $fixed = FALSE;
    preg_match("|<$tag_name>|", $text, $match1, PREG_OFFSET_CAPTURE);
    preg_match("|</$tag_name>|", $text, $match2, PREG_OFFSET_CAPTURE);
    if ($match1 && $match2) {
      $start = $match1[0][1];
      $length = $match2[0][1] - $start + 9;
      $search = mb_substr($text, $start, $length);
      $fixed = preg_replace("|$search|", '', $text);
      $text = !empty($fixed) ? $fixed : $text;
    }
    return $text;
  }

}
