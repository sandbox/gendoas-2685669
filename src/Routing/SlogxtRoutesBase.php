<?php

/**
 * @file
 * Contains \Drupal\slogxt\Routing\SlogxtRoutesBase.
 */

namespace Drupal\slogxt\Routing;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class SlogxtRoutesBase {

  protected static $optAjaxBase = ['_theme' => 'ajax_base_page'];
  
  
}
