<?php

/**
 * @file
 * Contains \Drupal\slogxt\Routing\SlogxtRoutes.
 */

namespace Drupal\slogxt\Routing;

use Drupal\slogxt\SlogXt;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class SlogxtRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    // consider: 'slogxt.' is prepended to each route name by default.
    // this is required for validating a changed base root.
    // see SlogappSettingsForm::validateForm()
    // by default:
    // basePath = 'slogxt/'
    // baseAjaxPath = 'slogxt/xtajx/'

    $basePath = trim(SlogXt::getBasePath(), '/');
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath(), '/');
    $routes = [];

    // this option will be evaluated by \Drupal\slogxt\Theme\SlogxtNegotiator,
    //todo::design::text::???
    // i.e. each of the following paths lead to a slog page or parts of this.
    //todo::redesign::theme::SJQLOUT_THEME_ROUTE

    $raw_data = [];
    $raw_data += $this->_rawAjaxUser($baseAjaxPath);
    $raw_data += $this->_rawAjaxEditMain($baseAjaxPath);
    $raw_data += $this->_rawAjaxMainSystem($baseAjaxPath);


    $defaults = [
      'requirements' => ['_permission' => 'access content'],
      'options' => [],
      'host' => '',
      'schemes' => [],
      'methods' => [],
      'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["slogxt.$key"] = new Route(
          $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxUser($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $baseUserPath = $baseAjaxPath . '/user';
    $controllerPath = '\Drupal\slogxt\Controller\User';
    $raw_data = [
      'ajax.user.login' => [
        'path' => "/$baseUserPath/login",
        'defaults' => [
          '_controller' => "{$controllerPath}\LoginController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'ajax.user.password' => [
        'path' => "/$baseUserPath/password",
        'defaults' => [
          '_controller' => "{$controllerPath}\PasswordController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'ajax.user.register' => [
        'path' => "/$baseUserPath/register",
        'defaults' => [
          '_controller' => "{$controllerPath}\RegisterController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'ajax.edit.profile.actions' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\ActionsController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.admin_reset' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/reset",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\ResetController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.default_reset' => [
        'path' => "/$baseAjaxPath/edit/profile/repair",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\RepairController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.default_role_change' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/defaultrole",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\DefaultRoleController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.password' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/password",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\PasswordChangeController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.email' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/email",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\EMailChangeController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.appearance' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/appearance",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\AppearanceChangeController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.permissions' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/permissions",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\PermissionsController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.resources' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/resources",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\ResourcesController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.settings_select' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/settselect",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\SettingsSelectController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.local_switches' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/localswitches",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\LocalSwitchesController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
      'profile.devtools_select' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/devselect",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\ActionsDevController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
    ];

    return $raw_data;
  }

  /**
   * 
   * @param type $baseAjaxPath
   * @return array
   */
  private function _rawAjaxEditMain($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $baseEditPath = $baseAjaxPath . '/edit_xtmain';
    $controllerPath = '\Drupal\slogxt\Controller\edit\xtMain';
    $options = self::$optAjaxBase + [
      'parameters' => [
        'base_entity_id' => 0,
      ],
    ];

    $raw_data = [
      'edit.main.actions' => [
        'path' => "/$baseEditPath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsMainController::getContentResult",
        ],
        'options' => $options,
      ],
    ];

    return $raw_data;
  }

  /**
   * 
   * @param type $baseAjaxPath
   * @return array
   */
  private function _rawAjaxMainSystem($baseAjaxPath) {
    $controllerPath = '\Drupal\slogxt\Controller';

    $raw_data = [
      'mainsys.tbmenus_list' => [
        'path' => "/$baseAjaxPath/mainsysaction/tbmnulist/{slogtx_rt}",
        'defaults' => [
          '_controller' => "{$controllerPath}\mainsys\TbMenuListController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
    ];

    return $raw_data;
  }

}
