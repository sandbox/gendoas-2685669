<?php

/**
 * @file
 * Contains \Drupal\slogxt\Context\UserDefaultRoleContext.
 */

namespace Drupal\slogxt\Context;

use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines the TopMenuContext service, for "per root term" caching.
 *
 * Cache context ID: 'user.roles' (to vary by all roles of the current user).
 * Calculated cache context ID: 'user.roles:%role', e.g. 'user.roles:anonymous'
 * (to vary by the presence/absence of a specific role).
 */
class UserDefaultRoleContextXXX implements CalculatedCacheContextInterface {
//      #  cache_context.slogxt_udrc:
//      #    class: Drupal\slogxt\Context\UserDefaultRoleContext
//      #    tags:
//      #      - { name: 'cache.context' }

  
//            $account = \Drupal::currentUser();
//            $default_role = XtUserData::getDefaultRole($account);
//            $user = User::load($account->id());
//            $user->addCacheContexts(['slogxt_udrc:' . $default_role]);
//            $user->addCacheTags(['slogxt_udr:' . $default_role]);
//
//            $renderer = \Drupal::service('renderer');
//            $renderer->addCacheableDependency($build, $user);
    
    //todo::check::  tags==user:1 ??? ist das gut ????
    //see CacheableMetadata !!!!
  
  
  
  
  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Menu by current root term');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($slogtx_rt = NULL) {
    if (!$slogtx_rt) {
      throw new \LogicException('No root term provided for menu cache context.');
    }

    return (string) $slogtx_rt;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($slogtx_rt = NULL) {
    return new CacheableMetadata();
  }

}

