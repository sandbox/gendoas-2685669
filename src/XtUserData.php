<?php

/**
 * @file
 * Contains \Drupal\slogxt\XtUserData.
 */

namespace Drupal\slogxt;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;
use Drupal\Core\Render\Element;
use Drupal\user\RoleInterface;

/**
 * Static functions and callbacks for extended user data.
 * 
 * Data stored in table {users_data}.
 */
class XtUserData {

  /**
   * Implements hook_ENTITY_TYPE_update() for user entities.
   * 
   * @see: slogxt_form_user_form_alter().
   */
  public static function formAlter(&$form, FormStateInterface $form_state) {
    $user = $form_state->getFormObject()->getEntity();
    if (!$user) {
      return;
    }

    $uid = $user->id();
    $input = $form_state->getUserInput();
    if (isset($input) && !empty($input['xt_default_role'])) {
      $default_role = $input['xt_default_role'];
      $color_tb = $input['xt_color_tb'] ?? '';
      $color_dialog = $input['xt_color_dialog'] ?? '';
      $color_active = $input['xt_color_active'] ?? '';
      $color_mwcomment = $input['xt_color_mwcomment'] ?? '';
      $colors_by_device = $input['xt_colors_by_device'] ?? FALSE;
    }
    else {
      $default_role = self::getDefaultRole($user);
      $user_data = self::getUserData($uid);
      $color_tb = $user_data['xt_color_tb'];
      $color_dialog = $user_data['xt_color_dialog'];
      $color_active = $user_data['xt_color_active'];
      $color_mwcomment = $user_data['xt_color_mwcomment'];
      $colors_by_device = $user_data['xt_colors_by_device'] ?? FALSE;
    }

    if (!$user->isAnonymous()) {
      $xtdata_id = 'edit-xt-default-role';
      $form['xtdata'] = [
        '#type' => 'details',
        '#id' => $xtdata_id,
        '#title' => t('Slogxt settings'),
        '#open' => TRUE,
        '#weight' => $form['account']['#weight'] + 0.01,
      ];

      $options = [];
      foreach (self::getUserXtRoles($user, TRUE) as $rid => $role) {
        if (SlogXt::isSxtRole($role)) {
          $options[$rid] = $role->label();
        }
      }
      // ensure default role is in options
      if (!in_array($default_role, array_keys($options))) {
        $default_role = RoleInterface::AUTHENTICATED_ID;
      }

      $form['xtdata']['xt_default_role'] = [
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('Default role'),
        '#description' => t("Select the default role. This role is used for role dependent contents."),
        '#default_value' => $default_role,
      ];
    }

    $default_label = ' (' . t('default') . ')';
    $options = self::getColorsTb();
    $default_color = 'sxttb-black2';
    $options[$default_color] .= $default_label;
    if (empty($color_tb) || !in_array($color_tb, array_keys($options))) {
      $color_tb = $default_color;
    }
    $form['xtdata']['xt_color_tb'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Toolbars'),
      '#description' => t("Select the toolbars color."),
      '#default_value' => $color_tb,
      '#attributes' => [
        'id' => "xt-color-tb",
      ],
    ];

    $options = self::getColorsActive();
    $default_color = 'sxtact-steelblue';
    $options[$default_color] .= $default_label;
    if (empty($color_active) || !in_array($color_active, array_keys($options))) {
      $color_active = $default_color;
    }
    $form['xtdata']['xt_color_active'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Active items'),
      '#description' => t("Select the active item color."),
      '#default_value' => $color_active,
      '#attributes' => [
        'id' => "xt-color-active",
      ],
    ];

    $options = self::getColorsDialog();
    $default_color = 'sxtdlg-black3';
    $options[$default_color] .= $default_label;
    if (empty($color_dialog) || !in_array($color_dialog, array_keys($options))) {
      $color_dialog = $default_color;
    }
    $form['xtdata']['xt_color_dialog'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Dialogs'),
      '#description' => t("Select the Dialog color."),
      '#default_value' => $color_dialog,
      '#attributes' => [
        'id' => "xt-color-dialog",
      ],
    ];

    if (SlogXt::isAllowedAppearanceMwComments()) {
      $options = self::getColorsMwComments();
      $default_color = 'sxtmwc-gray';
      $options[$default_color] .= $default_label;
      if (empty($color_mwcomment) || !in_array($color_mwcomment, array_keys($options))) {
        $color_dialog = $default_color;
      }
      $form['xtdata']['xt_color_mwcomment'] = [
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('MW Comments'),
        '#description' => t("Select MediaWiki comment colors."),
        '#default_value' => $color_mwcomment,
        '#attributes' => [
          'id' => "xt-color-mwcomment",
        ],
      ];
    }


    if ($user->isAnonymous()) {
      $form['xtdata']['xt_colors_by_device'] = [
        '#type' => 'hidden',
        '#value' => TRUE,
      ];
    }
    elseif (SlogXt::isAllowedAppearanceByDevice()) {
      $form['xtdata']['xt_colors_by_device'] = [
        '#type' => 'checkbox',
        '#title' => t('Set colors by device'),
        '#description' => t('By default appearance is the same for all your devices. You may manage appearance for each device.'),
        '#default_value' => $colors_by_device,
      ];
    }


    if ($user->isAnonymous()) {
      // no save for anonymous user
      $form['#validate'] = $form['#submit'] = ['\Drupal\slogxt\XtUserData::doNothing'];
      $form['actions']['submit']['#submit'] = ['\Drupal\slogxt\XtUserData::doNothing'];
    }
    else {
      $roles = &$form['account']['roles'];
      $roles['#ajax'] = [
        'callback' => get_class() . '::ajaxOnChangedRoles',
        'wrapper' => $xtdata_id,
        'effect' => 'fade',
      ];

      $form['actions']['submit']['#submit'][] = '\Drupal\slogxt\XtUserData::formSubmit';
    }
  }

  public static function doNothing($form, FormStateInterface $form_state) {
    // do nothing but clear errors
    // there is no save on submit saved
    $form_state->clearErrors();
  }

  public static function getColorsFor($pre) {
    $colors = [
      'black1' => 'Black', // #0f0f0f
      'black2' => 'DarkGray-1', // #333333	
      'black3' => 'DarkGray-2', // #4d4d4d
      'steelblue' => 'SteelBlue', // steelblue
      'blue1' => 'CadetBlue-1', // #263e6b
      'blue2' => 'CadetBlue-2', // #395c78
      'green1' => 'DarkGreen-1', // #334d33
      'green2' => 'DarkGreen-2', // #265926
      'khaki1' => 'DarkKhaki-1', // #353318
      'khaki2' => 'DarkKhaki-2', // #585528
      'chocolate1' => 'Chocolate-1', // #42210a
      'chocolate2' => 'Chocolate-2', // #6f3811
      'orchid1' => 'Orchid-1', // #34103d
      'orchid2' => 'Orchid-2', // #561b65
    ];

    if (!empty($pre)) {
      $tmp = $colors;
      $colors = [];
      foreach ($tmp as $key => $value) {
        $colors["$pre-$key"] = $value;
      }
    }

    return $colors;
  }

  public static function getColorsTb() {
    return self::getColorsFor('sxttb');
  }

  public static function getColorsDialog() {
    return self::getColorsFor('sxtdlg');
  }

  public static function getColorsActive() {
    return self::getColorsFor('sxtact');
  }

  public static function getColorsMwComments() {
    return [
      'sxtmwc-gray' => 'Silver/Gainsboro',
      'sxtmwc-gainsboro' => 'Gainsboro/Whitesmoke',
      'sxtmwc-tan' => 'Tan/Wheat',
    ];
  }

  /**
   * Ajax callback.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   *  By ajax requeste part of renderable form array
   */
  public static function ajaxOnChangedRoles($form, FormStateInterface $form_state) {
    return $form['xtdata'];
  }

  /**
   * Submit callback for the user profile form to save the xtdata settings.
   */
  public static function formSubmit($form, FormStateInterface $form_state) {
    $uid = $form_state->getFormObject()->getEntity()->id();
    if ($uid) {
      $values = $form_state->getValues();
      $input = $form_state->getUserInput();
      $user_data = self::getUserData($uid);
      foreach (Element::children($form['xtdata']) as $name) {
        if (isset($values[$name])) {
          $user_data[$name] = $values[$name];
        }
      }

      if (!SlogXt::isAllowedAppearanceByDevice()) {
        $user_data['xt_colors_by_device'] = FALSE;
      }

      self::setUserData($uid, $user_data);
    }
  }

  /**
   * Return user role ids/objects, inclusive ANONYMOUS
   * 
   * @param AccountInterface $account
   * @param boolean $as_object
   * @return array of role ids or role objects
   */
  public static function getUserXtRoles(AccountInterface $account, $as_object = FALSE) {
    $role_ids = array_merge([RoleInterface::ANONYMOUS_ID], $account->getRoles());
    if ($as_object) {
      return Role::loadMultiple($role_ids);
    }
    return $role_ids;
  }

  /**
   * Return user's default role as string or as object.
   * 
   * For anonymous user return anonymous role.
   * 
   * @param AccountInterface $account
   *  User for which to get the default role
   * @param boolean $as_object optional, defauls to FALSE
   *  On TRUE return as object
   * @return string|Drupal\user\Entity\Role
   */
  public static function getDefaultRole(AccountInterface $account, $as_object = FALSE) {
    static $loaded = [];
    if ($account->isAuthenticated()) {
      $uid = $account->id();
      if (empty($loaded[$uid])) {
        $role_ids = self::getUserXtRoles($account);
        $default_role = self::getUserData($uid, 'xt_default_role');
        if (!$default_role || !in_array($default_role, $role_ids)) {
          $default_role = RoleInterface::AUTHENTICATED_ID;
        }
        $loaded[$uid] = $default_role;
      }
      $default_role = $loaded[$uid];
    }
    else {
      $default_role = RoleInterface::ANONYMOUS_ID;
    }

    if ($as_object) {
      // return role object
      return Role::load($default_role);
    }

    return $default_role;
  }

  public static function getAppearanceData() {
    $data = [];
    $user = \Drupal::currentUser();
    if ($user->isAnonymous()) {
      $data['xt_color_tb'] = 'sxttb-black2';
      $data['xt_color_dialog'] = 'sxtdlg-black3';
      $data['xt_color_active'] = 'sxtact-steelblue';
      $data['xt_color_mwcomment'] = 'sxtmwc-gray';
      $data['xt_colors_by_device'] = TRUE;
    }
    else {
      $user_data = self::getUserData(\Drupal::currentUser()->id());
      $fields = ['xt_color_tb', 'xt_color_dialog', 'xt_color_active', 'xt_color_mwcomment', 'xt_colors_by_device'];
      foreach ($fields as $name) {
        if (isset($user_data[$name])) {
          $data[$name] = ($user_data[$name]);
        }
      }
      if (!SlogXt::isAllowedAppearanceByDevice()) {
        $data['xt_colors_by_device'] = FALSE;
      }
    }

    $data['classes_tb'] = implode(' ', array_keys(self::getColorsTb()));
    $data['classes_dialog'] = implode(' ', array_keys(self::getColorsDialog()));
    $data['classes_active'] = implode(' ', array_keys(self::getColorsActive()));
    $data['classes_mwcomments'] = implode(' ', array_keys(self::getColorsMwComments()));

    return $data;
  }

  /**
   * Return user data stored for slogxt module.
   * 
   * @param integer $uid
   *  User id
   * @param string $key optional, defauls to NULL
   *  Return part of data by key, if set
   * @return mixed
   */
  public static function getUserData($uid, $key = NULL) {
    $module = 'slogxt';
    $root = 'data';
    $data = \Drupal::service('user.data')->get($module, $uid, $root);
    return $key ? $data[$key] : $data;
  }

  /**
   * Store additional data for slogxt module. 
   * 
   * @param string $uid
   *  User id
   * @param mixed $value
   *  Value to store
   * @param string $key optional, defauls to NULL
   *  Set part of data by key, if set
   */
  public static function setUserData($uid, $value, $key = NULL) {
    $module = 'slogxt';
    $root = 'data';
    $user_data = \Drupal::service('user.data');
    $data = $user_data->get($module, $uid, $root);
    $data = is_array($data) ? $data : [];
    if ($key) {
      if (isset($value)) {
        $data[$key] = $value;
      }
      else {
        unset($data[$key]);
      }
    }
    else {
      $data = is_array($value) ? $value : [];
    }
    if (!empty($data)) {
      $user_data->set($module, $uid, $root, $data);
    }
    else {
      $user_data->delete($module, $uid, $root);
    }
  }

}
