<?php

/**
 * @file
 * Definition of \Drupal\slogxt\Handler\XtGrpPermHandlerInterface.
 */

namespace Drupal\slogxt\Handler;

interface XtGrpPermHandlerInterface {
  
  /**
   * Return the own module name.
   * 
   * @return string
   */
  public function getProvider();
      
  /**
   * Return an array of permissions for current user and current default role.
   * 
   * @return array of strings
   */
  public function getPermissionsCurrent();
      
}
