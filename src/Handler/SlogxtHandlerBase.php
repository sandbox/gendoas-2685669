<?php

/**
 * @file
 * Definition of \Drupal\slogxt\Handler\SlogxtHandlerBase.
 */

namespace Drupal\slogxt\Handler;

abstract class SlogxtHandlerBase implements SlogxtHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function getRouteName($route_name_ext = '.on.menu_item_select', $sub_handler = TRUE) {
    if ($sub_handler) {
      return $this->getSubHandler()->getRouteName($route_name_ext);
    }
    return $this->getProvider() . $route_name_ext;
  }

}
