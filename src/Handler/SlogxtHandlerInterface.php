<?php

/**
 * @file
 * Definition of \Drupal\slogxt\Handler\SlogxtHandlerInterface.
 */

namespace Drupal\slogxt\Handler;

interface SlogxtHandlerInterface {
  
  /**
   * Return the own module name.
   * 
   * @return string
   */
  public function getProvider();
  
  /**
   * Return the sub handler.
   * 
   * @return string
   */
  public function getSubHandler();
  
  /**
   * Return the route name.
   * 
   * @return string
   */
  public function getRouteName($route_name_ext = '.on.menu_item_select', $sub_handler = TRUE);
  
  /**
   * Return data for client side resolve node id.
   * 
   * @param string $key
   * @return array or FALSE
   *  Array provides key, function, provider 
   */
  public function getJsDataResolvePathNode($key);
  
}
