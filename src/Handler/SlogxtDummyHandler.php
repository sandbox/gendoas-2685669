<?php

/**
 * @file
 * Definition of \Drupal\slogxt\Handler\SlogxtDummyHandler.
 */

namespace Drupal\slogxt\Handler;

class SlogxtDummyHandler extends SlogxtHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return 'slogxt';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getSubHandler() {
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getJsDataResolvePathNode($key) {
    return FALSE;
  }

}
