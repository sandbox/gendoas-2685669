<?php

/**
 * @file
 * Definition of \Drupal\slogxt\Handler\XtGrpPermHandlerBase.
 */

namespace Drupal\slogxt\Handler;

abstract class XtGrpPermHandlerBase implements XtGrpPermHandlerInterface {

  public function getPermissionsCurrent() {
    return ['bypass group perms'];
  }
}
