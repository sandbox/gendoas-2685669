<?php

/**
 * @file
 * Definition of \Drupal\slogxt\Handler\XtGrpPermDummyHandler.
 */

namespace Drupal\slogxt\Handler;

class XtGrpPermDummyHandler extends XtGrpPermHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return 'slogxt';
  }
  
}
