<?php

/**
 * @file
 * Contains \Drupal\slogxt\XtExtrasTrait.
 */

namespace Drupal\slogxt;

/**
 * A Trait for slogxt date functionality.
 */
trait XtExtrasTrait {

  protected function getInputFieldWrapper($inline = FALSE) {
    $class_inline = $inline ? ' slogxt-input-inline' : '';
    return [
      '#prefix' => '<div class="slogxt-input-field' . $class_inline . ' js-form-wrapper form-wrapper">',
      '#suffix' => '</div>',
    ];
  }

  protected function htmlHrPlus($plus = '>>>') {
    return "<hr />$plus ";
  }

}
