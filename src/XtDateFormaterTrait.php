<?php

/**
 * @file
 * Contains \Drupal\slogxt\XtDateFormaterTrait.
 */

namespace Drupal\slogxt;

/**
 * A Trait for Views broken handlers.
 */
trait XtDateFormaterTrait {
  
  private $xt_date_formatter = NULL;

  public function getXtDateFormater() {
    if (!isset($this->xt_date_formatter)) {
      $this->xt_date_formatter = \Drupal::service('date.formatter');
    }
    return $this->xt_date_formatter;
  }

}
