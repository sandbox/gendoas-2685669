<?php

/**
 * @file
 * Contains \Drupal\slogxt\Event\SlogxtEvents.
 */

namespace Drupal\slogxt\Event;

/**
 * Defines events thrown by slogxt.
 */
final class SlogxtEvents {

  /**
   * User changed the default role by ajax call
   */
  const REGISTER_SLOGXT_HANDLER = 'slogxt.register_slogxt_handler';

  /**
   * 
   */
  const REGISTER_GROUP_PERM_HANDLER = 'slogxt.register_group_permission_handler';

  /**
   * 
   */
  const AJAXFORM_RESPONSE_ALTER = 'slogxt.ajaxform_response_alter';

  /**
   * 
   */
  const ACTION_GET_APPLY_ALTER = 'slogxt.action_get_apply_alter';

  /**
   * 
   */
  const SLOGXT_HAS_MAIN_EDIT_PERM = 'slogxt.has_main_edit_perm';

}
