<?php

/**
 * @file
 * Contains \Drupal\slogxt\Event\SlogxtEventSubscriber.
 */

namespace Drupal\slogxt\Event;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\XtUserData;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Render\HtmlResponse;

/**
 * Slog toolbar event subscriber.
 */
class SlogxtEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //
    $events[SlogxtEvents::REGISTER_SLOGXT_HANDLER][] = ['onRegisterSlogxtHandler', 9999];
    //
    $events[SlogxtEvents::REGISTER_GROUP_PERM_HANDLER][] = ['onRegisterGroupPermHandler', 9999];
    //
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 9999];

    return $events;
  }

  /**
   * 
   * @param Event $event
   */
  public function onRegisterSlogxtHandler(Event $event) {
    $event->slogxtHandlerClass = 'Drupal\slogxt\Handler\SlogxtDummyHandler';
  }
  
  /**
   * 
   * @param Event $event
   */
  public function onRegisterGroupPermHandler(Event $event) {
    $event->slogxtHandlerClass = 'Drupal\slogxt\Handler\XtGrpPermDummyHandler';
  }

  /**
   * 
   * @param Event $event
   */
  public function onKernelResponse(Event $event) {
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse && !SlogXt::isAdminRoute()) {
      $to_add['drupalSettings']['slogxt'] = $this->_getHtmlAttachments();
      $response->addAttachments($to_add);
    }
  }

  function _getHtmlAttachments() {
    $current_user = \Drupal::currentUser();
    $config_register = \Drupal::config('user.settings')->get('register');
    $default_role = XtUserData::getDefaultRole($current_user, TRUE);
    $default_role_id = $default_role->id();
    $xtHandler = SlogXt::getSlogxtHandler();
    $xtSubHandler = $xtHandler->getSubHandler();
    $base_path = SlogXt::getBasePath();
    $base_path_trimmed = trim($base_path, '/');

    $role_header_path = [];
    $vocabs = SlogTx::getVocabulariesByToolbar('role');
    foreach (XtUserData::getUserXtRoles($current_user, TRUE) as $rid => $role) {
      if (SlogXt::isSxtRole($role)) {
        $role_header_path[$rid] = [];
        foreach ($vocabs as $vid => $vocabulary) {
          $toolbartab = SlogTx::getToolbartabFromVid($vid);
          $vocabulary->getTargetEntityPlugin()->setEntity($role);
          $role_header_path[$rid][$toolbartab] = $vocabulary->headerPathLabel();
        }
      }
    }
    
    $attachments = [
      'basePath' => $base_path,
      'baseAjaxPath' => SlogXt::getBaseAjaxPath(),
      'basePathProvider' => [$base_path_trimmed => 'slogxt'],
      'resolveDelim' => SlogXt::XTURL_DELIMITER,
      'autoload' => [],
      'xtHandler' => $xtHandler->getProvider(),
      'xtSubHandler' => $xtSubHandler->getProvider(),
      'tbSysId' => SlogTx::TOOLBAR_ID_SYS,
      'tbSysNode' => SlogTx::TOOLBAR_ID_NODE_SUBSYS,
      'tbSysDummy' => SlogTx::TBTAB_SYSID_DUMMY,
      'tbSysInfoData' => SlogXt::getTbSysInfoData(),
      'tbRankList' => SlogXt::getToolbarsRankSorted(),
      'roleHeaderPath' => $role_header_path,
      'user' => [
        'isAnonymous' => $current_user->isAnonymous(),
        'isRoleAdmin' => 'isRoleAdmin::NotImplemented', // is admin for default_role entities
        'isAdminRole' => $default_role->isAdmin(), // is role administrator
        'defaultRole' => $default_role_id,
        'appearance' => XtUserData::getAppearanceData(),
        'canRegister' => (strpos($config_register, USER_REGISTER_VISITORS) === 0),
      ],
    ];
    
    // attach breakpoints
    $breakpoints = \Drupal::service('breakpoint.manager')->getBreakpointsByGroup('slogxt');
    if (!empty($breakpoints)) {
      $media_queries = [];
      foreach ($breakpoints as $id => $breakpoint) {
        $media_queries[$id] = $breakpoint->getMediaQuery();
      }
      $attachments['breakpoints'] = $media_queries;
    }

    return $attachments;
  }

}
