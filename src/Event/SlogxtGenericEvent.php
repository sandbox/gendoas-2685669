<?php

/**
 * @file
 * Contains \Drupal\slogxt\Event\SlogxtGenericEvent.
 */

namespace Drupal\slogxt\Event;

use Symfony\Component\EventDispatcher\Event;

class SlogxtGenericEvent extends Event {
  
  protected $data;

  public function __construct($data = NULL) {
    $this->data = $data;
  }

  public function getData() {
    return $this->data;
  }

}
