<?php

/**
 * @file
 * Contains \Drupal\slogxt\SlogxtMin.
 */

namespace Drupal\slogxt;

/**
 * Contains constants and functions needed on each request.
 */
class SlogxtMin {
  
  /**
   * Theme with jquery.layout.
   */
  const SJQLOUT_THEME = 'sjqlout';

  /**
   * ....
   */
  const SJQLOUT_THEME_ROUTE = 'sjqlout_theme_route';

  /**
   * Determines whether the sjqlout theme is the active theme.
   * 
   * @return boolean
   */
  public static function themeIsJqLout() {
    $theme = \Drupal::theme()->getActiveTheme()->getName();
    return ($theme == self::SJQLOUT_THEME);
  }

}
