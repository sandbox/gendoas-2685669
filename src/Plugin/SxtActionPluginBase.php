<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\SxtActionPluginBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\SlogxtMin;

/**
 * Provides a base implementation for an Slogxt Action plugin.
 */
abstract class SxtActionPluginBase extends PluginBase implements SxtActionPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getActionData() {
    $provider = $this->getXtProvider();
    $actionData = [
        'label' => $this->getTitle(),
        'href' => SlogXt::getHRefClientside('', $provider) . $this->getPath(),
        'class' => $this->getCssClass(),
        'path' => $this->getPath(),
        'provider' => $provider,
    ];
    $value = $this->getValue();
    if (isset($value)) {
      $actionData['value'] = $value;
    }
    $item_id = $this->getCssItemId();
    if (!empty($item_id)) {
      $actionData['itemId'] = $item_id;
    }
    $item_cls = $this->getCssItemClass();
    if (!empty($item_cls)) {
      $actionData['itemCls'] = $item_cls;
    }
    $group = $this->getGroup();
    if (isset($group)) {
      $actionData['group'] = $group;
    }
    $notop = $this->getNoTop();
    if (isset($notop)) {
      $actionData['notop'] = $notop;
    }
    $auth = $this->getNeedsAuth();
    if (isset($auth)) {
      $actionData['needsAuth'] = $auth;
    }
    $filter = $this->getUseFilter();
    if (!empty($filter)) {
      $actionData['useFilter'] = $filter;
    }

    return $actionData;
  }

//  public function getActionRoleSettings() {
//    return NULL;
//  }

  public function access() {
    $theme = $this->getTheme();
    if (!empty($theme)) {
      switch ($theme) {
        case 'sjqlout': return (SlogxtMin::themeIsJqLout() === TRUE);
        case 'notsjqlout': return (SlogxtMin::themeIsJqLout() === FALSE);
      }
    }
    return TRUE;
  }

  public function getTitle() {
    return (string) $this->get('title');
  }

  public function getDescription() {
    $result =  (string) $this->get('description');
    if (empty($result)) {
      $result = '{Description not set for: ' . $this->getTitle() . '}';
    }
    return $result;
  }

  public function getGroup() {
    return $this->get('group');
  }

  public function getPath() {
    return $this->get('path');
  }

  public function getValue() {
    return $this->get('value');
  }

  public function getCssClass() {
    return $this->get('cssClass');
  }
  
  public function getCssItemId() {
    return $this->get('cssItemId');
  }
  
  public function getCssItemClass() {
    return $this->get('cssItemClass');
  }
  
  public function getXtProvider() {
    return $this->get('xtProvider');
  }

  public function getTheme() {
    return $this->get('theme');
  }

  public function getNoTop() {
    return $this->get('notop');
  }

  public function getNeedsAuth() {
    return $this->get('needsAuth');
  }

  public function getUseFilter() {
    return $this->get('useFilter');
  }

  public function get($key = NULL) {
    if (empty($key)) {
      return $this->configuration;
    } elseif (isset($this->configuration[$key])) {
      return $this->configuration[$key];
    }
  }

  public function setWeight($weight) {
    $this->configuration['weight'] = $weight;
  }

  /**
   * Mark for plugin with _sys toolbar tab (vocabularies)
   * 
   * @return boolean - defaults to FALSE
   */
  public function hasXtSysRootTerm() {
    return FALSE;
  }
  
  public function getApply() {
    return TRUE;
  }
  
}
