<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\XtPluginOptionsBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\Component\Serialization\Json;

/**
 * Defines a base class for slogxt options plugins.
 */
abstract class XtPluginOptionsBase extends XtPluginBase {

  public function getXtOptions() {
    $return = false;
    if ($this->access()) {
      $return = [
          'liTitle' => $this->getTitle(),
          'liDescription' => $this->getDescription(),
          'action' => $this->getAction(),
      ];
    }

    return $return;
  }

  protected function getAction() {
    $action = $this->getXtProvider() . ':' . $this->getPath();
    $value = $this->getValue();
    if (isset($value)) {
      if (is_array($value)) {
        $value = Json::encode($value);
      }
      $action .=  ':' . $value;
    }
    return $action;
  }

}
