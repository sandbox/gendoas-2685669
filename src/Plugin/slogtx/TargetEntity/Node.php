<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogtx\TargetEntity\Node.
 */

namespace Drupal\slogxt\Plugin\slogtx\TargetEntity;

use Drupal\slogtx\Plugin\slogtx\TargetEntityBase;
use Drupal\slogtx\SlogTx;
use Drupal\Core\Entity\EntityInterface;

/**
 * @todo.
 *
 * @SlogtxTargetEntity(
 *   id = "node",
 *   title = @Translation("Node"),
 *   description = @Translation("Section for node aware subjects."),
 *   tbtab_edit_class = "Drupal\slogxt\Form\Tbtab\XtTbtabEditNode",
 *   entity_required = TRUE,
 *   entity_is_fixed = FALSE,
 *   lock_entity_required = TRUE,
 *   provider = "slogxt",
 * )
 */
class Node extends TargetEntityBase {
  
  private static $global_entity = FALSE;

  /**
   * {@inheritdoc}
   */
  public function setDefaultEntity() {
    $this->entity = self::$global_entity;
    return $this->getCurrentEntity();
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDefaultTargetTermName() {
    if (!$entity = $this->getCurrentEntity()) {
      return SlogTx::TARGETTERM_DEFAULT;
    }
    return parent::getDefaultTargetTermName();
  }
  
  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    parent::setEntity($entity);
    self::$global_entity = parent::getCurrentEntity();
    return $this->setDefaultEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function isNodeEntityType() {
    return TRUE;
  }

}
