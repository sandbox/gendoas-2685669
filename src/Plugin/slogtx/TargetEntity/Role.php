<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogtx\TargetEntity\Role.
 */

namespace Drupal\slogxt\Plugin\slogtx\TargetEntity;

use Drupal\slogtx\Plugin\slogtx\TargetEntityBase;
use Drupal\slogxt\XtUserData;

/**
 * @todo.
 *
 * @SlogtxTargetEntity(
 *   id = "user_role",
 *   title = @Translation("Role"),
 *   description = @Translation("Section for role aware subjects."),
 *   tbtab_edit_class = "Drupal\slogxt\Form\Tbtab\XtTbtabEditRole",
 *   entity_required = TRUE,
 *   entity_is_fixed = TRUE,
 *   lock_entity_required = TRUE,
 *   provider = "slogxt",
 * )
 */
class Role extends TargetEntityBase {

  /**
   * {@inheritdoc}
   */
  public function hasStatus() {
//todo::tx:: TxTermBase::hasStatus --- wrong: not by target entity, only by getEntityType()->getKey('status')
// do hasLock() instead
    return TRUE;
  }
  
  /**
   * Override function setDefaultEntity
   * 
   * Set current user's default role as object.
   * 
   * @return EntityInterface
   */
  public function setDefaultEntity() {
    static $current_role;
    if (!$current_role) {
      $current_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    }
    
    $this->setEntity($current_role);
    return $this->getCurrentEntity();
  }
  
  /**
   * {@inheritdoc}
   */
//todo::current:: new::promote - autoprepare::disabled
//              public function doAutoPrepareTbtabRootTerm() {
//                return TRUE;
//              }

}
