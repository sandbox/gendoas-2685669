<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogtx\TargetEntity\User.
 */

namespace Drupal\slogxt\Plugin\slogtx\TargetEntity;

use Drupal\slogtx\Plugin\slogtx\TargetEntityBase;
use Drupal\user\Entity\User as UserEntity;

/**
 * @todo.
 *
 * @SlogtxTargetEntity(
 *   id = "user",
 *   title = @Translation("User"),
 *   description = @Translation("Section for user aware subjects."),
 *   tbtab_edit_class = "Drupal\slogxt\Form\Tbtab\XtTbtabEditUser",
 *   entity_required = TRUE,
 *   entity_is_fixed = TRUE,
 *   lock_entity_required = TRUE,
 *   provider = "slogxt",
 * )
 */
class User extends TargetEntityBase {

  /**
   * {@inheritdoc}
   */
  public function isValidEntityId($entity_id) {
    return ($entity_id == 0) || parent::isValidEntityId($entity_id);
  }

  /**
   * Override function setDefaultEntity
   * 
   * Set the current user as the current entity.
   * 
   * @return EntityInterface
   */
  public function setDefaultEntity() {
    static $current_user;
    if (!$current_user) {
      $current_user = UserEntity::load(\Drupal::currentUser()->id());
    }
    
    $this->setEntity($current_user);
    return $this->getCurrentEntity();
  }
  
  /**
   * {@inheritdoc}
   */
//todo::current:: new::promote - autoprepare::disabled
//            public function doAutoPrepareTbtabRootTerm() {
//              return TRUE;
//            }

}
