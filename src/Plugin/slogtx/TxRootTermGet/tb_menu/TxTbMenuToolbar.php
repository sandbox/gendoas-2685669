<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogtx\TxRootTermGet\tb_menu\TxTbMenuToolbar.
 */

namespace Drupal\slogxt\Plugin\slogtx\TxRootTermGet\tb_menu;

use Drupal\slogtx\Plugin\slogtx\TxTbMenuRootTermGet;

/**
 * @TxTbMenuRootTermGet(
 *   id = "txmenutoolbar",
 *   deriver = "Drupal\slogxt\Plugin\slogtx\Derivative\TxTbMenuToolbar"
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxTbMenuToolbar extends TxTbMenuRootTermGet {
  
}
