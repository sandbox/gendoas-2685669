<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogtx\TxRootTermGet\sys_sys\TxArchive.
 */

namespace Drupal\slogxt\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "archive",
 *   deriver = "Drupal\slogxt\Plugin\slogtx\Derivative\TxSysMenuToolbar"
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxArchive extends TxSysSysRootTermGet {
  
}
