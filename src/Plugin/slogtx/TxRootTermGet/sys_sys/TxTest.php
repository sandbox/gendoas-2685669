<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogtx\TxRootTermGet\sys_sys\TxTest.
 */

namespace Drupal\slogxt\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "test",
 *   deriver = "Drupal\slogxt\Plugin\slogtx\Derivative\TxSysMenuToolbar"
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxTest extends TxSysSysRootTermGet {
  
}
