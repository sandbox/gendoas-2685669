<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogtx\TxRootTermGet\sys_sys\TxTrash.
 */

namespace Drupal\slogxt\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "trash",
 *   deriver = "Drupal\slogxt\Plugin\slogtx\Derivative\TxSysMenuToolbar"
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxTrash extends TxSysSysRootTermGet {
  
}
