<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogtx\Derivative\TxTbMenuToolbar.
 */

namespace Drupal\slogxt\Plugin\slogtx\Derivative;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Provides ...
 */
class TxTbMenuToolbar extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $base_plugin_id = $base_plugin_definition['id'];
    if ($base_plugin_id !== SlogTx::TXMENUTOOLBAR) {
      $message = "Plugin id not allowed: $base_plugin_id";
      throw new \Exception($message);
    }

    $max_toolbar_tabs = SlogXt::getMaxToolbarTabs();
    foreach (SlogTx::getMenuToolbars() as $toolbar_id => $toolbar) {
//todo::current:: TxRootTermGetPlugin - getDerivativeDefinitions
      if (!$toolbar->isUnderscoreToolbar()) {
        $tabbase = $toolbar_id[0];
        if ($toolbar_id === 'help') {
          $tabbase = 'he';
        }

        for ($idx = 0; $idx < $max_toolbar_tabs; $idx++) {
          $tbtab_idx = $idx + 1;
          $toolbartab = $tabbase . $tbtab_idx;
          $vid = SlogTx::getVocabularyIdFromParts([$toolbar_id, $toolbartab]);
          $icon_id = $toolbar_id . $tbtab_idx;
          $xx = 0;

          $new_data = [
            'provider' => $base_plugin_definition['provider'],
            'icon_id' => $icon_id,
            'weight' => $tbtab_idx,
          ];
          $vocabulary = SlogTx::ensureVocabulary($vid, $new_data, ['delete']);
          if (!$vocabulary) {
            $message = "Slog vocabulary not prepared: $vid";
            throw new \Exception($message);
          }

          $this->derivatives[$vid] = [
            'toolbar_id' => $toolbar_id,
            'vocabulary_id' => $vid,
            'icon_id' => $icon_id,
            'weight' => $tbtab_idx,
          ];
          $this->derivatives[$vid] += $base_plugin_definition;
        }
      }
    }

    return $this->derivatives;
  }

}
