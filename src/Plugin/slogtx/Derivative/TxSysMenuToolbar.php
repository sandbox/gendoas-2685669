<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogtx\Derivative\TxSysMenuToolbar.
 */

namespace Drupal\slogxt\Plugin\slogtx\Derivative;

use Drupal\slogtx\SlogTx;
use Drupal\Component\Plugin\Derivative\DeriverBase;
//use \Drupal\node\Entity\NodeType;

/**
 * Provides NodeContent plugin definitions for all node types.
 */
class TxSysMenuToolbar extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    
    // getMenuToolbars
//    $admin_label = SlogTb::ADMIN_TB_CONTENT;
//    $delimiter = SlogTb::ADMIN_TB_SEPARATOR;
    
    
    
    $xx = 0;
    $xx_mtbs = SlogTx::getMenuToolbars();
    
    $base_plugin_id = $base_plugin_definition['id'];
    foreach (SlogTx::getMenuToolbars() as $toolbar_id => $toolbar) {
//todo::current:: TxRootTermGetPlugin - getDerivativeDefinitions

      $vid = $toolbar->getSysToolbarVid($base_plugin_id);
      $tb_name = $toolbar->label();
      $this->derivatives[$vid] = [
        'toolbar_id' => $toolbar_id,
        'vocabulary_id' => $vid,
      ];
      $this->derivatives[$vid] += $base_plugin_definition;
      $xx = 0;



//      $tb_name = $toolbar->label();
//      $this->derivatives[$toolbar_id] = [
//        'toolbar_id' => $toolbar_id,
//        'admin_label' => "{$admin_label}{$delimiter}{$tb_name}",
//      ];
//      $this->derivatives[$toolbar_id] += $base_plugin_definition;
    }
    
    
    
    

//                $node_types = NodeType::loadMultiple();
//
//                foreach ($node_types as $type_id => $node_type) {
//                  $label = $node_type->label();
//                  $this->derivatives[$type_id] = [
//                      'bundle' => $type_id,
//                      'admin_label' => "Xtsi Node Type: " . $node_type->label(),
//                  ];
//                  $this->derivatives[$type_id] += $base_plugin_definition;
//                }

    return $this->derivatives;
  }

}
