<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\SxtActionPluginSysRoottermBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Event\SlogxtEvents;
use Drupal\slogxt\Event\SlogxtGenericEvent;
use Drupal\Core\Entity\EntityInterface;
use Drupal\slogxt\Plugin\SxtActionPluginBase;
use Drupal\slogtx\Entity\TxTermBase;

abstract class SxtActionPluginSysRoottermBase extends SxtActionPluginBase {

  /**
   * Return a root term instance with current or given entity (if any).
   * 
   * @param string $target_type
   *  Plugin id of \Drupal\slogtx\Plugin\slogtx\TargetEntityInterface instance
   * @param EntityInterface $entity optional
   *  A valid entity for the plugin (plugin's id equals to entity's type id, e.g. user, user_role, node...)
   * @return \Drupal\slogtx\Entity\RootTerm
   */
  abstract public function getSysRootTerm($target_type, EntityInterface $entity = NULL);

  public function getRootTermIds() {
    $rootterm_ids = [];
    if (!empty($this->root_terms)) {
      foreach ($this->root_terms as $key => $root_term) {
        if ($root_term instanceof TxTermBase && $root_term->isRootTerm()) {
          $rootterm_ids[$key] = $root_term->id();
        }
        elseif (is_string($root_term)) {
          $rootterm_ids[$key] = $root_term;
        }
      }
    }
    return $rootterm_ids;
  }

  /**
   * Mark for plugin with _sys toolbar tab (vocabularies)
   * 
   * @return boolean - defaults to TRUE
   */
  public function hasXtSysRootTerm() {
    return TRUE;
  }

  public function getXtSysRootTerm($base_entity_id) {
    $result = ['doTbMenuSelect' => TRUE];
    // this is for toolbar id as second parameter only, override function for others (see Bmstorage)
    list($target, $toolbar_id, $entity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
    $toolbar = SlogTx::getToolbar($toolbar_id);
    if (empty($toolbar)) {
      $message = t('Toolbar not found: @tbid.', ['@tbid' => $toolbar_id]);
      throw new \LogicException($message);
    }

    $targetentity_id = $toolbar->getEnforceTargetEntity();
    if (empty($targetentity_id)) {
      $message = t('Toolbar does not enforce target entity type: @tbid.', ['@tbid' => $toolbar_id]);
      throw new \LogicException($message);
    }

    if (!$targetentity_plugin = SlogTx::getTargetEntityPlugin($targetentity_id)) {
      $message = t('Not found target entity plugin: @pid.', ['@pid' => $targetentity_id]);
      throw new \LogicException($message);
    }

    $entity = NULL;
    if (!empty($entity_id) && $targetentity_plugin->isEntitySettable()) {
      if (!$entity = \Drupal::entityTypeManager()->getStorage($targetentity_id)->load($entity_id)) {
        $args = [
          '@entitytype' => $targetentity_id,
          '@entityid' => $entity_id,
        ];
        $message = t('Entity not found: @entitytype / @entityid', $args);
        throw new \LogicException($message);
      }
    }

    $retrieve_rootterm = TRUE;
    if ($targetentity_plugin->isEntitySettable() && empty($entity)) {      
      if (empty($entity_id)) {
        $retrieve_rootterm = FALSE;
        $result += [
          'type' => 'client_resolve',
          'value' => [
            'resolve' => SlogXt::getJsDataResolvePathNode($base_entity_id),
            'args' => [
              'xtTitle' => t('xxxxxxxxxxx'),
              'xtInfo' => t('Select by clicking a node title .....'),
            ],
          ],
        ];
      }
    }

    if ($retrieve_rootterm) {
      if ($rootterm = $this->getSysRootTerm($toolbar_id, $entity)) {
        $result += [
          'type' => 'rootterm',
          'value' => $rootterm,
        ];
      }
      else {
        $message = t('Root term not found for target type @type.', ['@type' => $targetentity_id]);
        throw new \LogicException($message);
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getApply() {
    $can_apply = [];
    $event = new SlogxtGenericEvent([
      'plugin_id' => $this->getPluginId(),
      'can_apply' => &$can_apply,
        ]);
    \Drupal::service('event_dispatcher')
        ->dispatch(SlogxtEvents::ACTION_GET_APPLY_ALTER, $event);

    return $can_apply;
    
    
  }

}
