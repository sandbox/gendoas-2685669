<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\SxtActionPluginTbLineBase.
 */

namespace Drupal\slogxt\Plugin;

class SxtActionPluginTbLineBase extends SxtActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getActionData() {
    $value = $this->getValue();
    $actionData =  [
        'path' => $this->getPath(),
//        'value' => $this->getValue(),
        'class' => $this->getCssClass(),
        'group' => $this->getGroup(),
        'provider' => $this->getXtProvider(),
    ];
    if (isset($value)) {
      $actionData['value'] = $value;
    }
    return $actionData;
  }


}
