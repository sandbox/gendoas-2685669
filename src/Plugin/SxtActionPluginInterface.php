<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\SxtActionPluginInterface.
 */

namespace Drupal\slogxt\Plugin;

/**
 * Provides an interface for an Slogxt Action plugin.
 */
interface SxtActionPluginInterface {

  public function access();
  
  public function getActionData();
  
  public function getApply();
  
//  public function getActionRoleSettings();

}
