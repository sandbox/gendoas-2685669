<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\Block\XtMainDropbuttonBlock.
 */

namespace Drupal\slogxt\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Cache\Cache;

/**
 * Provides a block for toolbar content target.
 *
 * @Block(
 *   id = "slogxt_main_dropbutton",
 *   admin_label = @Translation("SlogXt main dropbutton"),
 *   category = "SlogXt",
 * )
 */
class XtMainDropbuttonBlock extends BlockBase {

  /**
   * Retrieve actions once only
   *
   * @var array 
   */
  protected $actions;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = ['#markup' => ''];
    $build['#attached']['drupalSettings']['slogxt']['mainActions'] = $this->getMainActions();
    $build['#attached']['library'][] = 'slogxt/slogxt.dropbutton';
    $actions = &$build['#attached']['drupalSettings']['slogxt']['mainActions'];

    // move attached to build for all actions
    foreach ($actions as & $action) {
      if (isset($action['#attached'])) {
        $build['#attached'] = array_merge_recursive($build['#attached'], $action['#attached']);
        unset($action['#attached']);
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['slogxt_main_dropbutton']);
  }

  public function getMainActions() {
    if (!isset($this->actions)) {
      $this->actions = SlogXt::pluginManager('action')
          ->getActionsData('main_dropbutton', FALSE, TRUE);
    }

    return $this->actions;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  public function getDummyActions() {
    $href = SlogXt::getHRefClientside('dummy');
    return [
      ['label' => 'Do', 'href' => $href, 'class' => 'icon-dummy'],
      ['label' => 'Some', 'href' => $href, 'class' => 'icon-dummy'],
      ['label' => 'More', 'href' => $href, 'class' => 'icon-dummy'],
      ['label' => 'Actions', 'href' => $href, 'class' => 'icon-dummy'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE] + parent::defaultConfiguration();
  }

}
