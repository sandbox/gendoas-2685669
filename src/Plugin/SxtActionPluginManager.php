<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\SxtActionPluginManager.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages slogxt actions used for dropbuttons.
 */
class SxtActionPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {
//todo::maybe::feature - with conditions: see \Drupal\user\Plugin\Condition\UserRole

  protected $cache_action_data = [];
  
  /**
   * Constructs a SxtActionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/slogxt/SxtAction', $namespaces, $module_handler, //
            'Drupal\slogxt\Plugin\SxtActionPluginInterface', //
            'Drupal\slogxt\Annotation\SlogxtAction');
    $this->setCacheBackend($cache_backend, 'slogxt_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'slogxt_dummy';
  }

  /**
   * Return all data for all actions available for a manu.
   * 
   * @param string $menu
   *  Menu identifier, e.g. 'main_dropbutton'
   * @param boolean $add_groups
   *  Add an array with groups.
   * @return array
   *  Each item an array with data for building an action
   */
  public function getActionsData($menu, $add_groups = FALSE, $add_dummy = FALSE) {
    $data_idx = $add_groups ? 1 : 0;
    $data_idx += $add_dummy ? 2 : 0;
    $data_idx = "{$menu}_{$data_idx}";
    if (empty($this->cache_action_data[$data_idx])) {
      $actions_data = [];
      $plugins = [];
      $delta = 0;
      $definitions = $this->getDefinitions();
      foreach ($definitions as $definition) {
        if ($definition['status'] && $definition['menu'] === $menu) {
          $plugin = $this->createInstance($definition['id'], $definition);
          if ($plugin->access()) {
            $weight = (integer) $plugin->get('weight');
            $idx = sprintf("%05d-%04d", $weight + 50000, ++$delta);
            $plugins[$idx] = $plugin;
          }
        }
      }
      if ($add_dummy) {
        reset($definitions);
        foreach ($definitions as $definition) {
          if ($definition['status'] && $definition['menu'] === 'xt_dummy') {
            $plugin = $this->createInstance($definition['id'], $definition);
            if ($plugin->access()) {
              $weight = (integer) $plugin->get('weight');
              $idx = sprintf("%05d-%04d", $weight + 50000, ++$delta);
              $plugins[$idx] = $plugin;
            }
          }
        }
      }

      if (!empty($plugins)) {
        // sort by weight
        ksort($plugins);

        // add actions data
        if ($add_groups) {
          $groups = [];
          foreach ($plugins as $plugin) {
            $group = $plugin->getGroup();
            if (!isset($groups[$group])) {
              $groups[$group] = $group;
            }
            $actions_data[] = $plugin->getActionData();
          }
          $actions_data['groups'] = array_keys($groups);
        } else {
          foreach ($plugins as $plugin) {
            $actions_data[] = $plugin->getActionData();
          }
        }
      }
      $this->cache_action_data[$data_idx] = $actions_data;
    }   
    
    return $this->cache_action_data[$data_idx];
  }
  
  
  /**
   * Return data for all role dependent actions.
   * 
   * Used after role is changed by ajax.
   * 
   * @return array
   *  Array of data for role dependent actions
   */
//          public function getActionsRoleSettings() {
//            //todo::verify::is it used ???
//            // looks like it is not used since the page is reloaded 
//            // after default role has been changed
//            $settings = [];
//            $definitions = $this->getDefinitions();
//            foreach ($definitions as $definition) {
//              if ($definition['status'] && $definition['dependsOnRole']) {
//                $plugin = $this->createInstance($definition['id'], $definition);
//                $as = $plugin->getActionRoleSettings();
//                if (is_array($as) && !empty($as['module']) && !empty($as['key'])) {
//                  $settings[$as['module']][$as['key']] = $as['value'];
//                }
//              }
//            }
//
//            return $settings;
//          }
  
}
