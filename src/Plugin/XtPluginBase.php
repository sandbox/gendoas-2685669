<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\XtPluginBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class for slogxt plugins.
 */
abstract class XtPluginBase extends PluginBase implements ContainerFactoryPluginInterface {

  protected $baseEntityId;
  protected $account;
  protected $route;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration += $this->pluginDefinition;
    $this->baseEntityId = isset($configuration['baseEntityId']) //
        ? $configuration['baseEntityId'] : false;
    $this->account = \Drupal::currentUser()->getAccount();
    if (!empty($plugin_definition['route_name'])) {
      $this->route = $route_provider->getRouteByName($plugin_definition['route_name']);
      $path = $this->route->getPath();
      if (isset($this->baseEntityId) && $this->baseEntityId > 0) {
        $path = str_replace('{base_entity_id}', $this->baseEntityId, $path);
      }
      $this->basePath = $path;
    }
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('router.route_provider'));
  }

  protected function preparedPath() {
    return $this->basePath;
//    return ($this->basePath) ? preg_replace('/\/\{.*\}/', '', $this->basePath) : false;
  }

  public function getTitle() {
    return (string) $this->get('title');
  }

  public function getDescription() {
    return (string) $this->get('description');
  }

  public function getPath() {
    return $this->get('path');
  }

  public function getValue() {
    return $this->get('value');
  }

  public function getXtProvider() {
    return $this->get('xtProvider');
  }

  public function get($key = NULL) {
    if (empty($key)) {
      return $this->configuration;
    }
    elseif (isset($this->configuration[$key])) {
      return $this->configuration[$key];
    }
  }

  protected function access() {
    return true;
  }

}
