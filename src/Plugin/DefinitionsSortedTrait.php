<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\DefinitionsSortedTrait.
 */

namespace Drupal\slogxt\Plugin;

/**
 * A Trait for Views broken handlers.
 */
trait DefinitionsSortedTrait {

  public function getDefinitionsSorted($desc = false) {
    $definitions = $this->getDefinitions();
    uasort($definitions, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    if ($desc) {
      $definitions = array_reverse($definitions, true);
    }
    return $definitions;
  }

}
