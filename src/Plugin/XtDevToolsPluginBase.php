<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\XtDevToolsPluginBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\sloggen\SlogGen;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * 
 */
class XtDevToolsPluginBase extends XtPluginEditBase {

  public function access() {
    if (\Drupal::currentUser()->id() == 1 && \Drupal::moduleHandler()->moduleExists('sloggen')) {
      $config = \Drupal::config('sloggen.settings');
      $integrate = (boolean) (SlogGen::configValue($config, 'sloggen_integrate', FALSE));
      return $integrate;
    }

    return FALSE;
  }

}
