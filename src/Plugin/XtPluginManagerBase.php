<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\XtPluginManagerBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Base plugin manager for some types.
 * 
 * @ingroup slogxt_plugins_manager
 */
class XtPluginManagerBase extends DefaultPluginManager {

  use DefinitionsSortedTrait;

  /**
   * Constructs a ViewsPluginManager object.
   *
   * @param string $type
   *   The plugin type, for example options, edit, content.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $plugin_definition_annotation_name = 'Drupal\slogxt\Annotation\Slogxt' . Container::camelize($type);
    $plugin_interface = null;
    parent::__construct("Plugin/slogxt/$type", $namespaces, $module_handler, //
            $plugin_interface, //
            $plugin_definition_annotation_name);
    $this->setCacheBackend($cache_backend, "slogxt:{$type}_plugins");
  }

}
