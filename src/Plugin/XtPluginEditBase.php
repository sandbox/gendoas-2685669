<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\XtPluginEditBase.
 */

namespace Drupal\slogxt\Plugin;

use Drupal\slogxt\XtUserData;
use Drupal\Core\Url;

/**
 * Defines a base class for slogxt edit plugins.
 */
abstract class XtPluginEditBase extends XtPluginBase {
  
  public static $defaultRole = FALSE;
  
  protected function getUserDefaultRole() {
    if (!self::$defaultRole) {
      self::$defaultRole = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    }
    return self::$defaultRole;
  }

  public function getPermissions() {
    return $this->get('permissions');
  }
  
  protected function getResolveBaseCommand() {
    return $this->get('resolve_base_command');
  }

  protected function getResolveNextCommand() {
    return $this->get('resolve_next_command');
  }

  protected function getResolveLinkCommand() {
    return $this->get('resolve_link_command');
  }

  protected function getResolveArgs() {
    return $this->get('resolve_args');
  }

  protected function getResolveData() {
    $data = [];
    if (!$this->baseEntityId) {
      $data['{base_entity_id}'] = $this->getResolveBaseCommand();
    }
    if ($resolveNext = $this->getResolveNextCommand()) {
      $data['{next_entity_id}'] = $resolveNext;
    }
    if ($resolveLink = $this->getResolveLinkCommand()) {
      $data['{link_entity_id}'] = $resolveLink;
    }

    return $data;
  }

  public function getActionData() {
    $data = false;
    if ($this->access() && $path = $this->preparedPath()) {
      $resolve_data = $this->getResolveData();
      if (!empty($resolve_data)) {
        $path = [
          'path' => $path,
          'resolve' => $resolve_data,
        ];
        if ($resolveArgs = $this->getResolveArgs()) {
          $path['resolveArgs'] = $resolveArgs;
        }
      }
      $data = [
        'actionId' => $this->getPluginId(),
        'liTitle' => $this->getTitle(),
        'liDescription' => $this->getDescription(),
        'skipable' => (boolean) $this->get('skipable'),
        'path' => $path,
      ];
    }

    //todo::feature:: trigger event for data change

    return $data;
  }

  protected function getPathFromRoute($route_name, $route_parameters) {
    $url = Url::fromRoute($route_name, $route_parameters);
    return $url->getInternalPath();
  }

}
