<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\XtPluginManagerEdit.
 */

namespace Drupal\slogxt\Plugin;

/**
 * Plugin manager for slogxt edit plugins.
 *
 * @ingroup slogxt_plugins_manager
 */
class XtPluginManagerEdit extends XtPluginManagerBase {

  public function getActionsData($bundle, $base_entity_id) {
    $definitions = $this->getDefinitionsSorted();
    $actionsData = [];
    $configuration = ['baseEntityId' => $base_entity_id];
    foreach ($definitions as $plugin_id => $definition) {
      if ($definition['bundle'] === $bundle) {
        $plugin = $this->createInstance($plugin_id, $configuration);
        if ($actionData = $plugin->getActionData()) {
          $actionsData[] = $actionData;
        }
      }
    }

    return $actionsData;
  }

  public function getForRolePermissions($bundle) {
    $permissions = [];
    $definitions = $this->getDefinitions();
    foreach ($definitions as $definition) {
      if ($definition['bundle'] === $bundle) {
        $perms = $definition['permissions'];
        if (!empty($perms) && !empty($perms['role'])) {
          $permissions[] = $perms['role'];
        }
      }
    }

    return $permissions;
  }

}
