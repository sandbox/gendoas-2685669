<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\XtPluginManagerOptions.
 */

namespace Drupal\slogxt\Plugin;

/**
 * Plugin manager for slogxt options plugins.
 *
 * @ingroup slogxt_plugins_manager
 */
class XtPluginManagerOptions extends XtPluginManagerBase {

  public function getXtOptions($bundle, $base_entity_id) {
    $definitions = $this->getDefinitionsSorted();
    $options = [];
    $configuration = ['baseEntityId' => $base_entity_id];
    foreach ($definitions as $plugin_id => $definition) {
      if ($definition['bundle'] === $bundle) {
        $plugin = $this->createInstance($plugin_id, $configuration);
        if ($option = $plugin->getXtOptions()) {
          $options[] = $option;
        }
      }
    }

    return $options;
  }

}
