<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\XtPluginManagerContent.
 */

namespace Drupal\slogxt\Plugin;

/**
 * Plugin manager for slogxt content plugins.
 *
 * @ingroup slogxt_plugins_manager
 */
class XtPluginManagerContent extends XtPluginManagerBase {
  
  protected $definitionByRouteName;

  public function getDefinitionByRouteName($route_name) {
    if (!$this->definitionByRouteName) {
      $this->definitionByRouteName = [];
      $definitions = $this->getDefinitions();
      foreach ($this->getDefinitions() as $definition) {
        $this->definitionByRouteName[$definition['route']] = $definition;
      }
    }

    return $this->definitionByRouteName[$route_name];
  }

}
