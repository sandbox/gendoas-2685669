<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\anonymous\Appearance.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\anonymous;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_anonymous_appearance",
 *   bundle = "anonymous",
 *   title = @Translation("Colors"),
 *   description = @Translation("Change the colors of toolbars and dialogboxes."),
 *   route_name = "slogxt.profile.appearance",
 *   skipable = false,
 *   weight = 22
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Appearance extends XtPluginEditBase {

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->basePath = str_replace('{base_entity_id}', 0, $this->basePath);
  }

  protected function getResolveData() {
    return FALSE;
  }

}
