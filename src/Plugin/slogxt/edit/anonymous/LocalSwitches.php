<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\anonymous\LocalSwitches.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\anonymous;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_anonymous_local_switches",
 *   bundle = "anonymous",
 *   title = @Translation("Switches"),
 *   description = @Translation("Some mor switches for appearance."),
 *   route_name = "slogxt.profile.local_switches",
 *   skipable = false,
 *   weight = 888
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class LocalSwitches extends XtPluginEditBase {

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->basePath = str_replace('{base_entity_id}', 0, $this->basePath);
  }
  
  protected function getResolveData() {
    return FALSE;
  }

}
