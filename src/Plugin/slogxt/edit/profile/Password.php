<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\Password.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_password",
 *   bundle = "profilesettings",
 *   title = @Translation("Change password"),
 *   description = @Translation("Change your password."),
 *   route_name = "slogxt.profile.password",
 *   skipable = false,
 *   weight = 20
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Password extends XtPluginEditBase {

}
