<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\LocalSwitches.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_local_switches",
 *   bundle = "profilesettings",
 *   title = @Translation("Local switches"),
 *   description = @Translation("Switches, locally stored for the current device."),
 *   route_name = "slogxt.profile.local_switches",
 *   skipable = false,
 *   weight = 888
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class LocalSwitches extends XtPluginEditBase {

  protected function access() {
    return SlogXt::isAllowedAppearanceChange();
  }

}
