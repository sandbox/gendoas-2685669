<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\ProfileAdminReset.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_admin_reset",
 *   bundle = "profile",
 *   title = @Translation("Reset"),
 *   description = @Translation("Administrative tasks: some resettings like cache, ..."),
 *   route_name = "slogxt.profile.admin_reset",
 *   skipable = false,
 *   weight = 999
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class ProfileAdminReset extends XtPluginEditBase {

  public function access() {
    return SlogXt::getUserHasAdminRole();
  }

}
