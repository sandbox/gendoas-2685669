<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\EMail.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_email",
 *   bundle = "profilesettings",
 *   title = @Translation("Change email"),
 *   description = @Translation("Change your email address."),
 *   route_name = "slogxt.profile.email",
 *   skipable = false,
 *   weight = 21
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class EMail extends XtPluginEditBase {

}
