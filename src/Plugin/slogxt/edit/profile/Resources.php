<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\Resources.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_resources",
 *   bundle = "profile",
 *   title = @Translation("Resources"),
 *   description = @Translation("This page gives you an overview of your resources."),
 *   route_name = "slogxt.profile.resources",
 *   skipable = false,
 *   weight = 5
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Resources extends XtPluginEditBase {

}
