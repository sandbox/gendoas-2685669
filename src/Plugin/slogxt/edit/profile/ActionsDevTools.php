<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\ActionsDevTools.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtDevToolsPluginBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_main_devtools",
 *   bundle = "profile",
 *   title = @Translation("dev: Tools"),
 *   description = @Translation("Tools for development purpose."),
 *   route_name = "slogxt.profile.devtools_select",
 *   skipable = false,
 *   weight = 99900
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class ActionsDevTools extends XtDevToolsPluginBase {

}
