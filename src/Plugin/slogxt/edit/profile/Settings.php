<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\Settings.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_settings",
 *   bundle = "profile",
 *   title = @Translation("Settings"),
 *   description = @Translation("Edit settings like password, e-mail, appearance, ..."),
 *   route_name = "slogxt.profile.settings_select",
 *   skipable = false,
 *   weight = 20
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Settings extends XtPluginEditBase {

}
