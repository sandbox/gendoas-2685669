<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\DefaultRoleChange.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_default_role_change",
 *   bundle = "profile",
 *   title = @Translation("Default role"),
 *   description = @Translation("You may participate in different areas by roles, but only one is shown at a time."),
 *   route_name = "slogxt.profile.default_role_change",
 *   skipable = false,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class DefaultRoleChange extends XtPluginEditBase {

}
