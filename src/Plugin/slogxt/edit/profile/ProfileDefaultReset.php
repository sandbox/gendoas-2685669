<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\ProfileDefaultReset.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_default_reset",
 *   bundle = "profile",
 *   title = @Translation("Repair"),
 *   description = @Translation("All cached data will be removed, including the current status."),
 *   route_name = "slogxt.profile.default_reset",
 *   skipable = false,
 *   weight = 999
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class ProfileDefaultReset extends XtPluginEditBase {

  public function access() {
    return !SlogXt::getUserHasAdminRole();
  }

}
