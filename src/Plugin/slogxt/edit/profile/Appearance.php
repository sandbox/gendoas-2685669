<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\Appearance.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_appearance",
 *   bundle = "profilesettings",
 *   title = @Translation("Change appearance"),
 *   description = @Translation("Change the appearance for all your devices."),
 *   route_name = "slogxt.profile.appearance",
 *   skipable = false,
 *   weight = 22
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Appearance extends XtPluginEditBase {

  protected function access() {
    return SlogXt::isAllowedAppearanceChange();
  }

}
