<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\edit\profile\Permissions.
 */

namespace Drupal\slogxt\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_profile_permissions",
 *   bundle = "profile",
 *   title = @Translation("Permissions"),
 *   description = @Translation("This page gives you an overview of your permissions."),
 *   route_name = "slogxt.profile.permissions",
 *   skipable = false,
 *   weight = 10
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Permissions extends XtPluginEditBase {

}
