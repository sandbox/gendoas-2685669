<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\TbSys\DoNavigateXXXX.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\TbSys;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtActionXXXX(
 *   id = "slogxt_tbsys_donavigate",
 *   title = @Translation("Navigate"),
 *   menu = "xt_dialog_tbsys",
 *   path = "donavigate",
 *   cssClass = "icon-navigate",
 *   xtProvider = "slogxt",
 *   notop = FALSE,
 *   weight = -9999
 * )
 */
class DoNavigateXXXX extends SxtActionPluginBase {

}
