<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Profile.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_profile",
 *   title = @Translation("Profile"),
 *   menu = "main_dropbutton",
 *   path = "profile",
 *   cssClass = "icon-profile",
 *   cssItemId = "slogxtmaindropbutton-li-profile",
 *   xtProvider = "slogxt",
 *   weight = -910
 * )
 */
class Profile extends SxtActionPluginBase {

  public function access() {
    return !\Drupal::currentUser()->isAnonymous();
  }

  public function getTitle() {
    $account = \Drupal::currentUser();
    if (!$account->isAnonymous()) {
      return $account->getAccountName();
    }
    
    return parent::getTitle();
  }
  
}
