<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Repair.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_repair",
 *   title = @Translation("Repair"),
 *   menu = "main_dropbutton",
 *   path = "repair",
 *   cssClass = "icon-repair",
 *   xtProvider = "slogxt",
 *   notop = TRUE,
 *   weight = 9988
 * )
 */
class Repair extends SxtActionPluginBase {

  public function access() {
    return \Drupal::currentUser()->isAnonymous();
  }

}
