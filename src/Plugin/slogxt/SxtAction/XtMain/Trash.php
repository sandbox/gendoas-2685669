<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Trash.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginSysRoottermBase;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Entity\EntityInterface;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_trash",
 *   title = @Translation("Trash"),
 *   description = @Translation("The trash bin for subjects managed in different areas."),
 *   menu = "main_dropbutton",
 *   path = "trash",
 *   target_select_form = "\Drupal\slogxt\Form\ToolbarsForSysTabsSelectForm",
 *   cssClass = "icon-trash",
 *   xtProvider = "slogxt",
 *   weight = 1999
 * )
 */
class Trash extends SxtActionPluginSysRoottermBase {

  /**
   * {@inheritdoc}
   * 
   * $target_type: is not a type but a toolbar id.
   * 
   */
  public function getSysRootTerm($target_type, EntityInterface $entity = NULL) {
    return SlogXt::getTrashRootTerm($target_type, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getActionData() {
    $actions = parent::getActionData();
    $attach_data = ['mainActionsData' => ['trash' => [
          'canApply' => $this->getApply(),
          'rootterm_ids' => $this->getRootTermIds(),
          'liTitle' => $this->getTitle(),
          'liDescription' => $this->getDescription(),
    ]]];
    $actions['#attached']['drupalSettings']['slogxt'] = $attach_data;
    return $actions;
  }

  public function access() {
    if ($hasAccess = parent::access() && \Drupal::currentUser()->isAuthenticated()) {
      $this->root_terms = SlogXt::getTrashRootTerms();
      $hasAccess = (!empty($this->root_terms));
    }
    return $hasAccess;
  }

}
