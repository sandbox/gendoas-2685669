<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Archive.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginSysRoottermBase;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Entity\EntityInterface;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_archive",
 *   title = @Translation("Archive"),
 *   description = @Translation("The archive for subjects managed in different areas."),
 *   menu = "main_dropbutton",
 *   path = "archive",
 *   target_select_form = "\Drupal\slogxt\Form\ToolbarsForSysTabsSelectForm",
 *   cssClass = "icon-archive",
 *   xtProvider = "slogxt",
 *   weight = 1998
 * )
 */
class Archive extends SxtActionPluginSysRoottermBase {

  /**
   * {@inheritdoc}
   * 
   * $target_type: is not a type but a toolbar id.
   * 
   */
  public function getSysRootTerm($target_type, EntityInterface $entity = NULL) {
    return SlogXt::getArchiveRootTerm($target_type, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getActionData() {
    $actions = parent::getActionData();
    $attach_data = ['mainActionsData' => ['archive' => [
          'canApply' => $this->getApply(),
          'rootterm_ids' => $this->getRootTermIds(),
          'liTitle' => $this->getTitle(),
          'liDescription' => $this->getDescription(),
    ]]];
    $actions['#attached']['drupalSettings']['slogxt'] = $attach_data;
    return $actions;
  }

  public function access() {
    if ($hasAccess = parent::access() && \Drupal::currentUser()->isAuthenticated()) {
      $this->root_terms = SlogXt::getArchiveRootTerms();
      $hasAccess = (!empty($this->root_terms));
    }
    return $hasAccess;
  }

}
