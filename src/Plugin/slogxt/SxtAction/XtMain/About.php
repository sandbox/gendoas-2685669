<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\About.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_about",
 *   title = @Translation("About"),
 *   menu = "main_dropbutton",
 *   path = "about",
 *   cssClass = "icon-about",
 *   xtProvider = "slogxt",
 *   notop = TRUE,
 *   weight = 9999
 * )
 */
class About extends SxtActionPluginBase {

}
