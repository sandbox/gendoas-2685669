<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\AnonymousAppearance.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_appearance",
 *   title = @Translation("Appearance"),
 *   menu = "main_dropbutton",
 *   path = "profile",
 *   cssClass = "icon-appearance",
 *   xtProvider = "slogxt",
 *   notop = TRUE,
 *   weight = 9900
 * )
 */
class AnonymousAppearance extends SxtActionPluginBase {

  public function access() {
    return (\Drupal::currentUser()->isAnonymous() && SlogXt::isAllowedAppearanceChange());
  }

}
