<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Login.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_login",
 *   title = @Translation("Log in"),
 *   menu = "main_dropbutton",
 *   path = "login",
 *   cssClass = "icon-login",
 *   xtProvider = "slogxt",
 *   weight = -999
 * )
 */
class Login extends SxtActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function access() {
    return \Drupal::currentUser()->isAnonymous();
  }
  
}
