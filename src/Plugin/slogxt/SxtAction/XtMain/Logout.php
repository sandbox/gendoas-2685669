<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Logout.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;
use Drupal\slogxt\SlogXt;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_logout",
 *   title = @Translation("Log out"),
 *   menu = "main_dropbutton",
 *   path = "logout",
 *   cssClass = "icon-logout",
 *   xtProvider = "slogxt",
 *   notop = TRUE,
 *   weight = 9997
 * )
 */
class Logout extends SxtActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function access() {
    return !\Drupal::currentUser()->isAnonymous();
  }

}
