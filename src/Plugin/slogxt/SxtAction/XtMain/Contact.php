<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Contact.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_contact",
 *   title = @Translation("Contact"),
 *   menu = "main_dropbutton",
 *   path = "contact",
 *   cssClass = "icon-contact",
 *   xtProvider = "slogxt",
 *   notop = TRUE,
 *   weight = 9998
 * )
 */
class Contact extends SxtActionPluginBase {

  public function access() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      return parent::access();
    }
    return FALSE;
  }

}
