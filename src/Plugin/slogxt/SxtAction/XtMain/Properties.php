<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Properties.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_main_properties",
 *   title = @Translation("Properties"),
 *   menu = "main_dropbutton",
 *   path = "mainProperties",
 *   cssClass = "icon-properties",
 *   xtProvider = "slogxt",
 *   notop = FALSE,
 *   weight = 9990
 * )
 */
class Properties extends SxtActionPluginBase {

  public function access() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      return parent::access();
    }
    return FALSE;
  }

}
