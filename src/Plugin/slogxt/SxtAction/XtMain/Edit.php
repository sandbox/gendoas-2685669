<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain\Edit.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_edit",
 *   title = @Translation("Edit"),
 *   menu = "main_dropbutton",
 *   path = "edit",
 *   cssClass = "icon-edit",
 *   xtProvider = "slogxt",
 *   weight = 9980
 * )
 */
class Edit extends SxtActionPluginBase {

  //todo::actions::main

  public function access() {
    if (parent::access() && \Drupal::currentUser()->isAuthenticated()) {
      return SlogXt::hasMainEditOtherPerm();
    }
    return FALSE;
  }

}
