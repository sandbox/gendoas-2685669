<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy\DummyTabs.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_dummy_tabs",
 *   title = @Translation("DummyTabs"),
 *   menu = "xt_dummy",
 *   path = "dummyTabs",
 *   cssClass = "icon-dummy",
 *   xtProvider = "slogxt",
 *   weight = 1111
 * )
 */
class DummyTabs extends DummyActionBase {


}
