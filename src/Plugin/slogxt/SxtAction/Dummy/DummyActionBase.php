<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy\DummyActionBase.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy;

use Drupal\slogxt\Plugin\SxtActionPluginBase;
use Drupal\slogxt\SlogXt;

class DummyActionBase extends SxtActionPluginBase {
  
  public function access() {
    return \Drupal::currentUser()->isAuthenticated() && SlogXt::addDummyActions();
  }

}
