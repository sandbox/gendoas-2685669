<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy\Dummy.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_dummy",
 *   title = @Translation("Dummy"),
 *   menu = "xt_dummy",
 *   path = "dummy",
 *   cssClass = "icon-dummy",
 *   xtProvider = "slogxt",
 *   weight = 1110
 * )
 */
class Dummy extends DummyActionBase {


}
