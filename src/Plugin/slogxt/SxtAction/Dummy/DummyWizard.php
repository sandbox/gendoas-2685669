<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy\DummyWizard.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\Dummy;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_dummy_wizard",
 *   title = @Translation("DummyWizard"),
 *   menu = "xt_dummy",
 *   path = "dummyWizard",
 *   cssClass = "icon-dummy",
 *   xtProvider = "slogxt",
 *   weight = 1112
 * )
 */
class DummyWizard extends DummyActionBase {


}
