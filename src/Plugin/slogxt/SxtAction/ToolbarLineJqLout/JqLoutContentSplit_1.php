<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit_1.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split1",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplit",
 *   value = 1,
 *   cssClass = "icon-split1",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "split",
 *   weight = 201
 * )
 */
class JqLoutContentSplit_1 extends SxtActionPluginTbLineBase {


}
