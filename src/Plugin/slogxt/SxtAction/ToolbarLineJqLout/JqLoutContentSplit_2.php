<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit_2.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split2",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplit",
 *   value = 2,
 *   cssClass = "icon-split2",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "split",
 *   weight = 202
 * )
 */
class JqLoutContentSplit_2 extends SxtActionPluginTbLineBase {


}
