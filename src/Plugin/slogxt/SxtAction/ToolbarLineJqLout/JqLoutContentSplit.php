<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplitToggle",
 *   cssClass = "icon-split",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "content",
 *   weight = -201
 * )
 */
class JqLoutContentSplit extends SxtActionPluginTbLineBase {


}
