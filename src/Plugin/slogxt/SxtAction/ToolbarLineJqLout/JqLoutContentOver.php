<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentOver.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_over",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutOverToggle",
 *   cssClass = "icon-over",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "content",
 *   weight = -200
 * )
 */
class JqLoutContentOver extends SxtActionPluginTbLineBase {


}
