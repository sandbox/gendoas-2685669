<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit_0.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split0",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplit",
 *   value = 0,
 *   cssClass = "icon-split0",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "split",
 *   weight = 200
 * )
 */
class JqLoutContentSplit_0 extends SxtActionPluginTbLineBase {


}
