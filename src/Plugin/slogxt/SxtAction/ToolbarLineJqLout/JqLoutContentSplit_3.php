<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit_3.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split3",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplit",
 *   value = 3,
 *   cssClass = "icon-split3",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "split",
 *   weight = 203
 * )
 */
class JqLoutContentSplit_3 extends SxtActionPluginTbLineBase {


}
