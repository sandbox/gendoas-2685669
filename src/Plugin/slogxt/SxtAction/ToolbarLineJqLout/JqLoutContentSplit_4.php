<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit_4.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split4",
 *   title = @Translation("Split"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplit",
 *   value = 4,
 *   cssClass = "icon-split4",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "split",
 *   weight = 204
 * )
 */
class JqLoutContentSplit_4 extends SxtActionPluginTbLineBase {


}
