<?php

/**
 * @file
 * Contains \Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout\JqLoutContentSplit.
 */

namespace Drupal\slogxt\Plugin\slogxt\SxtAction\ToolbarLineJqLout;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_jqlout_content_split_target",
 *   title = @Translation("Split target"),
 *   menu = "xt_tbline_content",
 *   path = "jqloutSplitTarget",
 *   cssClass = "icon-split-target",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   group = "content",
 *   weight = -202
 * )
 */
class JqLoutContentSplitTarget extends SxtActionPluginTbLineBase {


}
