<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\dummy\DummyOption01.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\dummy;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_dummy_item01",
 *   bundle = "xt_dummy",
 *   title = @Translation("Dummy-Item-01"),
 *   description = @Translation(".................Dummy-Item-01."),
 *   path = "dummy",
 *   value = "01",
 *   xtProvider = "slogxt",
 *   weight = 991
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class DummyOption01 extends XtPluginOptionsBase {

}
