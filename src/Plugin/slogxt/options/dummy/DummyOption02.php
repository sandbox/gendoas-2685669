<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\dummy\DummyOption02.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\dummy;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_dummy_item02",
 *   bundle = "xt_dummy",
 *   title = @Translation("Dummy-Item-02"),
 *   description = @Translation(".................Dummy-Item-02."),
 *   path = "dummy",
 *   value = "02",
 *   xtProvider = "slogxt",
 *   weight = 992
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class DummyOption02 extends XtPluginOptionsBase {

}
