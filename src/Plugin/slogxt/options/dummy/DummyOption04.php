<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\dummy\DummyOption04.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\dummy;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_dummy_item04",
 *   bundle = "xt_dummy",
 *   title = @Translation("DummyItem-04"),
 *   description = @Translation(".................DummyItem-04."),
 *   path = "dummy",
 *   value = "04",
 *   xtProvider = "slogxt",
 *   weight = 994
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class DummyOption04 extends XtPluginOptionsBase {

}
