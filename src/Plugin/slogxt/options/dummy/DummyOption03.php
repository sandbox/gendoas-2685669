<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\dummy\DummyOption03.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\dummy;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_dummy_item03",
 *   bundle = "xt_dummy",
 *   title = @Translation("DummyItem-03"),
 *   description = @Translation(".................DummyItem-03."),
 *   path = "dummy",
 *   value = "03",
 *   xtProvider = "slogxt",
 *   weight = 993
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class DummyOption03 extends XtPluginOptionsBase {

}
