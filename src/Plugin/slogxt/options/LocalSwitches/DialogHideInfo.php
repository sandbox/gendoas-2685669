<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\LocalSwitches\ClearIdbStorage.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_localswitches_dialoghideinfo",
 *   bundle = "local_switches",
 *   title = @Translation("Hide dialog info"),
 *   description = @Translation("Hide information in dialog that is not really needed by experienced users."),
 *   path = "localSwitches",
 *   value = "DialogHideInfo",
 *   xtProvider = "slogxt",
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class DialogHideInfo extends XtPluginOptionsBase {

}
