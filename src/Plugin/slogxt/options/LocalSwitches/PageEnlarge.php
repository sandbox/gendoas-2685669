<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\LocalSwitches\PageEnlarge.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_localswitches_pageenlarge",
 *   bundle = "local_switches",
 *   title = @Translation("Enlarge page"),
 *   description = @Translation(".........Enlarge page"),
 *   path = "localSwitches",
 *   value = "PageEnlarge",
 *   xtProvider = "slogxt",
 *   weight = 2
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class PageEnlarge extends XtPluginOptionsBase {

}
