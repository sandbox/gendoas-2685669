<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\LocalSwitches\ClearIdbStorage.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_localswitches_dialogclosewarnmessage",
 *   bundle = "local_switches",
 *   title = @Translation("Close dialog info message"),
 *   description = @Translation("Keep closed info message in dialog that ...."),
 *   path = "localSwitches",
 *   value = "DialogCloseInfoMessage",
 *   xtProvider = "slogxt",
 *   weight = 1
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class DialogCloseWarnMessage extends XtPluginOptionsBase {

}
