<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\LocalSwitches\PageEnlarge.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_localswitches_reloadpause",
 *   bundle = "local_switches",
 *   title = @Translation("Pause reloading"),
 *   description = @Translation(".........Pause reloading"),
 *   path = "localSwitches",
 *   value = "ReloadPause",
 *   xtProvider = "slogxt",
 *   weight = 100
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class ReloadPause extends XtPluginOptionsBase {

}
