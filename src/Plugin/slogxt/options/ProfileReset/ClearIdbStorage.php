<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\ProfileReset\ClearIdbStorage.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\ProfileReset;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_profile_reset_clear_idb_storage",
 *   bundle = "profile_reset",
 *   title = @Translation("Clear Content Cache"),
 *   description = @Translation("Clear permanent cache for contents, ..."),
 *   path = "profileReset",
 *   value = "ClearIdbStorage",
 *   xtProvider = "slogxt",
 *   weight = 20
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class ClearIdbStorage extends XtPluginOptionsBase {

}
