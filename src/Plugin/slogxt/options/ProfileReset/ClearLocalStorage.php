<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\ProfileReset\ClearLocalStorage.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\ProfileReset;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_profile_reset_clear_local_storage",
 *   bundle = "profile_reset",
 *   title = @Translation("Clear Local Storage"),
 *   description = @Translation("Clear permanent cache for menus, ..."),
 *   path = "profileReset",
 *   value = "ClearLocalStorage",
 *   xtProvider = "slogxt",
 *   weight = 10
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class ClearLocalStorage extends XtPluginOptionsBase {

}
