<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\ProfileReset\ClearLogData.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\ProfileReset;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_profile_reset_clear_log_storage",
 *   bundle = "profile_reset",
 *   title = @Translation("Admin: Clear Log Data"),
 *   description = @Translation("Log data is stored in indexedDB if console.log not available."),
 *   path = "profileReset",
 *   value = "ClearLogData",
 *   xtProvider = "slogxt",
 *   weight = 990
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class ClearLogData extends XtPluginOptionsBase {

}
