<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\ProfileReset\ClearStateOnReload.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\ProfileReset;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_profile_reset_clear_state_storage",
 *   bundle = "profile_reset",
 *   title = @Translation("Clear State"),
 *   description = @Translation("Clear permanent cache for state (e.g. history, bookmarks) and refresh content."),
 *   path = "profileReset",
 *   value = "ClearStateOnReload",
 *   xtProvider = "slogxt",
 *   weight = 30
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class ClearStateOnReload extends XtPluginOptionsBase {

}
