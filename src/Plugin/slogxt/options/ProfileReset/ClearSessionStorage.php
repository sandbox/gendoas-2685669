<?php

/**
 * @file
 * Definition of Drupal\slogxt\Plugin\slogxt\options\ProfileReset\ClearSessionStorage.
 */

namespace Drupal\slogxt\Plugin\slogxt\options\ProfileReset;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "slogxt_profile_reset_clear_session_storage",
 *   bundle = "profile_reset",
 *   title = @Translation("Clear Session Storage"),
 *   description = @Translation("Clear session cache (e.g. lists, ...)"),
 *   path = "profileReset",
 *   value = "ClearSessionStorage",
 *   xtProvider = "slogxt",
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class ClearSessionStorage extends XtPluginOptionsBase {

}
