<?php

/**
 * @file
 * Contains \Drupal\slogxt\XtDateTimeTrait.
 */

namespace Drupal\slogxt;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * A Trait for slogxt date functionality.
 */
trait XtDateTimeTrait {

  public function xtDateAddDays($timestamp, $days) {
    $timestamp = !empty($timestamp) ? (integer) $timestamp : REQUEST_TIME; 
    $days = (integer) $days; 
    if ($days <= 0) {
      $days = $this->getMaxDaysToAdd();
    }
    $datetime = DrupalDateTime::createFromTimestamp($timestamp);
    $datetime->add(new \DateInterval("P{$days}D"));
    return $datetime->getTimestamp();
  }

  public function xtDateDiffDays($timestamp_from = NULL, $timestamp_to = NULL) {
    $dt_from = !is_numeric($timestamp_from) ? new DrupalDateTime('') : DrupalDateTime::createFromTimestamp($timestamp_from);
    $dt_to = !is_numeric($timestamp_to) ? new DrupalDateTime('') : DrupalDateTime::createFromTimestamp($timestamp_to);
    $diff = $dt_from->diff($dt_to);
    $days = $diff->days;
    if ($days >= 0 && $diff->h > 12) {
      $days += 1;
    }

    return $days;
  }

  public function getExpireTimestamp($days, $start = REQUEST_TIME) {
    return $this->xtDateAddDays($start, $days);
  }

  public function getMaxDaysToAdd($start = REQUEST_TIME) {
    $time_left = 2147483647 - $start;
    return (integer) ($time_left / 86400);
  }

}
