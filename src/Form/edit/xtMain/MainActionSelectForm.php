<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\edit\xtMain\MainActionSelectForm.
 */

namespace Drupal\slogxt\Form\edit\xtMain;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class MainActionSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtedit_select_main_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    //todo::actions::main
    return SlogXt::pluginManager('edit')->getActionsData('main', $baseEntityId);
  }

}
