<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm.
 */

namespace Drupal\slogxt\Form\edit\xtMain;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\TbMenuRadiosFormBase;

/**
 * Provides a ... form.
 */
class TbMenuSelectForm extends TbMenuRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbmenu_select';
  }

  protected function getPath($id) {
    if (empty($this->path_info)) {
      $request = \Drupal::request();
      $this->path_info = urldecode($request->getPathInfo());
      $this->replace_key = $request->get('pathReplaceKey', '{next_entity_id}');
    }

    return str_replace($this->replace_key, $id, $this->path_info);
  }

  protected function getNoneOption() {
    if ((boolean) \Drupal::request()->get('addNoneOption')) {
      return [
        'liTitle' => t('None'),
        'liDescription' => t('Disable feature for this entity.'),
        'path' => $this->getPath(0),
        'selected' => ($this->selectedMenuTid === 0)
      ];
    }
  }

}
