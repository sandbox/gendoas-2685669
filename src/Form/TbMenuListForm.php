<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\TbMenuListForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\TbMenuRadiosFormBase;
use Drupal\Core\Url;

/**
 * Provides a ... form.
 */
class TbMenuListForm extends TbMenuRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbmenu_list';
  }

  protected function hideRadios() {
    return TRUE;
  }

  protected function getPath($id) {
    $route_name = SlogXt::getSlogxtHandler()->getRouteName('.on.menu_item_select', TRUE);
    $parameters = ['tbmenu_tid' => $id];
    $url = Url::fromRoute($route_name, $parameters);
    return $url->getInternalPath();
  }

}
