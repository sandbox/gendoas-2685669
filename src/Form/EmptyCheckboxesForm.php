<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\EmptyCheckboxesForm.
 */

namespace Drupal\slogxt\Form;

/**
 * Provides a ... form.
 */
class EmptyCheckboxesForm extends XtCheckboxesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_empty_checkboxes';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    return FALSE;
  }

}
