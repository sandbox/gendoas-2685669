<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\TxTargetEntitySelectForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class Xxxx_TxTargetEntitySelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tx_target_entity_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $path_target = $request->get('path_target');
    $path_search = "/{$path_target}/";


    $options = [];
    $plugins = SlogTx::getTargetEntityPlugins();
    foreach ($plugins as $plugin_id => $plugin) {
      $path_replace = "/{$path_target}" . SlogXt::XTURL_DELIMITER . "{$plugin_id}/";
      $options[] = [
        'entityid' => $plugin_id,
        'liTitle' => $plugin->getOptionLabel(),
        'liDescription' => $plugin->getDescription(),
        'path' => str_replace($path_search, $path_replace, $path_info),
      ];
    }

    return $options;
  }

}
