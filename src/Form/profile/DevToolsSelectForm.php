<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\profile\DevToolsSelectForm.
 */

namespace Drupal\slogxt\Form\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class DevToolsSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_edit_select_profile_devtools_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    //todo::actions::main
    return SlogXt::pluginManager('edit')->getActionsData('profiledevtools', $baseEntityId);
  }

}
