<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\profile\LocalSwitchesForm.
 */

namespace Drupal\slogxt\Form\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtCheckboxesFormBase;

/**
 * Provides a ... form.
 */
class LocalSwitchesForm extends XtCheckboxesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_options_profile_localswitches';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('options')->getXtOptions('local_switches', $baseEntityId);
  }

}
