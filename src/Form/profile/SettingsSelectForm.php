<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\profile\SettingsSelectForm.
 */

namespace Drupal\slogxt\Form\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class SettingsSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_edit_select_profile_settings_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    //todo::actions::main
    return SlogXt::pluginManager('edit')->getActionsData('profilesettings', $baseEntityId);
  }

}
