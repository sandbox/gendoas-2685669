<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\profile\ResetOptionsForm.
 */

namespace Drupal\slogxt\Form\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtCheckboxesFormBase;

/**
 * Provides a ... form.
 */
class ResetOptionsForm extends XtCheckboxesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_options_profile_reset';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('options')->getXtOptions('profile_reset', $baseEntityId);
  }

}
