<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\profile\ActionSelectForm.
 */

namespace Drupal\slogxt\Form\profile;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class ActionSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_edit_select_profile_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    $bundle = !empty($baseEntityId) ? 'profile' : 'anonymous';
    return SlogXt::pluginManager('edit')->getActionsData($bundle, $baseEntityId);
  }

}
