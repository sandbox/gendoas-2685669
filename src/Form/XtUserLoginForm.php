<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtUserLoginForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\user\Form\UserLoginForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class XtUserLoginForm extends UserLoginForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_user_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('slogxtData', [
      'runCommand' => 'reload',
      'args' => ['reloadImmediately' => TRUE],
    ]);
    $account = $this->userStorage->load($form_state->get('uid'));
    user_login_finalize($account);
  }

}
