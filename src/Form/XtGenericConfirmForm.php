<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtGenericConfirmForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Provides a generic confirm form for ajax calls.
 */
class XtGenericConfirmForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_generic_confirm';
  }

  public function getRequestData() {
    $request = \Drupal::request();
    $data = ($request->get('confirm_request_data') ?: []) + [
      'question' => t('MyQuestion'),
      'description' => t('MyDescription'),
      'form_name' => 'generic_confirm_form',
      'confirm_text' => t('Save'),
    ];
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $data = $this->getRequestData();
    $form_name = (string) $data['form_name'];

    $form['#title'] = $data['question'];

    $form['#attributes']['class'][] = 'confirmation';
    $form[$form_name] = ['#type' => 'hidden', '#value' => 1];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $data['confirm_text'],
      '#button_type' => 'primary',
    ];

    // By default, render the form using theme_confirm_form().
    if (!isset($form['#theme'])) {
      $form['#theme'] = 'confirm_form';
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no need for validation.
   * 
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no submission.
   * 
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

}
