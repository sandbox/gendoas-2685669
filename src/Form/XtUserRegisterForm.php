<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtUserRegisterForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\user\RegisterForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Defines a controller ....
 */
class XtUserRegisterForm extends RegisterForm {

  public function __construct(EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_manager, $language_manager, $entity_type_bundle_info, $time);
    $this->setStringTranslation(\Drupal::service('string_translation'))
        ->setModuleHandler(\Drupal::moduleHandler())
        ->setEntityTypeManager(\Drupal::entityTypeManager())
        ->setOperation('register');
    $this->entity = $this->entityTypeManager
        ->getStorage('user')
        ->create();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_user_register_form';
  }

//        public function validateForm(array &$form, FormStateInterface $form_state) {
//          parent::validateForm($form, $form_state);
//          $xx = 0;
//        }
//
//        public function submitForm(array &$form, FormStateInterface $form_state) {
//          parent::submitForm($form, $form_state);
//          $xx = 0;
//
//      //    $form_state->set('slogxtData', [
//      //      'runCommand' => 'reload',
//      //    ]);
//      //    $account = $this->userStorage->load($form_state->get('uid'));
//      //    user_login_finalize($account);
//        }

}
