<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtEmptyInfoForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Provides a ... form.
 */
class XtEmptyInfoForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_empty_message';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // not realy a form
    $form['dummy_message'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no need for validation.
   * 
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no submission.
   * 
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

}
