<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtSettingsForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SlogTx settings form.
 */
class XtSettingsForm extends ConfigFormBase {

  public function getFormId() {
    return 'slogxt_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['slogxt.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('slogxt.settings');

    $form['max_toolbar_tabs'] = [
      '#type' => 'textfield',
      '#title' => t('Max Toolbar Tabs'),
      '#description' => t("Max tabs for single toolbars. Min=2, Max=6, default=4"),
      '#default_value' => SlogXt::getMaxToolbarTabs(),
      '#size' => 10,
    ];

    $form['allow_appearance_change'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow appearance change'),
      '#description' => t('Check this for allowing users to change the appearance/colors of the application.'),
      '#default_value' => $config->get('allow_appearance_change'),
    ];

    $form['allow_appearance_clientside'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow colors on client side'),
      '#description' => t('By default appearance is managed server side, i.e. the same appearance for all devices. You may allow to manage appearance for each device.'),
      '#default_value' => $config->get('allow_appearance_clientside'),
    ];

    if (\Drupal::moduleHandler()->moduleExists('sxt_mediawiki')) {
      $form['mwcomment'] = [
        '#type' => 'details',
        '#title' => t('MediaWiki Comments'),
        '#open' => TRUE,
      ];
      $form['mwcomment']['allow_appearance_mwcomments'] = [
        '#type' => 'checkbox',
        '#title' => t('Allow'),
        '#description' => t('If appearance is allowed specify if to allow changing MediaWiki comments collors.'),
        '#default_value' => $config->get('allow_appearance_mwcomments'),
      ];
      $form['mwcomment']['mwcomment_max_depth'] = [
        '#type' => 'textfield',
        '#title' => t('Max Depth'),
        '#description' => t("Max depth for MW Comments. Min=3, Max=5, default=3"),
        '#default_value' => SlogXt::getMwCommentMaxDepth(),
        '#size' => 10,
      ];
      $form['mwcomment']['mwcomment_toc_compact'] = [
        '#type' => 'textfield',
        '#title' => t('MW Toc compact'),
        '#description' => t("When to compact toc. Min=10, Max=30, default=15"),
        '#default_value' => SlogXt::getMwCommentTocCompact(),
        '#size' => 10,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $max_toolbar_tabs = (integer) $form_state->getValue('max_toolbar_tabs');
    $mwcomment_max_depth = (integer) $form_state->getValue('mwcomment_max_depth');
    $msg_not_valid = t('Not a valid value.');
    if ($max_toolbar_tabs < 3 || $max_toolbar_tabs > 5) {
      $form_state->setErrorByName('max_toolbar_tabs', $msg_not_valid);
    }
    if ($mwcomment_max_depth < 3 || $mwcomment_max_depth > 5) {
      $form_state->setErrorByName('mwcomment_max_depth', $msg_not_valid);
    }
    $mwcomment_toc_compact = (integer) $form_state->getValue('mwcomment_toc_compact');
    if ($mwcomment_toc_compact < 10 || $mwcomment_toc_compact > 30) {
      $form_state->setErrorByName('mwcomment_toc_compact', $msg_not_valid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('slogxt.settings');
    $config->set('max_toolbar_tabs', $values['max_toolbar_tabs'])
        ->set('allow_appearance_change', $values['allow_appearance_change'])
        ->set('allow_appearance_clientside', $values['allow_appearance_clientside'])
        ->set('allow_appearance_mwcomments', (boolean) ($values['allow_appearance_mwcomments'] ?? FALSE))
        ->set('mwcomment_max_depth', $values['mwcomment_max_depth'] ?? 3)
        ->set('mwcomment_toc_compact', $values['mwcomment_toc_compact'] ?? 20)
        ->save();
    drupal_set_message($this->t('Settings have been saved.'));
  }

}
