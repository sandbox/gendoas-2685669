<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtRadiosFormBase.
 */

namespace Drupal\slogxt\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a ... form.
 */
abstract class XtRadiosFormBase extends FormBase {

  abstract protected function getXtOptions();

  protected function getNoneOption() {
    if ((boolean) \Drupal::request()->get('addNoneOption')) {
      return [
        'entityid' => 0,
        'liTitle' => t('None'),
        'liDescription' => t('Disable feature for this entity.'),
      ];
    }
  }

  protected function hideRadios() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // not realy a form
//    $form['dummy'] = [
//      '#type' => 'hidden',
//      '#value' => 'dummy',
//    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form_state->set('slogxtData', [
      'runCommand' => 'radios',
      'hideRadios' => $this->hideRadios(),
      'items' => $this->getXtOptions(),
      'emptyWarning' => $this->getEmptyWarning(),
    ]);

    return $form;
  }

  /**
   * Overridable notice for empty list.
   * 
   * @return string
   */
  public function getEmptyWarning() {
    return t('There is no content in this list.');
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no need for validation.
   * 
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no submission.
   * 
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

}
