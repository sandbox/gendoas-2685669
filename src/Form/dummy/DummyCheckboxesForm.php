<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\dummy\DummyCheckboxesForm.
 */

namespace Drupal\slogxt\Form\dummy;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtCheckboxesFormBase;

/**
 * Provides a ... form.
 */
class DummyCheckboxesForm extends XtCheckboxesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_dummy_checkboxes';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
//    $baseEntityId = \Drupal::request()->get('base_entity_id');
    $baseEntityId = NULL;
    return SlogXt::pluginManager('options')->getXtOptions('xt_dummy', $baseEntityId);
  }

}
