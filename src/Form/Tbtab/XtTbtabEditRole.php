<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\Tbtab\XtTbtabEditRole.
 */

namespace Drupal\slogxt\Form\Tbtab;

/**
 */
class XtTbtabEditRole extends XtTbtabEditBase {

  /**
   * {@inheritdoc}
   */
  protected function getCurrentEntity() {
    return \Drupal::request()->get('user_role');
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbtab_editrole';
  }

}
