<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\Tbtab\XtTbtabEditNode.
 */

namespace Drupal\slogxt\Form\Tbtab;

/**
 */
class XtTbtabEditNode extends XtTbtabEditBase {

  /**
   * {@inheritdoc}
   */
  protected function getCurrentEntity() {
    return \Drupal::request()->get('node');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbtab_editnode';
  }

}
