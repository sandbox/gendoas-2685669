<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\Tbtab\XtTbtabRolePrepareConfirm.
 */

namespace Drupal\slogxt\Form\Tbtab;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;

/**
 * Deletion confirmation form for slog toolbar.
 */
class XtTbtabRolePrepareConfirm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_roletab_confirm_prepare';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $args = [
        '%target' => $this->vocabulary->pathLabel(),
        '%role_id' => $this->role_id,
    ];
    return $this->t('Prepare %target for role %role_id?', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $target = $this->vocabulary->id();
    $rtname = 'UserRole.' . $this->role_id;
    $default = SlogTx::ROOTTERM_DEFAULT;
    $args = ['%path' => "role/$target/$rtname/$default"];
    return $this->t('This creates all Objects until the root term %path', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $args = ['user_role' => $this->role_id];
    return new Url('slogxt.tbtab.vocabs_user_role', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Prepare');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->user_role = $request->get('user_role');
    $this->role_id = $this->user_role->id();
    $this->vocabulary = $request->get('vocabulary');
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());

    // execute prepare
    $vid = $this->vocabulary->id();
//    $result = SlogTx::prepareTbtabRootTerm($vid, $this->user_role);
//todo::current:: TxRootTermGetPlugin - new::todo::remove action (role.list.operations)
    $plugin_id = SlogTx::getTxMenuToolbarPluginId($vid);
    $rootterm = SlogTx::getTbMenuRootTerm($plugin_id, $this->user_role, TRUE);

    // message/log
    $args = [
        '%target' => $this->vocabulary->pathLabel(),
        '%role_id' => $this->user_role->id(),
    ];
    if (!empty($rootterm)) {
      $msg = $this->t('%target has been prepared for role %role_id. XXXXXXXXX', $args);
      drupal_set_message($msg);
      SlogXt::logger()->notice($msg);
    } else {
      $msg = $this->t("%target couldn't be prepared for role %role_id.", $args);
      drupal_set_message($msg, 'error');
      SlogXt::logger()->error($msg);
    }
  }

}
