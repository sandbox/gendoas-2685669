<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\Tbtab\XtTbtabEditUser.
 */

namespace Drupal\slogxt\Form\Tbtab;

/**
 */
class XtTbtabEditUser extends XtTbtabEditBase {

  /**
   * {@inheritdoc}
   */
  protected function getCurrentEntity() {
    return \Drupal::request()->get('user');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbtab_edituser';
  }

}
