<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\Tbtab\XtTbtabUserPrepareConfirm.
 */

namespace Drupal\slogxt\Form\Tbtab;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;

/**
 * Deletion confirmation form for slog toolbar.
 */
class XtTbtabUserPrepareConfirm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_usertab_confirm_prepare';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $args = [
      '%target' => $this->vocabulary->pathLabel(),
      '%user_id' => $this->user->label(),
    ];
    return $this->t('Prepare %target for user %user_id?', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $target = $this->vocabulary->id();
    $rtname = 'User.' . $this->user_id;
    $default = SlogTx::ROOTTERM_DEFAULT;
    $args = ['%path' => "user/$target/$rtname/$default"];
    return $this->t('This creates all Objects until the root term %path', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $args = ['user' => $this->user_id];
    return new Url('slogxt.tbtab.vocabs_user', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Prepare');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->user = $request->get('user');
    $this->user_id = $this->user->id();
    $this->vocabulary = $request->get('vocabulary');
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());

    // execute prepare
    $vid = $this->vocabulary->id();
//    $result = SlogTx::prepareTbtabRootTerm($vid, $this->user);
//todo::current:: TxRootTermGetPlugin - new::todo::remove action (user.list.operations)
    $plugin_id = SlogTx::getTxMenuToolbarPluginId($vid);
    $rootterm = SlogTx::getTbMenuRootTerm($plugin_id, $this->user, TRUE);

    // message/log
    $user_id = $this->user->id();
    $user_txt = $this->user->label() . " ($user_id)";
    $args = [
      '%target' => $this->vocabulary->pathLabel(),
      '%user_id' => $user_txt,
    ];
    if (!empty($rootterm)) {
      $msg = $this->t('%target has been prepared for user %user_id.', $args);
      drupal_set_message($msg);
      SlogXt::logger()->notice($msg);
    }
    else {
      $msg = $this->t("%target couldn't be prepared for user %user_id.", $args);
      drupal_set_message($msg, 'error');
      SlogXt::logger()->error($msg);
    }
  }

}
