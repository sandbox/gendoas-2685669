<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\Tbtab\XtTbtabEditBase.
 */

namespace Drupal\slogxt\Form\Tbtab;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;

/**
 * Configure book settings for this site.
 */
abstract class XtTbtabEditBase extends ConfigFormBase {

  protected $vocabulary;
  protected $entity;
  protected $entity_type;
  protected $targetEntityPlugin;

  /**
   * Return the entity for which to edit toolbar tab, 
   * 
   * Toolbar tab is generated on a vocabulary with TargetEntityPlugin not
   * equal to 'none'. Entity's type has to be equal to TargetEntityPlugin's id.
   * 
   */
  abstract protected function getCurrentEntity();
  
  /**
   * Return an unique form id.
   */
  abstract public function getFormId();
  
  
  /**
   * 
   * @param ConfigFactoryInterface $config_factory
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->vocabulary = \Drupal::request()->get('vocabulary');
    $this->entity = $this->getCurrentEntity();
    $this->entity_type = $this->entity->getEntityTypeId();
    $this->targetEntityPlugin = $this->vocabulary->getTargetEntityPlugin();
    $this->targetEntityPlugin->setEntity($this->getCurrentEntity());
  }

  /**
   * Title callback
   */
  public function getTitle() {
    $tep = $this->vocabulary->getTargetEntityPlugin();
    $args = [
        '%target' => $this->vocabulary->pathLabel(),
        '@entity' => strtolower($tep->getTitle()),
        '%role_id' => $this->entity->label(),
    ];
    return t('Edit %target for @entity %role_id?', $args);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->targetEntityPlugin->getConfigKey()];
  }

  /**
   * Returns the route to go to if the user cancels the action.
   * 
   * By default is built on the received entity.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  protected function getCancelUrl() {
    $args = [$this->entity_type => $this->entity->id()];
    return new Url("slogxt.tbtab.vocabs_{$this->entity_type}", $args);
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->targetEntityPlugin->getTbtabConfig();

    $form['name'] = [
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => $config->get('name'),
        '#description' => t('The name to display as menu tab.'),
    ];
    $form['description'] = [
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#default_value' => $config->get('description'),
        '#description' => t("The text to display as menu tab's tooltip."),
    ];

    $form = parent::buildForm($form, $form_state);
    
    if (!\Drupal::request()->get('_drupal_ajax')) {
      if ($url = $this->getCancelUrl()) {
        $form['actions']['cancel'] = [
          '#type' => 'link',
          '#title' => t('Cancel'),
          '#attributes' => ['class' => ['button']],
          '#url' => $url,
        ];
      }
    }

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());
    // save
    $values = $form_state->getValues();
    $this->config($this->targetEntityPlugin->getConfigKey())
            ->set('name', $values['name'])
            ->set('description', $values['description'])
            ->save();

    // invalidate cache
    $this->vocabulary->getCurrentRootTerm()->invalidateCache();
    // parent
    parent::submitForm($form, $form_state);
  }

}
