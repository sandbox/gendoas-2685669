<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\TbMenuFormBase.
 */

namespace Drupal\slogxt\Form;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class TbMenuRadiosFormBase extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbmenu_base';
  }

  protected function getPath($id) {
    if (empty($this->path_info)) {
      $request = \Drupal::request();
      $this->path_info = urldecode($request->getPathInfo());
      $this->replace_key = $request->get('pathReplaceKey', '{next_entity_id}');
    }

    return str_replace($this->replace_key, $id, $this->path_info);
  }

  protected function getNoneOption() {
    if ((boolean) \Drupal::request()->get('addNoneOption')) {
      return [
        'liTitle' => t('None'),
        'liDescription' => t('Disable feature for this entity.'),
        'path' => $this->getPath(0),
        'selected' => ($this->selectedMenuTid === 0)
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $request = \Drupal::request();
    $excludeMenuTids = $request->get('excludeMenuTids');
    if (empty($excludeMenuTids)) {
      $excludeMenuTids = [];
    }
    $this->selectedMenuTid = (integer) $request->get('selectedMenuTid');
    $mask_item_id = $request->get('maskItemId');

    $hasActionData = FALSE;
    $actionPluginId = $request->get('actionPluginId', FALSE);
    if (!empty($actionPluginId)) {
      $manager = SlogXt::pluginManager('edit');
    }

    $noneOption = $this->getNoneOption();
    $options = $noneOption ? [$noneOption] : [];
    if ($rootterm = $request->get('root_term')) {
      $vocabulary = $rootterm->getVocabulary();
      $tree = $vocabulary->getVocabularyMenuTree($rootterm->id(), 3, TRUE);
      if (!empty($tree)) {
        $prefix = ['', '--.', '--.--.'];
        foreach ($tree as $item) {
          $id = (integer) $item->id();
          if (!in_array($id, $excludeMenuTids)) {
            $request->attributes->set('next_entity_id', $id);
            $plugin = !empty($actionPluginId) ? $manager->createInstance($actionPluginId) : FALSE;
            if (!empty($plugin) && $actionData = $plugin->getActionData()) {
              $path = $actionData['path'];
            }
            else {
              $path_id = $id;
              if (!empty($mask_item_id)) {
                $path_id = str_replace('%id%', $id, $mask_item_id);
              }
              $path = $this->getPath($path_id);
            }

            $options[] = [
              'liTitle' => $prefix[$item->depth] . $item->label(),
              'liDescription' => $item->getDescription(),
              'path' => $path,
              'entityid' => $id,
              'selected' => ($this->selectedMenuTid === $id)
            ];
          }
        }
      }
    }

    $menuSelectAlter = $request->get('menuSelectAlter', FALSE);
    if (!empty($menuSelectAlter)) {
      $menuSelectAlterIsSave = $request->get('menuSelectAlterIsSave', FALSE);
      list($class, $module) = explode('::', $menuSelectAlter);
      $class = \Drupal::service('class_resolver')->getInstanceFromDefinition($class);
      $options = $class::{$module}($options, $menuSelectAlterIsSave);
    }

    return $options;
  }

}
