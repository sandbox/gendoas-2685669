<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtUrgentMessageForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Provides a ... form.
 */
class XtUrgentMessageForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_urgent_message';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // not realy a form
    $form['dummy_message'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];

    // but deposit message data
    $request = \Drupal::request();
    $slogxt = & $form_state->get('slogxt');
    $slogxt['urgentMsgData'] = [
      'message' => $request->get('urgent_msg_message'),
      'type' => $request->get('urgent_msg_type'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no need for validation.
   * 
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no submission.
   * 
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

}
