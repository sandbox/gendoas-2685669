<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\ToolbarsForSysTabsSelectForm.
 */

namespace Drupal\slogxt\Form;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class ToolbarsForSysTabsSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogxt_tbforsystabs_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());

    $plugin_id = $request->get('action_plugin_id');
    $action_plugin = SlogXt::pluginManager('edit')->createInstance($plugin_id);
    $action_perms = $action_plugin->getPermissions();

    $path_target = $request->get('path_target');
    $path_search = "/{$path_target}/";

    $options = [];
    $toolbars = SlogXt::getToolbarsForSysTabs();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $add_toolbar = SlogXt::hasPermToolbar($toolbar_id, $action_perms);
      if ($add_toolbar) {
        $root_term = FALSE;
        $vid = $toolbar->getSysToolbarVid($path_target, $toolbartab);
        if ($vocabulary = SlogTx::getVocabulary($vid)) {
          $root_term = $vocabulary->getCurrentRootTerm();
        }
        if (!$root_term || !$root_term->isRootTerm()) {
          $add_toolbar = FALSE;
          SlogXt::logger()->error(t("Root term not found for vocabulary %vid.", ['%vid' => $vid]));
        }
        elseif ($toolbar->isNodeSubsysToolbar()) {
          $add_toolbar = TRUE;
        }
        else {
          $add_toolbar = $action_plugin->hasActionItemsForSysTab($root_term);
        }
      }

      if ($add_toolbar) {
        $path_replace = "/{$path_target}" . SlogXt::XTURL_DELIMITER . "{$toolbar_id}/";
        $options[] = [
          'entityid' => $toolbar_id,
          'hooks' => ['onSelected' => 'onSysTbSelected'],
          'liTitle' => $toolbar->label(),
          'liDescription' => $toolbar->getDescription(),
          'path' => str_replace($path_search, $path_replace, $path_info),
        ];
      }
    }

    return $options;
  }

}
