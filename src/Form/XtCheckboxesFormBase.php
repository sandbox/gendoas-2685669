<?php

/**
 * @file
 * Contains \Drupal\slogxt\Form\XtCheckboxesFormBase.
 */

namespace Drupal\slogxt\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a ... form.
 */
abstract class XtCheckboxesFormBase extends FormBase {

  abstract protected function getXtOptions();

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // not realy a form
    $form['dummy'] = [
      '#type' => 'hidden',
      '#value' => 'dummy',
    ];

    $form_state->set('slogxtData', [
      'runCommand' => 'checkboxes',
      'items' => $this->getXtOptions(),
      'emptyWarning' => t('There is no content in this list.'),
    ]);

    return $form;
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no need for validation.
   * 
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   * 
   * Dummy. This is not really a form. There is no submission.
   * 
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

}
