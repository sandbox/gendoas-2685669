/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings, _, undefined) {

  "use strict";

  var sxtExt = {
    StopWatch: function () {
      this.StartMilliseconds = 0;
      this.ElapsedMilliseconds = 0;
      this.start = function () {
        this.StartMilliseconds = new Date().getTime();
      };
      this.stop = function () {
        this.ElapsedMilliseconds = new Date().getTime() - this.StartMilliseconds;
      };
    },
    isSuperUser: function () {
      return (this.getUserId() === 1);
    },
    isAdmin: function () {
      return this.isSuperUser();
    },
    getUser: function () {
      return drupalSettings.user || false;
    },
    getUserId: function () {
      var user = this.getUser();
      return user ? parseInt(user.uid, 10) : 0;
    },
    isUserAuthenticated: function () {
      return (this.getUserId() > 0);
    },
    getUserDefaultRole: function () {
      return drupalSettings.slogxt.user.defaultRole;
    },
    getUserAppearance: function () {
      return drupalSettings.slogxt.user.appearance;
    },
    setUserAppearance: function (app) {
      drupalSettings.slogxt.user.appearance = app;
    },
    getLocalAppearance: function () {
      return JSON.parse(localStorage.getItem('slogxt:profile::Appearance'));
    },
    setLocalAppearance: function (app) {
      var sdata = JSON.stringify(app);
      localStorage.setItem('slogxt:profile::Appearance', sdata);
    },
    getUserSigned: function (text) {
      text = !!text ? ':' + text : '';
      return 'u' + this.getUserId() + text;
    },
    getLiLabels: function ($li) {
      var $clone = $li.clone();
      $clone.find('span').remove();
      return {
        pathLabel: $li.find('span').text(),
        mainLabel: $clone.text()
      };
    },
    storageKey: function (prefix) {
      var sep = ''
          , key = !!prefix ? prefix + '::' : '';
      for (var i = 1; i < arguments.length; i++) {
        key += sep + arguments[i];
        sep = '.';
      }
      return key;
    },
    windowPosition: function ($el) {
      var offset = $el.offset()
          , $document = $(document)
          , top = offset.top - $document.scrollTop()
          , left = offset.left - $document.scrollLeft();
      return {
        top: top,
        left: left,
        width: window.innerWidth,
        height: window.innerHeight,
        ratioX: parseInt(left * 100 / window.innerWidth) / 100,
        ratioY: parseInt(top * 100 / window.innerHeight) / 100
      };
    },
    notImplemented: function (target) {
      alert("Not implemented:\n\n..." + target);
    },
    getMainActionData: function (action, provider) {
      action = action || false;
      provider = provider || 'slogxt';
      var dsp = drupalSettings[provider] || {}
      , mad = dsp.mainActionsData || {}
      , data = action ? mad[action] : mad;
      return (data || {});
    },
    canApply: function (bActionId, can, args) {
      can = can || false;
      !bActionId && (can = false);
      if (_.isBoolean(can)) {
        return can;
      }

      if (_.isObject(can)) {
        var isTarget = !!args.isTarget
            , isSource = !!args.isSource
            , idx = isTarget ? 'target' : isSource ? 'source' : 'single'
            , canIds = can[idx] || ''
            , splitted = $.map(canIds.split(','), $.trim);
        if (_.indexOf(splitted, bActionId) >= 0) {
          return true;
        }
      }

      return false;
    },
    helper: {
      buildPath: function (parts, delimiter) {
        delimiter = delimiter || '/'
        var path = '', part;
        for (var idx in parts) {
          !!path && (path += delimiter);
          path += parts[idx];
        }
        return path;
      },
      splitPath: function (path, delimiter, keys) {
        var p1 = path.split(delimiter)[1] || false
            , p2 = p1 ? p1.split('?') : false
            , part = p2[0] || false
            , parts = part ? part.split('/') : false
            , result = {}
        ;
        if (parts) {
          result['path'] = delimiter + p1;
          for (var idx in keys) {
            result[keys[idx]] = parts[idx] || '';
          }
        }
        return result;
      },
      splitID: function (id, keys) {
        var parts = id ? id.split('-') : false
            , result = {}
        ;
        if (parts) {
          result['id'] = id;
          for (var idx in keys) {
            result[keys[idx]] = parts[idx] || '';
          }
        }
        return result;
      },
      toPath: function (input) {
        return input.replace(/[.|-]/g, '/');
      },
      toID: function (input) {
        return input.replace(/[.|\/]/g, '-');
      },
      toClass: function (input) {
        return input.replace(/[.|\/]/g, '-');
      },
      emptyObject: function () {
        return {};
      },
      emptyArray: function () {
        return [];
      },
      showInvisibly: function ($E, force) { // copy from jquery.layout.js
        if ($E && $E.length && (force || $E.css("display") === "none")) { // only if not *already hidden*
          var s = $E[0].style
              // save ONLY the 'style' props because that is what we must restore
              , CSS = {display: s.display || '', visibility: s.visibility || ''};
          // show element 'invisibly' so can be measured
          $E.css({display: "block", visibility: "hidden"});
          return CSS;
        }
        return {};
      }

    }
  };
  // extend Drupal.slogxt
  $.extend(true, Drupal.slogxt, sxtExt);

})(jQuery, Drupal, drupalSettings, _);
