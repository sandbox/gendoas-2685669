/**
 * @file
 * Attaches behavior for state extension.
 */

(function ($, Drupal) {

  "use strict";
  
  var _sxt = Drupal.slogxt;
  
  Drupal.behaviors.SlogxtState = {
    attach: function () {
      // create state model if not created yet.
      if (!_sxt.models.stateModel) {
//        slogLog('behaviors.attach......   .....SlogxtState');
        _sxt.models.stateModel = new _sxt.StateModel()
      }     
    }
  };
 
  // extend _sxt
  var sxtStateExt = {
    stateModel: function () {
      return this.models.stateModel;
    }
  };
  // now extend
  $.extend(true, _sxt, sxtStateExt);
  sxtStateExt = null;

  //
  // on document ready
  //
  $(document).ready(function () {
    _sxt.stateModel().onDocumentReady();
  });

})(jQuery, Drupal);
