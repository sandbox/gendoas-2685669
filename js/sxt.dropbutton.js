/**
 * @file
 * Extensions for Drupal.DropButton.prototype and Drupal.slogxt.
 */

(function ($, Drupal, window, _) {

  "use strict";

  var _sxt = Drupal.slogxt;

  Drupal.behaviors.slogxtDropbutton = {
    attach: function (context) {
//        slogLog('behaviors.attach......   .....slogxtDropbutton');
      var $dbtnBlock = $('.block-slogxt-main-dropbutton');
      if ($dbtnBlock.length && !_sxt.views.mainActionsView) {
        var model = _sxt.models.mainActionsModel = new _sxt.MainActionsModel();
        _sxt.views.mainActionsView = new _sxt.MainActionsView({
          el: $dbtnBlock.get(0),
          model: model
        });

        // test occurrences
        if ($dbtnBlock.length > 1) {
          slogErr('Multiple occurrences of main dropbutton block.');
        }
      }

    }
  };





  /**
   * Extensions for Drupal.DropButton.prototype
   */
  var prototypeExt = {
    isOpen: false,
    toggle: function (show) {
      // toggle class as in parent
      var screenModel = _sxt.screenModel();
      show = (typeof show === 'boolean') ? show : !this.$dropbutton.hasClass('open');
      this.$dropbutton.toggleClass('open', show);

      if (!show && !screenModel.get('openDropButton')) {
        return;
      }

      // close open popup, i.e. dropbutton or toolbar tray or ...
//      slogLog('Drupal.DropButton........toggle: ' + show);
      show && _sxt.closeOpenPopup();

      // open or close dropbutton, ensure correct view
      var open = this.isOpen = this.$dropbutton.hasClass('open')
          , viewCid = this.slogView ? this.slogView.cid : false
          , drbData = open ? {dropbutton: this, viewCid: viewCid} : null;
      this.$dropbutton.sxtRegionToTop(open);
      screenModel.set('openDropButton', drbData);
      (!!viewCid) && this.slogView.model.set('dropbuttonIsOpen', open);
      screenModel.set('openPopUp', 'dropbutton');
    },
    hoverOut: function () { /* override */
      if (this.isOpen && !_sxt.isTouchSupported()) {
        setTimeout($.proxy(this, 'close'), 500);
      }
    },
    sxtMoveToTop: function (el) {
//      slogLog('Drupal.DropButton.prototype..........................sxtMoveToTop');
      var $liNew = $(el).closest('li.dropbutton-action')
          , $liFirst = $(this.$actions.get(0));
      if ($liNew.length && $liNew.hasClass('secondary-action')) {
        this.$list.prepend($liNew);
        $liNew.removeClass('secondary-action');
        $liFirst.addClass('secondary-action');
        this.$actions = this.$list.find('li.dropbutton-action');
      }
      return this;
    },
    dispatch: function (view, $el) {
      var notop = !!$el.attr('notop')
          , action = view.curAction = $el.attr('action') || ''
          , provider = $el.attr('provider') || '???'
          , args = {value: $el.attr('value'), href: $el.attr('href')};
      !notop && this.sxtMoveToTop($el);
      this.isOpen && this.close();

      action = 'action' + action.capitalize();
      var callback = view.actions[provider]
          ? view.actions[provider][action] || false : false;
      if (callback && $.isFunction(callback)) {
        callback.call(view, args);
      } else {
        var target = view.sxtThisClass + '.actions.' + provider + '.' + action;
        _sxt.notImplemented(target);
      }
    }
  }
  $.extend(true, Drupal.DropButton.prototype, prototypeExt);
  prototypeExt = null;


  /**
   * Extensions for _sxt
   */
  var slogxtExt = {
    mainActionsView: function () {
      return this.views.mainActionsView;
    },
    dropbuttonClickHandler: function (e) {
      e.preventDefault();
      e.stopPropagation();  //yes
      var dBtn = _sxt.getDropbutton($(e.target))
          , show = dBtn && !dBtn.$dropbutton.hasClass('open');
      dBtn && (show ? dBtn.open() : dBtn.close());
    },
    getDropbutton: function ($element) {
      var dropbuttons = Drupal.DropButton.dropbuttons || []
          , $dropbutton = $element.closest('.dropbutton-wrapper');
      for (var idx in dropbuttons) {
        if (dropbuttons[idx].$dropbutton.is($dropbutton)) {
          return dropbuttons[idx];
        }
      }

      // not found
      return false;
    },
    dropbuttonPreventActions: function (actions, prevent) {
      if (_.isArray(actions) && _.isArray(prevent))
        actions = actions.filter(function (action) {
          return !_.contains(prevent, action.path);
        });
      return _.compact(actions);
    },
    dropbuttonPrepareActions: function (actions, curAction) {
      if (_.isArray(actions) && !!curAction) {
        for (var idx in actions) {
          var action = actions[idx];
          if (action.path === curAction) {
            if (!action.notop) {
              delete actions[idx];
              actions.unshift(action);
            }
            break;
          }
        }
      }
      return _.compact(actions);
    },
    dropbuttonActionDispatch: function (e, view) {
      e.preventDefault();
      e.stopPropagation();
      this.closeOpenPopup();
      var $element = $(e.target)
          , dButton = this.getDropbutton($element);
      dButton && dButton.dispatch(view, $element);
    },
    clearDropbuttons: function ($el) {
      // remove DropButton objects belonging to $el
      var dropbuttons = Drupal.DropButton.dropbuttons || [];
      if (dropbuttons.length > 0) {
        $el.find('.dropbutton-wrapper').each(function () {
          var $dropbutton = $(this);
          for (var idx in dropbuttons) {
            if (dropbuttons[idx].$dropbutton.is($dropbutton)) {
              delete dropbuttons[idx];
              Drupal.DropButton.dropbuttons = _.compact(dropbuttons);
              break;
            }
          }
        });
      }
    }
  };
  // now extend
  $.extend(true, _sxt, slogxtExt);
  slogxtExt = null;


  // extend JQuery.fn
  $.fn.sxtDropbuttonSetView = function (view) {
    return this.each(function () {
      var dBtn = _sxt.getDropbutton($(this));
      dBtn && (dBtn.slogView = view);
    });
  };

})(jQuery, Drupal, window, _);
