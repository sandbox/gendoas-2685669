/**
 * @file
 * A Backbone View for ...
 */

(function ($, Drupal, Backbone, _, undefined) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var dlgDoViewExt = {
    sxtThisClass: 'slogxt.DlgDoNavigateView',
    edges: {
      // 0 - 5 - 1
      // 8 - 4 - 6
      // 3 - 7 - 2
      0: {
        my: 'left top',
        at: 'left+4 top+4'
      },
      1: {
        my: 'right top',
        at: 'right-4 top+4'
      },
      2: {
        my: 'right bottom',
        at: 'right-4 bottom-4'
      },
      3: {
        my: 'left bottom',
        at: 'left+4 bottom-4'
      },
      4: {
        my: 'center center',
        at: 'center center'
      },
      5: {
        my: 'center top',
        at: 'center top+4'
      },
      6: {
        my: 'right center',
        at: 'right-4 center'
      },
      7: {
        my: 'center bottom',
        at: 'center bottom-4'
      },
      8: {
        my: 'left center',
        at: 'left+4 center'
      }
    },
    initialize: function (options) {
      this.sxtIsPermanent = true;
      this.$info = this.$el.find('.slogxt-don-info');
      this.$infoCount = this.$el.find('#don-selected-count');
      this.$btnSubmit = this.$el.find('.slogxt-don-btn-submit')
      this.$btnDonAbort = this.$el.find('.slogxt-don-btn-abort')
      this.$btnDlgAbort = this.$el.find('.slogxt-don-dlg-abort')
      this.$body = $('body');
      this.screenModel = _sxt.screenModel();

      this.listenTo(this.model, 'change:baseData', this.onChangeBaseData);
      this.listenTo(this.model, 'change:rawData', this.onChangeRawData);
      this.listenTo(this.model, 'change:preparedData', this.onChangePreparedData);
      this.listenTo(this.model, 'change:isActive', this.onChangeIsActive);

      this.attach();
    },
    attach: function () {
      var opts = {handle: '.slogxt-don-btn-move', containment: 'body'};
      _sxt.themeIsJqLout() && (opts.scroll = false);
      this.$el.draggable(opts);
      this.$btnSubmit.on('click', $.proxy(this.onClickBtnSubmit, this));
      this.$btnDonAbort.on('click', $.proxy(this.onClickBtnAbort, this));
      this.$btnDlgAbort.on('click', $.proxy(this.onClickDlgAbort, this));
    },
    attachItemAction: function ($itemAction) {
      $itemAction.off().on('click', $.proxy(function (e) {
        e.stopPropagation();
        this.$btnSubmit.click();
      }, this));
    },
    onChangeBaseData: function (model, baseData) {
      var multiple = baseData.multiple || false
          , hideSelect = !!baseData.hideSelect
          , tooltips = baseData.tooltips || {};
      this.$el.toggleClass('select-multiple', multiple);
      this.$el.toggleClass('don-hide-select', hideSelect);
      multiple && (tooltips.submit = Drupal.t('Add selected item'));
      (hideSelect || multiple) && (tooltips.abortDon = Drupal.t('Return to dialog'));
      this.setToolTips(tooltips);
    },
    setToolTips: function (tooltips) {
      tooltips = tooltips || {};
      this.$btnSubmit.attr('title', tooltips.submit || Drupal.t('Return to dialog with selected item'));
      this.$btnDonAbort.attr('title', tooltips.abortDon || Drupal.t('Return to dialog without selection'));
      this.$btnDlgAbort.attr('title', tooltips.abortDlg || Drupal.t('Abort dialog'));
    },
    onChangeRawData: function (model, rawData) {
      function validate(rData) {
        return true;
      }
      function prepare(rData) {
        return rData;
      }

      var baseData = this.model.get('baseData')
          , preparedData = this.model.get('preparedData');
      if (baseData) {
        var fnValidate = baseData.validate || validate
            , fnPrepare = baseData.prepare || prepare;
        if (fnValidate.call(this.dialogView, rawData)) {
          preparedData = fnPrepare.call(this.dialogView, rawData);
          model.set('rawData', null, {silent: true});
          model.set('preparedData', null, {silent: true});
        }
        this.model.set('preparedData', preparedData);
      }
    },
    onChangePreparedData: function (model, data) {
      // slogLog('DlgDoNavigateView.......onChangePreparedData()');
      var enabled = data && !!data.label;
      if (enabled) {
        this.setInfo({label: data.label, title: data.info});
      } else {
        var baseData = model.get('baseData');
        this.setInfo({label: '...' + (baseData.infoHint || '')});
      }
      this.$btnSubmit.toggleClass('disabled', !enabled);
    },
    onDrop: function (e, ui) {
      function calcEdgeX(lastEdge, xToLeft) {
        var newEdge = lastEdge;
        switch (lastEdge) {
          case 1:
          case 0:
            newEdge = 5;
            break;
          case 6:
          case 8:
            newEdge = 4;
            break;
          case 2:
          case 3:
            newEdge = 7;
            break;
          case 5:
            newEdge = xToLeft ? 0 : 1;
            break;
          case 4:
            newEdge = xToLeft ? 8 : 6;
            break;
          case 7:
            newEdge = xToLeft ? 3 : 2;
            break;
        }
        return newEdge
      }
      function calcEdgeY(lastEdge, yToTop) {
        var newEdge = lastEdge;
        switch (lastEdge) {
          case 0:
          case 3:
            newEdge = 8;
            break;
          case 5:
          case 7:
            newEdge = 4;
            break;
          case 1:
          case 2:
            newEdge = 6;
            break;
          case 8:
            newEdge = yToTop ? 0 : 3;
            break;
          case 4:
            newEdge = yToTop ? 5 : 7;
            break;
          case 6:
            newEdge = yToTop ? 1 : 2;
            break;
        }
        return newEdge
      }
      function calcEdgeXY(lastEdge, xToLeft, yToTop) {
        newEdge = lastEdge;
        switch (lastEdge) {
          case 0:
          case 1:
          case 2:
          case 3:
            newEdge = 4;
            break;
          case 4:
            if (xToLeft) {
              newEdge = yToTop ? 0 : 3;
            } else {
              newEdge = yToTop ? 1 : 2;
            }
            break;
          case 5:
          case 7:
            newEdge = xToLeft ? 8 : 6;
            break;
          case 6:
          case 8:
            newEdge = yToTop ? 5 : 7;
            break;
        }
        return newEdge;
      }

      var isSmall = !!this.screenModel.get('isSmall')
          , yPlus = _sxt.isAdmin() ? 20 : 0
          , lastPos = this.screenModel.get('donCurPos')
          , newPos = ui.position
          , moveX = newPos.left - lastPos.left
          , moveY = newPos.top - lastPos.top
          , xToLeft = moveX < 0
          , yToTop = moveY < 0
          , absX = Math.abs(moveX)
          , absY = Math.abs(moveY)
          , xNone = absX < 20
          , xWide = absX > 150
          , yNone = absY < (20 + yPlus)
          , yWide = absY > 150
          , max = Math.max(absX, absY)
          , min = Math.min(absX, absY)
          , newEdge = this.screenModel.get('donCurEdge') || 0
          , lastEdge = newEdge
          , lastInCenterX = !!_.indexOf([-1, 4, 5, 7], lastEdge)
          , lastInCenterY = (lastEdge === 4 || lastEdge === 6 || lastEdge === 8)
          , rel = min / max
          , both = rel > 0.3
          , moreX, intMore, posF;
      this.edgeOnSmall = isSmall;
      if (lastEdge < 4 && (isSmall || (xWide && (yNone || yWide)) || (yWide && (xNone || xWide)))) {
        moreX = absX > absY;
        intMore = moreX ? -1 : 1;
        posF = Math.pow(-1, lastEdge + 1);
        newEdge = both ? lastEdge + 2 : lastEdge + 4 + (intMore * posF);
        this.toEdge(newEdge % 4);
      } else if (xNone && yNone) {
        this.toEdge(lastEdge);
      } else if (xWide && yNone && _.indexOf([4, 6, 8], lastEdge) !== -1) {
        if (lastEdge === 4) {
          newEdge = xToLeft ? 8 : 6;
        } else {
          newEdge = (lastEdge === 6) ? 8 : 6;
        }
        this.toEdge(newEdge);
      } else if (yWide && xNone && _.indexOf([4, 5, 7], lastEdge) !== -1) {
        if (lastEdge === 4) {
          newEdge = yToTop ? 5 : 7;
        } else {
          newEdge = (lastEdge === 5) ? 7 : 5;
        }
        this.toEdge(newEdge);
      } else if ((!xWide || lastInCenterX) && !xNone) {
        if (yNone) {
          newEdge = calcEdgeX(lastEdge, xToLeft);
        } else {
          newEdge = calcEdgeXY(lastEdge, xToLeft, yToTop);
        }
        this.toEdge(newEdge);
      } else if ((!yWide || lastInCenterY) && xNone) {
        newEdge = calcEdgeY(lastEdge, yToTop);
        this.toEdge(newEdge);
      } else {
        this.toEdge(lastEdge);
      }
    },
    toEdge: function (edge) {
      if (edge !== undefined) {
        edge = parseInt(edge, 10);
        (edge < 0 || edge > 8) && (edge = 1);
        this.screenModel.set('donCurEdge', edge);
      }
      (edge === undefined) && (edge = this.screenModel.get('donCurEdge'));
      (edge === undefined) && (edge = 1);
      this.edgeOnSmall && edge > 3 && (edge = 1);

      var pos = (this.edges[edge]) ? _.clone(this.edges[edge]) : _.clone(this.edges[1]);
      pos.of = this.$body;
      pos.collision = 'fit fit';
      pos.using = function (pos) {
        $(this).animate(pos, {
          duration: 'slow',
          complete: function () {
            _sxt.screenModel().set('donCurPos', $(this).position());
          }
        });
      };
      this.$el.position(pos);
    },
    onClickBtnSubmit: function (e) {
      !this.$btnSubmit.hasClass('disabled') && this.selectPreparedData();
    },
    selectPreparedData: function (preparedData, reset) {
      // slogLog('DlgDoNavigateView.......selectPreparedData()');
      !preparedData && (preparedData = this.model.get('preparedData'));
      this.model.set('preparedData', null, {silent: true});
      this.setResult('submit', preparedData, reset);
    },
    onClickBtnAbort: function (e) {
      this.setResult('none');
    },
    onClickDlgAbort: function (e) {
      this.setResult('abort');
    },
    setInfo: function (infos) {
      this.$info.text(infos.label || '');
      this.$info.attr('title', infos.title || '');
    },
    onChangeIsActive: function (model, isActive) {
      var dialogView = this.dialogView
          , baseData = model.get('baseData')
          , bArgs = baseData.args || {}
      , canCreate = (isActive && bArgs.isMoveTarget) ? true : false
          , duration = 'fast';
      if (canCreate) {
        var source = this.model.get('resultSource') || {}
        , sToolbar = source.toolbar || false
            , testTb = sToolbar === _sxt.getTbSysId() ? source.targetTb || false : sToolbar
            , pTbs = bArgs.preparableToolbars || {};
        canCreate = !!testTb && !!pTbs[testTb];
      }
      _sxt.getMainDropBtnEl().toggleClass('can-menu-create', canCreate);

      if (isActive) {
        this.resetResult();
        this.resetRawData();
        this.$el.position({
          my: 'center center',
          at: 'center center',
          of: dialogView.$widget
        });

        dialogView.$widget.fadeOut(duration);
        dialogView.$dialogWrapper.fadeOut(duration);
        this.$el.fadeIn(duration);
        this.toEdge();
        this.screenModel.set('donView', this);
        this.$body.droppable({
          accept: '#slogxt-do-navigate-wrapper',
          drop: $.proxy(this.onDrop, this)
        });
        this.resetSelected(baseData)
      } else {  // !isActive
        this.$el.hide();
        this.$el.css({left: 0, top: 0});
        _sxt.closeOpenPopup();
        dialogView.$dialogWrapper.show();
        dialogView.$widget.show()
        this.screenModel.set('donView', null);
        this.$body.droppable('destroy');
      }
      !!baseData.onChangeDonIsActive && baseData.onChangeDonIsActive(isActive);
    },
    activate: function (activate) {
      if (!activate) {
        var navDialog = _sxt.getModalDialogDialog(true);
        !!navDialog && navDialog.close();
      }
      this.model.set('isActive', activate);
    },
    isActive: function () {
      return !!this.model.get('isActive');
    },
    highlight: function () {
      this.$el.position({
        my: 'center center',
        at: 'center center',
        of: this.$body
      });
      this.toEdge();
    },
    showSelectedCount: function (count) {
      this.$infoCount.text(count);
    },
    resetSelected: function (baseData, infoHint) {
      this.resetResult();
      this.resetRawData();
      this.$btnSubmit.addClass('disabled');
      this.setInfo({label: '...' + (infoHint || baseData.infoHint || '')});
    },
    resetPrepared: function () {
      this.model.set('preparedData', null, {silent: true});
    },
    resetRawData: function () {
      this.model.set('rawData', false, {silent: true});
    },
    resetResult: function () {
      this.model.set('donResult', {}, {silent: true});
    },
    setResult: function (action, data, reset) {
      !!reset && this.resetResult();
      this.model.set('donResult', {action: action, data: data});
    },
    getResult: function () {
      return this.model.get('donResult');
    },
    getInfoCount: function () {
      return (this.$infoCount ? parseInt(this.$infoCount.text(), 10) : 0);
    }

  };

  /**
   * Backbone View for ...
   */
  _sxt.DlgDoNavigateView = Backbone.View.extend(dlgDoViewExt);

})(jQuery, Drupal, Backbone, _);
