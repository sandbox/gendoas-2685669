/**
 * @file
 * Backbone View for main action profile dialog
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var profileExt = {
    sxtThisClass: 'slogxt.ProfileView',
    onPreInitialize: function (opts) {
      this.profilePath = opts.profilePath;
      this._sxtActions = this._sxtActions || {slogxt: {}};
      $.extend(true, this._sxtActions.slogxt, sxt_actions);
    },
    dialogOptions: function () {
      var options = {
        title: Drupal.t('Profile'),
        dialogClass: 'profile-dialog',
        slogxt: {
          dialogId: 'ProfileView',
          restoreState: true,
          isWizard: true,
          contentData: {
            provider: 'slogxt',
            path: 'edit/profile/' + this.profilePath
          }
        }
      };
      return options;
    },
    onDialogClosed: function (e) {
      if (this.doAppearance) {
        _sxt.setAppearanceColors();
        delete this.doAppearance;
      }
    }
  };

  var sxt_actions = {
    actionProfileReset: function (actions, firstRun) {
      var runAgain = false
          , idx, action;
      for (idx in actions) {
        action = actions[idx];
        switch (action) {
          case 'ClearSessionStorage':
            sessionStorage.clear();
            break;
          case 'ClearLocalStorage':
            localStorage.clear();
            break;
          case 'ClearIdbStorage':
            $.sxtIdbStorage.clear({store: 'slogxtContent'});
            break;
          case 'ClearStateOnReload':
            if (firstRun) {
              runAgain = true;
            } else {
              _sxt.stateModel().set('clearStateOnReload', true);
            }
            break;
          case 'ClearLogData':
            $.sxtIdbStorage.clear({store: 'slogxtLog'});
            break;
        }
      }
      if (firstRun && runAgain) {
        return {doNext: 'runAgain'};
      }
      return {doNext: 'doClose'};
    },
    actionLocalSwitches: function ($content, storageKey) {
      var data = {};
      $content.find('li.checkbox').each(function () {
        var $item = $(this)
            , action = $item.attr('action');
        data[action] = $item.hasClass('checked');
      });

      localStorage.setItem(storageKey, JSON.stringify(data));
      _sxt._sxtApplyLocalSwitches();
    }
  }

  /**
   * Backbone View for main action profile dialog
   */
  _sxt.ProfileView = _sxt.DialogViewBase.extend(profileExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
