/**
 * @file
 * A Backbone View for ....
 */

(function ($, Drupal) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var xtEditFormExt = {
    onPreInitialize: function (opts) {
      this.startTitle = opts.startTitle || '...';
      this.startPath = opts.startPath || false;
      this.editTarget = opts.editTarget;
      this.entityId = opts.entityId || 0;
      this.provideWizardPath = opts.noWizardPath ? false : true;;
      this.showNavigateBtn = opts.showNavigateBtn || false;
      this.contentProvider = opts.contentProvider || 'slogxt';
      !!opts.mainView && (this.mainView = opts.mainView);
      opts.actionBase && this._sxtCacheSet('dialog', 'actionBase', opts.actionBase);
      !!opts.onViewInitialized && (this.onViewInitialized = opts.onViewInitialized);
      !!opts.onChangedActiveTab && (this.onChangedActiveTab = opts.onChangedActiveTab);
      !!opts.onDialogClosed && (this.onDialogClosed = opts.onDialogClosed);
    },
    dialogOptions: function () {
      var options = {
        title: this.startTitle,
        dialogClass: 'slogxt-edit-' + _sxt.helper.toClass(this.editTarget),
        slogxt: {
          dialogId: 'XtEditFormView',
          restoreState: true,
          isWizard: true,
          provideWizardPath: this.provideWizardPath || false,
          contentData: {
            provider: this.contentProvider,
            path: this.startPath || this.editTarget + '/' + this.entityId + '/actions'
          }
        }
      };
      return options;
    }
  };

  // Backbone View for ....
  _sxt.XtEditFormView = _sxt.DialogViewBase.extend(xtEditFormExt);

})(jQuery, Drupal);
