/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var slogLoginExt = {
    sxtThisClass: 'slogxt.LoginView',
    onPreInitialize: function (opts) {
      this.startTarget = opts.startTarget || 'login';
    },
    dialogOptions: function () {
      if (!drupalSettings.slogxt.user) {
        slogErr('Unable to open login dialog.');
        return {};  // error
      }

      var isSmall = this.isScreenSmall
              , tabsData = {
                targets: ['login', 'password'],
                startTarget: this.startTarget,
                login: {
                  labels: {
                    dialogTitle: Drupal.t('Log in'),
                    submitLabel: Drupal.t('Log in')
                  },
                  path: 'user/login'
                },
                password: {
                  labels: {
                    dialogTitle: isSmall ? Drupal.t('New password') : Drupal.t('Request new password'),
                    submitLabel: isSmall ? Drupal.t('New password') : Drupal.t('Request new password')
                  },
                  path: 'user/password'
                },
              }
      ;
      if (drupalSettings.slogxt.user.canRegister) {
        tabsData.targets.push('register')
        tabsData.register = {
          labels: {
            dialogTitle: isSmall ? Drupal.t('New account') : Drupal.t('Create new account'),
            submitLabel: isSmall ? Drupal.t('New account') : Drupal.t('Create new account')
          },
          path: 'user/register'
        };
      }

      return {
        title: tabsData.login.title,
        position: {
          my: 'right top',
          at: 'right-6px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'LoginView',
          restoreState: true,
          btnSubmitShow: true,
          tabsData: tabsData
        }
      };
    }
  };

  /**
   * Backbone View for login forms
   */
  _sxt.LoginView = _sxt.DialogViewBase.extend(slogLoginExt);

})(jQuery, Drupal, drupalSettings);
