/**
 * @file
 * A Backbone View for History Dialog.
 */

(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var xtPropsViewExt = {
    sxtThisClass: 'sxt_slogitem.XtMainPropsView',
    actions: {},
    nix: {}
  };

  var actionsExt = {
    __actionBookmark: function () {
      alert('XtMainPropsView::__actionBookmark');
    },
    __actionSave: function () {
      alert('XtMainPropsView::__actionSave');
    }
  };
  xtPropsViewExt.actions.sxt_slogitem = actionsExt;

  /**
   * Backbone View for Content Properties Dialog
   */
  _sxt.XtMainPropsView = _sxt.XtEditFormView.extend(xtPropsViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
