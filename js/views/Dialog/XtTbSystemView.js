/**
 * @file
 * A Backbone View for Bookmark Dialog
 */

(function ($, Drupal, drupalSettings, _) {

  "use strict";

  var _sxt = Drupal.slogxt
      , _sxth = _sxt.helper;

  var xtTbSysExt = {
    sxtThisClass: 'slogxt.XtTbSystemView',
    actions: {},
    onPreInitialize: function (opts) {
      this.tbSysInfoData = _sxt.getTbSysInfoData();//dummy
      this.tbSysTarget = opts.tbSysTarget;
      this.startTarget = opts.startTarget;
      this.showNavigateBtn = true;
      this.curNodeId = 0;
    },
    dialogOptions: function () {
      var actions = _sxt.getTbSysActions();
      var options = {
        title: 'TbSystemDialog',
        dialogClass: 'xt-tbsystem',
        slogxt: {
          dialogId: 'TbSystemDialog',
          restoreState: true,
          btnSubmitShow: false,
          hasPushpin: false,
          actionsForRadios: true,
          tabsData: this.buildTabsData(),
          actions: Drupal.theme.slogxtDropbutton(actions)
        }
      };
      return options;
    },
    buildTabsData: function () {
      if (!_.isEmpty(this.tbSysInfoData)) {
        var tbSysTarget = this.tbSysTarget || '???'
            , data = this.tbSysInfoData
            , targets = []
            , tabsData = {};
        for (var idx in data) {
          var item = data[idx] || ''
              , entityid = item.entityid
              , liTitle = item.liTitle
              , title = tbSysTarget.capitalize() + ': ' + liTitle
              , path = item.path
              , splitted = path.split('/xtajx/')
              , tabData = {
                targetid: entityid,
                useActions: true,
                labels: {
                  dialogTitle: title,
                  tabLabel: liTitle
                },
                contentHtmlCallback: this.buildTabContentHtml
              };

          if (splitted.length === 2) {
            var bpProvider = drupalSettings.slogxt.basePathProvider || {}
            , provider = (splitted[0]).replace('/', '');
            !!bpProvider[provider] && (provider = bpProvider[provider]);
            tabData.provider = provider;
            tabData.path = splitted[1];
          } else {
            slogErr('Invalid path: ' + item.path);
          }

          targets.push(entityid);
          tabsData[entityid] = tabData;
        }

        tabsData.targets = targets;
        if (_.indexOf(targets, this.startTarget) >= 0) {
          tabsData.startTarget = this.startTarget;
        }
        return tabsData;
      }
    },
    buildTabContentHtml: function (targetid) {
      function getResolved(resolve_id) {
        var cmView = _sxt.xtContentMainView || false
            , retval = resolve_id + _sxt.getResolveDelim()
            , entity_id = 0;
        if (targetid === _sxt.getTbSysNode()) {
          if (cmView && cmView.hasTbnodeVisible()) {
            entity_id = parseInt(cmView.getTbnodeId() || 0, 10);
            if (entity_id > 0) {
              this.curNodeId = entity_id;
              return (retval + entity_id);
            }
          }
          this.curNodeId = 0;
        }
        return false;
      }

      if (this.loading[targetid]) {
        return;
      }

      var tbSysTarget = this.tbSysTarget || '???'
          , actionData = _sxt.getMainActionData(tbSysTarget)
          , rootterm_ids = actionData.rootterm_ids || {}
      , idx_resolve = targetid + '::resolve'
          , rootterm_id = parseInt(rootterm_ids[targetid], 10)
          , resolve_id = rootterm_ids[idx_resolve]
          , tabData = this.tabsData[targetid] || {}
      , path = tabData.path || false
          , msg = _sxt.unknownError;
      if (!path) {
        return Drupal.theme.slogxtMessage(msg, 'error', true);
      }

      !!resolve_id && (rootterm_id = getResolved.call(this, resolve_id));
      if (!rootterm_id) {
        if (!!resolve_id && targetid === _sxt.getTbSysNode()) {
          msg = Drupal.t('Current content has no submenu.');
          return Drupal.theme.slogxtMessage(msg, 'warning', true);
        }
        return Drupal.theme.slogxtMessage(msg, 'error', true);
      }

      this.loading[targetid] = true;
      setTimeout($.proxy(function () {
        delete this.loading[targetid];
      }, this), 3000);

      path = path.replace('sysresolve__' + targetid, rootterm_id);
      return this._sxtPlaceholderHtml(_sxth.toPath(path), targetid, actionData.provider);
    },
    onChangedActiveTab: function (model, data) {
      if (!this.actionWizardBaseData) {
        this._sxtSetDropbuttonDisabled(true);
        this._sxtDelayedClick(this.$activeContent.find('ul>li.is-current'));
      }
    },
    onDonReturn: function () {
      var targetid = this._sxtGetActiveTabId()
          , cmView = _sxt.xtContentMainView || false
          , nid = (cmView && cmView.hasTbnodeVisible())
          ? parseInt(cmView.getTbnodeId() || 0, 10) : 0;
      if (nid !== this.curNodeId) {
        this.resetNodePage();
        if (targetid === _sxt.getTbSysNode()) {
          this.model.slogxtReTrigger('change:activeTab');
        }
      }
    },
    resetNodePage: function () {
      var targetid = _sxt.getTbSysNode()
          , pageData = this.pagesData[targetid] || {}
      , contentId = pageData.contentId || false;
      !!this.loading[targetid] && (delete this.loading[targetid]);
      !!this.loaded[targetid] && (delete this.loaded[targetid]);
      contentId && this.$Content.find('#' + contentId).remove();
    }
  };

  var actionsExt = {
    actionXxx: function () {
    }
  };
  xtTbSysExt.actions.slogxt = actionsExt;

  /**
   * Backbone View for Bookmark Dialog
   */
  _sxt.XtTbSystemView = _sxt.DialogViewBase.extend(xtTbSysExt);

})(jQuery, Drupal, drupalSettings, _);
