/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var msgExt = {
    sxtThisClass: 'slogxt.XtMessageView',
    titlePrefixes: {
      info: Drupal.t('Info'),
      warning: Drupal.t('Warning'),
      error: Drupal.t('Error')
    },
    onPreInitialize: function (opts) {
      this.xtMsgOpts = opts || {};
    },
    storageKey: function () {
      if (this.xtMsgOpts && this.xtMsgOpts.path) {
        return _sxt.storageKey('XtMsg', this.xtMsgOpts.path);
      }
    },
    dialogOptions: function () {
      var msgOpts = this.xtMsgOpts || {}
      , hasPath = (msgOpts.type === 'path')
          , hasContent = (msgOpts.type === 'content')
          , pre = this.titlePrefixes[msgOpts.status || '']
          , prefix = pre ? pre + ': ' : ''
          , status = msgOpts.status || 'info'
          , contentData = {
            labels: {
              dialogTitle: prefix + (msgOpts.title || '...'),
            }
          }
      , options = {
        dialogClass: 'xtmessage xtmsg-' + status,
        position: {
          my: 'right top',
          at: 'right-10px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'XtMessageView',
          restoreState: false,
          btnSubmitShow: false,
          btnBackShow: false,
          hasPushpin: false
        }
      };

      if (hasContent) {
        contentData.contentHtml = msgOpts.contentHtml || '...';
      } else if (hasPath) {
        if (msgOpts.cachable) {
          var storageKey = this.storageKey()
              , cached = storageKey ? JSON.parse(sessionStorage.getItem(storageKey)) : false;
          cached && (contentData.contentHtml = cached);
        }
        if (!contentData.contentHtml) {
          contentData.path = msgOpts.path;
          contentData.provider = msgOpts.provider;
        }
      } else {
        slogErr('XtMessageView.dialogOptions: not handled - type: msgOpts.type');
      }

      options.slogxt.contentData = contentData;
      return options;
    },
    onAjaxResponseDone: function ($c, data) {
      if (data.message && this.xtMsgOpts.cachable) {
        var storageKey = this.storageKey();
        sessionStorage.setItem(storageKey, JSON.stringify(data.message));
      }
    }

  };

  /**
   * Backbone View for message forms
   */
  _sxt.XtMessageView = _sxt.DialogViewBase.extend(msgExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
