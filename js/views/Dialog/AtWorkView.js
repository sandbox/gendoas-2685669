/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var constExt = {
    sxtThisClass: 'slogxt.AtWorkView',
    onPreInitialize: function (opts) {
      this.infoText = opts.infoText || 'under construction';
    },
    dialogOptions: function () {
      var infoContent = _tse('div', {class: 'sxt-at-work-info'}, '... ' + this.infoText)
      , options = {
        dialogClass: 'at-work',
        position: {
          my: 'right top',
          at: 'right-10px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'AtWorkDialog',
          restoreState: false,
          btnSubmitShow: false,
          btnBackShow: false,
          hasPushpin: false,
          contentData: {
            labels: {
//              dialogTitle: Drupal.t('Under construction: ')  + infoText
              dialogTitle: 'Under construction: ' + this.infoText
            },
            contentHtml: _tse('div', {class: 'sxt-at-work'}, infoContent),
          }
        }
      };
      return options;
    }
  };

  /**
   * Backbone View for login forms
   */
  _sxt.AtWorkView = _sxt.DialogViewBase.extend(constExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
