/**
 * @file
 * A Backbone View for a Slogitem List.
 */

(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

// _sxtDialogDefaults

  var _sxt = Drupal.slogxt
      , _sxth = _sxt.helper
      , _tse = Drupal.theme.slogxtElement;

  var dialogExt = {
    sxtThisClass: 'slogxt.DialogViewBase',
    actions: {},
    actionStartIdx: 100,
    iconMaxClassesAll: 'ui-icon-arrow-4-diag ui-icon-newwin',
    // hooks
    // do not put hooks into components (e.g. dvc.ajax.js) !!!
    // this would override real hook functions !!!
    dialogOptions: $.noop, // override by the special dialog
    onPreInitialize: $.noop, // hook: initialize function entered
    onViewInitialized: $.noop, // hook: view is initialized
    onDialogCreated: $.noop, // hook: dialog is created
    onDialogClosed: $.noop, // hook: dialog is closed
    onChangedCheckboxCount: $.noop, // hook: dialog is closed
    onTabsCreated: $.noop, // hook: after tabs has been created
    onTabContentCreated: $.noop, // hook: after tab content has been created
    onPreChangeActiveTab: $.noop, // hook: before tab and content are changed
    onChangedActiveTab: $.noop, // hook: after tab and content are changed
    onListItemClicked: $.noop, // hook: 
    onFormSubmitSuccess: $.noop, // hook: 
    onFormSubmitError: $.noop, // hook: 
    onPreAjaxLoadContent: $.noop, // hook, maybe load from cache: 
    ajaxAlterBeforeTrigger: $.noop,
    ajaxFormBeforeSubmit: $.noop,
    ajaxFormOnSuccess: $.noop,
    onAjaxResponseDone: $.noop,
    showDialog: function () { // hook: customize by override
      this._sxtShowDialog();
    },
    //
    _sxtComponentAdd: function (component, targetObj) {
      if (!this.componentDone[component]) {
        targetObj = targetObj || this;
        $.extend(true, targetObj, _sxt.dvc[component] || {});
        this.componentDone[component] = true;
      }
    },
    events: function () {
      function triggerNext(e) {
        if (e.keyCode === 13 && this.$btnSubmit) {
          if (this.isWizard) {
            var do_submit = true
                , pageData = this._sxtWizardPageData()
                , slogxtData = pageData.slogxtData || {}
            , $submit = slogxtData.$submit || false;
            $submit && (do_submit = !$submit.hasClass('button--danger'));
            do_submit && this.$btnSubmit.click();
          } else {
            this.$btnSubmit.click();
          }
          return false;
        }
      }
      return {'keypress input': triggerNext};
    },
    initialize: function (options) {
//      slogLog('DialogViewBase..........initialize');
      this.componentDone = {};  // track added components
      this._sxtComponentAdd('cache');
      // reset all cache
      this._sxtCacheReset();

      this.onPreInitialize(options); //hook
      this.isForNavigate = options.isForNavigate || false;
      this.isForNavigate && (this.showNavigateBtn = false);
      this._sxth = _sxth;
      // add core components
      this._sxtComponentAdd('init');
      this._sxtComponentAdd('dom');
      this._sxtComponentAdd('tabs');
      this._sxtComponentAdd('ajax');
      this._sxtComponentAdd('actions', this._sxtActions);
      this._sxtComponentAdd('commands', this._sxtCommands);
      this._sxtComponentAdd('hooks', this._sxtHooks);
      this._sxtComponentAdd('donavigate');
      // init this view ad dialog
      this._sxtInitThisView(options);
      this._sxtInitDialog();

      this.listenTo(this.model, 'change:activeTab', this._sxtOnChangeActiveTab);
      this.listenTo(this.model, 'change:pushpinChecked', this._sxtOnChangePushpinChecked);
      this.listenTo(this.model, 'change:actionsOpen', this._sxtOnChangeActionsOpen);
      this.listenTo(this.model, 'change:widgetHeight', this._sxtOnChangedWidgetHeight);
      this.listenTo(this.model, 'change:maximized', this._sxtOnChangeMaximized);
      this.listenTo(this.model, 'change:contentCheckedItems', this._sxtOnChangeContentCheckedItems);
      this.listenTo(this.model, 'change:dropbuttonIsOpen', this._sxtOnChangeDropbuttonIsOpen);
      this.listenTo(this.model, 'change:wizardCurrentPage', this._sxtOnChangeWizardCurrentPage);

      this.listenTo(this.screenModel, 'change:isSmall', this._sxtOnChangeIsSmall);
      this.listenTo(this.screenModel, 'change:windowDims', this._sxtOnChangeWindowDims);

      if (!this.isForNavigate) {
        this.$body.on('click .ui-widget-overlay', $.proxy(this._sxtDialogOverlayClick, this));
        this.donView = _sxt.getDoNavigateView();
        if (this.donView) {
          this.listenTo(this.donView.model, 'change:donResult', this._sxtOnChangeDonResult);
        }
      }

      this.onViewInitialized(); //hook
      return this;
    },
    _sxtSetOverlayTransparent: function (value) {
      var $overlay = this.$dialogWrapper.find('>.ui-widget-overlay');
      $overlay.toggleClass('xt-transparent', value);
      $overlay.css('opacity', '');
    },
    _sxtInitThisView: function (options) {  // on initialize, before create
      (!this.model) && (this.model = new Backbone.Model());
      options = options || {};
      this.startOptions = options.startOptions || false;
      this.$body = $('body');
      this.$dialogWrapper = $('#slogxt-modal-dialog-wrapper');
      this.$pageWrapper = $('#page-wrapper');
      this.$btnSubmit = $();
      this.$btnBack = $();
      this.$btnItemAction = $('<div id="sxt-item-action"></div>');
      this.isScreenSmall = _sxt.isScreenSmall();
      this.screenModel = _sxt.screenModel();
      this.tabsId = 'slogxt-dialog-tabs';
      this.tabsHelperID = 'slogxt-dialog-tabs-helper';
      this.contentID = 'slogxt-dialog-content';
      this._sxtSyncDialogWrapper();
    },
    _sxtDestroyThisView: function () {
      //prevent multiple event handling
      !this.isForNavigate && this.$body.off('click .ui-widget-overlay');
      // remove circular references
      delete this.dialog.view;
      _.each(this.loaded, function (contentId) {
        // ajax objects have a pointer to this view
        _sxt.ajax[contentId] && (delete _sxt.ajax[contentId]);
      });

      // destroy child views if any and stop listening
      this.slogxtDestroy();
      // slogxtDestroy doesn't remove $el
      this.$el.remove();
    },
    _sxtShowDialog: function () {
      if (this.isDialogModal && !_sxt.canOpenModalDialog(this.isForNavigate)) {
        slogErr('_sxtShowDialog: Modal dialog is already open: ' + this.sxtThisClass);
      } else {
        _sxt.setIsOpenModalDialog(this.dialog);
        this.isDialogModal ? this.dialog.showModal() : this.dialog.show();
        this.$el.dialog('open');
        !this.isWizard && this._sxtTabsMoveHelper();
        this.restoreState && this._sxtStateRestore();
        this.$widget.sxtSetVisualHidden(false);
        this._sxtDialogSetTitleMax();
      }
    },
    _sxtCloseWithDelay: function (delay) {
      delay = delay || 1000;
      setTimeout($.proxy(function () {
        this.$btnClose.click();
      }, this), delay);
    },
    _sxtStateRestore: function () {
      var isSmall = !!this.screenModel.get('isSmall')
          , maxExplicit = !!this.model.get('maximizedExplicit');
      this._sxtDoMaximize(isSmall || maxExplicit);
    },
    _sxtStateWrite: function () {
      if (this.restoreState) {
        var stateDialog = this.screenModel.get('stateDialog')
            , stateThis;
        stateDialog[this.dialogId] = stateDialog[this.dialogId] || {};
        stateThis = stateDialog[this.dialogId];
        stateThis['maximized'] = this.model.get('maximizedExplicit') || false;
        stateThis['widthPercent'] = this.model.get('widthPercent') || false;
        stateThis['initPosition'] = this.model.get('initPosition') || this._sxtGetDefaultInitPosition();
      }
    },
    _sxtRestoreState: function () {
      if (this.restoreState) {
        var stateDialog = this.screenModel.get('stateDialog')
            , stateThis = stateDialog[this.dialogId] || false;
        if (stateThis) {
          this.model.set('maximizedExplicit', stateThis['maximized']);
          this.model.set('widthPercent', stateThis['widthPercent']);
          this.model.set('initPosition', stateThis['initPosition']);
        }
      }
    },
    _sxtGetDefaultInitPosition: function () {
      return {
        left: 40,
        top: 60
      }
    },
    _sxtStateLoad: function () {
      var stateDialog = this.screenModel.get('stateDialog')
          , stateThis = stateDialog[this.dialogId] || {};
      this.model.set('maximizedExplicit', !!stateThis['maximized']);
      this.model.set('widthPercent', stateThis['widthPercent'] || '50%');
      this.model.set('initPosition', stateThis['initPosition'] || this._sxtGetDefaultInitPosition());
    },
    _sxtActions: {slogxt: {}}, // extra namespace for each provider - actions
    _sxtCommands: {slogxt: {}}, // extra namespace for each provider - commands
    _sxtHooks: {slogxt: {}}, // extra namespace for each provider - hooks
    _sxtHookCall: function (hook, args) {
      var callback = this._sxtResolveCallback(hook, undefined, 'hook');
      if (callback && $.isFunction(callback)) {
        callback.apply(this, args);
      } else {
        slogErr('Hook not found: ' + hook);
      }
    },
    _sxtGetCallbackNamespace: function (type, provider) {
      var ns = false;
      switch (type) {
        case 'action':
          ns = this._sxtActions[provider] || false;
          break;
        case 'command':
          ns = this._sxtCommands[provider] || false;
          break;
        case 'hook':
          ns = this._sxtHooks[provider] || false;
          break;
      }
      return ns;
    },
    _sxtResolveCallback: function (rawCommand, rawProvider, type) {
      rawProvider = rawProvider || 'slogxt';
      type = type || 'command';
      var parts = String(rawCommand).split('::')
          , hasProvider = (parts.length === 2)
          , provider = hasProvider ? parts[0] || rawProvider : rawProvider
          , command = hasProvider ? parts[1] : parts[0]
//          , ns = this._sxtCommands[provider] || false
          , ns = this._sxtGetCallbackNamespace(type, provider)
          , callback = ns ? ns[command] || false : false;
      return callback;
    },
    _sxtIsCallback: function (rawCommand, rawProvider) {
      var callback = this._sxtResolveCallback(rawCommand, rawProvider);
      return (callback && $.isFunction(callback));
    },
    _sxtHasCommandCallback: function (items, provider) {
      var thisView = this
          , hasCallback = false;
      $.each(items, function (key, cmd) {
        if (thisView._sxtIsCallback(cmd, provider)) {
          hasCallback = true;
          return false;
        }
      });
      return hasCallback;
    },
    _sxtCommandCallback: function (rawCommand, rawProvider) {
      var callback = this._sxtResolveCallback(rawCommand, rawProvider);
      if (callback && $.isFunction(callback)) {
        return callback;
      } else {
        var parts = rawCommand + ' / ' + rawProvider;
        slogErr('DialogViewBase._sxtCommandCallback: Callback not found: ' + parts);
        return false;
      }
    },
    // ***
    _sxtDialogPosOpts: function (options) {
      var posOpts = ['width', 'height', 'minWidth', 'minHeight', 'maxHeight', 'maxWidth', 'position'];
      return _.pick(options, posOpts);
    },
    _sxtThrobberHtml: function () {
      return Drupal.theme.slogxtThrobber();
    },
    _sxtThrobberAddMsg: function ($el, msg) {
      Drupal.theme.slogxtThrobberAddMsg($el, msg);
    },
    _sxtSetTitleText: function (text) {
      this.$titlebar.find('span.ui-dialog-title').html(text);
    },
    _sxtSetButtonText: function ($btn, text) {
      $btn.text(text);
    },
    _sxtSetButtonsFinalized: function (closeLabel) {
      closeLabel = closeLabel || Drupal.t('Close');
      this._sxtSetButtonText(this.$btnClose, closeLabel);
      this._sxtSetCloseOnEscape(!this.doReload);
      if (this.doReload) {
        this.$titlebar.find('.ui-dialog-buttonset').hide();
      } else {
        this.$titlebar.find('.ui-dialog-buttonset').show();
      }
      this.$btnClose.button('enable');
      this.$btnClose.focus();
      this.$btnClose.siblings().hide();
    },
    _sxtReload: function (execute) {
      this.doReload = true;
      this._sxtSetButtonsFinalized(Drupal.t('Reload'));
      this.$btnClose.button('enable');
      if (!!execute) {
        setTimeout($.proxy(function () {
          this._sxtReloadExecute();
        }, this), 10);
      }
    },
    _sxtReloadExecute: function () {
      this.$activeContent.find('.messages.messages--error').remove();
      var $msg = this.$activeContent.find('.messages__wrapper');
      this.$activeContent.html(this._sxtThrobberHtml());
      this._sxtThrobberAddMsg(this.$activeContent, Drupal.t('Reloading. Please wait...'));
      $msg.length && this.$activeContent.prepend($msg);
      this.$btnClose.button('disable');
      this._sxtDialogAdjust();
      location.reload();
    },
    _sxtLogoutExecute: function () {
      this.$activeContent.find('.messages.messages--error').remove();
      var $msg = this.$activeContent.find('.messages__wrapper');
      this.$activeContent.html(this._sxtThrobberHtml());
      this._sxtThrobberAddMsg(this.$activeContent, Drupal.t('Logout. Please wait...'));
      $msg.length && this.$activeContent.prepend($msg);
      this.$btnClose.button('disable');
      this._sxtDialogAdjust();
      _sxt.logoutExecute();
    },
    _sxtPlaceholderHtml: function (path, targetid, provider) {
      var baPath = _sxt.baseAjaxPath(provider)
          , prefix = (path.indexOf(baPath) >= 0) ? '' : baPath
          , opts = {
            class: 'slogxt-ajax-placeholder',
            'data-path': prefix + _sxth.toPath(path.replace(/^\//, '')),
            targetid: targetid
          };
      return _tse('div', opts);
    },
    _sxtSetHtml: function ($el, html) {
      $el.html(html);
      setTimeout($.proxy(function () {
        this._sxtDialogAdjust();
      }, this), 10);
    },
    _sxtTabContentHtml: function (targetid) {
      var tabData = this.tabsData[targetid] || {}
      , callback = tabData.contentHtmlCallback || false;

      if (!!tabData.contentHtml) {
        return tabData.contentHtml;
      } else if (callback && _.isFunction(callback)) {
        return callback.call(this, targetid);
      } else if (!!tabData.path) {
        return this._sxtPlaceholderHtml(_sxth.toPath(tabData.path), targetid, tabData.provider);
      }
    },
    _sxtDialogSetFocus: function ($content) {
      if (!this.isScreenSmall) {
        $content = $content || this.$activeContent;
        var $fields = $content.find('form .sxt-input-current');
        !$fields.length && ($fields = $content.find('form input:visible:enabled'));
        $fields.first().focus();
      }
    },
    _sxtDialogSetLabels: function (labels) {
      if (labels) {
        labels.dialogTitle && this._sxtSetTitleText(labels.dialogTitle);
        labels.submitLabel && this._sxtSetButtonText(this.$btnSubmit, labels.submitLabel);
        labels.backLabel && this._sxtSetButtonText(this.$btnBack, labels.backLabel);
      }
    },
    sdHasPrevUnskippedPageIdx: function (startIdx) {
      if (this.isWizard && !this.wizardFinished) {
        if (startIdx + 1 === this.actionStartIdx) {
          return true;
        }

        var pData = this.pagesData || [], sData, i;
        for (i = startIdx; i >= 0; i--) {
          sData = pData[i] ? pData[i].slogxtData : false;
          if (sData && !sData.skipPage)
            return true;
        }
      }
      return false;
    },
    _sxtIsWizardCheckboxes: function (pageData) {
      if (this.isWizard) {
        pageData = pageData || this._sxtWizardPageData();
        return (pageData.type && pageData.type === 'checkboxes');
      }
      return false;
    },
    _sxtHtmlListCheckboxes: function (data, ul_opts, headerStyle, icons) {
      return Drupal.theme.slogxtListCheckboxes(data, ul_opts, headerStyle, icons);
    },
    _sxtHtmlListRadios: function (data, ul_opts, headerStyle, icons) {
      return Drupal.theme.slogxtListRadios(data, ul_opts, headerStyle, icons);
    },
    _sxtDialogSubmit: function () {
//      slogLog('DialogView::....._sxtDialogSubmit..........._sxtDialogSubmit....');
      this.$btnSubmit.button('disable');
      this.isWizard ? this._sxtWizardGoNext() : this._sxtActiveTabSubmit();
    },
    getWizardCurPageIdx: function () {
      return (this.model.get('wizardCurrentPage') || 0);
//      return this.model.get('wizardCurrentPage');
    },
    _sxtCallFinishedActionsWizard: function () {
      this._sxtCommandCallback('finishedActionsWizard').call(this);
    },
    _sxtDialogBack: function () {
      if (this.isWizard) {  // back for wizard only
        var curPageIdx = this.getWizardCurPageIdx()
            , sxtd = this._sxtWizardPageData().slogxtData || {};
        if (!this.actionWizardBaseData || curPageIdx > 0) {
          this._sxtWizardGoBack();
        } else if (sxtd.trackFormPages && (sxtd.isOnPreview || !sxtd.isOnFirst)) {
          this._sxtWizardGoBack();
        } else {
          this._sxtActionsWizardReset();
        }
      }
    },
    _sxtHasActionStartPage: function () {
      return !!this.pagesData[this.actionStartIdx];
    },
    _sxtGetOptions: function () {
      return this.$el.dialog('option');
    },
    _sxtSetOptions: function (options) {
      this.$el.dialog('option', options);
    },
    _sxtSetCloseOnEscape: function (val) {
      this._sxtSetOptions({closeOnEscape: val});
    },
    _sxtIsEditForm: function () {
      var tabData = this.isWizard ? this._sxtWizardPageData().tabData || {} : {};
      return tabData.isEditForm;
    },
    _sxtCalculateCheckedItems: function () {
//      slogLog('_sxt.DialogViewBase::....._sxtCalculateCheckedItems');
      var $checked = this.$activeContent.find('li.checked')
          , silent = this._sxtIsEditForm()
          , args = silent ? {silent: true} : {}
      , data = {
        $checked: $checked,
        checked: $checked.length,
        from: this.$activeContent.find('li').length
      };
      this.model.set('contentCheckedItems', data, args);
      return data;
    },
    _sxtDialogOverlayClick: function (e) {
//      var xx_io = _sxt.isOpenModalDialog();
//        slogLog('_sxt.DialogView::....._sxtDialogOverlayClick...isOpenModalDialog: ' + xx_io);
      var $target = $(e.target);
      if (_sxt.isOpenModalDialog() && $target.hasClass('ui-widget-overlay')) {
        e.stopPropagation();
        var tabData = this.isWizard ? this._sxtWizardPageData().tabData || {} : {}
        , final = tabData.transparentOverlay ? 0 : 0.3;
        $target.fadeTo(10, 0.6, function () {
          $target.fadeTo('slow', final);
          return false;
        });
      }
      return false;
    },
    _sxtOnChangeIsSmall: function (screenModel, isSmall) {
//      slogLog('_sxt.DialogView::....._sxtOnChangeIsSmall: ' + isSmall);
      if (this.$btnMaximize) {
        var doMaximize = isSmall || this.model.get('maximizedExplicit');
        this._sxtDoMaximize(doMaximize);
        !isSmall && this.model.slogxtSetAndTrigger('change:maximized', null, doMaximize);
      }
    },
    _sxtDoMaximize: function (maximize) {
      this.$widget.toggleClass('maximized', maximize);
      this.model.set('maximized', maximize);
    },
    _sxtSyncDialogWrapper: function () {
//      slogLog('_sxt.DialogView::....._sxtSyncDialogWrapper.');
      var pwpos = this.$pageWrapper.position();
      var styles = {
        top: -pwpos.top,
        left: -pwpos.left,
        height: this.$pageWrapper.height() + pwpos.top - 4,
        width: this.$pageWrapper.width() + pwpos.left
      };
      this.$dialogWrapper.css(styles);
    },
    _sxtOnChangeWindowDims: function (screenModel, winDims) {
//      slogLog('_sxt.DialogView::....._sxtOnChangeWindowDims.');
      this._sxtSyncDialogWrapper();
      this._sxtDialogAdjust(true);
      this.donView && this.donView.model.get('isActive') && this.donView.toEdge();
    },
    _sxtOnChangePushpinChecked: function (model, checked) {
      if (this.hasPushpin) {
        this.$btnPushpin.toggleClass('checked', checked);
        !checked && this.$btnActionsOpen && this._sxtActionsOpen(checked);
      }
    },
    _sxtOnChangeActionsOpen: function (model, open) {
      if (this.$btnActionsOpen) {
        this.$btnActionsOpen.toggleClass('checked', open);
        this.$buttonpane.toggleClass('editing', open);
        this.$activeContent.toggleClass('type-checkboxes', open);
        open && this.hasPushpin && this._sxtPushpinChecked(open);
        this._sxtShowActionsCheckboxes(open);
        open && this._sxtCalculateCheckedItems();
      }
    },
    _sxtButtonShow: function ($btn, show) {
      $btn.sxtSetVisualHidden(!show);
      show && $btn.css('display', 'inline-block');
    },
    _sxtButtonChecked: function (attribKey, checked) {
      if (_.isBoolean(checked)) {
        this.model.set(attribKey, checked);
      }
      return this.model.get(attribKey) || false;
    },
    _sxtPushpinChecked: function (checked) {
      return this._sxtButtonChecked('pushpinChecked', checked);
    },
    _sxtActionsOpen: function (checked) {
      return this._sxtButtonChecked('actionsOpen', checked);
    },
    _sxtActionsReOpen: function () {
      this._sxtActionsOpen(false);
      this._sxtActionsOpen(true);
    },
    _sxtSetWidgetHeight: function (reset) {
      !!reset && this.model.set('widgetHeight', 0, {silent: true});
      this.model.set('widgetHeight', this.$widget.height());
    },
    _sxtOnChangedWidgetHeight: function (m, height) {
//      slogLog('_sxt.DialogViewBase::....._sxtOnChangedWidgetHeight: ' + height);
      var $textarea = this.$activeContent.find('.form-textarea').last()
          , $field = $textarea.closest('.slogxt-input-field')
          , $form = $field.closest('form');

      if ($form.length) {
        var field_height = height - this.$titlebar.height() - this.$buttonpane.height()
            , prev_height = $textarea[0].offsetTop
            , tmp_prev = prev_height
            , tmp_next = $textarea.closest('.js-form-item').next().outerHeight()
            , next_height = !!tmp_next ? tmp_next + 30 : 40;
        $.each($textarea.parentsUntil($form), function (idx, el) {
          prev_height += (tmp_prev - el.offsetTop);
          tmp_prev = el.offsetTop;
        });

        field_height -= (prev_height + next_height);
        $textarea.height(Math.max(field_height, 80));
      }
    },
    _sxtOnChangeMaximized: function (m, maximized) {
//      slogLog('_sxt.DialogViewBase::....._sxtOnChangeMaximized: ' + maximized);
      var title = maximized ? Drupal.t('Restore') : Drupal.t('Maximize')
          , removeClass = this.iconMaxClassesAll
          , addClass = maximized ? 'ui-icon-newwin' : 'ui-icon-arrow-4-diag'
          , enable = maximized ? 'disable' : 'enable';
      this.$widget.resizable(enable);
      this.$btnMaximize.attr('title', title);
      this.$btnMaximizeIcon.removeClass(removeClass);
      this.$btnMaximizeIcon.addClass(addClass);
      this._sxtDialogAdjust();
      this._sxtSetWidgetHeight();
    },
    _sxtOnChangeContentCheckedItems: function (model, chkCount) {
//      slogLog('_sxt.DialogViewBase::....._sxtOnChangeContentCheckedItems');
      chkCount = chkCount || {};
      chkCount.checked = chkCount.checked || 0;
      chkCount.from = chkCount.from || 0;
      var $checkbox = this.$actionsCheckbox
          , enable = (chkCount.checked > 0) ? 'enable' : 'disable';
      if ($checkbox) {
        var isChecked = (chkCount.checked > 0 && chkCount.checked === chkCount.from)
            , isUndefined = (chkCount.checked > 0 && !isChecked);
        $checkbox.removeClass('checked undefined');
        isChecked && $checkbox.addClass('checked');
        isUndefined && $checkbox.addClass('undefined');
      }
      this.$btnSubmit.button(enable);
      this.onChangedCheckboxCount(model, chkCount); // hook:
    },
    _sxtSelectNextUnchecked: function (curIdx) {
//      slogLog('...DialogViewBase::....._sxtSelectNextUnchecked');
      var $items = this.$activeContent.find('ul.slogxt-list > li.checkbox')
          , $prevUnchecked = false
          , curFound = false
          , done = false;
      $items.each(function () {
        var $item = $(this)
            , idx = parseInt($item.attr('index'), 10);
        if (!$item.hasClass('checked')) {
          if (curFound) {
            $item.click();
            done = true;
            return false; // all done
          }
          $prevUnchecked = $item;
        }
        (curIdx === idx) && (curFound = true);
      });
      !done && $prevUnchecked && $prevUnchecked.click(); // select previous
    },
    _sxtGetCheckedItemsIdxData: function (ensureCurrent) {
      ensureCurrent = !!ensureCurrent;
      var itemsData = this.model.get('contentCheckedItems') || {}
      , $checked = itemsData.$checked || false
          , idxData = false;
      if ($checked && itemsData.checked > 0) {
        var $curItem = $checked.filter('.is-current')
            , preserveCur = ensureCurrent && (itemsData.checked === itemsData.from);
        idxData = {
          checked: [],
          count: itemsData.checked,
          from: itemsData.from
        };
        idxData.curIdx = ($curItem.length)
            ? parseInt($curItem.attr('index'), 10) : -1;
        $checked.each(function () {
          var $item = $(this)
              , idx = parseInt($item.attr('index'), 10)
              , isCurrent = (idxData.curIdx === idx);
          (!isCurrent || !preserveCur) && idxData.checked.push(idx);
          isCurrent && (idxData.curIsChecked = true);
        });

//        if (ensureCurrent && !preserveCur && idxData.curIsChecked) {
//          this._sxtSelectNextUnchecked(idxData.curIdx);
//        }
      }

      return idxData;
    },
    _sxtOnChangeDropbuttonIsOpen: function (model, isOpen) {
//      slogLog('DialogView: _sxtOnChangeDropbuttonIsOpen.IsOpen: ' + isOpen);
      var $dbWidget = this.$buttonpane.find('.dropbutton-widget');
      if (isOpen) {
        var wHeight = $dbWidget.height()
            , liHeight = $dbWidget.find('li.dropbutton-action').height()
            , offset = $dbWidget.offset()
            , flip = (offset.top + wHeight + 10 > window.innerHeight)
            , margin = flip ? liHeight + 1 - wHeight : 0;
        $dbWidget.css('marginTop', margin);
      } else {
        $dbWidget.css('marginTop', 0);
      }
    },
    _sxtOnChangeWizardCurrentPage: function (model, curPageIdx) {
//      slogLog('DialogView: _sxtOnChangeWizardCurrentPage.curPageIdx: ' + curPageIdx);
      var pageData = this._sxtWizardPageData(curPageIdx)
          , slogxtData = pageData.slogxtData || {};
      slogxtData.skipPage && pageData.$content.addClass('visually-hidden');
      pageData.$tab && pageData.$tab.click();
      slogxtData.skipPage && this._sxtDialogSetLabels({dialogTitle: '...'});

    },
    _sxtDoNavigate: function (enforce) {
      var hasBtn = !this.$btnNavigate.hasClass('visually-hidden');
      if (!this.isForNavigate && (enforce || hasBtn)) {
        this._sxtDonStart({
          noDonSelect: true,
          hideSelect: true,
          onDonReturn: this.onDonReturn
        });
      }
    },
    _sxtShowWizardPath: function (pageIdx, show) {
      var pageData = this._sxtWizardPageData(pageIdx);
      if (pageData && pageData.$tab) {
        show ? pageData.$tab.show() : pageData.$tab.hide();
        var wizardPath = pageData.wizardPath || {};
        if (show && wizardPath.processed) {
          show = (wizardPath.processed === wizardPath.selected)
        }
        this._sxtShowWizardPath(pageIdx + 1, show);
      }
    },
    _sxtSetWizardPath: function (data) {
      var pageData = this._sxtWizardPageData()
          , label = _.isString(data) ? data : data.label || '???'
          , selected = data.selected || false;
      pageData.$tab.text(label);
      if (selected) {
        pageData.wizardPath.selected = selected;
        var pageIdx = this.getWizardCurPageIdx() + 1
            , show = pageData.wizardPath.processed === selected;
        this._sxtShowWizardPath(pageIdx, show);
      }
    },
    _sxtAttachRadios: function ($content) {
      $content.find('li.radio')
          .once('slogxt')
          .on('click', $.proxy(function (e) {
            var $li = $(e.target).closest('li')
                , entityid = $li.attr('entityid') || false
                , index = $li.attr('index') || false
                , id = $li.attr('id') || false;
            $li.addClass('checked is-current');
            $li.siblings().removeClass('checked is-current');
            if (this.provideWizardPath) {
              var tTmp = $li.find('.li-title').sxtGetTopText()
                  , selected = entityid || index || id
                  , wpData = {
                    label: (tTmp.split('.?')[0]).sxtTruncate(20),
                    selected: selected
                  };
              this._sxtSetWizardPath(wpData);
            }

            if (entityid && this.startEntityId && entityid === this.startEntityId) {
              this.$btnSubmit.button('disable');
            } else {
              this.$btnSubmit.button('enable');
            }
            this.onListItemClicked(e);  //hook
            return false;
          }, this));
    },
    _sxtDetachSlogxt: function ($content) {
      $content.removeOnce('slogxt').off();
      $content.off();
    },
    _sxtSetDropbuttonDisabled: function (disabled) {
      this.$actionsWrapper && this.$actionsWrapper
          .find('.dropbutton-wrapper')
          .toggleClass('disabled', disabled);
    },
    _sxtShowActionsCheckboxes: function (show) {
      if (!!this.actionsForRadios) {
        this.$actionsWrapper && this.$actionsWrapper.show();
      } else {
        if (!this._sxtIsEditForm()) {
          if (show) {
            if (!this.$actionsCheckbox) {
              this._sxtActionsCheckboxCreate();
            }
            this.$actionsCheckbox.show();
          } else {
            this.$actionsCheckbox && this.$actionsCheckbox.hide();
          }
        }
        this.$activeContent.toggleClass('type-checkboxes', show);
      }
    },
    _sxtDialogWithinDims: function ($target) {
      $target = $target || this.$body;
      return {
        width: $target.outerWidth(true),
        height: $target.height(),
        maxWidth: $target.outerWidth(true) - 20,
        maxHeight: $target.height() - (!!this.model.get('maximized') ? 20 : 90)
      };
    },
    _sxtPositionUsing: function (offset, feedback) {
      var withinDims = this._sxtDialogWithinDims(this.$dialogWrapper)
          , e = feedback.element  // this.$widget
          , isMaximized = !!this.model.get('maximized')
          , newOpts = false;

      if (!isMaximized) {
        if (e.height > withinDims.maxHeight) {
          newOpts = {height: withinDims.maxHeight};
        } else if (e.width > withinDims.maxWidth) {
          e.element.css('width', withinDims.maxWidth);
          e.width = e.element.width();
        }

        if (!newOpts) {
          var dX = withinDims.width - (e.left + e.width) - 10
              , dY = withinDims.height - (e.top + e.height) - 10;
          (dX < 0) && (offset.left += dX);
          (dY < 0) && (offset.top += dY);
        }
      }

      if (!!newOpts) {
        this._sxtSetOptions(newOpts);
      } else {
        this.$widget.css(offset);
      }
    },
    _sxtDialogAdjust: function (winResized) {
      if (!this.model.get('dialogCreated')) {
        return; // there is no dialog to adjust
      }

      var isMaximized = !!this.model.get('maximized')
          , withinDims = this._sxtDialogWithinDims(this.$dialogWrapper)
          , dialogOpts = this._sxtGetOptions()
          , curPosOpts = this._sxtDialogPosOpts(dialogOpts)
          , opts = {draggable: !isMaximized, resizable: !isMaximized};
      if (isMaximized) {
        if (winResized) {
          setTimeout($.proxy(function () {
            this._sxtSetWidgetHeight(true);
          }, this), 100);
        } else {
          this.model.set('restoreOpts', curPosOpts);
        }
        opts.height = withinDims.maxHeight;
      } else {
        if (winResized) {
//          slogLog('_sxt.DialogView::showDialog.........._sxtDialogAdjust');
          this.model.set('restoreOpts', curPosOpts);
          opts = curPosOpts;
          opts.position.using = $.proxy(this._sxtPositionUsing, this);
        } else {
          opts = $.extend(opts, this.model.get('restoreOpts') || dialogOpts);
        }
        opts.maxWidth = withinDims.maxWidth;
        opts.maxHeight = withinDims.maxHeight;
      }

      opts = this._sxtDialogPosOpts(opts);
      this._sxtSetOptions(opts);
      this._sxtDialogSetTitleMax();

      if (!isMaximized) {
        this.AdjustTO && clearTimeout(this.AdjustTO);
        this.AdjustTO = setTimeout($.proxy(function () {
          var dpos = this.$widget.position()
              , dtop = dpos.top
              , dh = this.$widget.height()
              , wh = this.$dialogWrapper.height();
          if (dtop + dh > wh) {
            var opos = opts.position;
            dtop = parseInt((wh - dh) / 2, 10);
            opos.at = 'left+' + dpos.left + ' top+' + dtop;
            this._sxtSetOptions(opts);
          }
          this.model.set('widgetHeight', dh);
        }, this), 10);
      }
    },
    _sxtDelayedClick: function ($item) {
      setTimeout(function () {
        $item.click();
      }, 10);
    },
    _sxtAlertNotSelected: function () {
      var $target = this.$widget;
      $target.fadeTo(10, 0.6, function () {
        $target.fadeTo('slow', 1.0);
        return false;
      });
    }
  };


  /**
   * Backbone View for ....
   */
  _sxt.DialogViewBase = Backbone.View.extend(dialogExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
