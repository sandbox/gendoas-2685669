/**
 * @file
 * A Backbone View for a Slogitem List.
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse, undefined) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var xtTbLineViewExt = {
    sxtThisClass: 'slogxt.TbLineView',
    actions: {},
    initializedReady: $.noop,
    activateHistoryButtons: $.noop,
    getCurrentContentView: $.noop,
    initialize: function (options) {
//      this.tbLineOptions = options.tbLineOptions;
      this.tabIndex = {};
      this.sxtIsPermanent = true;
      this.hasSearch = false;
      this.xtRegionData = options.xtRegionData;
      this.isContent = this.xtRegionData.isContent;
      this.model.isContent = this.isContent;
      this.hasSlogtb = !!Drupal.slogtb;
      this.hasSxtSlogitem = !!Drupal.sxt_slogitem;

      this.listenTo(this.model, 'change:currentTabIndex', this.onChangeCurrentTabIndex);
      if (this.hasSlogtb) {
        this.listenTo(Drupal.slogtb.mainModel(), 'change:loadingContent', this.onChangeSlogtbLoadingContent);
      }

      // build all toolbar buttons
      this.buildToolbarLine();
      this.attach();
      // 
      if (this.isContent && this.hasSxtSlogitem) {
        (this.bmModel = Drupal.sxt_slogitem.bookmarkModel());
        this.$bookmarkBtn = this.$tblineWrapper.find('.xt-tbline-item.icon-bookmarks');
        this.listenTo(this.bmModel, 'change:lastAction', this.onChangeBookmarkLastAction);
        this.bmModel.set('lastAction', {action: 'init'});
      }

      slogLog('//todo::doItBetter:: ....... !!!! this.initializedReady(); !!!');
      this.initializedReady();
      return this;
    },
    attach: function () {
      this.$tblineWrapper.on('click', function (e) {
        _sxt.closeOpenPopup();
      });
      this.$tblineWrapper.find('.xt-tbline-item')
          .on('click', $.proxy(this.onClickTbLineItem, this));
    },
    onClickTbLineItem: function (e) {
      var $el = $(e.target);
      if (!$el.hasClass('disabled')) {
        var action = 'action' + ($el.attr('action')).capitalize()
            , provider = $el.attr('provider')
            , callback = this.actions[provider]
            ? this.actions[provider][action] || false : false;
        if (callback && $.isFunction(callback)) {
          callback.apply(this, [$el, e]);
        } else {
          slogErr('TbLineView.onClickTbLineItem:: Action not found: ' + action);
        }
      }
    },
    getCurTabContentView: function () {
      return this.tabsContentViews[this.model.get('currentTabIndex')];
    },
    activateTblineTab: function (toolbar) {
      var idx = this.tabIndex[toolbar];
      (idx !== undefined) && this.model.set('currentTabIndex', idx);
    },
    getCurrentTabIndex: function () {
      return (this.model.get('currentTabIndex') || 0);;
    },
    setCurrentTabIndex: function (index) {
      this.model.set('currentTabIndex', index);
    },
    hasToolbarTab: function (toolbar) {
      return !!this.tabIndex[toolbar];
    },
    onChangeSlogtbLoadingContent: function (stbMModel, data) {
      if (this.tabIndex[data.toolbar] !== undefined) {
        this.activateTblineTab(data.toolbar || 0);
        var view = this.getCurrentContentView();
        view && view.show();
      }
    },
    onChangeBookmarkLastAction: function (bmModel, data) {
//todo::current::        sxt_slogitem::XtsiBookmarkView::totalBookmarks
      this.$bookmarkBtn.toggleClass('is-complete', !!bmModel.isComplete());
    },
    onChangeCurrentTabIndex: function (model, index) {
      var $blocks = this.xtRegionData.$blocks //content blocks !!
          , $curTab = this.$tabs ? $(this.$tabs.get(index)) : false;
      if ($blocks && $curTab) {
        var $curBlock = $($blocks.get(index))
            , isSearch = $curBlock.hasClass('block-search');
        this.$tabs.removeClass('active');
        $curTab.addClass('active');
        $blocks.addClass('visually-hidden');
        $curBlock.removeClass('visually-hidden');
        if (this.hasSearch) {
          var $items = this.$tblineWrapper.find('.xt-tbline-group').not('.xt-tbline-xttabs');
          isSearch ? $items.hide() : $items.show();
        }
        if (!isSearch && this.hasSlogtb) {
          var curView = this.tabsContentViews[index];
          if (curView) {
            this.activateHistoryButtons(curView.getHistoryTarget());
            if (!curView.isContent) {
              _sxt.mainActionsView().model.set('currentXtsiListView', curView);
            }
          }
        }

        this.enableSlogtbTab();
        var $prev = $curTab.prevAll('.disabled')
            , left = this.offTabsHelper + 26 * (index - $prev.length);
        this.$tabsHelper.css('left', left + 'px')
      }
    },
    onSearchSubmit: function (e) {
        var args = {
          view: 'AtWorkView',
          provider: 'slogxt',
          options: {
            infoText: 'onSearchSubmit'
          }
        };
      _sxt.dialogViewOpen(args);
//      _sxt.notImplemented('TbLineView ...onSearchSubmit');
//      slogLog('TbLineView...............onSearchSubmit');
//      e.preventDefault();
//      e.stopPropagation();
      return false;
    },
    enableSlogtbTabByToolbar: function (enable, toolbar) {
      var tabIdx = this.tabIndex[toolbar];
      (tabIdx !== undefined) && this.enableSlogtbTab(enable, tabIdx);
    },
    enableSlogtbTab: function (enable, tabIdx) {
      if (this.$tabs) {
        enable = (enable === undefined) ? true : !!enable;
        (tabIdx === undefined) && (tabIdx = this.model.get('currentTabIndex'));
        !enable && this.ensureSlogtbTab(tabIdx);
        $(this.$tabs[tabIdx]).toggleClass('disabled', !enable);
      }
    },
    ensureSlogtbTab: function (preventIdx) {
      if (!this.$tabs) {
        return;
      }

      var curIdx = this.model.get('currentTabIndex');
      if (preventIdx == curIdx) {
        var done = false;
        while (curIdx > 0) {
          if (!$(this.$tabs[--curIdx]).hasClass('disabled')) {
            this.model.set('currentTabIndex', curIdx);
            done = true;
            break;
          }
        }
        if (!done) {
          var idxLast = this.$tabs.length - 1;
          curIdx = preventIdx < -1 ? -1 : preventIdx;
          while (curIdx < idxLast) {
            if (!$(this.$tabs[++curIdx]).hasClass('disabled')) {
              this.model.set('currentTabIndex', curIdx);
              break;
            }
          }
        }
      }
    },
    ensureContent: function () {
      var contentView = this.getCurrentContentView();
      contentView && contentView.ensureContent();
    },
    actionTabSelect: function ($element, event) {
      this.model.set('currentTabIndex', ($element.data('value') || 0));
    },
    appendTabsHelper: function ($tabsHelper) {
      this.$tblineWrapper.prepend($tabsHelper);
      this.offTabsHelper = 3;
    },
    buildToolbarLine: function () {
      // module sxt_slogitem is required
      if (!this.hasSxtSlogitem) {
        return;
      }

//      slogLog('TbLineView...............buildToolbarLine');
      this.$tblineWrapper = $(_tse('div', {class: 'xt-tbline-wrapper'}));
      this.$el.prepend(this.$tblineWrapper);
      var thisView = this
          , tbActions = Drupal.sxt_slogitem.getTblineActions(this.isContent)
          , groups = tbActions.groups || false
          , tseGroups = ''
          , tseTabsGroup = false
          , hideTabs = true
          , iconf, cls, isTabsGroup;
      if (!groups) {
        return;
      }

      tbActions = _.clone(tbActions);
      delete tbActions.groups;

      this.contentViews = [];
      this.contentIndizes = [];
      if (this.isContent) {
        $.each(Drupal.sxt_slogitem.views.itemContent, $.proxy(function (id, view) {
          view.tblineModel = this.model;
          view.tblineView = this;
          view.tblineIndex = id;
          view.initTblineView && view.initTblineView();
          this.contentViews[id] = view;
          this.contentIndizes[id] = id;
        }, this));
      } else {
        this.tabsContentViews = [];
        var $blocks = this.xtRegionData.$blocks
            , xtsiViews = this.xtRegionData.xtsiViews || {};
        hideTabs = ($blocks.length < 2);
        $.each($blocks, function (idx, block) {
          var $block = $(block)
              , toolbar = $block.attr('toolbar') || false
              , isSlogtb = !!toolbar && $block.hasClass('block-slogtb')
              , extClass = isSlogtb ? ' disabled' : ''
              , xtsiView = xtsiViews[toolbar] || false
              , isSearch = !toolbar ? $block.hasClass('block-search') : false;
          tseTabsGroup = true;
          !toolbar && isSearch && (toolbar = 'xtsearch');
          thisView.contentViews[toolbar] = xtsiView;
          thisView.contentIndizes[toolbar] = idx;
          thisView.tabsContentViews[idx] = xtsiView;
          if (xtsiView) {
            xtsiView.tblineModel = thisView.model;
            xtsiView.tblineView = thisView;
            xtsiView.tblineIndex = idx;
            xtsiView.initTblineView && xtsiView.initTblineView();
          }

          isSearch && $block.addClass('xt-search-tab-block');
          tbActions[toolbar] = {
            path: 'xtTabsSelect',
            index: idx,
            group: 'xttabs',
            class: 'xt-tabs-item' + extClass,
            provider: 'slogxt'
          };
          (isSlogtb || isSearch) && (tbActions[toolbar].id = 'xt-tbtab-' + toolbar);
          thisView.tabIndex[toolbar] = idx;

          if (isSearch) {
            var title = $block.find('>h2').text();
            $block.find('>h2').remove();
            $block.prepend($(Drupal.theme.slogxtHeader()));
            $block.find('.sxt-header-wrapper .main-label').text(title);
            thisView.$searchForm = $block.find('.search-form');
            thisView.$searchForm.on('submit', $.proxy(thisView.onSearchSubmit, thisView));
          }
        });

        if (tseTabsGroup) {
          groups.push('xttabs');
          this.$tabsHelper = $(_tse('div', {class: 'xt-tabs-helper'}));
          this.appendTabsHelper(this.$tabsHelper);
          hideTabs && this.$tabsHelper.addClass('visually-hidden');
          this.$tabsHelper.css('left', this.offTabsHelper + 'px')
        }
      }

      for (var gidx in groups) {
        var group = groups[gidx]
            , isTabsGroup = (group === 'xttabs')
            , tseItems = '';
        for (var idx in tbActions) {
          var action = tbActions[idx];
          if (action.group === group) {
            cls = 'xt-tbline-item ' + action.class;
            iconf = {
              class: cls + (group === 'history' ? ' disabled' : ''),
              action: action.path,
              provider: action.provider
            };
            !!action.id && (iconf['id'] = action.id);
            if (action.value !== undefined) {
              iconf['value'] = action.value;
            }
            isTabsGroup && (iconf.index = action.index);
            tseItems += _tse('div', iconf);
          }
        }

        if (tseItems !== '') {
          cls = 'xt-tbline-group xt-tbline-' + group;
          iconf = {
            class: cls + (isTabsGroup && hideTabs ? ' hidden' : '')
          };
          if (isTabsGroup) {
            tseTabsGroup = _tse('div', iconf, tseItems);
          } else {
            tseGroups += _tse('div', iconf, tseItems);
          }
        }
      }

      // append now collected elements
      if (tseGroups) {
        if (this.isContent) {
          tseGroups = _tse('div', {id: 'xt-tbline-items-wrapper'}, tseGroups);
          tseGroups += _tse('div', {id: 'xt-tbline-scroll-down', class: 'hidden'});
        }
        this.$tblineWrapper.append($(tseGroups));
      }

      if (tseTabsGroup) {
        this.$tblineWrapper.prepend($(tseTabsGroup));
        this.$tabs = this.$tblineWrapper.find('.xt-tbline-xttabs .xt-tabs-item');
        this.model.set('currentTabIndex', 0);
        this.$searchTab = this.$tabs.filter('#xt-tbtab-xtsearch');
        this.hasSearch = !!this.$searchTab.length;
      }

      // provide class for sxt_slogitem
      this.$el.closest('.column.sidebar').find('>aside').addClass('xtsi-has-slogtb');
    },
    enlargeContentView: function (isNext) {
      var selector = '.xt-tbline-item.icon-split' + (isNext ? 3 : 1);
      this.$el.find(selector).click();
    },
    onDocumentReady: function () {
      var thisView = this
          , stbToolbars = this.hasSlogtb ? drupalSettings.slogtb.toolbars || false : false;
      if (stbToolbars) {
        $.each(stbToolbars, function (toolbar) {
          var $tab = thisView.$tblineWrapper.find('#xt-tbtab-' + toolbar || '')
              , tbView = Drupal.slogtb.getSlogtbView(toolbar);
          if (!!$tab.length && tbView && !!tbView.tbIconDarkUrl) {
            $tab.css('backgroundImage', tbView.tbIconDarkUrl);
          }
        });
      }
    }

  };

  var xtTbLineActions = {
    actionXtTabsSelect: function ($el, e) {
      var index = parseInt($el.attr('index'), 10);
      this.model.set('currentTabIndex', index);
    }
  };
  xtTbLineViewExt.actions.slogxt = xtTbLineActions;

  /**
   * Backbone View for a slogitem list.
   */
  _sxt.TbLineView = Backbone.View.extend(xtTbLineViewExt);

}(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement));
