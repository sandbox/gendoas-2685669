/**
 * @file
 * Backbone View for main actions dropbutton.
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var mainActionsExt = {
    sxtThisClass: 'slogxt.MainActionsView',
    actions: {},
    initialize: function () {
      this.initDropbuttons();
      this.listenTo(this.model, 'change:dropbuttonIsOpen', this.onChangeDropbuttonIsOpen);
      return this;
    },
    initDropbuttons: function () {
      var $block = this.$el
          , html = this.buildMainDropbutton();
      if (html) {
        $block.find('>.content').html(html);
        $block.show();
        var width = $block.width()
            , $region = $block.closest('.region');
        $region.css('padding-right', width + 'px');
        this.attach();

        // set background
        var $region = $block.closest('.region')
            , $stb = $region.find('.block-slogtb .toolbar-bar').first();
        $stb.length && $block.css('backgroundColor', $stb.css('backgroundColor'));
      }
    },
    attach: function () {
      Drupal.behaviors.dropButton.attach(this.$el.get(0), {});
      this.$el.find('.dropbutton-toggle')
          .once('dropbutton-toggle')
          .on('click', _sxt.dropbuttonClickHandler)
          .sxtDropbuttonSetView(this);
      this.$el.find('.dropbutton-action a')
          .once('slogxt')
          .on('click', $.proxy(function (e) {
            _sxt.dropbuttonActionDispatch(e, this);
          }, this));
      this.$el.find('.slogxt-dropbutton ul.dropbutton li.dropbutton-action a')
          .on('contextmenu', function () {
            return false;
          });
    },
    buildMainDropbutton: function () {
      var html, actions = drupalSettings.slogxt.mainActions || false;
      actions && (html = Drupal.theme.slogxtDropbutton(actions));
      return html || false;
    },
    onChangeDropbuttonIsOpen: function (model, open) {
//      slogLog('MainActionsView.onChangeDropbuttonIsOpen...... open: ' + open);
    },
    openLoginView: function (startTarget) {
      var args = {
        view: 'LoginView',
        provider: 'slogxt',
        options: {
          startTarget: startTarget
        }
      };
      _sxt.dialogViewOpen(args);
    },
    getActionsRoot: function (provider) {
      return this.actions[provider];
    }
  };

  var actionsExt = {
    actionDummy: function () {
      _sxt.dialogViewOpen('DummyView');
    },
    actionDummyTabs: function () {
      _sxt.dialogViewOpen('DummyTabsView');
    },
    actionDummyWizard: function () {
      _sxt.dialogViewOpen('DummyWizardView');
    },
    actionLogin: function () {
      this.openLoginView('login');
    },
    actionLogout: function () {
      _sxt.logoutExecute();
    },
    actionContact: function () {
      var args = {
        view: 'AtWorkView',
        provider: 'slogxt',
        options: {
          infoText: 'actionContact'
        }
      };
      _sxt.dialogViewOpen(args);
    },
    actionAbout: function () {
      var args = {
        view: 'AtWorkView',
        provider: 'slogxt',
        options: {
          infoText: 'actionAbout'
        }
      };
      _sxt.dialogViewOpen(args);
//      _sxt.notImplemented('MainActionsView ...actionAbout');
    },
    actionEdit: function () {
      _sxt.resetGroupPerms();
      var args = {
        view: 'XtEditFormView',
        provider: 'slogxt',
        options: {
          editTarget: 'edit_xtmain',
          entityId: 0
        }
      };
      _sxt.dialogViewOpen(args);
    },
    _doActionProfile: function (profilePath) {
      var args = {
        view: 'ProfileView',
        provider: 'slogxt',
        options: {profilePath: profilePath}
      };
      _sxt.dialogViewOpen(args);
    },
    actionProfile: function () {
      var profilePath = _sxt.getUserId() + '/actions';
      this.getActionsRoot('slogxt')._doActionProfile.call(this, profilePath);
    },
    actionRepair: function () {
      this.getActionsRoot('slogxt')._doActionProfile.call(this, 'repair');
    },
    setXtSysDonData: function (actionKey) {
      var donView = _sxt.getDoNavigateView()
          , baseActionId = donView.dialogView.baseActionId || false
          , actionData = _sxt.getMainActionData(actionKey)
          , baseData = _sxt.getDonBaseData() || {}
      , baseArgs = baseData.args || {};

      donView.resetSelected(baseData, _sxt.invalidSel);
      if (baseData.noDonSelect) {
        donView.highlight && donView.highlight();
      } else if (_sxt.canApply(baseActionId, actionData.canApply, baseArgs)) {
//        slogLog('MainActionsView...... setXtSysDonData: ' + actionKey + ' / ' + baseActionId);
        var isTarget = !!baseArgs.isTarget
            , isSource = !!baseArgs.isSource
            , isSingle = (!isSource && !isTarget)
            , targetKey = baseData.targetKey || false
            , entityId = false
            , serverResolve = false
            , targetTb = _sxt.tokenTargetTb;

        if (isSingle || isSource) {
          entityId = -1;
          serverResolve = baseData.resolveKey + '::' + actionKey;
        }

        var donData = {
          toolbar: _sxt.getTbSysId(),
          toolbartab: actionKey + '_' + targetTb,
          actionKey: actionKey,
          targetKey: targetKey,
          targetTb: targetTb,
          entityId: entityId,
          serverResolve: serverResolve,
          label: actionData.liTitle,
          info: actionData.liDescription
        };
        _sxt.setDoNavigateRawData(donData, true);
      }
    },
    _doActionTbSys: function (tbSysTarget) {
      if (_sxt.isDoNavigateActive()) {
        this.getActionsRoot('slogxt').setXtSysDonData(tbSysTarget);
      } else {
        var args = {
          view: 'XtTbSystemView',
          provider: 'slogxt',
          options: {
            tbSysTarget: tbSysTarget,
            startTarget: _sxt.getSysViewStartTarget()
          }
        };
        _sxt.dialogViewOpen(args);
      }
    },
    actionTrash: function () {
      this.getActionsRoot('slogxt')._doActionTbSys.call(this, 'trash');
    },
    actionArchive: function () {
      this.getActionsRoot('slogxt')._doActionTbSys.call(this, 'archive');
    },
    actionMainProperties: function () {
      var args = {
        view: 'AtWorkView',
        provider: 'slogxt',
        options: {
          infoText: 'actionMainProperties'
        }
      };
      _sxt.dialogViewOpen(args);
    }
  };
  mainActionsExt.actions.slogxt = actionsExt;


  /**
   * Backbone View for ....
   */
  _sxt.MainActionsView = Backbone.View.extend(mainActionsExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
