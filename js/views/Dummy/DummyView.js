/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var dummyExt = {
    sxtThisClass: 'slogxt.DummyView',
    execute: function () {
      slogLog('_sxt.DummyDialog.................execute');
    },
//          htmlCheckboxes: function () {
//      //      slogLog('_sxt.DummyDialog.................htmlCheckboxes');
//            return Drupal.theme.slogxtDummyCheckboxes();
//          },
    dialogOptions: function () {
      var options = {
//        title: 'DummyDialog',
        dialogClass: 'dummy',
        position: {
          my: 'right top',
          at: 'right-10px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'DummyDialog',
          restoreState: true,
          btnSubmitShow: true,
          btnBackShow: true,
          hasPushpin: true,
//          submitData: {
//            title: Drupal.t('DummySubmit'),
//            submitCallback: $.proxy(this.execute, this)
//          },
          backData: {
            title: Drupal.t('DoThis'),
            submitCallback: $.proxy(this.execute, this)
          },
          contentData: {
            labels: {
              dialogTitle: Drupal.t('InfoTitle'),
              submitLabel: Drupal.t('InfoSubmit')
            },
            autoEnableSubmit: true,
            contentHtml: '<div style="padding:15px;">InfoContent:: todo</div>',
            submitCallback: $.proxy(this.executeInfo, this)
          }
        }
      };
      return options;
    }



  };

  /**
   * Backbone View for login forms
   */
  _sxt.DummyView = _sxt.DialogViewBase.extend(dummyExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
