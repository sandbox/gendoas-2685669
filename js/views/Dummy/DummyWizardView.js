/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var dummyExt = {
    sxtThisClass: 'slogxt.DummyWizardView',
    tabsDataFn: {
      dummyPages: function () {
        return {
          type: 'radios',
          labels: {
            dialogTitle: Drupal.t('DummyPages selection')
          },
          contentHtml: this.dummyPagesHtml()
        };        
      },
      dummyInfo: function (thisView) {
        return {
          labels: {
            dialogTitle: Drupal.t('InfoPage'),
            submitLabel: Drupal.t('InfoFinish')
          },
          contentHtml: Drupal.theme.slogxtDummyInfo()
        };        
      },
      dummyCheckboxes: function () {
        return {
          type: 'checkboxes',
          labels: {
            dialogTitle: Drupal.t('CheckboxesPage'),
            submitLabel: Drupal.t('CheckboxesFinish')
          },
          contentHtml: Drupal.theme.slogxtDummyCheckboxes()
        };        
      },
      dummyRadios: function () {
        return {
          type: 'radios',
          labels: {
            dialogTitle: Drupal.t('RadiosPage'),
            submitLabel: Drupal.t('RadiosFinish')
          },
          contentHtml: Drupal.theme.slogxtDummyRadios()
        };        
      }
    },
    startHtml: function () {
      var data = [
        {
          liTitle: 'DummyPages',
          liDescription: 'DummyPages - Description',
          path: 'tabsDataFn/dummyPages'
        },
        {
          liTitle: 'DummyEdit',
          liDescription: 'DummyEdit - Description',
          path: 'xxx/yyy'
        }
      ];

      return Drupal.theme.slogxtListRadios(data);
    },
    dummyPagesHtml: function () {
//      slogLog('_sxt.DummyWizardView.................dummyPagesHtml');
      var data = [
        {
          liTitle: 'DummyInfo',
          liDescription: 'DummyInfo - Description',
          path: 'tabsDataFn/dummyInfo'
        },
        {
          liTitle: 'DummyCheckboxess',
          liDescription: 'DummyRadios - Description',
          path: 'tabsDataFn/dummyCheckboxes'
        },
        {
          liTitle: 'DummyRadios',
          liDescription: 'DummyRadios - Description',
          path: 'tabsDataFn/dummyRadios'
        }
      ];

      return Drupal.theme.slogxtListRadios(data);
    },
    dialogOptions: function () {
      var options = {
        title: 'DummyWizardDialog',
        dialogClass: 'dummy-wizard',
        position: {
          my: 'right top',
          at: 'right-10px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'DummyWizardDialog',
          restoreState: true,
          isWizard: true,
          contentData: {
            type: 'radios',
            labels: {
              dialogTitle: Drupal.t('Start selection')
            },
            contentHtml: this.startHtml()
          }
        }
      };
      return options;
    }
  };

  /**
   * Backbone View for login forms
   */
  _sxt.DummyWizardView = _sxt.DialogViewBase.extend(dummyExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
