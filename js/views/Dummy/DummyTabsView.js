/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var dummyExt = {
    sxtThisClass: 'slogxt.DummyTabsView',
    dialogOptions: function () {
      var tabsData = {
//        autoEnableSubmit: true,
        targets: ['info', 'checkboxes', 'radios'],
        info: {
          labels: {
            dialogTitle: Drupal.t('InfoTitle'),
            tabLabel: Drupal.t('Info'),
            submitLabel: Drupal.t('InfoSubmit')
          },
          autoEnableSubmit: true,
          contentHtml: Drupal.theme.slogxtDummyInfo(),
          submitCallback: $.proxy(this.executeInfo, this)
        },
        checkboxes: {
          type: 'checkboxes',
          labels: {
            dialogTitle: Drupal.t('CheckboxesTitle'),
            tabLabel: Drupal.t('Checkboxes'),
            submitLabel: Drupal.t('CheckboxesSubmit')
          },
          contentHtml: Drupal.theme.slogxtDummyCheckboxes(10),
          submitCallback: $.proxy(this.executeCheckboxes, this)
        },
        radios: {
          type: 'radios',
          labels: {
            dialogTitle: Drupal.t('RadiosTitle'),
            tabLabel: Drupal.t('Radios'),
            submitLabel: Drupal.t('RadiosSubmit')
          },
//          title: Drupal.t('RadiosTitle'),
//          tabLabel: Drupal.t('Radios'),
//          submitLabel: Drupal.t('RadiosSubmit'),
          contentHtml: Drupal.theme.slogxtDummyRadios(),
          submitCallback: $.proxy(this.executeRadios, this)
        }
      }
      , options = {
        title: 'DummyTabsDialog',
        dialogClass: 'dummy-tabs',
        position: {
          my: 'right top',
          at: 'right-10px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'DummyTabsDialog',
          restoreState: true,
//          btnSubmitShow: true,
          hasPushpin: true,
          tabsData: tabsData
        }
      };
      return options;
    },
    executeInfo: function () {
      slogLog('_sxt.DummyTabsDialog.................executeInfo');
    },
    executeCheckboxes: function () {
      slogLog('_sxt.DummyTabsDialog.................executeCheckboxes');
    },
    executeRadios: function () {
      slogLog('_sxt.DummyTabsDialog.................executeRadios');
    }
  };

  /**
   * Backbone View for login forms
   */
  _sxt.DummyTabsView = _sxt.DialogViewBase.extend(dummyExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
