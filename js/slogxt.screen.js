/**
 * @file
 * Attaches behavior for screen extension.
 */

(function ($, Drupal) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  Drupal.behaviors.SlogxtScreen = {
    attach: function () {
      // create screen model if not created yet.
      if (!_sxt.models.screenModel) {
//        slogLog('behaviors.attach......   .....SlogxtScreen');
        _sxt.models.screenModel = new _sxt.ScreenModel();
        // Update the model when the window size changes.
        $(window).on('resize', function () {
          _sxt.screenModel().set('windowDims', {
            height: innerHeight,
            width: innerWidth
          });
        });
        // Update the model when the viewport offset changes.
        $(document).on('drupalViewportOffsetChange', function (event, offsets) {
          //todo::better::Drupal.debounce = function (func, wait, immediate)
          _sxt.vpTO && clearTimeout(_sxt.vpTO);
          _sxt.vpTO = setTimeout(function () {
//            slogLog('_sxt.behaviors.SlogxtScreen: .......drupalViewportOffsetChange');
            _sxt.screenModel().set('offsets', offsets);
          });
        });

      }
    }
  };

  // extend _sxt
  var sxtScreenExt = {
    screenModel: function () {
      return this.models.screenModel;
    },
    isScreenSmall: function () {
//      slogLog('_sxt.......................isScreenSmall');
      return (this.screenModel().get('loutColumns') === 1);
    },
    isScreenMedium: function () {
      return (this.screenModel().get('loutColumns') === 2);
    },
    resetRegionToTop: function () {
      $('body').sxtRegionToTop(false);
    },
    closeOpenPopup: function () {
//      slogLog('_sxt.......................closeOpenPopup');
      this.resetRegionToTop();
      this.screenModel().set('openPopUp', null);
    }

  };
  // now extend
  $.extend(true, _sxt, sxtScreenExt);
  sxtScreenExt = null;

  //
  // on document ready
  //
  $(document).ready(function () {
    $('#page-wrapper').on('click', function (e) {
//      slogLog('slogxt.screen..ready: ........#page-wrapper.click......#page-wrapper.click......');
      _sxt.closeOpenPopup();
    });
    _sxt.screenModel().onDocumentReady();
  });

})(jQuery, Drupal);
