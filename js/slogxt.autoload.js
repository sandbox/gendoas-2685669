/**
 * @file
 * Attaches behavior for autoload extension.
 */

(function ($, Drupal) {

  "use strict";
  
  var _sxt = Drupal.slogxt;
  
  Drupal.behaviors.SlogxtAutoload = {
    attach: function () {
      // create autoload model if not created yet.
      if (!_sxt.models.autoloadModel) {
//        slogLog('behaviors.attach......   .....SlogxtAutoload');
        _sxt.models.autoloadModel = new _sxt.AutoloadModel()
      }     
    }
  };
 
  // extend _sxt
  var sxtAutoloadExt = {
    autoloadModel: function () {
      return this.models.autoloadModel;
    }
  };
  // now extend
  $.extend(true, _sxt, sxtAutoloadExt);

  //
  // on document ready
  //
  $(document).ready(function () {
    _sxt.autoloadModel().onDocumentReady();
  });

})(jQuery, Drupal);
