/**
 * @file
 * Extend non slogxt object: String, Backbone, window.
 */

(function (window, $, Drupal, Backbone, undefined) {

  "use strict";

  if (!String.capitalize) {
    String.prototype.capitalize = function () {
      return this.charAt(0).toUpperCase() + this.slice(1);
    };
  }
  if (!String.prototype.includes) {
    String.prototype.includes = function () {
      'use strict';
      return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
  }
  if (!String.prototype.sxtTruncate) {
    String.prototype.sxtTruncate = function (length, ending) {
      !length && (length = 100);
      (ending === undefined) && (ending = '...');
      if (this.length > length) {
        return this.substring(0, length - ending.length) + ending;
      } else {
        return this;
      }
    };
  }

  // extend backbone
  Backbone.Model.prototype.slogxtReTrigger = function (trigger, valKey) {
    !valKey && (valKey = trigger.split(':')[1]);
    this.trigger(trigger, this, this.get(valKey));
  };
  Backbone.Model.prototype.slogxtSetAndTrigger = function (trigger, valKey, newValue) {
    !valKey && (valKey = trigger.split(':')[1]);
    this.set(valKey, newValue, {silent: true});
    this.slogxtReTrigger(trigger);
  };

  // http://stackoverflow.com/questions/7379263/disposing-of-view-and-model-objects-in-backbone-js
  Backbone.View.prototype.slogxtDestroy = function () {
    // this for custom cleanup purposes
    if (this.sxtIsDestroying) {
      // prevent endless loop
      return;
    }

    if (this.sxtIsPermanent) {
      // do not destroy
      return;
    }

    this.sxtIsDestroying = true;
    this.detach && $.isFunction(this.detach) && this.detach();

    // first loop through childViews[] if defined, in collection views
    //  populate an array property i.e. this.childViews[] = new ControlViews()
    if (this.childViews) {
      _.each(this.childViews, function (child) {
        child.slogxtDestroy();
      });
    }

    // close all child views that are referenced by property, in model views
    //  add a property for reference i.e. this.toolbar = new ToolbarView();
    for (var prop in this) {
      if (this[prop] instanceof Backbone.View) {
        this[prop].slogxtDestroy();
      }
    }

    // remove all listeners
    this.stopListening();
  };

  window.slogLogCount = 0;
  window.slogLogExec = function (msg, log) {
    function logSorageKey(log) {
      var splitted = ('000000000000' + ++slogLogCount).split('');
      return log + ':' + _.last(splitted, 10).join('');
    }

    log = log || 'Log';
    if (console.log) {
      (log === 'Err') ? window.console.error(msg) : window.console.log(msg);
    } else if ($.sxtIdbStorage) {
      $.sxtIdbStorage.setItem({store: 'slogxtLog'}, logSorageKey(log), msg);
    }
  };
  window.slogLog = function (msg) {
    window.slogLogExec(msg, 'Log');
  };
  window.slogErr = function (msg) {
    window.slogLogExec(msg, 'Err');
  };
// getestet: für firefox immer vorhanden (ff auf rechner und ff for android)
  window.slogLog('console.log: ....' + (console.log ? 'available' : 'NOT available'));

  // extend JQuery.fn
  var jqExt = {
    sxtGetTopText: function () {
      return $(this).clone().children().remove().end().text();
    },
    sxtSetVisualHidden: function (hide) {
      this.toggleClass('visually-hidden', hide);
    },
    sxtRegionToTop: function (toTop, zIndex) {
//      slogLog('$.fn.sxtRegionToTop..........................toTop: ' + toTop);
      $('.region').css('zIndex', 0);  // reset all regions
      toTop = !!toTop;
      var $region = toTop ? $(this).first().closest('.region') : null
          , regionId = $region ? $region.attr('id') : null
          , regionOnTopData = regionId ? {$region: $region, regionId: regionId} : null;
      $region && $region.css('zIndex', zIndex || 110);
      Drupal.slogxt.screenModel().set('regionOnTop', regionOnTopData);
    }
  };
  $.fn.extend(jqExt);


})(window, jQuery, Drupal, Backbone);
