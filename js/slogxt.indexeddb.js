/**
 * @file
 * ...
 */

(function ($) {
                //window.setTimeout( process );
                //xx::hacked::disabled::window.setTimeout( process );
                //
                //xx::hacked::xx::hacked::xx::hacked::xx::hacked::xx::hacked::
                // jQuery JavaScript Library v3.2.1 ----  see line 3689
                //
  if (!$.sxtIdbStorage) {
    $.sxtIdbStorage = {
      proofTarget: function (target) {
        target = target || {};
        return {
          db: target.db || 'slogxtStorage',
          store: target.store || 'slogxtTmp'
        };
      },
      clear: function (target) {
        target = this.proofTarget(target);
        this[target.db].objectStore(target.store).clear();
      },
      getItem: function (target, key) {
        target = this.proofTarget(target);
        return this[target.db].objectStore(target.store).get(key);
      },
      removeItem: function (target, key) {
        target = this.proofTarget(target);
        this[target.db].objectStore(target.store).delete(key);
      },
      setItem: function (target, key, value) {
        target = this.proofTarget(target);
        this[target.db].transaction([target.store]).then(function () {
//          console.log('sxtIdbStorage.setItem: Transaction complete');
          // Nothing to do when transaction is complete
        }, function () {
          console.error('sxtIdbStorage.setItem: Transaction aborted - Store: ' + target.store);
        }, function (t) {
          t.objectStore(target.store).delete(key).then(function () {
//            console.log('sxtIdbStorage.setItem: Delete complete');
            // key deleted, add new key/value
            t.objectStore(target.store).add(value, key).then(function () {
//              console.log('sxtIdbStorage.setItem: Add complete');
              // Nothing to do, key is added
            }, function () {
              console.error('sxtIdbStorage.setItem: Add error');
            });
          }, function () {
            console.error('sxtIdbStorage.setItem: Delete error');
          });
        });
      },
      slogxtStorage: $.indexedDB('slogxtIDB', {
        version: 2,
        schema: {
          1: function (transaction) {
            console.log('sxtIdbStorage.slogxtStorage: transaction.createObjectStore');
            transaction.createObjectStore('slogxtTmp');
            transaction.createObjectStore('slogxtLog');
            transaction.createObjectStore('slogxtContent');
            transaction.createObjectStore('slogtbContent');
            transaction.createObjectStore('slogappContent');
//          },
//          2: function (transaction) {
//            transaction.createObjectStore('slogxtXXXXXXX');
          }
        }
      }),
      xx: false
    };

//              indexedDB.deleteDatabase('slogxtIDB');

  }

  $.sxtIdbStorage.clear({store: 'slogxtLog'});

})(jQuery);
