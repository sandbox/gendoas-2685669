/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings, _) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  Drupal.AjaxCommands.prototype.slogxtInvoke = function (ajax, response, status) {
    slogLog('AjaxCommands.slogxtInvoke');
//    var xx_d = Drupal
//        , xx_ds = drupalSettings
//        ,xx_data = response.data || {}
//        , xx_tid = xx_data.tid || false
//        , xx = 0
//        ;
//        if (!!xx_tid) {
//          slogLog('slogxtInvoke: ....xx_tid: ' + xx_tid);
//          xx = 0;
//        }
    if (ajax.slogxt) {
      var callback = ajax.slogxt.targetCallback;
      if (callback && $.isFunction(callback)) {
        callback.apply(ajax.slogxt.targetThis, [ajax, response, status]);
      }
    }
  };

  var sxtExt = {
    ajax: {},
    ajaxCreate: function (event, $element, noprogress) {
      Drupal.ajax.instances = _.compact(Drupal.ajax.instances);
      var settings = {
        base: false,
        element: $element[0] ? $element[0] : false,
        $xtelement: $element,
        xttimer: new _sxt.StopWatch(),
        url: '#',
        method: 'html',
        event: event,
        progress: {
          type: noprogress ? 'none' : 'throbber',
          message: Drupal.t('Please wait...')
        }
      }
      , ajax = new Drupal.ajax(settings);

      ajax.slogxt = {};
      return ajax;
    }
  };

  // extend _sxt
  $.extend(true, _sxt, sxtExt);
  sxtExt = null;

})(jQuery, Drupal, drupalSettings, _);

