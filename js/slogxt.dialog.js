/**
 * @file
 * Attaches behavior for ...
 */

(function ($, Drupal, _, _tse, undefined) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  var sxtDialogExt = {
    getModalDialogDialog: function (forNavigate) {
      var attr = !!forNavigate ? 'activeModalDialogNavigate' : 'activeModalDialog';
      return _sxt.screenModel().get(attr);
    },
    isOpenModalDialog: function (forNavigate) {
      return !!(this.getModalDialogDialog(forNavigate));
    },
    setIsOpenModalDialog: function (dialog) {
      var attr = this.isDoNavigateActive() ? 'activeModalDialogNavigate' : 'activeModalDialog';
      this.screenModel().set(attr, dialog);
    },
    canOpenModalDialog: function (forNavigate) {
      if (!this.isOpenModalDialog()) {
        return true;
      } else if (this.isDoNavigateActive()) {
        return !!forNavigate;
      }
      return false;
    },
    getModalDialogEl: function (forNavigate) {
      var idXt = forNavigate ? '-for-navigate' : ''
          , id = 'slogxt-modal-dialog' + idXt
          , selector = '#' + id
          , $dialog = $(selector);
      if (!$dialog.length) {
        var wid = 'slogxt-modal-dialog-wrapper'
            , wselector = '#' + wid
            , $wrapper = $(wselector);
        if (!$wrapper.length) {
          $('body').append($(_tse('div', {id: wid})));
          $wrapper = $(wselector);
        }
        $wrapper.append($(_tse('div', {id: id})));
        $dialog = $(selector);
      }
      return $dialog;
    },
    getModalDialogWidget: function (forNavigate) {
      return this.getModalDialogEl(forNavigate).closest('.ui-dialog.ui-widget');
    },
    getModalDialogWidgetOrEl: function (forNavigate) {
      var $widget = this.getModalDialogWidget(forNavigate);
      return $widget.length ? $widget : this.getModalDialogEl(forNavigate);
    },
    dialogViewOpen: function (view, isModal, provider, moreOpts) {
      if (_.isObject(view) && view.view) {
        isModal = view.modal || true;
        provider = view.provider;
        moreOpts = view.options;
        view = view.view;
      }

      var forNavigate = (moreOpts && moreOpts.isForNavigate) ? true : false;
      isModal = isModal || true;  //todo::no effect - always modal !!!???
      if (isModal && !_sxt.canOpenModalDialog(forNavigate)) {
        var donView = _sxt.screenModel().get('donView');
        if (donView && donView.highlight) {
          donView.highlight();
        } else {
          slogErr('Modal dialog is already open: ' + this.sxtThisClass);
        }
      } else {
        provider = provider || 'slogxt';
        var opts = isModal ? {el: _sxt.getModalDialogEl(forNavigate).first()} : {}
        , options = moreOpts ? $.extend(true, opts, moreOpts) : opts
            , dlgView = new Drupal[provider][view](options);
        dlgView && dlgView.showDialog(options);
      }
    },
    getDoNavigateView: function (create) {
      function createDonView() {
        var id = 'slogxt-do-navigate-wrapper'
            , $donEl = $('#' + id)
            , donView = false;
        if (!$donEl.length) {
          var model = new _sxt.DlgDoNavigateModel
              , btnMove = _tse('div', {
                class: 'sxt-button slogxt-don-btn-move draggable',
                title: Drupal.t('Move navigation tool')
              })
              , btnSubmit = _tse('div', {class: 'sxt-button slogxt-don-btn-submit disabled'})
              , btnAbort = _tse('div', {class: 'sxt-button slogxt-don-btn-abort'}, '<div id="don-selected-count">0</div>')
              , dlgAbort = _tse('div', {class: 'sxt-button slogxt-don-dlg-abort'})
              , buttons = _tse('div', {class: 'slogxt-don-buttons'}, btnMove, btnSubmit, btnAbort, dlgAbort)
              , info = _tse('div', {class: 'slogxt-don-info', title: 'slogxt-don-info-title'}, 'XXX')
              , html = Drupal.theme.slogxtWrapper(id, [buttons, info]);

          $('body').append($(html));
          $donEl = $('#' + id);

          // create view
          donView = new _sxt.DlgDoNavigateView({
            el: $donEl,
            model: model
          });
        }

        return donView;
      }
      //
      if (!!create && !_sxt.views.doNavigateView) {
        this.setDoNavigateView(createDonView());
      }
      return _sxt.views.doNavigateView;
    },
    setDoNavigateView: function (view) {
      _sxt.views.doNavigateView = view;
    },
    isDoNavigateActive: function () {
      var donView = this.getDoNavigateView();
      return (donView && donView.isActive());
    },
    isDoNavigateActiveWizard: function () {
      var donView = this.getDoNavigateView();
      if (donView && donView.isActive()) {
        return !!donView.dialogView.isWizard;
      }
      return false;
    },
    setDoNavigateRawData: function (data, retrigger) {
      if (this.isDoNavigateActive()) {
        var donView = this.getDoNavigateView()
            , baseData = donView.model.get('baseData');
        if (baseData && (baseData.targetKey === data.targetKey)) {
          !!retrigger && donView.model.set('rawData', false, {silent: true});
          donView.model.set('rawData', data);
        }
      }
    },
    getDonBaseData: function (force) {
      if (!!force || this.isDoNavigateActive()) {
        var donView = this.getDoNavigateView();
        return donView.model.get('baseData');
      }
    },
    getDonTargetKey: function () {
      var baseData = this.getDonBaseData() || {}
      return baseData.targetKey || '';
    },
    setDonResultSource(data, force) {
      if (!!force || this.isDoNavigateActive()) {
        var donView = this.getDoNavigateView();
        !!donView && donView.model.set('resultSource', data);
      }
    },
    getDonResultSource(force) {
      if (!!force || this.isDoNavigateActive()) {
        var donView = this.getDoNavigateView();
        return !!donView ? donView.model.get('resultSource') : undefined;
      }
    },
    isDonTargetKey: function (key) {
      return (this.getDonTargetKey() === key);
    }

  };

  $.extend(true, _sxt, sxtDialogExt);

})(jQuery, Drupal, _, Drupal.theme.slogxtElement);
