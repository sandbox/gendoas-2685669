/**
 * @file
 * Backbone model for autoload.
 */

(function ($, Backbone, Drupal, drupalSettings, _) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  /**
   * Backbone model for state support.
   */
  var autoloadModelExtensions = {
    sxtThisClass: 'slogxt.AutoloadModel',
    defaults: {
      /**
       * Field for triggering events loading and loaded.
       * 
       * Event loading: 
       *  - Triggered before autoloading in order to collect tasks.
       *  - Modules may push their tasks in 'tasks' (without a key).
       * Event loaded: 
       *  - This is a notification, that tasks are all done.
       */
      autoloadState: '',
      /**
       * Contains the tasks for autoloading.
       */
      tasks: {}
    },
    onDocumentReady: function () {
      this.autoloadLibraries();
    },
    prepareTasks: function () {
      this.tasks = drupalSettings.slogxt.autoload || {};
      this.set('autoloadState', 'loading');
    },
    autoloadLibraries: function () {
//      slogLog('_sxt.AutoloadModel ................autoloadLibraries');
      this.prepareTasks();
      if (_.isEmpty(this.tasks)) {
        this.set('autoloadState', 'loaded');
      } else {
        for (var tkey in this.tasks) {
          var task = this.tasks[tkey]
                  , ajax = task.ajax = _sxt.ajaxCreate('sxtLoadLibraries', $('body'), true);
          ajax.url = ajax.options.url = Drupal.url(task.path);
          ajax.slogxt.taskKey = tkey;
          ajax.slogxt.targetThis = this;
          ajax.slogxt.targetCallback = this.ajaxResponse;
          ajax.$xtelement.trigger('sxtLoadLibraries');
        }
      }
    },
    ajaxResponse: function (ajax, response, status) {
      var tkey = ajax.slogxt.taskKey;
      this.tasks[tkey] && (delete this.tasks[tkey]);
      _.isEmpty(this.tasks) && this.set('autoloadState', 'loaded');
    }
  };


  _sxt.AutoloadModel = Backbone.Model.extend(autoloadModelExtensions);

})(jQuery, Backbone, Drupal, drupalSettings, _);
