/**
 * @file
 * Backbone model for state support.
 */

(function ($, Backbone, Drupal, localStorage) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  /**
   * Backbone model for state support.
   */
  var stateModelExtensions = {
    sxtThisClass: 'slogxt.StateModel',
    defaults: {
      /**
       * Field for triggering events loaded and writing.
       * 
       * Event loaded: 
       *  - Triggered after data are loaded and stored in 'stateData'.
       *  - With the data modules may restore their own state.
       * Event writing: 
       *  - Triggered before storing state in order to collect state data.
       *  - Modules may place their state data in 'stateData' with an unique key.
       */
      stateState: '',
      /**
       * Contains the loaded state data or collected data for writing the state.
       */
      stateData: {},
      /**
       * Event is triggered after the state data are loaded and handled by all modules.
       */
      stateLoadReady: false,
      /**
       * If set to TRUE the state is cleared on reloading page,
       */
      clearStateOnReload: false
    },
    initialize: function () {
      var aModel = _sxt.autoloadModel();
      this.listenTo(aModel, 'change:autoloadState', this.onChangeAutoloadState);
      this.theme = _sxt.getTheme();
      return this;
    },
    onDocumentReady: function () {
      // Event beforeunload to store/write the state 
      $(window).bind('beforeunload', function () {
        _sxt.stateModel().writeState();
      });
    },
    onChangeAutoloadState: function (aModel, autoloadState) {
      if (autoloadState === 'loaded') {
        // do not load state immediately,
        // contextual splits its action by timeout and runs into errors
        // if we override blocks too early
        setTimeout(function () {
          _sxt.stateModel().loadState();
        });
      }
    },
    writeState: function () {
      var clearStateOnReload = this.get('clearStateOnReload') || false
              , storageKey = this.getStorageKey();
      if (clearStateOnReload) {
        // clear all state
        localStorage.removeItem(storageKey);
      } else {
        this.set('stateData', {}, {silent: true});

        // trigger event for collecting state data
        this.set('stateState', 'writing');

        // write collected state data into local storage
        var sdata = JSON.stringify(this.get('stateData'));
        localStorage.setItem(storageKey, sdata);
      }
    },
    loadState: function () {
      var storageKey = this.getStorageKey()
              , sdata = localStorage.getItem(storageKey)
              , data = !!sdata ? JSON.parse(sdata) : false;
      if (data) {
        this.set('stateData', data, {silent: true});
        this.set('stateState', 'loaded');
        this.set('stateData', {}, {silent: true});
      }

      // trigger event state data are loaded and handled by the modules.
      this.set('stateLoadReady', true);
    },
    getStorageKey: function () {
      return _sxt.getUserSigned('sxtStateData:' + this.theme);
    }
  };


  _sxt.StateModel = Backbone.Model.extend(stateModelExtensions);

})(jQuery, Backbone, Drupal, localStorage);
