/**
 * @file
 * Backbone model for autoload.
 */

(function ($, Backbone, Drupal, drupalSettings) {

  "use strict";

  /**
   * Backbone model for state support.
   */
  var dlgDoModelExt = {
    sxtThisClass: 'slogxt.DlgDoNavigateModel',
    defaults: {
      baseData: null,
      rawData: null,
      preparedData: null,
      isActive: false,
      curPos: null, // 
      curEdge: 1  // 'right top' by default
    }
  };


  Drupal.slogxt.DlgDoNavigateModel = Backbone.Model.extend(dlgDoModelExt);

})(jQuery, Backbone, Drupal, drupalSettings);
