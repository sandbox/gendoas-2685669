/**
 * @file
 * Backbone model for dropbutton.
 */

(function ($, Backbone, Drupal, drupalSettings) {

  "use strict";

  /**
   * Backbone model for dropbutton.
   */
  var modelExt = {
    sxtThisClass: 'slogxt.MainActionsModel',
    defaults: {
    }
  };


  Drupal.slogxt.MainActionsModel = Backbone.Model.extend(modelExt);

})(jQuery, Backbone, Drupal, drupalSettings);
