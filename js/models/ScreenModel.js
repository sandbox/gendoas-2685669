/**
 * @file
 * Backbone model for screen.
 */

(function ($, Backbone, Drupal, drupalSettings, window) {

  "use strict";

  var _sxt = Drupal.slogxt;
  
  /**
   * Backbone model for screen.
   */
  var screenModelExtensions = {
    sxtThisClass: 'slogxt.ScreenModel',
    defaults: {
      offsets: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      },
      windowDims: {
        height: 0,
        width: 0
      },
      stateDialog: {},
      isPortrait: null, //
      isSmall: null, // <600px
      isMedium: null, // >=600px
      isWide: null, // >=900px
      loutColumns: null, // 3:isWide, 2:isMedium, 1:isSmall
      openDropButton: null,
      regionOnTop: null,
      openPopUp: null,
      activeModalDialog: null,
      donView: null
    },
    /**
     * {@inheritdoc}
     */
    initialize: function () {
      this.initBreakpoints();
      this.$pageWrapper = $('#page-wrapper')
      this.set('windowDims', {
        height: window.innerHeight,
        width: window.innerWidth
      }, {silent: true});

      this.listenTo(this, 'change:offsets', this.onChangeOffsets);
      this.listenTo(this, 'change:windowDims', this.renderWindow);
      this.listenTo(this, 'change:isMedium change:isWide', this.onChangeMediaWidth);
      this.listenTo(this, 'change:openPopUp', this.onChangeOpenPopUp);
      this.listenTo(this, 'change:regionOnTop', this.onChangeRegionOnTop);
      var stateModel = _sxt.stateModel();
      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);

    },
    initBreakpoints: function () {
      var breakpoints = drupalSettings.slogxt.breakpoints || {};
      for (var label in breakpoints) {
        if (breakpoints.hasOwnProperty(label)) {
          var mq = breakpoints[label]
              , mql = window.matchMedia(mq);
          mql.slogxt = {
            label: label,
            model: this
          };
          mql.addListener(this.mediaQueryChangeHandler);
          setTimeout(this.mediaQueryChangeHandler.call(mql, mql), 10);
        }
      }
    },
    onDocumentReady: function () {
      this.slogxtReTrigger('change:isWide');
      this.slogxtReTrigger('change:windowDims');
    },
    mediaQueryChangeHandler: function (mql) {
      if (this.slogxt) {
        var label = this.slogxt.label
            , model = this.slogxt.model;
        slogLog('slogxt.mediaQueryChangeHandler: ' + label + '/' + mql.matches);
        switch (label) {
          case 'slogxt.orientation':
            model.set({'isPortrait': mql.matches});
            break;
          case 'slogxt.medium':
            model.set({'isMedium': mql.matches});
            break;
          case 'slogxt.wide':
            model.set({'isWide': mql.matches});
            break;
          default:
            break;
        }
      }
    },
    onChangeOffsets: function (model, offsets) {
//      slogLog('_sxt.ScreenModel: .......onChangeOffsets');
      this.slogxtReTrigger('change:windowDims');
    },
    onChangeOpenPopUp: function (model, openPopUp) {
//      slogLog('ScreenModel.onChangeOpenPopUp: ' + openPopUp);
      if (!openPopUp) {
        // close dropbutton if any open
        var drbData = this.get('openDropButton');
        drbData && drbData.dropbutton && drbData.dropbutton.close();
      }
    },
    onChangeRegionOnTop: function (model, data) {
      var pData = model.previous('regionOnTop');
      pData && pData.$region && pData.$region.removeClass('region-on-top');
      data && data.$region && data.$region.addClass('region-on-top');
    },
    renderWindow: function (model, windowDims) {
//      slogLog('ScreenModel: renderWindow');
      var $adminTrays = $('#toolbar-administration').find('> .toolbar-tray')
          , offsets = model.get('offsets');

      // ensure windowDims is set
      if (windowDims.height === 0) {
        windowDims = {height: innerHeight, width: innerWidth};
      }

      // body css first
      $('body').css({
        paddingTop: offsets.top,
        height: windowDims.height - offsets.top
      });

      $adminTrays.css({maxHeight: windowDims.height});
      this.$pageWrapper.css({top: offsets.top, left: offsets.left});
    },
    onChangeMediaWidth: function (model, value) {
      var isWide = model.get('isWide')
          , isMedium = model.get('isMedium')
          , columns = isWide ? 3 : isMedium ? 2 : 1;
//      slogLog('...ScreenModel: onChangeMediaWidth.columns: ' + columns);
      model.set('isSmall', (!isWide && !isMedium));

      this.MediaTimeout && clearTimeout(this.MediaTimeout);
      this.MediaTimeout = setTimeout(function () {
        var columns = isWide ? 3 : isMedium ? 2 : 1;
//        slogLog('...ScreenModel.onChangeMediaWidth:MediaTimeout.......columns: ' + columns);
        model.set('loutColumns', columns);
      }, 10);
    },
    onChangeStateState: function (stateModel, stateState) {
      var stateData = stateModel.get('stateData');
      try {
        switch (stateState) {
          case 'loaded':
            if (stateData.slogxtScreen) {
              var data = stateData.slogxtScreen || {};
              this.set('donCurEdge', data.donCurEdge || 1);
              this.set('stateDialog', data.stateDialog || {});
            }
            break;
          case 'writing':
            stateData.slogxtScreen = {
              donCurEdge: this.get('donCurEdge'),
              stateDialog: this.get('stateDialog')
            };
            break;
        }
      } catch (e) {
        slogErr('ScreenModel.onChangeStateState: ' + stateState + "\n" + e.message);
      }
    },
    xx: false




  };

  _sxt.ScreenModel = Backbone.Model.extend(screenModelExtensions);

})(jQuery, Backbone, Drupal, drupalSettings, window);
