/**
 * @file
 * A Backbone Model for ???.
 */

(function ($, Backbone, Drupal, _) {

  "use strict";

  var _sxt = Drupal.slogxt;

  /**
   * Backbone model for .
   */
  Drupal.slogxt.TbLineModel = Backbone.Model.extend({
    sxtThisClass: 'slogxt.TbLineModel',
    defaults: {
      currentTabIndex: -1,
      currentContent: {}
    },
    getStateData: function () {
      return {
        'defaultRole': _sxt.getUserDefaultRole(),
        'currentTabIndex': this.get('currentTabIndex'),
        'currentContent': this.get('currentContent')
      };
    },
    setStateData: function (data) {
      data = data || {};
      var curContent = data['currentContent'] || {}
      , hashes = {}
          , idx, item, sid;
      if (this.isContent) {
        for (var idx in curContent) {
          item = curContent[idx];
          sid = item.sid || false;
          item.testHash && sid && !hashes[sid] && (hashes[sid] = item.testHash);
        }
        for (var idx in curContent) {
          item = curContent[idx];
          sid = item.sid || false;
          sid && hashes[sid] && (item.testHash = hashes[sid]);
        }
      } else if (data['defaultRole'] && data['defaultRole'] !== _sxt.getUserDefaultRole()) {
        this.set('currentTabIndex', 0);
      } else {
        this.set('currentTabIndex', data['currentTabIndex'] || 0);
        this.set('currentContent', curContent);
      }
    }
  });

})(jQuery, Backbone, Drupal, _);
