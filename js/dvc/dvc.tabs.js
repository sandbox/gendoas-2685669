/**
 * @file
 * slogxt/js/dvc/dvc.tabs.js
 * 
 * Tabs components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  var _sxt = Drupal.slogxt
      , _sxth = _sxt.helper
      , _tse = Drupal.theme.slogxtElement;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.tabs = {
    _sxtCreateAWStartTab: function () {
      var bundle = (this.dialogId).toLowerCase()
          , aStartIdx = this.actionStartIdx
          , tabHtml = this._sxtTabsHtml([{
              contentId: _sxth.toID(['slogxt-' + bundle, aStartIdx].join('-')),
              itemId: _sxth.toID(bundle + '-' + aStartIdx),
              label: this.provideWizardPath ? '...' : 'p' + aStartIdx,
              targetId: aStartIdx
            }])
          , $newTab = $(tabHtml);
      $newTab.addClass('actions-wizard visually-hidden');
      this.$Tabs.append($newTab);
      this._sxtTabsAttach();
      this.$tabActionWizartStart = this.$Tabs.find('.actions-wizard');
      this.$tabActionWizartStart.sxtSetVisualHidden(true);
    },
    _sxtTabsCreate: function () {
      this.loaded = {};
      this.loading = {};
      var tabsData = this.tabsData
          , bundle = (this.dialogId).toLowerCase()
          , cItems = [];

      if ((this.hasStartActions || this.hasItemAction) && !this.isWizard) {
        for (var idx in tabsData.targets) {
          var targetid = tabsData.targets[idx]
              , cls = tabsData[targetid].cls || '';
          tabsData[targetid].cls = cls + ' actions-base-tab';
        }

        this.pagesData = [];
        var aStartIdx = this.actionStartIdx;
        tabsData.targets.push(aStartIdx);
        tabsData[aStartIdx] = {
          targetid: aStartIdx,
          type: 'wizardstart',
          cls: 'actions-wizard',
          labels: {
            dialogTitle: Drupal.t('WizardTitle'),
            submitLabel: Drupal.t('WizardSubmit'),
            tabLabel: 'p' + aStartIdx
          },
        };
      }

      for (var idx in tabsData.targets) {
        var targetid = tabsData.targets[idx]
            , labels = tabsData[targetid].labels || {}
        , cls = tabsData[targetid].cls || false
            , tabLabel = (labels.tabLabel || labels.dialogTitle || 'Select').capitalize();
        tabsData[targetid].targetid = targetid;
        cItems.push({
          contentId: _sxth.toID(['slogxt-' + bundle, targetid].join('-')),
          itemId: _sxth.toID(bundle + '-' + targetid),
          label: tabLabel,
          title: tabLabel,
          cls: cls,
          targetId: targetid
        });
      }

      if (!_.isEmpty(cItems)) {
        var htmlTabs = this._sxtTabsHtml(cItems);
        this.tabsLength = cItems.length;
        this.$Tabs.append($(htmlTabs));
        this.$Tabs.sxtSetVisualHidden(this.hideTabs || cItems.length < 2);
        this.$Content.toggleClass('slogxt-has-tabs', (htmlTabs !== ''));
        var $tabActionWizartStart = this.$Tabs.find('.actions-wizard');
        if ($tabActionWizartStart.length) {
          this.$tabActionWizartStart = this.$Tabs.find('.actions-wizard');
          this.$tabActionWizartStart.sxtSetVisualHidden(true);
        }
        this.onTabsCreated();
      }
    },
    _sxtTabsAttach: function () {
      if (this.$Tabs) {
        this.$Tabs.find('.tabs-item')
            .once('slogxt')
            .on('click', $.proxy(function (e) {
              if (!!this.sxtTabsLocked) {
                return; // do nothing
              }
              var $item = $(e.currentTarget)
                  , targetid = $item.attr('targetid');
              this.model.set('contentCheckedItems', {}, {silent: true});
              this.preventClose = true;
              this.model.set('activeTab', {$item: $item, targetid: targetid});
              delete this.preventClose;
              return false;
            }, this));
      }
    },
    _sxtTabsDetach: function () {
      if (this.$Tabs) {
        this.$Tabs.removeOnce('slogxt').off();
      }
    },
    _sxtTabsHtml: function (items) {
      var html = '';
      for (var idx in items) {
        var item = items[idx]
            , label = _tse('span', {class: 'tab-label'}, item.label || '???')
            , cls = item.cls ? 'tabs-item ' + item.cls : 'tabs-item'
            , opts = {
              class: cls,
              contentid: item.contentId,
              targetid: item.targetId
            };
        !!item.title && (opts.title = item.title);
        html += _tse('div', opts, label);
      }
      return html;
    },
    _sxtTabContentWrapperHtml: function (id, content) {
      return _tse('div', {id: id, class: 'tabs-content'}, content);
    },
    _sxtTabsMoveHelper: function () {
      var $active = this.$Tabs.find('.tabs-item.active');
      if (this.tabsLength > 1 && $active.length) {
        this.$tabsHelper.width($active.width() - 1);
        this.$tabsHelper.position({
          my: 'left top',
          at: 'left+1 bottom-1',
          of: $active
        });
        this.$tabsHelper.show();
      } else {
        this.$tabsHelper.hide();
      }
    },
    _sxtTabElement: function (targetid) {
      return this.$Tabs.find('.tabs-item[targetid="' + targetid + '"]');
    },
    _sxtOnChangeActiveTab: function (model, data) {
//      slogLog('_sxt.DialogViewBase::....._sxtOnChangeActiveTab');
      this._sxtSetOverlayTransparent(false);
      this.model.set('contentCheckedItems', {}, {silent: true});  //reset
      if (data && data.$item) {
        this.onPreChangeActiveTab(model, data); //hook:
        data.$item.addClass('active');
        data.$item.siblings().removeClass('active');
        this._sxtTabsMoveHelper();
        this._sxtContentForActiveTab(model, data);
        var tabData = this.tabsData[data.targetid] || {}
        , closeOnEscape = true;
        !!tabData.autoEnableSubmit && this.$btnSubmit.button('enable');
        if (this.isWizard) {
          var isCheckboxes = this._sxtIsWizardCheckboxes()
              , curPageIdx = this.getWizardCurPageIdx()
//todo::current::isWizardActionPage - actions per page  !!!!!
// curPageIdx === this.actionStartIdx ....
              , hasPrev = this.sdHasPrevUnskippedPageIdx(curPageIdx - 1);
          if (isCheckboxes && !this.isWizardActionPage) {
            this._sxtShowActionsCheckboxes(true);
            this._sxtCalculateCheckedItems();
          }
          hasPrev && this.$btnBack.button('enable');
          closeOnEscape = (curPageIdx === 0 && tabData.type !== 'wizardstart');
        } else {
          var useActions = this._sxtUseActions()
              , isActionsOpen = this._sxtActionsOpen();
          useActions && this._sxtShowActionsCheckboxes(isActionsOpen);
          isActionsOpen && this._sxtActionsReOpen();
        }
        this._sxtSetCloseOnEscape(closeOnEscape);
        !!this.donView && this._sxtDonResetIsCurrent();
        this.onChangedActiveTab(model, data); //hook:
      }
    },
    _sxtUseActions: function (targetid) {
      targetid = targetid || this.activeTargetId;
      var tabData = this.tabsData[targetid] || {};
      return (this.hasStartActions && !!tabData.useActions);
    },
    _sxtContentForActiveTab: function (model, data) {
      var thisView = this
          , contentId = data.$item.attr('contentid') || false
          , targetid = data.$item.attr('targetid')
          , loaded = !!this.loaded[targetid]
          , $content = this.$Content.find('#' + contentId)
          , tabsData = this.tabsData
          , tabData = tabsData[targetid] || {}
      , type = (!!tabData.useActions && !this.hasStartActions) ? false : tabData.type || false
          , showACheckbox = (type === 'checkboxes')
          , useActions = this._sxtUseActions(targetid)
          , calculateChecked = (type === 'checkboxes') || (type === 'radios')
          , labels = tabData.labels || {};

      this.isWizard && (targetid = parseInt(targetid, 10));
      this.activeHasActions = useActions;
      if (useActions && !this._sxtActionsOpen()) {
        showACheckbox = false;
      }
      this._sxtShowActionsCheckboxes(showACheckbox);
      if (contentId && !$content.length) {
        var content = this._sxtTabContentHtml(targetid);
        if (content instanceof jQuery) {
          var $w = $(this._sxtTabContentWrapperHtml(contentId));
          content = $w.append(content);
        } else {
          content = this._sxtTabContentWrapperHtml(contentId, content);
        }
        this.$Content.append($(content));
        $content = this.$Content.find('#' + contentId);
        type && !useActions && $content.addClass('type-' + type);
        this.loaded[targetid] = contentId;
        this._sxtDialogSetFocus($content);
        this._sxtDialogAdjust();
        this._sxtDialogContentAttach($content);
        calculateChecked && this._sxtCalculateCheckedItems();

        if (this.isWizard) {
          this.pagesData[targetid] = {
            type: type,
            $tab: this.$Tabs.find('.tabs-item.active'),
            labels: tabData.labels || {},
            contentId: contentId,
            $content: this.$Content.find('#' + contentId),
            tabData: tabData,
            slogxtData: {}
          };
        } else {
          this.pagesData[targetid] = {
            contentId: contentId
          };
        }
      }

      $content.siblings().hide();
      $content.show();
      tabData.transparentOverlay && this._sxtSetOverlayTransparent(true);
      this.activeTargetId = targetid;
      this.$activeContent = $content;
      this.onTabContentCreated();

      if (this.isWizard) {
        this.model.set('wizardCurrentPage', targetid, {silent: true});
        labels = this._sxtWizardPageData().labels || {};
        !labels.submitLabel && (labels.submitLabel = Drupal.t('Next'));
        !labels.backLabel && (labels.backLabel = Drupal.t('Back'));
      }
      this._sxtDialogSetLabels(labels);

      if (loaded) {
        this.isWizardActionPage && this._sxtActionsOpen() && this._sxtActionsReOpen();
        this._sxtDialogSetFocus($content);
        this._sxtDialogAdjust();
        calculateChecked && this._sxtCalculateCheckedItems();
      } else if (!!tabData.contentHtml || (!!tabData.contentElements && !tabData.path)) {
        this.$btnSubmit.button('disable');
        if (!!tabData.contentHtml) {
          this._sxtSetHtml($content, tabData.contentHtml);
        } else {
          $content.empty();
          $content.append(tabData.contentElements || $('XXXXX'));
        }
        type && !useActions && $content.addClass('type-' + type);
        this.loaded[targetid] = contentId;
        this._sxtDialogSetFocus($content);
//        this._sxtDialogAdjust();
        this._sxtDialogContentAttach($content);
        calculateChecked && this._sxtCalculateCheckedItems();
      } else if (!!tabData.path) {
        if (this.isWizard && tabData.resolve //
            && (!tabData.serverResolve || tabData.serverResolved)
            && this._sxtHasCommandCallback(tabData.resolve, tabData.provider)) {
          $.each(tabData.resolve, function (key, cmd) {
            if (thisView._sxtIsCallback(cmd, tabData.provider)) {
              var callback = thisView._sxtCommandCallback(cmd, tabData.provider);
              callback.apply(thisView, [key, tabData]);
              return false;
            }
          });
          if (!!tabData['dialogTitle']) {
            labels = this._sxtWizardPageData().labels || {};
            labels['dialogTitle'] = tabData['dialogTitle'];
            delete tabData['dialogTitle'];
            this._sxtDialogSetLabels(labels);
          }
        } else if (!this.onPreAjaxLoadContent($content, data, tabData) /*returns done*/) {
          if (this.isWizard && tabData.serverResolve) {
            var splitted = tabData.serverResolve.split('::')
                , resolve = tabData.resolve || {};
            if (splitted.length === 2 && !!resolve[splitted[0]]) {
              resolve[splitted[0]] = splitted[1];
            }
          }
          this.$btnSubmit.button('disable');
          //todo::better::Drupal.debounce = function (func, wait, immediate)
          this.ajaxTO && clearTimeout(this.ajaxTO);
          this.ajaxTO = setTimeout(function () {
            thisView._sxtAjaxLoadContent($content);
          });
        }
      }
    },
    _sxtGetActiveTabId: function () {
      return this.model.get('activeTab').targetid;
    },
    _sxtActiveTabSubmit: function () {
      var targetid = this._sxtGetActiveTabId()
          , tabData = this.tabsData[targetid] || {}
      , callback = tabData.contentHtmlCallback || false;
      if (tabData.submitCallback) {
        tabData.submitCallback();
        if (callback && $.isFunction(callback)) {
          callback.apply(this);
        }
      } else {
        var $form = this.$activeContent.find('form')
            , $invalid = $form.find('input:invalid')
            , isValid = ($invalid.length == 0);
        !isValid && $invalid[0].checkValidity();
        if (isValid) {
          $form.submit();
        } else {
          $form.find('.messages').remove();
          var items = ''
              , $invalid = $form.find('input:invalid');
          $invalid.each(function (index, node) {
            var label = $("label[for=" + node.id + "] ")
                , message = node.validationMessage || 'Invalid value.';
            items += _tse('li', {class: 'messages__item'}, '<span>' + label.html() + ':</span>' + message);
          });
          var msg = _tse('ul', {class: 'messages__list'}, items)
              , $msg = Drupal.theme.slogxtMessage(msg, 'error');
          $form.prepend($msg);
          this.$btnSubmit.button('enable');
          $invalid.first().focus();
        }
      }
    },
    _sxtInitTabDataByPath: function (path, resolve, resolveArgs) {
      resolve = resolve || false;
      resolveArgs = resolveArgs || false;
      var pageData = this._sxtWizardPageData() || {}
      , prevTabData = pageData.tabData || {}
      , serverResolve = !!prevTabData.serverResolve
          , serverResolved = !!prevTabData.serverResolved
          , tabData = {path: path}
      , splitted = path.split('/xtajx/');
      if (splitted.length === 2) {
        var bpProvider = drupalSettings.slogxt.basePathProvider || {}
        , provider = (splitted[0]).replace('/', '');
        !!bpProvider[provider] && (provider = bpProvider[provider]);
        tabData.provider = provider;
        tabData.path = splitted[1];
        tabData.resolve = serverResolve ? prevTabData.resolve : resolve;
        tabData.resolveArgs = serverResolve ? prevTabData.resolveArgs : resolveArgs;
        serverResolve && !serverResolved && (tabData.serverResolve = prevTabData.serverResolve);
        if (serverResolved && !!this.donResolveReset) {
          var drr = this.donResolveReset || {};
          tabData.resolve[drr.key] = drr.command;
          tabData.onClientResolve = false;
          this.donResolveReset = false;
        }
      }
      return tabData;
    }
  };

})(jQuery, Drupal, drupalSettings, Backbone, _);
