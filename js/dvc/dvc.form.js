/**
 * @file
 * slogxt/js/dvc/dvc.form.js
 * 
 * Form components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  var _sxt = Drupal.slogxt
      , _tse = Drupal.theme.slogxtElement;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.form = {
//          _sxtFormBehaviors: [
//      //      'formSingleSubmit',
//      //      'formUpdated',
//      //      'fillUserInfoFromBrowser',
//            'AJAX',
//            'editor',
//            'textSummary',
//            'tableDrag',
//            'fileAutoUpload',
//            'fileButtons',
//            'fileValidateAutoAttach'
//          ],
    _sxtFormAttach: function ($content) {
      function attachSxtMessage() {
        var closeInfoMessage = _sxt.localSwitches.closeInfoMessage;
        $content.find('.messages--warning').each(function () {
          var $message = $(this)
              , $toggler = $message.find('>.sxt-msg-toggler');
          if (!$message.hasClass('slogxt-msg')) {
            $message.addClass('slogxt-msg');
            closeInfoMessage && $message.addClass('slogxt-msg-closed');
            if (!$toggler.length) {
              $message.prepend($(_tse('div', {class: 'sxt-msg-toggler'})));
              $toggler = $message.find('>.sxt-msg-toggler');
            }
            $toggler.on('click', function (e) {
              var isClosed = $message.hasClass('slogxt-msg-closed');
              _sxt.localSwitches.closeInfoMessage = !isClosed;
              $message.toggleClass('slogxt-msg-closed', !isClosed);
            })
          }
        });
      }


      Drupal.attachBehaviors($content.get(0));


//      Drupal.attachBehaviors(context);  //is not working
//      _.each(this._sxtFormBehaviors, function (behavior) {
//        if (Drupal.behaviors[behavior] && Drupal.behaviors[behavior].attach) {
//          Drupal.behaviors[behavior].attach(context, drupalSettings);
//        }
//      });

      var ifields = $content.find('form input, form textarea')
          , $radios = $content.find('form .slogxt-input-field .js-form-type-radio')
          , $checkboxes = $content.find('form .slogxt-input-field .js-form-type-checkbox');

      //todo::fix:: workaround for not working radios
      $radios.find('>input.form-radio').on('click', function (e) {
        var checked = $(this).prop('checked');
        setTimeout($.proxy(function () {
          this.checked = checked;
        }, this));
      })
      $radios.find('>label').on('click', function (e) {
        var inputId = $(this).attr('for')
            , $input = $radios.find('input#' + inputId);
        $input.prop('checked', 'checked');
        $input.click();
      })

      //todo::fix:: workaround for not working checkbox
      $checkboxes.find('>input.form-checkbox').on('click', function (e) {
        var checked = $(this).prop('checked');
        setTimeout($.proxy(function () {
          this.checked = checked;
        }, this));
      })
      $checkboxes.find('>label').on('click', function (e) {
        var inputId = $(this).attr('for');
        $checkboxes.find('input#' + inputId).click();
      })
      // remember current input field for restoring focus
      ifields.on('focus', function (e) {
        ifields.removeClass('sxt-input-current');
        $(this).addClass('sxt-input-current');
      })

      if ($content.find('form.slogxt-msg').length) {
        $content.addClass('visually-hidden');
        setTimeout($.proxy(function () {
          attachSxtMessage();
          $content.removeClass('visually-hidden');
        }, this), 10);
      }

    },
    _sxtFormDetach: function ($content) {
      Drupal.detachBehaviors($content.get(0));



//      Drupal.detachBehaviors(context);  //is not working
//      _.each(this._sxtFormBehaviors, function (behavior) {
//        if (Drupal.behaviors[behavior] && Drupal.behaviors[behavior].detach) {
//          Drupal.behaviors[behavior].detach(context, drupalSettings, 'unload');
//        }
//      });
    }
  };


})(jQuery, Drupal, drupalSettings, Backbone, _);
