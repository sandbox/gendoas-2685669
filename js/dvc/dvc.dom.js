/**
 * @file
 * slogxt/js/dvc/dvc.ajax.js
 * 
 * Ajax components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, _, _tse, undefined) {

  var _sxt = Drupal.slogxt
      , _tse = Drupal.theme.slogxtElement;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.dom = {
    _sxtDomUlCloneSelected: function ($ul, selected) {
      var message = selected.message || false
          , msgType = selected.messageType || 'status'
          , checked = selected.checked ? selected.checked : selected;
      if (_.isArray(checked) && !_.isEmpty(checked)) {
        $ul = $ul.clone();
        $ul.find('li .li-checkbox').remove();
        $ul.find('>li').each(function () {
          var $item = $(this)
              , idx = parseInt($item.attr('index'), 10);
          if (_.indexOf(checked, idx) < 0) {
            $item.remove();
          }
        });

        if (message) {
          message = Drupal.theme.slogxtMessage(message, msgType);
          var $wrapper = $(_tse('div', null, message))
          $wrapper.append($($ul));
          return $wrapper;
        }

        return $ul;
      }
//      slogErr('_sxt.dvc.dom::......................._sxtDomUlCloneSelected');
      return $();
    }
  };

})(jQuery, Drupal, _, Drupal.theme.slogxtElement);
