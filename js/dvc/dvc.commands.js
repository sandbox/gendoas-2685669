/**
 * @file
 * slogxt/js/dvc/dvc.commands.js
 * 
 * Commands components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, drupalSettings, _) {

  var _sxt = Drupal.slogxt;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.commands = _sxt.dvc.commands || {};

  var sxtCommands = {
    dummyCommand: function () {
      slogErr('dummyCommand: A rael command not implemented yet.');
    },
    logout: function (args) {
      this._sxtLogoutExecute();
    },
    reload: function (args) {
      this._sxtReload(!!args['reloadImmediately'] || !_sxt.localSwitches.reloadPause);
    },
    viewcontent: function ($content, data) {
      var slogxtData = data.slogxtData || {}
      , htmlContent = slogxtData.htmlContent || '???';
      data.labels && this._sxtDialogSetLabels(data.labels);
      this._sxtSetHtml($content, htmlContent);
    },
    checkboxesAllHidden: function ($content, data) {
      (this._sxtCommandCallback('checkboxes')).call(this, $content, data);
      $content.find('ul.slogxt-list').sxtSetVisualHidden(true);
      this.$actionsCheckbox.sxtSetVisualHidden(true);
      this.$actionsCheckbox.click();
      !!data.messages && $content.prepend($(data.messages));
    },
    checkboxes: function ($content, data) {
      function assignValues(storageKey) {
        var values = JSON.parse(localStorage.getItem(storageKey));
        if (_.isObject(values) && !_.isEmpty(values)) {
          $content.find('li.checkbox').each(function () {
            var $item = $(this)
                , action = $item.attr('action') || ''
                , parts = action.split(':')
                , assign = parts.length > 1 ? storageKey.indexOf(parts[1]) : false
                , checked = values[action] || false;
            assign && $item.toggleClass('checked', checked);
          });
        }
      }

      var slogxtData = data.slogxtData
          , ul_opts = slogxtData.ulOpts || {}
      , headerStyle = !!slogxtData.headerStyle
          , icons = !!slogxtData.icons
          , cls = 'type-select' + (!!slogxtData.hideRadios ? '' : ' type-checkboxes')
          , hasItems = (slogxtData.items && slogxtData.items.length > 0)
          , pageData = this._sxtWizardPageData()
          , tabData = pageData.tabData || {};
      pageData && (pageData.type = 'checkboxes');
      if (hasItems) {
        this._sxtSetHtml($content, this._sxtHtmlListCheckboxes(slogxtData.items, ul_opts, headerStyle, icons));
        $content.addClass(cls);
        this._sxtDialogContentAttach($content);
        if (!tabData.isEditForm && !this.isWizardActionPage) {
          this._sxtCalculateCheckedItems();
          this._sxtShowActionsCheckboxes(true);
        } else if (!!tabData.storageLocalKey) {
          assignValues(tabData.storageLocalKey);
        }
        var curPageIdx = this.getWizardCurPageIdx()
            , hasPrev = this.sdHasPrevUnskippedPageIdx(curPageIdx);
        hasPrev && this.$btnBack.button('enable');
        tabData.isEditForm && this.$btnSubmit.button('enable');
        if (this.isWizardActionPage && pageData.actionsHtml) {
          if (!this.$actionsWrapper) {
            this._sxtActionsCreate(pageData.actionsHtml);
            this._sxtDialogAttachActions();
          }
          this._sxtResetWizardActionPage(true);
        }
        // add message if exists
        !!slogxtData.message && Drupal.theme.slogxtDlgAddMsg($content, slogxtData.message);
      }
    },
    radios: function ($content, data) {
      var slogxtData = data.slogxtData
          , ul_opts = slogxtData.ulOpts || {}
      , headerStyle = !!slogxtData.headerStyle
          , icons = !!slogxtData.icons
          , cls = 'type-select' + (!!slogxtData.hideRadios ? '' : ' type-radios')
          , pageData = this.isWizard ? this._sxtWizardPageData() : false
          , tabData = pageData ? pageData.tabData : this.tabsData[this.activeTargetId] || {};
      !!tabData.serverResolve && slogxtData.serverResolved && (tabData.serverResolved = true);

      pageData && (pageData.type = 'radios');
      this._sxtSetHtml($content, this._sxtHtmlListRadios(slogxtData.items, ul_opts, headerStyle, icons));

      $content.addClass(cls);
      this._sxtAttachRadios($content);
      this._sxtCalculateCheckedItems();
      var singleItem = (slogxtData.items.length === 1)
          , noItems = (slogxtData.items.length === 0);

      this.isWizard && singleItem && this._sxtDelayedClick($content.find('li').first());
      if (noItems && !!slogxtData.emptyWarning) {
        var $msg = Drupal.theme.slogxtMessage(slogxtData.emptyWarning, 'warning', true);
        $content.prepend($msg);
      } else {
        if (singleItem && slogxtData.items[0].skipable) {
          pageData.slogxtData.skipPage = true;
          this._sxtDialogSetLabels({dialogTitle: '...'});
          this._sxtDelayedClick(this.$btnSubmit);
        } else if (!!slogxtData.infoMsg) {
          var $msg = Drupal.theme.slogxtMessage(slogxtData.infoMsg, 'warning', true);
          $content.prepend($msg);
        }
        // add message if exists
        !!slogxtData.message && Drupal.theme.slogxtDlgAddMsg($content, slogxtData.message);
      }
    },
    doFinish: function ($content, data) {
      var slogxtData = data.slogxtData || {}
      , onFinished = slogxtData.onWizardFinished || {}
      , finishedCommand = onFinished.command || false;
      if (finishedCommand) {
        var callback = this._sxtCommandCallback(finishedCommand);
        if (callback) {
          setTimeout($.proxy(function () {
            callback.call(this, onFinished.args || {});
          }, this), 10);
        }
      }
    },
    default: function ($content, data) {
      var slogxtData = data.slogxtData || {};
      if (data.showMessage) {
        data.message = data.message || '...?';
        data.type = data.type || 'warning';
        data.message = Drupal.theme.slogxtMessage(data.message, data.type, true);
        $content.prepend($(data.message));
      } else if (slogxtData.formResult === 'edit') {
        this._sxtCommandCallback('defaultForm').apply(this, [$content, data]);
      } else if (!this.isWizard && slogxtData.formResult === 'saved') {
        this._sxtSetHtml($content, slogxtData.htmlContent);
        data.messages = !!data.messages ? data.messages : Drupal.theme.slogxtMessage('...?', 'warning');
        if (!!data.messages) {
          $content.prepend($(data.messages));
          Drupal.theme.slogxtMessagesPlain($content.find('.messages a'));
        }
      }
    },
    defaultForm: function ($content, data) {
      var slogxtData = data.slogxtData || {}
      , htmlContent = slogxtData.htmlContent || '';
      this._sxtComponentAdd('form');
      this._sxtFormDetach($content);
      $content.hide();
      data.labels && this._sxtDialogSetLabels(data.labels);
      this._sxtSetHtml($content, htmlContent);
      this._sxtFormAttach($content);
      var $form = $content.find('form');
      $content.show();
      this._sxtDialogSetFocus($content);
      this.$btnSubmit.button('enable');

      // messages 
      if (!!data.messages) {
        $content.prepend($(data.messages));
        Drupal.theme.slogxtMessagesPlain($content.find('.messages a'));
      }

      if (slogxtData.doReload) {
        this._sxtReload(true);
      } else {
        // using 'ajaxForm' 
        // see http://malsup.com/jquery/form/#ajaxForm
        $form.ajaxForm({
          target: '#' + $content.attr('id'),
          beforeSerialize: this._sxtAjaxFormBeforeSerialize,
          beforeSubmit: this._sxtAjaxFormBeforeSubmit,
          success: this._sxtAjaxFormOnSuccess,
          dataType: 'json',
          targetView: this
        });
        //fix
        sxtCommands.fixFormAjaxForm.call(this, $form);
      }
    },
    fixFormAjaxForm: function ($form) {
      //todo::!!!::unsolved - binding in ajaxForm is not working
      // workaround, binding in ajaxForm is not working
      $form.find('.form-actions input[type="submit"]')
          .on('click', $.proxy(function (event) {
            event.preventDefault();
            var pageData = this._sxtWizardPageData()
                , slogxtData = pageData.slogxtData || {}
            , selector = $(event.target).data('drupalSelector')
                , isSubmitBtn = (selector === 'edit-submit');
            if (slogxtData.trackFormPages && slogxtData.isOnEdit && isSubmitBtn
                && (slogxtData.needsPreview || !slogxtData.isOnLast)) {
              // prevent submission without preview
              this._sxtTrackFormNext(slogxtData);
            } else {
              var $form = this.$activeContent.find('form');
              $form.get(0).clk = event.target;
              $form.submit();
            }
          }, this));
    },
    preSubmitDefaultRole: function (slogxtData) {
      var $checked = slogxtData.$content.find('ul.slogxt-list li.radio.checked');
      slogxtData.$content
          .find('select[data-drupal-selector="edit-xt-default-role"]')
          .val($checked.attr('entityid'));
    },
    wizardDefaultRole: function ($content, data) {
      this._sxtCommandCallback('wizardFormEdit').apply(this, [$content, data]);
      var slogxtData = data.slogxtData
          , formResult = slogxtData.formResult || false;
      if (formResult === 'edit') {
        var items = this._sxtHtmlListRadios(slogxtData.items)
            , default_role = slogxtData.default_role || '???'
            , selector = 'ul.slogxt-list li[entityid="' + default_role + '"]'
            , pageData = this._sxtWizardPageData();
        $content.find('form').prepend($(items));
        $content.addClass('type-select type-radios li-no-margin');
        this._sxtAttachRadios($content);
        this.startEntityId = default_role;
        pageData.slogxtData.$content = $content;
        pageData.slogxtData.preSubmit = 'preSubmitDefaultRole';
        $content.find(selector).click();
      }
    },
    wizardUrgent: function ($content, data) {
      var clientResolve = data.slogxtData.clientResolve || false;
      if (clientResolve) {
        var pageData = this._sxtWizardPageData()
            , tabData = pageData.tabData
            , crr = clientResolve.resolve
            , key = crr.key
            , rnew = _.object([key], [crr.func])
            , callback = this._sxtCommandCallback(crr.func);
        tabData.resolve = $.extend({}, rnew, tabData.resolve);
        tabData.resolveArgs = tabData.resolveArgs || {};
        tabData.resolveArgs[key] = clientResolve.args;
        tabData.onClientResolve = true;
        tabData.serverResolved = false;
        callback.apply(this, [key, tabData]);
      } else if (!!data.messages) {  // messages 
        $content.prepend($(data.messages));
        Drupal.theme.slogxtMessagesPlain($content.find('.messages a'));
      }
      this._sxtDialogAdjust();
    },
    wizardFormInfo: function ($content, data) {
      var slogxtData = data.slogxtData || {}
      , htmlContent = slogxtData.htmlContent || '...no info';
      $content.hide();
      this._sxtSetHtml($content, htmlContent);
      $content.show();
      this.$btnSubmit.hide();
      this._sxtDialogAdjust();
    },
    wizardFormEdit: function ($content, data) {
      var slogxtData = data.slogxtData
          , formResult = slogxtData.formResult
          , htmlContent = slogxtData.htmlContent || ''
          , pageData = this._sxtWizardPageData()
          , submitted = data.submitted || false
          , submitError = submitted && data.hasError
          , callback;
      if (formResult === 'saved') {
        this.wizardFinished = true;
        this.$Tabs.sxtSetVisualHidden(true);
        this._sxtSetButtonsFinalized();
        this._sxtComponentAdd('form');
        this._sxtFormDetach($content);
        this._sxtSetHtml($content, htmlContent);
        pageData.slogxtData.savedLabels = data.labels;
        this._sxtTrackFormFinalize(pageData.slogxtData, data.labels);
        this._sxtDialogAdjust();
        var onFinished = slogxtData.onWizardFinished || {}
        , finishedCommand = onFinished.command || false;
        if (finishedCommand) {
          callback = this._sxtCommandCallback(finishedCommand);
          if (callback) {
            setTimeout($.proxy(function () {
              callback.call(this, onFinished.args || {});
            }, this), 10);
          }
        }
        // messages 
        if (!!data.messages) {
          $content.prepend($(data.messages));
          Drupal.theme.slogxtMessagesPlain($content.find('.messages a'));
        }
      } else if (!!htmlContent) {
        var alterForm = slogxtData.alterForm || {}
        , alterCommand = alterForm.command || false
            , alterTabPath = slogxtData.alterTabPath
            , tabPath = pageData.tabData.path;

        this._sxtComponentAdd('form');
        this._sxtFormDetach($content);
        $content.hide();
        this._sxtSetHtml($content, htmlContent);
        this._sxtFormAttach($content);
        var $form = $content.find('form');
        $form.find('.slogxt-input-field:not(:has(*))').remove();
        if (alterCommand) {
          callback = this._sxtCommandCallback(alterCommand);
          callback && callback.call(this, alterForm.args || {});
        }

        var $fields = $form.find('.slogxt-input-field').not('.visually-hidden')
            , $fWrapper = $fields.closest('.form-wrapper, .js-form-wrapper');
        if ($fields.length && $fields.length !== $fWrapper.length) {
          slogErr('EditFormView.wizardFormEdit::........fWrapper-missmatch...........');
        }
        $fields.last().addClass('xt-last-wrapper');

        pageData.slogxtData.$fWrapper = $fWrapper;
        pageData.slogxtData.$submit = $form.find('.form-actions input[id^="edit-submit"]');
        pageData.slogxtData.$preview = $form.find('.form-actions input[id^="edit-preview"]');
        pageData.slogxtData.needsPreview = pageData.slogxtData.$preview.length;
        pageData.slogxtData.preSubmit = slogxtData.preSubmit || false;

        var hasPreview = (formResult === 'preview') && !!slogxtData.htmlPreview;
        if (hasPreview) {
          $content.prepend($(slogxtData.htmlPreview));
          $content.scrollTop(0);
          pageData.slogxtData.curField = 'preview';
          pageData.slogxtData.previewLabels = data.labels;
          pageData.slogxtData.formValues = $form.formSerialize();
        } else {
          pageData.slogxtData.editLabels = data.labels;
          this._sxtTrackFormPrepare(pageData);
        }
        $content.toggleClass('show-preview', hasPreview);
        $content.show();

        // messages 
        if (!!data.messages) {
          $content.prepend($(data.messages));
          Drupal.theme.slogxtMessagesPlain($content.find('.messages a'));
        }

        this._sxtTrackFormFinalize(pageData.slogxtData, data.labels);
        !!slogxtData.tabledrag && this._sxtTabledragFormFinalize(slogxtData.tabledrag);
//        this._sxtDialogAdjust();
        pageData.slogxtData.isOnEdit && this._sxtDialogSetFocus($content);


        if (!slogxtData.formBroken) {
          if (alterTabPath && pageData.tabData.path) {
            for (var search in alterTabPath) {
              var replace = alterTabPath[search];
              tabPath = tabPath.replace(search, replace);
            }
            pageData.tabData.path = tabPath;

            var action = $form[0].action
                , splitted = action.split('/xtajx/');
            if (splitted.length === 2) {
              $form[0].action = splitted[0] + '/xtajx/' + tabPath;
            }
          }
          // using 'ajaxForm' 
          // see http://malsup.com/jquery/form/#ajaxForm
          $form.ajaxForm({
            target: '#' + $content.attr('id'),
            beforeSerialize: this._sxtAjaxFormBeforeSerialize,
            beforeSubmit: this._sxtAjaxFormBeforeSubmit,
            success: this._sxtAjaxFormOnSuccess,
            dataType: 'json',
            targetView: this
          });
          //fix
          sxtCommands.fixFormAjaxForm.call(this, $form);
        } else {
          this.$btnSubmit.button('disable');
          this._sxtSetButtonText(this.$btnSubmit, 'broken');
        }
      } else if (!!slogxtData.emptyWarning) {
        var $msg = Drupal.theme.slogxtMessage(slogxtData.emptyWarning, 'warning', true);
        $content.prepend($msg);
      }

      // add message if exists
      !!slogxtData.message && Drupal.theme.slogxtDlgAddMsg($content, slogxtData.message);

      if (submitted) {
        if (!submitError) {
          this.onFormSubmitSuccess($content, data);
        } else if (this.isScreenSmall) {
          this._sxtSetFocusInvalid(pageData.slogxtData);
          this.onFormSubmitError($content, data);
        }
      }

      this.$btnClose.button('enable');
    },
    attachSysTbmenuList: function ($content) {
      var xtHandler = _sxt.getXtHandler()
          , xtSubHandler = _sxt.getXtSubHandler()
          , pathPart = xtSubHandler ? xtSubHandler.baseAjaxPath() + 'menutid/' : false;
      if (!!pathPart) {
        $content.find('li.radio')
            .once('sxt-tbmenu-list')
            .on('click', $.proxy(function (e) {
              var tid = $(e.target).closest('li').attr('entityid')
                  , path = pathPart + tid
                  , pathData = {
                    path: path,
                    tid: tid,
                    toolbar: _sxt.getTbSysId(),
                    toolbartab: _sxt.getTbSysDummy()
                  };
              xtHandler.loadMenuSelectedSys(pathData, true);
              this._sxtSetDropbuttonDisabled(false);
            }, this));
        $content.find('li.radio.checked').click();
      }
    },
    finishedActionsWizard: function (args) {
//      slogLog('_sxt.dvc.commands.slogxt::......finishedActionsWizard.......');
      if (this.isWizard && this.actionWizardBaseData) {
        this.$btnBack.button('enable');
        this.$btnBack.show();
      }
      this.mainView && this.mainView.model.slogxtSetAndTrigger('change:finishedActionsWizard', null, args);
    },
    finishedAppearance: function (args) {
      var user = drupalSettings.slogxt.user;
      if (user.isAnonymous) {
        args.xt_color_tb = this.xtColorTb;
        args.xt_color_dialog = this.xtColorDlg;
        args.xt_color_active = this.xtColorAct;
        args.xt_color_mwcomment = this.xtColorMwc;
      }

      drupalSettings.slogxt.user.appearance = args;
      _sxt.setAppearanceColors();
      if (args.xt_colors_by_device) {
        _sxt.setLocalAppearance(args);
      }
    },
    alterFormAppearance: function () {
      var thisView = this
          , $form = this.$activeContent.find('form')
          , app = _sxt.getUserAppearance();
      if (app.xt_colors_by_device) {
        var values = _sxt.getLocalAppearance() || {};
        values.xt_color_tb && $form.find('#xt-color-tb').val(values.xt_color_tb);
        values.xt_color_dialog && $form.find('#xt-color-dialog').val(values.xt_color_dialog);
        values.xt_color_active && $form.find('#xt-color-active').val(values.xt_color_active);
        values.xt_color_mwcomment && $form.find('#xt-color-mwcomment').val(values.xt_color_mwcomment);
      }
      $form.find('#xt-color-tb')
          .once('sxt')
          .on('change', $.proxy(function (e) {
            e.stopPropagation();
            this.doAppearance = true;
            this.$pageWrapper
                .removeClass(app.classes_tb)
                .addClass(e.target.value);
            _sxt.setBgColorMainDropBtnEl();
            thisView.xtColorTb = e.target.value;
          }, thisView));
      $form.find('#xt-color-dialog')
          .once('sxt')
          .on('change', $.proxy(function (e) {
            e.stopPropagation();
            this.doAppearance = true;
            this.$body
                .removeClass(app.classes_dialog)
                .addClass(e.target.value);
            thisView.xtColorDlg = e.target.value;
          }, thisView));
      $form.find('#xt-color-active')
          .once('sxt')
          .on('change', $.proxy(function (e) {
            e.stopPropagation();
            this.doAppearance = true;
            this.$body
                .removeClass(app.classes_active)
                .addClass(e.target.value);
            thisView.xtColorAct = e.target.value;
          }, thisView));
      $form.find('#xt-color-mwcomment')
          .once('sxt')
          .on('change', $.proxy(function (e) {
            e.stopPropagation();
            this.doAppearance = true;
            this.$body
                .removeClass(app.classes_mwcomments)
                .addClass(e.target.value);
            thisView.xtColorMwc = e.target.value;
          }, thisView));
    }
  };
  _sxt.dvc.commands.slogxt = sxtCommands;

})(jQuery, Drupal, drupalSettings, _);
