/**
 * @file
 * slogxt/js/dvc/dvc.hooks.js
 * 
 * Hook components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, _) {

  var _sxt = Drupal.slogxt;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.hooks = _sxt.dvc.hooks || {};

  var sxtHooks = {
    onSysTbSelected: function ($item) {
      var entityid = $item.attr('entityid')
          , donView = _sxt.getDoNavigateView()
      , sourceData = donView.model.get('resultSource') || {};
      if (entityid && _sxt.isTbSysId(sourceData.toolbar)) {
        sourceData.targetTb = entityid;
        sourceData.toolbartab = sourceData.toolbartab.replace(_sxt.tokenTargetTb, entityid);
      }
    }
  };
  _sxt.dvc.hooks.slogxt = sxtHooks;

})(jQuery, Drupal, _);
