/**
 * @file
 * slogxt/js/dvc/dvc.donavigate.js
 * 
 * DoNavigate components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, _, _tse, undefined) {

  var _sxt = Drupal.slogxt;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.donavigate = {
    _sxtDonStartSilent: function (baseData) {
      this.donView = this._sxtDonEnsureView();
      this.donView.model.set('baseData', baseData);
    },
    _sxtDonStart: function (baseData) {
      this._sxtDonStartSilent(baseData);
      this.donView.activate(true);
    },
    _sxtDonEnsureView: function () {
      !this.donView && (this.donView = _sxt.getDoNavigateView(true));
      this.donView.dialogView = this;
      return this.donView;
    },
    _sxtDonResetIsCurrent: function () {
      var curPageIdx = this.getWizardCurPageIdx()
          , wId = 'donWrapper' + curPageIdx
          , $donWrapper = this.$activeContent.find('#' + wId);
      $donWrapper.find('li').removeClass('is-current');
    },
    _sxtOnChangeDonResult: function (donModel, result) {
      if (result && this.donView) {
        var action = result.action || false
            , donView = this.donView
            , baseData = _.clone(donModel.get('baseData'))
            , multiple = !!baseData.multiple
            , data = result.data || false
            , wpLabel = data.label || '???';
        this._sxtDonResetIsCurrent();

        if (action === 'none') {
          if (!multiple) {
            var result = this._sxtCacheGet('action', 'result');
            donModel.set('donResult', result, {silent: true});
          } else if (this.provideWizardPath) {
            var count = donView.getInfoCount();
            wpLabel = Drupal.t('Items') + ': ' + count;
            this._sxtSetWizardPath(wpLabel);
          }
          donView.activate(false);
          !!baseData.onDonReturn && baseData.onDonReturn.call(this, data);
        } else if (action === 'abort') {
          donView.activate(false);
          this.dialog.close();
        } else if (action === 'submit') {
          var baseArgs = baseData.args || {};
          if (!multiple) {
            this._sxtCacheSet('action', 'result', result);
            if (data.updateSource && _.isArray(data.updateSource)) {
              var rSource = _sxt.getDonResultSource(true);
              if (!!rSource) {
                _.each(data.updateSource, function (key) {
                  rSource[key] = data[key] || false;
                });
              }
            }
            !!baseArgs.isSource && (_sxt.setDonResultSource(data));
            this.provideWizardPath && this._sxtSetWizardPath(wpLabel.sxtTruncate(30));
            // close donView and show dialog
            donView.activate(false);
          }

          var pageData = this._sxtWizardPageData()
              , tabData = pageData.tabData
              , curPageIdx = this.getWizardCurPageIdx()
              , wId = 'donWrapper' + curPageIdx
              , $donWrapper = this.$activeContent.find('#' + wId)
              , liData = [{
                  liTitle: data.label,
                  liDescription: data.info,
                  entityid: data.entityId
                }]
              , htmlUL = this._sxtHtmlListCheckboxes(liData)
              , enable = (!!data.isEmptyList || !!data.entityId) ? 'enable' : 'disable'
              , html, $ul, $li;

          if (!$donWrapper.length) {
            html = Drupal.theme.slogxtWrapper(wId, '');
            this.$activeContent.find('.sxt-do-button').before(html);
            $donWrapper = this.$activeContent.find('#' + wId);
          }

          if (!multiple) {
            $donWrapper.html(htmlUL);
            $li = $donWrapper.find('ul>li');
          } else {
            var tEntity = data.targetEntity || {}
            , maxCount = baseData.maxCount || 10
                , infoHint = Drupal.t('Select more or return')
                , selCount;

            $ul = $donWrapper.find('>ul');
            if ($ul.length) {
              $li = $(htmlUL).find('>li');
              $ul.append($li);
            } else {
              $donWrapper.html(htmlUL);
              $li = $donWrapper.find('ul>li');
            }
            $li.attr('nodeid', tEntity.eid);

            donView.showSelectedCount($donWrapper.find('li').length);
            selCount = donView.getInfoCount();
            (selCount >= maxCount) && (infoHint = Drupal.t('max. number reached'))
            donView.resetSelected(baseData, infoHint);
            !!baseData.onChangeDonIsActive && baseData.onChangeDonIsActive(true, true);
          }

          if (!!tabData.path && data && data.entityId !== undefined) {
            pageData.type = 'resolved';
            pageData.resolved = true;
            if (multiple || tabData.resolve[baseData.resolveKey] !== data.entityId) {
              var nextPageData = this._sxtWizardPageData(curPageIdx + 1)
                  , nextTabData = !!nextPageData ? nextPageData.tabData || false : false
                  , nextPreserve = nextTabData ? !!nextTabData.preserve : false
                  , drr = this.donResolveReset || {}
              , nextReset = (!!drr.key && !!drr.command)
                  , nextOff = !nextReset && nextPreserve ? 2 : 1;
              if (nextReset && drr.key !== baseData.resolveKey) {
                tabData.resolve[drr.key] = drr.command;
                this.donResolveReset = false;
              }
              this._sxtWizardPagesDestroy(curPageIdx + nextOff);
            }
            tabData.resolve[baseData.resolveKey] = data.entityId;

            // attach
            if (baseData.onDonItemClick) {
              $li = $donWrapper.find('li').once('xt-don');
              $li.on('click', $.proxy(baseData.onDonItemClick, this));
              $li.click();
            }

            tabData.onClientResolve && (tabData.serverResolved = true);
            this.$btnSubmit.button(enable);
          }
          !!baseData.onDonSelected && baseData.onDonSelected.call(this, data);
        }
      }
    }
  };

})(jQuery, Drupal, _, Drupal.theme.slogxtElement);
