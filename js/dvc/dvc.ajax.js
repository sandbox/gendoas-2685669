/**
 * @file
 * slogxt/js/dvc/dvc.ajax.js
 * 
 * Ajax components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  var _sxt = Drupal.slogxt
      , _tse = Drupal.theme.slogxtElement;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.ajax = {
    completeLabel: Drupal.t('Finish'),
    _sxtAjaxResolvePath: function (path) {
      var pageData = this.isWizard ? this._sxtWizardPageData() : false
          , tabData = pageData ? pageData.tabData : {};
      if (tabData.resolve) {
        $.each(tabData.resolve, function (key, entityid) {
          var splitted = ('' + entityid).split(':');
          if (!!entityid && splitted.length === 1) {
            path = path.replace(new RegExp(key), entityid);
          }
        });
      }
      return path;
    },
    _sxtAjaxLoadContent: function ($content) {
//      slogLog('_sxt.DialogViewBase::......................._sxtAjaxLoadContent');
      $content = $content || this.$activeContent;
      var contentId = $content.attr('id')
          , $placeholder = $content.find('.slogxt-ajax-placeholder')
          , path = $placeholder.data('path') || false
          , targetid = $placeholder.attr('targetid') || false;
      if (path && targetid) {
        this.sxtTabsLocked = true;
        $placeholder.data('path', false); //once only
        var ajax = _sxt.ajaxCreate('slogRetrieveForm', $placeholder);
        // make ajax object global
        _sxt.ajax[contentId] = ajax;
        // send ajax request by triggering slogRetrieveForm event
        path = this._sxtAjaxResolvePath(path);
        ajax.url = ajax.options.url = Drupal.url(path.replace(/^\//, ''));
        ajax.options.data = {
          wrapperId: contentId
        };
        ajax.slogxt.targetid = targetid;
        ajax.slogxt.$content = $content;
        ajax.slogxt.targetThis = this;
        ajax.slogxt.targetCallback = this._sxtAjaxResponse;

        this.ajaxAlterBeforeTrigger(ajax); //hook
        ajax.$xtelement.trigger('slogRetrieveForm');
        this.loaded[targetid] = contentId;
      }
    },
    _sxtAjaxResponse: function (ajax, response, status) {
//      slogLog('DialogViewBase._sxtAjaxResponse::.....status: ' + status);
      if (status !== 'success') {
        return;
      }

      var $content = ajax.slogxt.$content
          , targetThis = ajax.slogxt.targetThis
          , data = response.data || {}
      , labels = data.labels || false
          , slogxtData = data.slogxtData || {}
      , runCommand = slogxtData.runCommand || 'default';
      !!targetThis && (targetThis.sxtTabsLocked = false);

      if (this.isWizard) {
        this.isLastPage = slogxtData.isLastPage || false;
        this.isWizardActionPage = slogxtData.isWizardActionPage || false;
        this.wizardFinished = slogxtData.wizardFinished || false;
        this.wizardFinalize = slogxtData.wizardFinalize || false;
        this.wizardFinalize && (this._sxtSetWizardPath(this.completeLabel));
        //wizardFinalize
        this._sxtDialogSetLabels(labels);
        if (this.isLastPage || this.isWizardActionPage) {
          this.$btnSubmit.hide();
        } else if (this.wizardFinished) {
          this.$btnSubmit.hide();
          this.$btnBack.hide();
        } else {
          this.$btnSubmit.show();
        }

        var pageData = this._sxtWizardPageData()
            , tabData = pageData.tabData;
        tabData.transparentOverlay = slogxtData.transparentOverlay || false;
        tabData.transparentOverlay && this._sxtSetOverlayTransparent(true);
        pageData.$content = $content;
        pageData.target = ajax.slogxt.targetid;
        pageData.labels = labels;
        pageData.isLastPage = this.isLastPage;
        pageData.isWizardActionPage = this.isWizardActionPage;
        if (slogxtData.pageActions && slogxtData.pageActions) {
          pageData.actionsHtml = Drupal.theme.slogxtDropbutton(slogxtData.pageActions);
        }
        !!slogxtData.autoEnableSubmit && (tabData.autoEnableSubmit = true);
        tabData.isEditForm = (slogxtData.isEditForm && !pageData.isWizardActionPage);
        if (tabData.isEditForm) {
          tabData.autoEnableSubmit = true;
          tabData.storageLocalKey = slogxtData.storageLocalKey;
        }
      }

      var callback = this._sxtCommandCallback(runCommand);
      if (callback) {
        if (runCommand === 'reload') {
          callback && callback.call(this, slogxtData.args || {});
        } else {
          callback && callback.apply(this, [$content, data]);
          if (!!slogxtData.attachCommand) {
            var callback = this._sxtCommandCallback(slogxtData.attachCommand);
            callback && callback.call(this, $content);
          }
          this.onAjaxResponseDone($content, data); //hook
        }
      }
    },
    _sxtAjaxFormBeforeSerialize: function (formData, $form, options) {
      slogLog('_sxt.DialogViewBase::.............._sxtAjaxFormBeforeSerialize');
      if ($form && drupalSettings.ajaxPageState) {
        $form.data = {ajax_page_state: drupalSettings.ajaxPageState};
      }
    },
    _sxtAjaxFormBeforeSubmit: function (formData, $form, options) {
      slogLog('_sxt.DialogViewBase::......................._sxtAjaxFormBeforeSubmit');
      // this for ajax
      var view = this.targetView;
      if (view) {
        view.$btnSubmit.button('disable');
        view.$btnBack.button('disable');
        $form.off();
        view.ajaxFormBeforeSubmit(formData, $form, options); //hook
      } else {
        slogErr('Ajax requires a pointer to the target view.');
        return false;
      }
    },
    _sxtAjaxFormOnSuccess: function (responses, status, xhr, $form) {
      slogLog('_sxt.DialogViewBase::......................._sxtAjaxFormOnSuccess');
      var idx, response, command, contentId
          , xtData, xtAjax, xtView;
      for (idx in responses) {
        response = responses[idx];
        command = response.command;
        if (command === 'slogxtInvoke') {
          xtData = response.data;
          contentId = xtData.wrapperId;
          xtAjax = contentId ? _sxt.ajax[contentId] : false;
          xtView = xtAjax ? xtAjax.slogxt.targetThis : false;
          break;
        }
      }
      if (xtAjax) {
        for (idx in responses) {
          response = responses[idx];
          command = response.command;
          switch (command) {
            case 'slogxtInvoke':
            case 'insert':
              break;  // do nothing
            default:
              xtAjax.commands[command](xtAjax, response, status);
              slogLog('_sxtAjaxFormOnSuccess::command: ' + command);
              break;
          }
        }
        if (xtView) {
          slogLog('_sxtAjaxFormOnSuccess::command: .........slogxtInvoke');
          xtView._sxtAjaxResponse(xtAjax, response, status);
          xtView.ajaxFormOnSuccess(response, status, xhr, $form); //hook
        }

      }
    }
  };

})(jQuery, Drupal, drupalSettings, Backbone, _);
