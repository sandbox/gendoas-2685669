/**
 * @file
 * slogxt/js/dvc/dvc.cache.js
 * 
 * Cache components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, _) {

  var _sxt = Drupal.slogxt;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.cache = {
    dvcCache: {},
    _sxtCacheReset: function (part) {
      !part ? (this.dvcCache = {}) : (this.dvcCache[part] = {});
    },
    _sxtCacheSet: function (part, subkey, val) {
      if (!part) {
        this.dvcCache = val;
      } else {
        this.dvcCache = this.dvcCache || {};
        if (!subkey) {
          this.dvcCache[part] = val;
        } else {
          this.dvcCache[part] = this.dvcCache[part] || {};
          this.dvcCache[part][subkey] = val;
        }
      }
    },
    _sxtCacheGet: function (part, subkey) {
      this.dvcCache = this.dvcCache || {};
      part && (this.dvcCache[part] = this.dvcCache[part] || {});
      var result = !part ? (this.dvcCache) : (this.dvcCache[part]);
      return (!subkey ? result : result[subkey]);
    }
  };

})(jQuery, Drupal, _);
