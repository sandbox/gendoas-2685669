/**
 * @file
 * slogxt/js/dvc/dvc.init.js
 * 
 * Initialize components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  var _sxt = Drupal.slogxt
      , _tse = Drupal.theme.slogxtElement;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.init = {
    _sxtInitDialog: function () {  // on initialize, create dialog
      var dlgOpt = this.dialogOptions() || {}
      , slogxt = dlgOpt.slogxt || {};
      dlgOpt.slogxt && (delete dlgOpt.slogxt);
      this.dialogId = _.result(slogxt, 'dialogId', false);
      this.restoreState = this.dialogId ? _.result(slogxt, 'restoreState', false) : false;
      this.restoreState && this._sxtRestoreState();

      var options = $.extend(true, this._sxtDialogDefaults(), dlgOpt)
          , contentData = _.result(slogxt, 'contentData', false);

      this.sxtTabsLocked = false;
      this.isDialogModal = _.result(slogxt, 'isDialogModal', true);
      this.isWizard = _.result(slogxt, 'isWizard', false);
      this.provideWizardPath = _.result(slogxt, 'provideWizardPath', false);
      this.pagesData = [];
      this.hideTabs = slogxt.isWizard || slogxt.hideTabs || false;
      //btnSubmitShow
      this.closeLabel = _.result(slogxt, 'closeLabel', this.isWizard ? Drupal.t('Cancel') : false);
      this.btnSubmitShow = _.result(slogxt, 'btnSubmitShow', true);
      this.hasBtnSubmit = this.btnSubmitShow || _.result(slogxt, 'hasBtnSubmit', true);
      this.btnBackShow = _.result(slogxt, 'btnBackShow', false);
      this.hasBtnBack = this.isWizard || this.btnBackShow || _.result(slogxt, 'hasBtnBack', false);
      this.hasMaximize = _.result(slogxt, 'hasMaximize', true);
      this.actionsForRadios = _.result(slogxt, 'actionsForRadios', false);
      this.hasItemAction = _.result(slogxt, 'hasItemAction', false);
      this.hasStartActions = false;
      if (!this.isForNavigate && !this.hasItemAction && _sxt.isUserAuthenticated()) {
        this.actionsHtml = _.result(slogxt, 'actions', false);
        if (_.isObject(this.actionsHtml)) {
          this.actionsHtml = Drupal.theme.slogxtDropbutton(this.actionsHtml);
        }
        this.hasStartActions = !!this.actionsHtml;
      }
      this.hasPushpin = _.result(slogxt, 'hasPushpin', false);
      this.tabsData = _.result(slogxt, 'tabsData', false);
      if (!this.tabsData && contentData) {
        this.tabsData = {
          targets: [0],
          0: contentData
        };
      }
      this.provideWizardPath && (this.tabsWizardData = {});

      // prepare content data
      this._sxtPrepareContentData();
      (this.isWizard || this.hasStartActions) && this._sxtComponentAdd('wizard');
      this.restoreState && this._sxtStateLoad();
      var widthPercent = this.model.get('widthPercent') || false;
      widthPercent && (options.width = widthPercent);

      if (_.isArray(dlgOpt.addComponent) && !_.isEmpty(dlgOpt.addComponent)) {
        for (var idx in dlgOpt.addComponent) {
          this._sxtComponentAdd(dlgOpt.addComponent[idx]);
        }
      }

      // now create the dialog
      this.dialog = Drupal.dialog(this.$el, options);
      this.dialog.view = this;  // be aware of circular reference      
    },
    _sxtPrepareContentData: function () {
      if (!this.tabsData) {
        slogErr('_sxtPrepareContentData: unvalid content data.');
        this.tabsData = {
          targets: [0],
          0: '<div>No valid content found.</div>'
        };
      }
      if (!this.dialogId) {
        slogErr('_sxtPrepareContentData: missing dialogId.');
        this.dialogId = 'NoValidDialogId';
      }

      var tabsData = this.tabsData
          , autoEnableSubmit = tabsData ? tabsData.autoEnableSubmit : undefined
          , tIdx, tabData, targetid;
      for (tIdx in tabsData.targets) {
        targetid = tabsData.targets[tIdx]
        tabData = tabsData[targetid];


        if (tabData.autoEnableSubmit === undefined && autoEnableSubmit !== undefined) {
          tabData.autoEnableSubmit = autoEnableSubmit;
        }
      }
    },
    _sxtDialogDefaults: function () { // dialog create defaults, see _sxtInitDialog
      function getPosAt() {
        var pos = thisView.model.get('initPosition') || thisView._sxtGetDefaultInitPosition();
        return 'left+' + pos.left + 'px top+' + pos.top + 'px';
      }

      var thisView = this
          , withinDims = this._sxtDialogWithinDims(this.$dialogWrapper)
          , options = {
            // slogxt extra data, not for dialog
            slogxt: {
              isDialogModal: true,
              hasMaximize: true
            },
            // event handling
            create: $.proxy(this._sxtDialogOnCreate, this),
            beforeClose: function () {
              thisView.model.set('initPosition', thisView.$widget.position());
              thisView._sxtStateWrite();
            },
            close: $.proxy(this._sxtDialogOnClose, this),
            resizeStop: $.proxy(this._sxtDialogOnResizeStop, this),
            // data
            autoOpen: false,
            autoResize: false,
            closeOnEscape: !_sxt.isScreenSmall(),
            closeText: Drupal.t('Close'),
            width: '50%',
            height: 'auto',
            minHeight: 240,
            minWidth: 360,
            maxHeight: withinDims.maxHeight,
            maxWidth: withinDims.maxWidth,
            position: {
              my: 'left top',
              at: getPosAt(),
              of: this.$dialogWrapper,
              collision: 'fit fit'
            },
            buttons: {
              Navigate: {
                text: Drupal.t('Navigate'),
                click: $.proxy(this._sxtDoNavigate, this),
                create: function () {
                  var cls = thisView.showNavigateBtn ? '' : ' visually-hidden';
                  thisView.$btnNavigate = $(this);
                  thisView.$btnNavigate.attr('id', 'sxtbtn-do-navigate');
                  thisView.$btnNavigate.addClass('ui-button-icon-only icon-navigate' + cls);
                }
              },
              Back: {
                text: Drupal.t('Back'),
                click: $.proxy(this._sxtDialogBack, this),
                create: function () {
                  thisView.$btnBack = $(this);
                }
              },
              Submit: {
                text: Drupal.t('Submit'),
                click: $.proxy(this._sxtDialogSubmit, this),
                create: function () {
                  thisView.$btnSubmit = $(this);
                }
              },
              Close: {
                text: Drupal.t('Close'),
                click: function () {
                  if (!!thisView.doReload) {
                    thisView.$btnClose.button('disable');
                    thisView._sxtReloadExecute();
                  } else {
                    $(this).dialog('close');
                  }
                },
                create: function () {
                  thisView.$btnClose = $(this);
                }
              }
            }
          };

      if (this.$dialogWrapper.length) {
        options.appendTo = '#' + this.$dialogWrapper[0].id;
      }
      return options;
    },
    _sxtDialogOnCreate: function (e) {  //on create event, see _sxtDialogDefaults::create()
      this._sxtInitDialogWidget();
      this._sxtDialogAttach();
      this._sxtInitContent();
      this.$dialogWrapper.show();
      this.onDialogCreated(); //hook
      // dialog is created
      this.model.set('dialogCreated', true);
    },
    _sxtDialogOnClose: function (e) {  //on close event, see _sxtDialogDefaults::close()
      this.donView && (this.donView.dialogView = null);
      this._sxtDetachDialog();
      this._sxtDestroyThisView();
      _sxt.setIsOpenModalDialog(null);
      Drupal.detachBehaviors(e.target, null, 'unload');
      this.$dialogWrapper.hide();
      this.onDialogClosed(e); //hook
      setTimeout(function () {  // event
        _sxt.stateModel().slogxtSetAndTrigger('change:dialogClosed');
      });
    },
    _sxtDialogSetTitleMax: function () {
      var mw = this.$widget.width() - 72 - (this.hasPushpin ? 38 : 0);
      this.$titlebar.find('.ui-dialog-title').css('max-width', mw);
    },
    _sxtDialogOnResizeStop: function (e, ui) {
      var ofWidth = this.$widget.width()
          , withinWidth = this.$body.outerWidth(true)
          , pwidth = parseInt((ofWidth / withinWidth) * 10000, 10) / 100;
      this.model.set('widthPercent', pwidth + '%');
      this.model.set('widgetHeight', ui.size.height);
      this._sxtStateWrite();
      this._sxtDialogSetTitleMax();
    },
    _sxtInitContent: function () {  //on create event, see _sxtDialogOnCreate()
      var startTarget = this.tabsData.startTarget || this.tabsData.targets[0]
          , $tabEl = this._sxtTabElement(startTarget);
      // create/show content by tab click
      $tabEl.click();
    },
    _sxtInitDialogWidget: function () {  //see _sxtDialogOnCreate()
//      slogLog('DialogViewBase................._sxtInitDialogWidget');
      var $widget = this.$widget = this.$el.dialog('widget')
          , idXt = this.isForNavigate ? '-for-navigate' : '';
      $widget.attr('id', 'slogxt-modal-dialog-widget' + idXt);
      this.$widget.sxtSetVisualHidden(true);

      var tabsData = this.tabsData
          , iconized = tabsData.iconized || false;

      this.$el.append($(_tse('div', {id: this.tabsId, class: 'visually-hidden'})));
      this.$el.append($(_tse('div', {id: this.contentID})));
      this.$Tabs = this.$el.find('#' + this.tabsId);
      this.$Tabs.toggleClass('iconized', iconized);
      this.$Content = this.$el.find('#' + this.contentID);
      this.$activeContent = this.$Content;
      this.$Tabs.append($(_tse('div', {id: this.tabsHelperID})));
      this.$tabsHelper = this.$Tabs.find('#' + this.tabsHelperID);
      this.doReload = false;

      this._sxtPrepareTitlebarButtons();
      this._sxtPrepareButtonpaneButtons();
      this._sxtTabsCreate();
    },
    _sxtPrepareTitlebarButtons: function () {
      var $titlebar = this.$titlebar = this.$widget.find('.ui-dialog-titlebar');
      $titlebar.attr('id', 'slogxt-modal-dialog-titlebar');
      $titlebar.on('click', $.proxy(this, '_sxtDialogSetFocus', undefined));

      // pushpin button
      if (this.hasPushpin) {
        this.$titlebar.prepend($(Drupal.theme.slogxtPushpin()));
      }

      // system buttons: close, maximize
      var $btnClose = this.$titlebar.find('.ui-dialog-titlebar-close')
          , $buttonSet = $('<div>').addClass('ui-dialog-buttonset slogxt-system');
      $btnClose.appendTo($buttonSet);
      $buttonSet.appendTo(this.$titlebar);
      if (this.hasMaximize) {
        var $btnMaximize = $("<button type='button'></button>")
            .button({
              create: function () {
                $(this).hover(function () {
                  $(this).addClass('ui-state-hover');
                }, function () {
                  $(this).removeClass('ui-state-hover');
                });
              },
              icons: {
                primary: 'ui-icon-newwin'
              },
              text: false
            })
            .addClass("ui-dialog-titlebar-maximize")
            .appendTo($buttonSet);
        this.$btnMaximizeIcon = $btnMaximize.find('>span.ui-icon');
      }

      this.$btnPushpin = this.$titlebar.find('.sxt-button.ui-icon-pushpin');
      this.$btnMaximize = this.$titlebar.find('.ui-dialog-titlebar-maximize');
      this.$btnMaximizeIcon = this.$btnMaximize.find('>span.ui-icon');
      this.model.set('maximized', false);
    },
    _sxtSetButtonpaneButtonsForRadios: function (value) {
      this.$buttonpane.toggleClass('action-for-radios', value);
      this.$actionsCheckbox.add(this.$btnActionsOpen).toggleClass('visually-hidden', value);
      this.$btnActionsOpen.toggleClass('checked', value);
      this._sxtShowActionsCheckboxes(value);
    },
    _sxtPrepareButtonpaneButtons: function () {
      var $buttonpane = this.$buttonpane = this.$widget.find('.ui-dialog-buttonpane');
      this.$dialogContent = this.$widget.find('.ui-dialog-content');
      $buttonpane.attr('id', 'slogxt-modal-dialog-buttonpane');
      $buttonpane.on('click', $.proxy(this, '_sxtDialogSetFocus', undefined));

      !!this.closeLabel && this._sxtSetButtonText(this.$btnClose, this.closeLabel);
      this.btnSubmitShow ? this.$btnSubmit.show() : this.$btnSubmit.hide();
      this.btnBackShow ? this.$btnBack.show() : this.$btnBack.hide();
      // build actions if required
      if (this.hasStartActions) {
        this._sxtActionsCreate(this.actionsHtml);
        if (!!this.actionsForRadios) {
          this._sxtSetButtonpaneButtonsForRadios(true);
          this._sxtSetDropbuttonDisabled(true);
        }
      }
    },
    _sxtDialogAttach: function () {
//      slogLog('_sxt.DialogView::..........._sxtDialogAttach');
      this.attach && this.attach();
      var $el = this.$el
          , $widget = this.$widget;
      $widget.once('slogxt')
          .on('click', function (e) {
            return false;
          });

      // $titlebar::pushpin
      if (this.hasPushpin) {
        this.$btnPushpin.once('slogxt')
            .on('click', $.proxy(function (e) {
              this._sxtPushpinChecked(!this.$btnPushpin.hasClass('checked'));
            }, this));
        if (!!this.isForNavigate) {
          this.$btnPushpin.hide();
          this._sxtPushpinChecked(true);
        }
      }
      // $titlebar::maximize/restore
      if (this.hasMaximize) {
        this.$btnMaximize.once('slogxt')
            .on('click', $.proxy(function (e) {
              e.stopPropagation();
              var doMaximize = !this.$widget.hasClass('maximized')
                  , isSmall = this.screenModel.get('isSmall');
              if (isSmall && !doMaximize) {
                this._sxtDoNavigate(true);
              } else {
                this._sxtDoMaximize(doMaximize);
                this.model.set('maximizedExplicit', doMaximize);
                this._sxtStateWrite();
              }
            }, this));
      }
      // actions
      this.hasStartActions && this._sxtDialogAttachActions();
      // attach tabs
      this._sxtTabsAttach();
    },
    _sxtDialogAttachActions: function () {
      Drupal.behaviors.dropButton.attach(this.$widget.get(0), {});
      this.$buttonpane.find('.dropbutton-toggle')
          .once('dropbutton-toggle')
          .on('click', _sxt.dropbuttonClickHandler)
          .sxtDropbuttonSetView(this);
      this.$buttonpane.find('.dropbutton-action a')
          .once('slogxt')
          .on('click', $.proxy(function (e) {
            _sxt.dropbuttonActionDispatch(e, this);
          }, this));

      this.$btnActionsOpen.once('slogxt')
          .on('click', $.proxy(function (e) {
            e.stopPropagation();
            this._sxtActionsOpen(!this.$btnActionsOpen.hasClass('checked'));
          }, this));
      this.$buttonpane.find('.slogxt-dropbutton ul.dropbutton li.dropbutton-action a')
          .on('contextmenu', function () {
            return false;
          });
    },
    _sxtDialogContentAttach: function ($content) {
      $content = $content || this.$activeContent;
      $content.find('li')
          .once('slogxt')
          .on('click', $.proxy(function (e) {
            e.stopPropagation();
            var $li = $(e.target).closest('li'), checkedData;
            if (!this.activeHasActions && $content.hasClass('type-checkboxes') && !this.isWizardActionPage) {
              $(e.currentTarget).toggleClass('checked');
              checkedData = this._sxtCalculateCheckedItems();
            } else if ($content.hasClass('type-radios')) {
              $li.addClass('checked');
              $li.siblings().removeClass('checked');
              checkedData = this._sxtCalculateCheckedItems();
            } else {
              var preventClose = this.preventClose || this.isWizardActionPage;
              $li.siblings().removeClass('is-current');
              $li.addClass('is-current');
              this.onListItemClicked(e);  //hook
              !preventClose && !this._sxtPushpinChecked() && this.dialog.close();
            }
          }, this));
      $content.find('li .li-checkbox')
          .once('slogxt')
          .on('click', $.proxy(function (e) {
            e.stopPropagation();
            $(e.target).closest('li').toggleClass('checked');
            this._sxtCalculateCheckedItems();
          }, this));
    },
    _sxtDetachDialog: function () {
      this.detach && this.detach();
      var $actions = this.$widget.find('.actions-content');
      if ($actions.length) {
        $actions.removeOnce('dropbutton-toggle').off();
        _sxt.clearDropbuttons(this.$buttonpane);
      }
      this.$widget.removeOnce('slogxt').off();
      this.$el.empty();
    },
    _sxtActionsCheckboxCreate: function () {
      if (!this.$actionsCheckbox) {
        this.$actionsCheckbox = $(_tse('div', {class: 'slogxt-icon actions-checkbox'}));
        this.$buttonpane.prepend(this.$actionsCheckbox);
        // attach outright, _sxtDialogAttach() is gone
        this.$actionsCheckbox.once('slogxt')
            .on('click', $.proxy(function (e) {
              e.stopPropagation();
              var $aChekbox = $(e.target)
                  , isChecked = $aChekbox.hasClass('checked')
                  , isUndefined = $aChekbox.hasClass('undefined');
              if (isUndefined || isChecked) {
                $aChekbox.removeClass('checked undefined');
                this.$activeContent.find('li').removeClass('checked');
              } else {
                $aChekbox.addClass('checked');
                this.$activeContent.find('li').addClass('checked');
              }
              this._sxtCalculateCheckedItems();
            }, this));
      }
    },
    _sxtActionsCreate: function (itemsHtml) {
      itemsHtml = itemsHtml || Drupal.theme.slogxtDummyDropbutton();
      var content = _tse('div', {class: 'actions-content'}, itemsHtml);
      this.$buttonpane.prepend($(Drupal.theme.slogxtDialogActions(content)));
      this.$btnActionsOpen = this.$buttonpane.find('.sxt-button.ui-icon-actions');
      this.$actionsWrapper = this.$buttonpane.find('.actions-wrapper');
      this._sxtActionsCheckboxCreate();
    }
  };

})(jQuery, Drupal, drupalSettings, Backbone, _);
