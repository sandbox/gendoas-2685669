/**
 * @file
 * slogxt/js/dvc/dvc.wizard.js
 * 
 * Wizard components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  var _sxt = Drupal.slogxt
      , _sxth = _sxt.helper;

  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.wizard = {
    _sxtWizardGoNext: function () {
      if (!this.isWizard || this.wizardFinished) {
        return;   // no next/back on wizardFinished
      }

      this.$btnSubmit.button('disable');
      this.$btnBack.button('disable');
      var curPageIdx = this.getWizardCurPageIdx()
          , pageData = this._sxtWizardPageData(curPageIdx)
          , pageType = pageData.type || 'none'
          , slogxtData = pageData.slogxtData || {}
      , tabData = pageData.tabData
          , newPageIdx = curPageIdx + 1
          , newPageData = this._sxtWizardPageData(newPageIdx);

      this.isLastPage = (newPageData && newPageData.isLastPage);
      this.isWizardActionPage = (newPageData && newPageData.isWizardActionPage);
      if (this.isWizardActionPage && this.$actionsWrapper) {
        this._sxtResetWizardActionPage(true);
        this.$actionsWrapper.show();
      }
      if (this._sxtIsWizardCheckboxes()) {
        var cbFunc = tabData.isEditForm
            ? '_sxtWizardProcessCheckboxesForm' : '_sxtWizardProcessCheckboxes';
//        this['_sxtWizardProcessCheckboxes'](pageData.$content, slogxtData, newPageIdx);
        this[cbFunc](pageData.$content, pageData);
      } else if (slogxtData.trackFormPages) {
        this._sxtTrackFormNext(slogxtData);
      } else if (tabData && tabData.submitCallback) {
        tabData.submitCallback();
      } else {
        var callback = '_sxtWizardProcess' + pageType.capitalize();
        if (this[callback](pageData.$content, slogxtData, newPageIdx)) { //returns changed
          this._sxtWizardPagesDestroy(newPageIdx);
          this[callback](pageData.$content, slogxtData, newPageIdx);
        }
        newPageData = this._sxtWizardPageData(newPageIdx);

        if (_.isEmpty(newPageData)) {
          var bundle = (this.dialogId).toLowerCase()
              , tabHtml = this._sxtTabsHtml([{
                  contentId: _sxth.toID(['slogxt-' + bundle, newPageIdx].join('-')),
                  itemId: _sxth.toID(bundle + '-' + newPageIdx),
                  label: this.provideWizardPath ? '...' : 'p' + newPageIdx,
                  targetId: newPageIdx
                }])
              , $newTab = $(tabHtml);
          !!this.actionWizardBaseData && $newTab.addClass('actions-wizard');
          this.$Tabs.append($newTab);
          this._sxtTabsAttach();

          // create new page data with $tab required
          newPageData = this._sxtWizardPageData(-1, {$tab: $newTab, slogxtData: {}});
        } else if (!!this.actionWizardBaseData && !!newPageData.isFinalPage) {
          this.$btnSubmit.hide();
        } else if (this.isLastPage || this.isWizardActionPage) {
          this.$btnSubmit.hide();
        }

        if (newPageData.slogxtData && newPageData.slogxtData.skipPage) {
          this._sxtDelayedClick(this.$btnSubmit);
        }

        if (!_.isEmpty(newPageData)) {
          var hideTabs = this.hideTabs || newPageIdx < 1;
          if (!!this.actionWizardBaseData) {
            hideTabs = true;
          } else if (this.isWizard && newPageIdx > 0 && this.provideWizardPath) {
            this.$el.addClass('has-wizard-path');
            hideTabs = false;
          }
          this.$Tabs.sxtSetVisualHidden(hideTabs);
          this.model.set('wizardCurrentPage', newPageIdx);
          slogxtData = newPageData.slogxtData;
          if (slogxtData.trackFormPages) {
            this._sxtTrackFormFinalize(slogxtData, slogxtData.editLabels);
          } else {
            this._sxtWizardShowBtnBack(slogxtData);
          }
          if (newPageData.type === 'radios') {
            var $item = newPageData.$content.find('li.radio.checked');
            $item.length && this.$btnSubmit.button('enable');
          } else if (newPageData.type === 'resolved') {
            !!newPageData.resolved && this.$btnSubmit.button('enable');
          }
        } else {
          slogErr('_sxt.dvc.wizard._sxtWizardGoNext.. not handled !!!!');
        }
      }
    },
    _sxtWizardGoBack: function () {
//      slogLog('_sxt.DialogView::......................._sxtWizardGoBack');
      var curPageIdx = this.getWizardCurPageIdx()
          , pageData = this._sxtWizardPageData(curPageIdx)
          , slogxtData = pageData.slogxtData || {};
      if (curPageIdx === this.actionStartIdx || this.wizardFinished) {
        this._sxtActionsWizardReset();
        return;
      }

      this.$btnSubmit.button('disable');
      this.$btnSubmit.show();
      this.$btnBack.button('disable');
      this._sxtResetWizardActionPage(false);

      var slogxtData = this._sxtWizardPageData().slogxtData || {};
      if (slogxtData.trackFormPages && (slogxtData.isOnPreview || !slogxtData.isOnFirst)) {
        this._sxtTrackFormBack(slogxtData);
      } else if (curPageIdx > 0) {
        this.model.set('wizardCurrentPage', curPageIdx - 1);
        curPageIdx = this.getWizardCurPageIdx();
        pageData = this._sxtWizardPageData(curPageIdx);
        slogxtData = pageData.slogxtData || {};
        this.$btnSubmit.button('enable');
        this.$btnBack.button('enable');
        this._sxtWizardShowBtnBack(slogxtData);
        if (curPageIdx > 0 && slogxtData.skipPage && slogxtData.hasPreviousPage) {
          this._sxtDelayedClick(this.$btnBack);
        }
      }
    },
    _sxtResetWizardActionPage: function (isWizardActionPage) {
      if (this.$actionsWrapper || this.hasStartActions) {
        var openActions = isWizardActionPage && this._sxtHasActionStartPage();
        this.$activeContent.toggleClass('type-checkboxes', openActions);
        openActions ? this.$actionsCheckbox.show() : this.$actionsCheckbox.hide();
        this._sxtActionsOpen(openActions);
        isWizardActionPage ? this.$actionsWrapper.show() : this.$actionsWrapper.hide();
        this.isWizardActionPage = (isWizardActionPage && !this.hasStartActions);
      } else {
        this.isWizardActionPage = false;
      }
    },
    _sxtWizardShowBtnBack: function (slogxtData) {
      if (this.wizardFinished) {
        this.$btnBack.hide();
      } else {
        var curPageIdx = this.getWizardCurPageIdx()
            , hasPrev = this.sdHasPrevUnskippedPageIdx(curPageIdx - 1);
        slogxtData.hasPreviousPage = hasPrev;
        if (slogxtData.trackFormPages && (slogxtData.isOnPreview || !slogxtData.isOnFirst)) {
          this.$btnBack.show();
        } else if (!!this.actionWizardBaseData) {
          this.$btnBack.show();
        } else {
          (curPageIdx && hasPrev) ? this.$btnBack.show() : this.$btnBack.hide();
        }
      }
    },
    _sxtWizardNextLabel: function (labels) {
      labels = labels || this._sxtWizardPageData().labels || {};
      return labels.submitNext || Drupal.t('Next');
    },
    _sxtWizardProcessNone: function ($content, slogxtData, newPageIdx) {
      return false;
    },
    _sxtWizardProcessResolved: function ($content, slogxtData, newPageIdx) {
      if (!this.tabsData[newPageIdx]) {
        this.tabsData.targets[newPageIdx] = newPageIdx;
        this.tabsData[newPageIdx] = _.clone(this._sxtWizardPageData().tabData);
      }
    },
    _sxtWizardProcessRadios: function ($content, slogxtData, newPageIdx) {
      var $item = $content.find('li.radio.checked')
          , itemTitle = $item.find('.li-title').sxtGetTopText()
          , actionId = $item.attr('id') || false
          , idxNew = parseInt($item.attr('index'), 10)
          , changed = (slogxtData.selectedIdx !== idxNew)
          , path = $item.data('path')
          , resolve = $item.data('resolve') || false
          , resolveArgs = $item.data('resolveargs') || false
          , wizardPath = this._sxtWizardPageData()['wizardPath'] || false
          , parts = _sxth.splitPath('::' + path, '::', ['ns', 'callback'])
          , ns = parts['ns'] || false
          , method = (ns === 'tabsDataFn') ? parts['callback'] || false : false
          , callback = method && this[ns] ? this[ns][method] || false : false
          , hooks = $item.data('hooks') || false;
      slogxtData.selectedIdx = idxNew;
      if (changed && !!hooks) {
        hooks = JSON.parse(decodeURI(hooks));
        !!hooks.onSelected && this._sxtHookCall(hooks.onSelected, [$item, slogxtData]);
      }
      !!resolve && (resolve = JSON.parse(decodeURI(resolve)));
      !!resolveArgs && (resolveArgs = JSON.parse(decodeURI(resolveArgs)));
      (this.provideWizardPath && wizardPath) && (wizardPath.processed = wizardPath.selected);

      if (!this.tabsData[newPageIdx]) {
        this.tabsData.targets[newPageIdx] = newPageIdx;
        if (callback && $.isFunction(callback)) {
          this.tabsData[newPageIdx] = callback.apply(this);
        } else if (!callback && !!path) {
          this.tabsData[newPageIdx] = this._sxtInitTabDataByPath(path, resolve, resolveArgs);
        }
        if (this.tabsData[newPageIdx]) {
          this.tabsData[newPageIdx]['actionTitle'] = itemTitle;
          this.tabsData[newPageIdx]['dialogTitle'] = itemTitle;
        }
      }

      if (!this.actionWizardBaseData && newPageIdx === 1) {
        (!this.baseActionId || this.baseActionId !== actionId) && this._sxtCacheReset('action');
        this.baseActionId = actionId;
        this.donResolveReset = false;
        _sxt.setDonResultSource({}, true);
      }
      return changed;
    },
    _sxtWizardProcessCheckboxesForm: function ($content, pageData) {
      var thisActions = this._sxtActions || {}
      , storageKey = pageData.tabData.storageLocalKey || ''
          , parts = storageKey.split('::')
          , action = !!parts[1] ? parts[1] : false
          , provider = action ? (parts[0]).split(':')[0] : false
          , func = action ? 'action' + action.capitalize() : false
          , self = '_sxtWizardProcessCheckboxesForm';
      if (!provider || !func) {
        slogErr(self + ': provider or function not found: ' + storageKey);
      } else if (thisActions[provider] && thisActions[provider][func]) {
        thisActions[provider][func]($content, storageKey);
      } else {
        func = provider + '.' + func;
        slogErr('_sxtWizardProcessCheckboxesForm: function not found: ' + func);
      }
      this.dialog.close();
    },
    _sxtWizardProcessCheckboxes: function ($content) {
      var thisActions = this._sxtActions || {}
      , values = {}, ferr = [];

      $content.find('li.checkbox.checked').each(function () {
        var $item = $(this)
            , action = $item.attr('action') || ''
            , parts = action.split(':')
            , provider = parts[0]
            , func, value, tmp;
        if (parts.length > 1) {
          values[provider] = values[provider] || {};
          func = 'action' + (parts[1]).capitalize();
          if (thisActions[provider] && thisActions[provider][func]) {
            values[provider][func] = values[provider][func] || [];
            value = parts[2] || true;
            if (values[provider][func].indexOf(value) === -1) {
              values[provider][func].push(value);
            }
          } else {
            tmp = provider + '.' + func;
            !ferr[tmp] && slogErr('_sxtWizardProcessCheckboxes: function not found: ' + tmp);
            ferr[tmp] = true;
          }
        }
      });

      var doReload = false
          , doClose = false
          , doCommand = false;
      // prevent reloading on first run
      for (var provider in values) {
        var pData = values[provider];
        for (var func in pData) {
          var result = thisActions[provider][func](pData[func], true) || {}
          , doNext = result.doNext || false
              , runAgain = (doNext === 'runAgain');
          !runAgain && (pData[func] = false);
          runAgain && (doReload = true);
          doCommand = doCommand || (doNext === 'doCommand');
          doClose = doClose || (doNext === 'doClose');
        }
      }

      if (doReload) {
        for (var provider in values) {
          var pData = values[provider];
          for (var func in pData) {
            pData[func] && thisActions[provider][func](pData[func]);
          }
        }
        this.$activeContent.html(this._sxtThrobberHtml());
        this._sxtThrobberAddMsg(this.$activeContent, 'Reloading...');
        this.$btnClose.button('disable');
        location.reload();
      } else if (doCommand) {
        _sxt.notImplemented('_sxtWizardProcessCheckboxes.doCommand');
      } else if (doClose) {
        this.dialog.close();
      } else {
        this.$btnBack.button('enable');
      }
    },
    _sxtWizardPagesDestroy: function (startIdx) {
      if (this.pagesData && this.pagesData.length > startIdx) {
        var pageData, idx, contentId;
        for (idx in this.pagesData) {
          idx = parseInt(idx, 10);
          if (!_.isNaN(idx) && idx >= startIdx && !!this.pagesData[idx]) {
            pageData = this.pagesData[idx];
            if (pageData.$tab) {
              this._sxtDetachSlogxt(pageData.$tab);
              pageData.$tab.remove();
            }
            if (pageData.$content) {
              this._sxtDetachSlogxt(pageData.$content);
              pageData.$content.remove();
            }
            contentId = this.loaded[idx];
            _sxt.ajax[contentId] && (delete _sxt.ajax[contentId]);
            delete this.loaded[idx];
            delete this.pagesData[idx];
            this.tabsData[idx] && (delete this.tabsData[idx]);
            this.tabsData.targets[idx] && (delete this.tabsData.targets[idx]);
          }
        }
        var newPagesData = []
            , pData, iIdx;
        for (idx in this.pagesData) {
          pData = this.pagesData[idx] || false;
          if (pData) {
            iIdx = parseInt(idx, 10);
            if (_.isNaN(iIdx) || iIdx < startIdx) {
              newPagesData[idx] = pData;
            }
          }
        }
        this.pagesData = newPagesData;
        this.tabsData.targets = _.compact(this.tabsData.targets);
      }
    },
    _sxtWizardPageData: function (pageIdx, data) {
      if (this.isWizard && this.pagesData) {
        if (pageIdx < 0) {
          pageIdx = this.pagesData.length;
          this.pagesData[pageIdx] = data || {};
        } else if (pageIdx === undefined) {
          pageIdx = this.getWizardCurPageIdx();
        }
        var pageData = this.pagesData[pageIdx];
        pageData && (pageData.wizardPath = pageData.wizardPath || {});
        return pageData || {};
      }
      return {};
    },
    _sxtActionsWizardStart: function (tabData) {
      this.$Tabs.sxtSetVisualHidden(true);
      this._sxtButtonShow(this.$btnNavigate, false);
      this.$btnPushpin.sxtSetVisualHidden(true);
      !this.$tabActionWizartStart && this._sxtCreateAWStartTab();

      this.isWizardSaved = this.isWizard;
      this.isWizard = true;
      this.wizardFinished = false;
      delete this.wizardFinalize;

      var aBaseTab = this.model.get('activeTab')
          , aStartIdx = this.actionStartIdx;
      this.actionWizardBaseData = {
        aBaseTab: aBaseTab,
        targetid: aBaseTab.targetid,
        $content: this.$activeContent,
        selected: tabData.selected
      };
      tabData.targetid = this.actionStartIdx;
      this.tabsData[aStartIdx] = tabData;
      this.$tabActionWizartStart.click();

      var type = tabData.type || false
          , sbtnEnable = !!tabData.path ? 'disable' : 'enable';
      this.pagesData[aStartIdx] = {
        type: type,
        $tab: this.$Tabs.find('.tabs-item.active'),
        labels: tabData.labels || {},
        contentId: 'contentId',
        $content: this.$Content.find('#' + 'contentId'),
        tabData: tabData,
        slogxtData: {}
      };
      this._sxtResetWizardActionPage(false);
      if (this.hasBtnSubmit) {
        this.$btnSubmit.show();
        this.$btnSubmit.button(sbtnEnable);
      }
      this.$btnBack.show();
      this.$btnBack.button('enable');
    },
    _sxtActionsBaseContentRebuild: function (contentHtml) {
      var awbData = this.actionWizardBaseData
          , targetid = awbData.targetid;
      awbData.$content.off().empty();
      var content = this.buildTabContentHtml(targetid);
      awbData.$content.append($(content));
      this._sxtDialogContentAttach(awbData.$content);
    },
    _sxtActionsWizardReset: function (msg, msgType) {
      var aBaseTab = this.actionWizardBaseData.aBaseTab
          , aStartIdx = this.actionStartIdx
          , $accontent = this.$activeContent
          , $pgcontent = this.pagesData[aStartIdx].$content || $()
          , isWizardSaved = !!this.isWizardSaved
          , showBack = isWizardSaved && aBaseTab.targetid > 0;
      this.isWizard = isWizardSaved;
      delete this.isWizardSaved;
      if (this.isWizard) {
        this.$tabActionWizartStart.empty();
      }
      this.actionWizardBaseData = false;
      this.$Tabs.sxtSetVisualHidden(this.isWizard);
      this._sxtButtonShow(this.$btnNavigate, this.showNavigateBtn);
      this.$btnPushpin.sxtSetVisualHidden(!this.hasPushpin);
      this.model.set('activeTab', aBaseTab);
      this._sxtResetWizardActionPage(true);

      this.$activeContent.find('.checkbox.is-current').click();
      this.hasBtnSubmit && this.$btnSubmit.hide();
      showBack ? this.$btnBack.show() : this.$btnBack.hide();
      this._sxtWizardPagesDestroy(aStartIdx + 1);
      $accontent.remove();
      $pgcontent.remove();
      delete this.loaded[aStartIdx];
      !!msg && this._sxtSetActionDoneMsg(this.$activeContent, msg, msgType);
    },
    _sxtDeleteActionDoneMsg: function () {
      if (this.$actionDoneMsg) {
        this.$actionDoneMsg.remove();
        delete this.$actionDoneMsg;
      }
    },
    _sxtSetActionDoneMsg: function ($target, msg, msgType) {
      this._sxtDeleteActionDoneMsg();

      if (!!msg) {
        msgType = msgType || 'status';
        var msg = Drupal.theme.slogxtMessage(msg, msgType, false, 'action-done-msg')
            , delay = (msgType === 'status') ? 2000 : 5000;
        this.$activeContent.prepend(msg);
        this.$actionDoneMsg = this.$activeContent.find('.action-done-msg');
        this.actionDoneMsgTO && clearTimeout(this.actionDoneMsgTO);
        this.actionDoneMsgTO = setTimeout($.proxy(function () {
          if (this.$actionDoneMsg) {
            this.$actionDoneMsg.hide('slow', this._sxtDeleteActionDoneMsg);
          }
        }, this), delay);
      }
    },
    _sxtTrackFormFinalize: function (slogxtData, labels) {
      slogxtData.isOnEdit = _.isNumber(slogxtData.curField);
      slogxtData.isOnFirst = (slogxtData.curField === 0);
      slogxtData.isOnLast = (slogxtData.curField === slogxtData.lastField);
      slogxtData.isOnPreview = (slogxtData.curField === 'preview');
      slogxtData.isOnSaved = (slogxtData.curField === 'saved');

      this._sxtDialogSetLabels(labels);
      if (slogxtData.isOnSaved) {
        this.$btnSubmit.hide();
        this.$btnBack.hide();
      } else {
        !slogxtData.submitPreserve && this.$btnSubmit.button('enable');
        if (slogxtData.isOnEdit && !slogxtData.isOnLast) {
          this._sxtSetButtonText(this.$btnSubmit, this._sxtWizardNextLabel(labels));
        }
        this.$btnBack.button('enable');
        this._sxtWizardShowBtnBack(slogxtData);
        !!this.model.get('maximized') && this._sxtSetWidgetHeight(true);
      }
      this.sxtTrackFormSetFocusVisible(slogxtData);
    },
    sxtTrackFormSetFocusVisible: function (slogxtData) {
      setTimeout($.proxy(function () {
        slogxtData.$fWrapper
            .find('input:visible, textarea:visible').filter(':enabled')
            .first().focus();
      }, this));
    },
    _sxtTabledragFormFinalize: function (tabledrag) {
      var thisView = this
          , tbdrag = Drupal.tableDrag ? Drupal.tableDrag[tabledrag] : false;
      if (tbdrag) {
        this.$btnSubmit.button('disable');
        tbdrag.onDrop = function () {
          thisView.$btnSubmit.button('enable');
        };
      }
    },
    _sxtTrackFormPrepare: function (pageData) {
      var slogxtData = pageData.slogxtData;
      slogxtData.trackFormPages = true;
      slogxtData.lastField = slogxtData.curField = 0;
      var $fields = pageData.$content.find('.slogxt-input-field').not('.visually-hidden')
          , $fWrapper = $fields.closest('.form-wrapper, .js-form-wrapper');
      if ($fields.length && $fields.length !== $fWrapper.length) {
        slogErr('EditFormView._sxtTrackFormPrepare::........fWrapper-missmatch...........');
      }
      slogxtData.$fWrapper = $fWrapper;
      if (this.isScreenSmall && $fWrapper.length > 1) {
        $fWrapper.hide();
        $fWrapper.first().show();
        slogxtData.lastField = $fWrapper.length - 1;
        this.sxtTrackFormSetFocusVisible(slogxtData);
      }
    },
    _sxtTrackFormNext: function (slogxtData) {
      //      slogLog('EditFormView._sxtTrackFormNext::.......................');
      var $content = this.$activeContent
          , $form = $content.find('form')
          , labels = slogxtData.editLabels
          , submitClicked = false;
      if (slogxtData.curField < slogxtData.lastField) {
        slogxtData.curField++;
        slogxtData.$fWrapper.hide();
        $(slogxtData.$fWrapper[slogxtData.curField]).show();
      } else if (slogxtData.isOnLast && slogxtData.needsPreview) {
        if (slogxtData.formValues === $form.formSerialize()) {
          this.$activeContent.addClass('show-preview');
          labels = slogxtData.previewLabels;
          slogxtData.curField = 'preview';
        } else {
          slogxtData.$preview.click();
          submitClicked = true;
        }
      } else if (!slogxtData.needsPreview || slogxtData.curField === 'preview') {
        this.$btnClose.button('disable');
        if (!!slogxtData.preSubmit) {
          var callback = this._sxtCommandCallback(slogxtData.preSubmit);
          callback && callback.apply(this, [slogxtData]);
        }

        slogxtData.$submit.click();
        submitClicked = true;
      }

      if (submitClicked) {
        setTimeout($.proxy(function () {
          this.$activeContent.html(this._sxtThrobberHtml());
        }, this), 10);
      } else {
        this._sxtTrackFormFinalize(slogxtData, labels);
      }

    },
    _sxtTrackFormBack: function (slogxtData) {
      if (slogxtData.curField === 'saved') {
        return;
      }
      var $content = this.$activeContent
          , labels = slogxtData.editLabels;
      if (slogxtData.curField === 'preview') {
        slogxtData.curField = slogxtData.lastField + 1;
        $content.removeClass('show-preview');
        this._sxtSetWidgetHeight(true);
      }

      (slogxtData.curField > 0) && slogxtData.curField--;
      if (this.isScreenSmall) {
        slogxtData.$fWrapper.hide();
        $(slogxtData.$fWrapper[slogxtData.curField]).show();
      }

      this._sxtTrackFormFinalize(slogxtData, labels);
    },
    _sxtSetFocusInvalid: function (slogxtData, showThrobber) {
      if (!this.isScreenSmall) {
        return; // for small only
      }
      var $fWrapper = slogxtData.$fWrapper
          , $invalid = $fWrapper.find('.form-item .error');
      if ($invalid.length) {
        slogxtData.curField = -1;
        for (var i = slogxtData.curField; i < slogxtData.lastField; i++) {
          this._sxtTrackFormNext(slogxtData);
          $invalid = $fWrapper.find('.form-item .error:visible');
          if ($invalid.length) {
            break;
          }
        }
      } else if (showThrobber) {
        this.$activeContent.html(this._sxtThrobberHtml());
      }
    }
  };

})(jQuery, Drupal, drupalSettings, Backbone, _);
