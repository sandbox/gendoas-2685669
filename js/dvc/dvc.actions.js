/**
 * @file
 * slogxt/js/dvc/dvc.actions.js
 * 
 * Commands components for DialogViewBase.
 * 
 * DialogViewBase is splitted for readability only.
 */

(function ($, Drupal, _) {

  var _sxt = Drupal.slogxt;
  
  _sxt.dvc = _sxt.dvc || {};
  _sxt.dvc.actions = _sxt.dvc.actions || {};

  var sxtActions = {
    actionDummy: function (actions) {
      var idx, selected = '';
      actions = actions || [];
      for (idx in actions) {
        !!selected && (selected += ', ');
        selected += actions[idx];
      }

      _sxt.notImplemented('actionDummy.selected: ' + selected);
    }
  };
  _sxt.dvc.actions.slogxt = sxtActions;

})(jQuery, Drupal, _);
