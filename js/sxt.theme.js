/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings, _, undefined) {

  var themeExt = {
//    slogxtDropbutton: 0,
    slogxtAttrib: function (key, value) {
      return (true || allowedAttribKeys.indexOf(key) >= 0)
          ? ' ' + key + '="' + value + '"'
          : '';
    },
    slogxtElement: function (type, config, children) {
      var propName
          , attribs = ''
          , childs = ''
          , tpl = '<§type§§attribs§>§childs§</§type§>'
          ;

      if (config !== null) {
        for (propName in config) {
          attribs += Drupal.theme.slogxtAttrib(propName, config[propName]);
        }
      }

      // Children can be more than one argument
      var childrenLength = arguments.length - 2;
      if (childrenLength === 1) {
        // for one argument, maybe it is an array
        if (_.isArray(children)) {
          for (var i in children) {
            childs += children[i];
          }
        } else {
          childs = children;
        }
      } else if (childrenLength > 1) {
        for (var i = 0; i < childrenLength; i++) {
          childs += arguments[i + 2];
        }
      }

      return tpl.replace(/§type§/g, type)
          .replace(/§attribs§/g, attribs)
          .replace(/§childs§/g, childs);
    },
    slogxtListCheckboxes: function (data, ul_opts, headerStyle, icons) {
      ul_opts = ul_opts || {};
      var list = ''
          , ulCls = ul_opts.class || '' + ' slogxt-list'
          , noData = !data || !_.isArray(data);
      if (noData) {
        data = [{title: 'No data found', path: false}];
      }

      for (var idx in data) {
        var item = data[idx]
            , path = item.path
            , titleCls = item.titleCls ? 'li-title ' + item.titleCls : 'li-title'
            , liCls = item.liCls ? 'checkbox ' + item.liCls : 'checkbox'
            , liConf = !noData ? {class: liCls, index: idx} : {}
        , plSpan = _tse('span', null, item.liDescription)
            , dLabel = _tse('div', {class: 'item-dlabel'}, plSpan)
            , liCheckbox = !noData ? _tse('div', {class: 'slogxt-icon li-checkbox'}) : ''
            , titleConf = !noData ? {class: titleCls} : {}
        , liDetails = item.liDetails ? _tse('div', {class: 'item-details'}, item.liDetails) : ''
//todo::current:: ----sxt_sxt_workflow --- current - htmlLiButton
            , liButton = item.liButton ? _tse('div', {class: 'button slogxt-li-button'}, item.liButton) : ''
            , liTitle = headerStyle
            ? _tse('div', titleConf, dLabel, liButton, item.liTitle, liDetails)
            : _tse('div', titleConf, liButton, item.liTitle, dLabel, liDetails);
        !!item.action && (liConf.action = item.action);
        !!path && (liConf['data-path'] = path);
        !!item.toolbar && (liConf['data-toolbar'] = item.toolbar);
        !!item.toolbartab && (liConf['data-toolbartab'] = item.toolbartab);
        !!item.roleId && (liConf['data-role'] = item.roleId);
        !!item.hooks && (liConf['data-hooks'] = encodeURI(JSON.stringify(item.hooks)));
        !!item.entityid && (liConf.entityid = item.entityid);
        list += _tse('li', liConf, liCheckbox, liTitle);
      }

      ul_opts = ul_opts || {};
      headerStyle && (ulCls += ' header-style');
      icons && (ulCls += ' icons');
      ul_opts.class && (ulCls += ' ' + ul_opts.class);
      ul_opts.class = ulCls;
      return _tse('ul', ul_opts, list);
    },
    slogxtListRadios: function (data, ul_opts, headerStyle, icons) {
      ul_opts = ul_opts || {};
      var list = ''
          , ulCls = ul_opts.class || '' + ' slogxt-list'
          , noData = !data || !_.isArray(data);
      if (noData) {
        data = [{title: 'No action data found', path: false}];
      }

      for (var idx in data) {
        var item = data[idx]
            , path = item.path
            , resolve = false
            , resolveArgs = false
            , clscur = !!item.selected ? ' checked' : ''
            , liConf = !noData ? {class: 'radio' + clscur, index: idx} : {}
        , plSpan = _tse('span', null, item.liDescription || '')
            , dLabel = _tse('div', {class: 'item-dlabel'}, plSpan)
            , liRadio = !noData ? _tse('div', {class: 'slogxt-icon li-radio'}) : ''
            , pTitle = this.prepareTitle(item.liTitle)
            , titleConf = !noData ? {class: 'li-title' + pTitle.class} : {}
        , liDetails = item.liDetails ? _tse('div', {class: 'item-details'}, item.liDetails) : ''
//todo::current:: ----sxt_sxt_workflow --- current - htmlLiButton
            , liButton = item.liButton ? _tse('div', {class: 'button slogxt-li-button'}, item.liButton) : ''
            , liTitle = headerStyle
            ? _tse('div', titleConf, dLabel, liButton, item.liTitle, liDetails)
            : _tse('div', titleConf, liButton, item.liTitle, dLabel, liDetails);
        if (path && !!path.path) {
          !!path.resolve && (resolve = encodeURI(JSON.stringify(path.resolve)));
          !!path.resolveArgs && (resolveArgs = encodeURI(JSON.stringify(path.resolveArgs)));
          path = path.path;
        }
        !!item.actionId && (liConf.id = item.actionId);
        !!path && (liConf['data-path'] = path);
        !!resolve && (liConf['data-resolve'] = resolve);
        !!resolveArgs && (liConf['data-resolveargs'] = resolveArgs);
        !!item.toolbar && (liConf['data-toolbar'] = item.toolbar);
        !!item.toolbartab && (liConf['data-toolbartab'] = item.toolbartab);
        !!item.roleId && (liConf['data-role'] = item.roleId);
        !!item.hooks && (liConf['data-hooks'] = encodeURI(JSON.stringify(item.hooks)));
        !!item.entityid && (liConf.entityid = item.entityid);
        list += _tse('li', liConf, liRadio, liTitle);
      }

      ul_opts = ul_opts || {};
      headerStyle && (ulCls += ' header-style');
      icons && (ulCls += ' icons');
      ul_opts.class && (ulCls += ' ' + ul_opts.class);
      ul_opts.class = ulCls;
      return _tse('ul', ul_opts, list);
    },
    prepareTitle: function (title) {
      var splitted = title.split('--.--.')
          , retTitle = (splitted.length === 2) ? splitted[1] : false
          , retClass = !!retTitle ? ' level2' : '';
      if (!retTitle) {
        splitted = title.split('--.')
        retTitle = (splitted.length === 2) ? splitted[1] : false;
        retClass = !!retTitle ? ' level1' : '';
      }
      if (!retTitle) {
        retTitle = title;
        retClass = '';
      }
      return {'title': retTitle, 'class': retClass};
    },
    slogxtDropbutton: function (actions, config) {
      _.isObject(actions) && (actions = _.toArray(actions));
      if (actions && actions.length) {
        config = config || {};
        var list = ''
            , paging = '';

        for (var idx in actions) {
          if (!actions[idx]) {
            continue;
          }
          var action = actions[idx]
              , aClass = config.aClass ? config.aClass + ' ' : ''
              , confItem = null
              , conf = {
                class: aClass + (action.class || ''),
                href: action.href || ('#' + action.action),
                title: action.label || '',
                action: action.path,
                provider: action.provider
              }
          , link;
          !!action.notop && (conf.notop = true);
          if (action.value !== undefined) {
            conf['value'] = action.value;
          }
          if (action.itemId !== undefined) {
            confItem = {id: action.itemId};
          }
          if (action.itemCls !== undefined) {
            confItem = confItem || {};
            confItem.class = action.itemCls;
          }

          link = _tse('a', conf, action.label);
          list += _tse('li', confItem, link);

          if (action.path === 'paging') {
            var pgprev = _tse('div', {class: 'sxt-pg-button sxt-pgprev'})
                , pgstop = _tse('div', {class: 'sxt-pg-button sxt-pgstop'})
                , pgnext = _tse('div', {class: 'sxt-pg-button sxt-pgnext'});
            paging = _tse('div', {class: 'sxt-paging'}, pgprev, pgstop, pgnext);
          }
        }

        var ul = _tse('ul', {class: 'dropbutton'}, list)
            , disabled = _tse('div', {class: 'dropbutton-overlay'})
            , widget = _tse('div', {class: 'dropbutton-widget'}, paging, ul);
        return _tse('div', {class: 'dropbutton-wrapper slogxt-dropbutton'}, disabled, widget);
      }

      return false;
    },
    slogxtMessagesPlain: function ($items) {
      $items.each(function () {
        var $item = $(this);
        $item.after(_tse('span', {}, $item.text()));
        $item.remove();
      });
    },
    slogxtInfoMessage: function (message, type, prepend_br) {
      return this.slogxtMessage(message, type, prepend_br, 'slogxt-info');
    },
    slogxtXtInfoMessage: function (message, type, prepend_br) {
      return this.slogxtMessage(message, type, prepend_br, 'slogxt-xtinfo');
    },
    slogxtMessage: function (message, type, prepend_br, cls) {
      var prepend = !!prepend_br ? '<br />' : '';
      message = message || '???-message';
      type = type || 'status';
      cls = !!cls ? ' ' + cls.trim() : '';
      return (prepend + _tse('div', {class: 'messages messages--' + type + cls}, message));
    },
    slogxtPushpin: function () {
      return '<div class="sxt-button ui-icon-pushpin"></div>';
    },
    slogxtDlgAddMsg: function ($content, message) {
      if (!!message && message.msg) {
        var type = message.type || 'warning'
            , prepend_br = !_.isUndefined(message.prepend_br) ? message.prepend_br : true
            , $msg = Drupal.theme.slogxtMessage(message.msg, type, prepend_br, message.cls);
        message.append ? $content.append($msg) : $content.prepend($msg);
      }
    },
    slogxtDlgDoButton: function (btnLabel) {
      return '<div class="button sxt-do-button">' + btnLabel + '</div>';
    },
    slogxtDialogDoButton: function ($target, btnLabel, info, xtInfo) {
      var btn = this.slogxtDlgDoButton(btnLabel)
          , msg = this.slogxtInfoMessage(info)
          , wrn = !!xtInfo ? this.slogxtXtInfoMessage(xtInfo, 'warning') : ''
          , $wrp = $(_tse('div', {class: 'sxt-dialog-do-wrapper'}, msg, wrn, btn))
          , $btn = $wrp.find('.sxt-do-button');
      $target && $target.append($wrp);
      $btn.button();
      return $btn;
    },
    slogxtDialogMoveButtons: function ($target) {
      var $sBtn = $(this.slogxtDialogDoButton(false, Drupal.t('Source')))
          , $tBtn = $(this.slogxtDialogDoButton(false, Drupal.t('Target')))
          , $wrp = $(_tse('div', {class: 'sxt-dialog-do-wrapper'}));
      $wrp.append($tBtn, $sBtn).appendTo($target);
      $sBtn.button();
      $tBtn.button();
      return {$source: $sBtn, $target: $tBtn};
    },
    slogxtHeader: function () {
      var path = _tse('div', {class: 'path-label'});
      var main = _tse('div', {class: 'main-label'});
      return _tse('div', {class: 'sxt-header-wrapper'}, path, main);
    },
    slogxtWrapper: function (id, children) {
      return _tse('div', {id: id}, children);
    },
    slogxtDialogActions: function (children) {
      var toggler = '<div class="sxt-button ui-icon-actions"></div>'
          , childs = children ? [children, toggler] : toggler
          , config = {class: 'actions-wrapper'};
      return this.slogxtElement('div', config, childs);
    },
    slogxtThrobber: function () {
      return '<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>';
    },
    slogxtThrobberAddMsg: function ($el, msg) {
      $el.find('.throbber').after('<div class="message">' + msg + '</div>');
    },
    slogxtDummyInfo: function (custom) {
      custom = custom || '...some content';
      return '<div style="padding:15px;">DummyContent:: ' + custom + '</div>';
    },
    slogxtDummyCheckboxes: function (number) {
      number = number || 6;
      var dTpl = 'DummyCheckboxes - Description-'
          , tTpl = 'Select-', data = [], i, ext;
      for (i = 1; i <= number; i++) {
        ext = (i < 10) ? '0' : '';
        data.push({liTitle: tTpl + ext + i, liDescription: dTpl + ext + i});
      }
      return this.slogxtListCheckboxes(data);
    },
    slogxtDummyRadios: function (number) {
      number = number || 6;
      var dTpl = 'DummyRadios - Description-'
          , tTpl = 'Option-', data = [], i, ext;
      for (i = 1; i <= number; i++) {
        ext = (i < 10) ? '0' : '';
        data.push({liTitle: tTpl + ext + i, liDescription: dTpl + ext + i});
      }
      return this.slogxtListRadios(data);
    },
    slogxtDummyActions: function () {
      return [
        {label: 'Do', href: '#do', class: 'icon-dummy', path: 'do', provider: 'slogxt'},
        {label: 'Some', href: '#some', class: 'icon-dummy', path: 'some', provider: 'slogxt'},
        {label: 'More', href: '#more', class: 'icon-dummy', path: 'more', provider: 'slogxt'},
        {label: 'Actions', href: '#actions', class: 'icon-dummy', path: 'actions', provider: 'slogxt'}
      ];
    },
    slogxtDummyDropbutton: function () {
      return this.slogxtDropbutton(this.slogxtDummyActions());
    }
  };

  // extend Drupal.theme
  $.extend(Drupal.theme, themeExt);

  var allowedAttribKeys = ['class', 'entityid', 'data-path', 'data-target', 'data-index', 'data-action', 'data-hooks']
      , _tse = Drupal.theme.slogxtElement;

})(jQuery, Drupal, drupalSettings, _);
