/**
 * @file
 * Attaches behavior for state extension.
 */

(function ($, Drupal, drupalSettings, window, _, undefined) {

  "use strict";

  // ensure base objects
  Drupal.slogxt = {};
  var _sxt = Drupal.slogxt;
  _sxt.models = {};
  _sxt.views = {};
  _sxt.commands = {};
  _sxt.dvc = {}; // dialog view components
  _sxt.localSwitches = {};

  var sxtExt = {
    touchSupported: undefined,
    tokenTargetTb: '__targettb__',
    invalidSel: Drupal.t('invalid selection'),
    unknownError: Drupal.t('Unknown error.'),
    txtLoaded: Drupal.t('loaded'),
    txtPreserved: Drupal.t('preserved'),
    txtExistent: Drupal.t('existent'),
    txtNoMenuSelected: Drupal.t('There is no menu item selected'),
    txtNoContentSelected: Drupal.t('There is no content selected'),
    txtSorryError: Drupal.t('Sorry, an error occured!'),
    $mainDropBtnEl: false,
    commentDelete: Drupal.t('Delete'),
    commentEdit: Drupal.t('Edit'),
    commentReply: Drupal.t('Reply'),
    isTouchSupported: function () {
      if (this.touchSupported === undefined) {
        this.touchSupported = !!('ontouchstart' in window);
      }
      return this.touchSupported;
    },
    logoutExecute: function () {
      location.href = Drupal.url('user/logout') + '?destination=' + location.href;
    },
    getResolveDelim: function () {
      return drupalSettings.slogxt.resolveDelim || '~';
    },
    sureArray: function (object) {
      if (_.isArray(object)) {
        return object;
      } else if (_.isObject_(object)) {
        return _.values(object);
      }
      return [];
    },
    basePath: function (provider, trim) {
      provider = provider || 'slogxt';
      var path = drupalSettings[provider].basePath;
      trim && (path = path.replace(/^\/+|\/+$/g, ''));
      return path;
    },
    baseAjaxPath: function (provider, trim) {
      provider = provider || 'slogxt';
      var path = (drupalSettings[provider] || {}).baseAjaxPath;
      if (!path && !!provider) {
        path = (drupalSettings['slogxt'].baseAjaxPath).replace(/slogxt/, provider);
      }
      trim && (path = path.replace(/^\/+|\/+$/g, ''));
      return path;
    },
    getTheme: function () {
      return drupalSettings.ajaxPageState.theme;
    },
    themeIsJqLout: function () {
      return false;
    },
    getElementTbLine: function ($region) {
      return $region;
    },
    getXtHandler: function () {
      var handler = drupalSettings.slogxt.xtHandler || 'xtUnvalid';
      return Drupal[handler] || false;
    },
    getXtSubHandler: function () {
      var handler = drupalSettings.slogxt.xtSubHandler || 'xtUnvalid';
      return Drupal[handler] || false;
    },
    getActions: function (key) {
      var actions = drupalSettings.slogxt['actions'] || {};
      return actions[key] || {};
    },
    getTbSysActions: function () {
//      return false
//      return this.getActions('dialogTbSys');
      return Drupal.theme.slogxtDummyActions();
    },
    getTbSysId: function () {
      return drupalSettings.slogxt.tbSysId;
    },
    isTbSysId: function (toolbar) {
      return (toolbar === drupalSettings.slogxt.tbSysId);
    },
    getTbSysNode: function () {
      return drupalSettings.slogxt.tbSysNode;
    },
    getTbSysDummy: function () {
      return drupalSettings.slogxt.tbSysDummy;
    },
    getTbSysInfoData: function () {
      return drupalSettings.slogxt.tbSysInfoData || {};
    },
    getSysViewStartTarget: function () {
      var view = _sxt.mainActionsView().model.get('currentXtsiListView');
      return (view ? view.getSysViewStartTarget() : '');
    },
    getRoleHeaderPath: function (pdRole, toolbartab) {
      if (!!pdRole && !!toolbartab) {
        var rhp = drupalSettings.slogxt.roleHeaderPath || {};
        return ((rhp[pdRole] || {})[toolbartab] || '???');
      }
    },
    getPermByTbRank: function (tbSource, tbTarget) {
      var rankTbs = drupalSettings.slogxt.tbRankList || {}
      , idx, toolbar;
      if (!tbSource || !tbTarget) {
        return false;
      } else if (tbSource === tbTarget) {
        return true;  // allow links from user to user too
      } else if (tbSource === 'user') {
        return false; // no links from other toolbars to user
      }

      // rankTbs are sorted by weight
      for (idx in rankTbs) {
        toolbar = rankTbs[idx];
        if (tbSource === toolbar) {
          return true;
        } else if (tbTarget === toolbar) {
          return false;
        }
      }

      return false;
    },
    getMainDropBtnEl: function () {
      !this.$mainDropBtnEl && (this.$mainDropBtnEl = $('#block-slogxtmaindropbutton'));
      return this.$mainDropBtnEl;
    },
    setBgColorMainDropBtnEl: function () {
      var $block = this.getMainDropBtnEl()
          , $region = $block.closest('.region')
          , $stb = $region.find('.block-slogtb .toolbar-bar').first();
      $stb.length && $block.css('backgroundColor', $stb.css('backgroundColor'));
    },
    setAppearanceColors: function () {
      var app = this.getUserAppearance()
          , cRemove = app.classes_dialog + ' ' + app.classes_active + ' ' + app.classes_mwcomments
          , cAdd = app.xt_color_dialog + ' ' + app.xt_color_active + ' ' + app.xt_color_mwcomment;
      $('#page-wrapper').removeClass(app.classes_tb).addClass(app.xt_color_tb);
      this.setBgColorMainDropBtnEl();
      $('body').removeClass(cRemove).addClass(cAdd);
    },
    setRoleHeaderPath: function (pdRole, toolbartab, pathLabel) {
      if (!!pdRole && !!toolbartab && !!pathLabel) {
        drupalSettings.slogxt.roleHeaderPath = drupalSettings.slogxt.roleHeaderPath || {};
        var rhp = drupalSettings.slogxt.roleHeaderPath;
        rhp[pdRole] = rhp[pdRole] || [];
        rhp[pdRole][toolbartab] = pathLabel;
        return rhp[pdRole][toolbartab];
      }
    },
    resetGroupPerms: function () {
      var grp = drupalSettings.sxt_group || {};
      grp.permissions && (grp.permissions = []);
    },
    hasPermToolbar: function (toolbar) {
      var perms = (drupalSettings.slogxt.permissions || {})['toolbars'] || {};
      if (_.contains(perms, 'administer slog taxonomy')) {
        return true;
      }
      return !!perms['administer toolbar ' + toolbar];
    },
    hasPermSlogtx: function (perm) {
      if (this.hasPermToolbar()) {
        return true;
      }
      var perms = (drupalSettings.slogxt.permissions || {})['slogtx'] || {};
      return !!perms[perm];
    },
    _initAppearanceColors: function () {
      var app = this.getUserAppearance()
          , values = app.xt_colors_by_device ? this.getLocalAppearance() : false;
      values && this.setUserAppearance(values);
      this.setAppearanceColors();
    },
    _sxtApplyLocalSwitches: function () {
//      slogLog('_sxt._sxtApplyLocalSwitches...............');
      var storageKey = 'slogxt:profile::localSwitches'
          , values = JSON.parse(localStorage.getItem(storageKey))
          , tasks = {};
      if (_.isObject(values) && !_.isEmpty(values)) {
        for (var idx in values) {
          var value = values[idx]
              , parts = idx.split(':');
          if (parts.length === 3) {
            var provider = parts[0]
                , func = 'apply' + (parts[1]).capitalize();
            tasks[provider] = tasks[provider] || {};
            tasks[provider][func] = tasks[provider][func] || {};
            tasks[provider][func][parts[2]] = value;
          }
        }

        if (!_.isEmpty(tasks)) {
          var err_msg = false;
          _.each(tasks, function (ptasks, provider) {
            _.each(ptasks, function (task, func) {
              var callback = Drupal[provider]['commands'][func] || false;
              if (callback && _.isFunction(callback)) {
                callback.call(this, task);
              } else {
                var arg = 'Drupal.' + provider + '.commands.' + func;
                err_msg = !err_msg ? arg : err_msg + ' ...' + arg;
              }
            });
          });

          if (err_msg) {
            _sxt.notImplemented(err_msg);
          }
        }
      }
    }
  };

  // extend _sxt
  $.extend(true, _sxt, sxtExt);
  sxtExt = null;

  var sxtCmds = {
    applyLocalSwitches: function (data) {
      _.each(data, function (val, key) {
        switch (key) {
          case 'DialogHideInfo':
            $('body').toggleClass('hide-sxt-info', val);
            break;
          case 'DialogCloseInfoMessage':
            _sxt.localSwitches.closeInfoMessage = val;
            break;
          case 'PageEnlarge':
            $('#page-wrapper').toggleClass('sxt-enlarge-page', val);
            break;
          case 'ReloadPause':
            _sxt.localSwitches.reloadPause = val;
            break;
          default:
            _sxt.notImplemented('_sxt.commands.applyLocalSwitches::' + key);
            break;
        }
      });
    }
  }

  // extend _sxt.commands
  $.extend(true, _sxt.commands, sxtCmds);
  sxtCmds = null;

  //
  // on document ready
  //
  $(document).ready(function () {
    _sxt._initAppearanceColors();
    _sxt._sxtApplyLocalSwitches();
  });

})(jQuery, Drupal, drupalSettings, window, _);
